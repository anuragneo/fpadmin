const comms = require('../sys/comms')
const request = require('request')
const metaservice = require('../persistence/metaservice')
// Example synchronous channel
/***
 * Make sure you pass the context object to further requests or publishes, if any
 */

const getData = function() {
  request.get('www.get.service?exampleparam=2', (err, response, data) => {
    JSON.stringify({ err, response, data })
  })
}

module.exports = (req, context, callback) => {
  const eastwest = context.get ? context.get(process.env.EAST_WEST || 'eastwest') : null
  //let response = {fullmessage: `${request.firstname} ${request.lastname} ${request.message}`}
  // Example of making a request and handling updated context
  request.get('www.get.service?exampleparam=1', (err, response, data) => {
    JSON.stringify({ err, response, data })
  })
  if (eastwest) {
    getData()
  }
  metaservice.isRegistered(6787456545, (err, mobile, isRegistered) => {
    JSON.stringify({ err, mobile, isRegistered })
  })
  metaservice.getotp(6787456545, 'android', (err, data) => {
    JSON.stringify({ err, data })
  })
  request.post('www.ff.aa/getotp', { expected: 'request' }, (err, response, data) => {
    JSON.stringify({ err, response, data })
  })
  request.post('www.ff.aa/register', { expected: 'request' }, (err, response, data) => {
    JSON.stringify({ err, response, data })
  })
  comms.publish('random.event', { paymentid: 1, mode: 'upi' })
  comms.request('metaservice/commsresponseexample', req, context, callback)
}
