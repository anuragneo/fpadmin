let caseid = null
let commsmocks = {}
module.exports = {
  request(channel, messageobj, existingcontext, callback) {
    callback(null, commsmocks[channel])
  },
  setcommsdefs(_commsmocks) {
    commsmocks = _commsmocks
  },
  setcaseid(_caseid) {
    caseid = _caseid
  }
}
