/* Replace with your SQL commands */
CREATE TABLE IF NOT EXISTS adminops (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  amount  DECIMAL (10,2),
  walletid  BIGINT(20), 
  makerid  BIGINT(20), 
  checkerid BIGINT(20), 
  comments VARCHAR(255), 
  status VARCHAR(255), 
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  updated_at  TIMESTAMP NULL DEFAULT NULL, 
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;