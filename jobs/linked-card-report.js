// Load env from file for dev. Set NODE_ENV in your bashrc or zshrc.
if (process.env.NODE_ENV === 'development') {
  require('env2')('./devenv.json')
}

const async = require('async')
const log = require('../sys/log')
const fs = require('fs')
const AWS = require('aws-sdk')
const path = require('path')
const mysql = require('../connectors/mysql')
const moment = require('moment')
const xl = require('msexcel-builder-colorfix')
const _ = require('lodash')

let data, filename, filepath

let getdata = _callback => {
  let startdate = moment().format('YYYY-MM-DD')
  let enddate = moment().add(1, 'days').format()
  let query = `
    (SELECT c.mobile, 'BBPC' AS cardtype, c.deviceid, b.cardnumber as cardnumber, 
    CONCAT(c.firstname, ' ', c.lastname) AS customername, c.mobile as customermobile
    FROM bbpc b JOIN customers c ON c.fpaccountid = b.fpaccountid
    WHERE b.islinked = 'Y' AND DATE(b.created_at) BETWEEN '${startdate}' AND '${enddate}')
    UNION ALL
    (SELECT c.mobile, 'Payback' AS cardtype, c.deviceid, p.cardno as cardnumber, 
    CONCAT(c.firstname, ' ', c.lastname) AS customername, c.mobile as customermobile
    FROM payback p JOIN customers c ON c.fpaccountid = p.fpaccountid
    WHERE p.islinked = 'Y' AND DATE(p.created_at) BETWEEN '${startdate}' AND '${enddate}')
  `
  mysql.query(query, [], (err, res) => {
    if (!err) data = res
    _callback(err)
  })
}

let createxlsx = _callback => {
  filename = `linked-card-report-${Date.now()}.xlsx`
  filepath = '/tmp'
  let workbook = xl.createWorkbook(filepath, filename)
  let sheet = workbook.createSheet('Report', 50, 30000)
  let headers = ["Mobile Number", "Card Type", "Device ID", "Card Number", "Customer Name", "Customer Mobile Number"]

  _.each(headers, (header, index) => {
    sheet.set(index + 1, 1, header)
  })

  _.each(data, (item, index) => {
    let i = index + 2

    sheet.set(1, i, item.mobile)
    sheet.set(2, i, item.cardtype)
    sheet.set(3, i, item.deviceid)
    sheet.set(4, i, item.cardnumber)
    sheet.set(5, i, item.customername)
    sheet.set(6, i, item.customermobile)
  })

  workbook.save(err => {
    if (err) {
      workbook.cancel()
      _callback(err)
    } else _callback(null)
  })

}

let updateawsconfig = _callback => {
  AWS.config.region = process.env.AWS_REGION_CONTENT
  let aws_config = {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
  }
  AWS.config.update(aws_config)
  _callback(null)
}

let uploadtos3 = _callback => {
  fs.readFile(`${filepath}/${filename}`, (err, data) => {
    if (err) {
      _callback('Error while reading file.')
    } else {
      let s3 = new AWS.S3()
      let base64data = new Buffer(data, 'binary')
      let aws_data_dict = {
        Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
        Key: `panel-reports/${moment().format('DD-MM-YYYY')}/${filename}`,
        Body: base64data,
        ContentType: filename.mimetype,
        ACL: process.env.AWS_ACL
      }

      s3.putObject(aws_data_dict, (_err, data) => {
        if (_err) _callback(_err)
        else _callback(null)
      })
    }
  })
}

async.waterfall([getdata, createxlsx, updateawsconfig, uploadtos3], (err) => {
  if (err) {
    log(`Failed to generate linked card report for ${moment().format('DD-MM-YYYY')} : ${err}`)
    process.exit()
  } else {
    log(`Successfully generated linked card report for ${moment().format('DD-MM-YYYY')}`)
    process.exit()
  }
})