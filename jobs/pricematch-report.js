// Load env from file for dev. Set NODE_ENV in your bashrc or zshrc.
if (process.env.NODE_ENV === 'development') {
  require('env2')('./devenv.json')
}

const async = require('async')
const log = require('../sys/log')
const fs = require('fs')
const AWS = require('aws-sdk')
const path = require('path')
const mysql = require('../connectors/mysql')
const moment = require('moment')
const xl = require('msexcel-builder-colorfix')
const _ = require('lodash')

let data, filename, filepath

let getdata = _callback => {
  let startdate = moment().format('YYYY-MM-DD')
  let enddate = moment().add(1, 'days').format()
  let query = `    
    SELECT mobile, billnumber, amount, 
    CASE state
      WHEN 'pending' THEN 'In Progress'
      WHEN 'bestprice' THEN 'No Refund'
      WHEN 'lowerprice' THEN 'Refund'
    END AS 'state',
    '' AS message, DATE_FORMAT(created_at, '%d/%m/%Y') AS logdate,
    DATE_FORMAT(updated_at, '%d/%m/%Y') AS modifydate, storecode,
    '' AS storename, '' AS storetype, billdate, id AS 'pricematchid', tillnumber
    FROM pricematch
    WHERE state != 'failed' AND DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
  `
  mysql.query(query, [], (err, res) => {
    if (!err) data = res
    _callback(err)
  })
}

let createxlsx = _callback => {
  filename = `pricematch-report-${Date.now()}.xlsx`
  filepath = '/tmp'
  let workbook = xl.createWorkbook(filepath, filename)
  let sheet = workbook.createSheet('Report', 50, 30000)
  let headers = [
    "Mobile Number", "Bill Number", "Amount Sanctioned", "Status", "Message",
    "Log Date", "Modify Date", "Store Code", "Store Name", "Store Type", "Bill Date",
    "Price Match ID", "Till Number"
  ]

  _.each(headers, (header, index) => {
    sheet.set(index + 1, 1, header)
  })

  _.each(data, (item, index) => {
    let i = index + 2

    sheet.set(1, i, item.mobile)
    sheet.set(2, i, item.billnumber)
    sheet.set(3, i, item.amount)
    sheet.set(4, i, item.state)
    sheet.set(5, i, item.message)
    sheet.set(6, i, item.logdate)
    sheet.set(7, i, item.modifydate)
    sheet.set(8, i, item.storecode)
    sheet.set(9, i, item.storename)
    sheet.set(10, i, item.storetype)
    sheet.set(11, i, item.billdate)
    sheet.set(12, i, item.pricematchid)
    sheet.set(13, i, item.tillnumber)
  })

  workbook.save(err => {
    if (err) {
      workbook.cancel()
      _callback(err)
    } else _callback(null)
  })

}

let updateawsconfig = _callback => {
  AWS.config.region = process.env.AWS_REGION_CONTENT
  let aws_config = {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
  }
  AWS.config.update(aws_config)
  _callback(null)
}

let uploadtos3 = _callback => {
  fs.readFile(`${filepath}/${filename}`, (err, data) => {
    if (err) {
      _callback('Error while reading file.')
    } else {
      let s3 = new AWS.S3()
      let base64data = new Buffer(data, 'binary')
      let aws_data_dict = {
        Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
        Key: `panel-reports/${moment().format('DD-MM-YYYY')}/${filename}`,
        Body: base64data,
        ContentType: filename.mimetype,
        ACL: process.env.AWS_ACL
      }

      s3.putObject(aws_data_dict, (_err, data) => {
        if (_err) _callback(_err)
        else _callback(null)
      })
    }
  })
}

async.waterfall([getdata, createxlsx, updateawsconfig, uploadtos3], (err) => {
  if (err) {
    log(`Failed to generate pricematch report for ${moment().format('DD-MM-YYYY')} : ${err}`)
    process.exit()
  } else {
    log(`Successfully generated pricematch report for ${moment().format('DD-MM-YYYY')}`)
    process.exit()
  }
})