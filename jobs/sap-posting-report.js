// Load env from file for dev. Set NODE_ENV in your bashrc or zshrc.
if (process.env.NODE_ENV === 'development') {
  require('env2')('./devenv.json')
}

const async = require('async')
const log = require('../sys/log')
const mysql = require('../connectors/mysql')
const moment = require('moment')
const xl = require('msexcel-builder-colorfix')
const _ = require('lodash')
const request = require('request')
const fs = require('fs')
const AWS = require('aws-sdk')
const path = require('path')

let txndata = []
let startdate = moment().format('YYYY-MM-DD')
let enddate = moment().add(1, 'days').format()
let sapid = Date.now()
let reqdate = moment().format('YYYYMMDD')
let tosend = null

let getwalletpayments = _callback => {
  let query = `
    Select storecode, sum(amount) as amount FROM payments 
    WHERE state = 'completed' AND walletid IN (01, 02, 03, 05) AND 
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}' 
    GROUP BY storecode;
  `
  mysql.query(query, [], (err, res) => {
    if (!err) {
      _.each(res, item => {
        let data = {
          INTERFACE_NAME: "FUTUREPAYDEBITPOSTING",
          STORE_CODE: item.storecode,
          SAP_POST_ID: sapid,
          REQUEST_DATE: reqdate,
          NET_AMT: item.amount,
          DISCOUNT: item.amount,
          GROSS_AMT: item.amount,
          RETAIL_TOPUP_MEDIA: 'FUP3',
          PT_TYPE: 'FUP3',
          BANK_NAME: ''
        }
        txndata.push(data)
      })
    }
    _callback(err)
  })
}

let getwalletrecharges = _callback => {
  let query = `
    Select storecode, sum(amount) as amount FROM recharges 
    WHERE state = 'completed' AND walletid IN (01, 02, 03, 05) AND 
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}' 
    GROUP BY storecode;
  `
  mysql.query(query, [], (err, res) => {
    if (!err) {
      _.each(res, item => {
        let data = {
          INTERFACE_NAME: "FUTUREPAYCREDITPOSTING",
          STORE_CODE: item.storecode,
          SAP_POST_ID: sapid,
          REQUEST_DATE: reqdate,
          NET_AMT: item.amount,
          DISCOUNT: item.amount,
          GROSS_AMT: item.amount,
          RETAIL_TOPUP_MEDIA: 'FUPU',
          PT_TYPE: 'FUPU',
          BANK_NAME: ''
        }
        txndata.push(data)
      })
    }
    _callback(err)
  })
}

let getbbpcpayments = _callback => {
  let query = `
    Select storecode, sum(amount) as amount FROM payments 
    WHERE state = 'completed' AND walletid IN (96) AND 
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}' 
    GROUP BY storecode;
  `
  mysql.query(query, [], (err, res) => {
    if (!err) {
      _.each(res, item => {
        let data = {
          INTERFACE_NAME: "BBPCDEBITPOSTING",
          STORE_CODE: item.storecode,
          SAP_POST_ID: sapid,
          REQUEST_DATE: reqdate,
          NET_AMT: item.amount,
          DISCOUNT: item.amount,
          GROSS_AMT: item.amount,
          RETAIL_TOPUP_MEDIA: 'FUP1',
          PT_TYPE: 'FUP1',
          BANK_NAME: ''
        }
        txndata.push(data)
      })
    }
    _callback(err)
  })
}

let getpaybackpayments = _callback => {
  let query = `
    Select storecode, sum(amount) as amount FROM payments 
    WHERE state = 'completed' AND walletid IN (97) AND 
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}' 
    GROUP BY storecode;
  `
  mysql.query(query, [], (err, res) => {
    if (!err) {
      _.each(res, item => {
        let data = {
          INTERFACE_NAME: "PAYBACKDEBITPOSTING",
          STORE_CODE: item.storecode,
          SAP_POST_ID: sapid,
          REQUEST_DATE: reqdate,
          NET_AMT: item.amount,
          DISCOUNT: item.amount,
          GROSS_AMT: item.amount,
          RETAIL_TOPUP_MEDIA: 'FUP2',
          PT_TYPE: 'FUP2',
          BANK_NAME: ''
        }
        txndata.push(data)
      })
    }
    _callback(err)
  })
}

let getwalletcashbacks = _callback => {
  //TODO:  Add query
  _callback(null)
}

let getwalletexpiredcashbacks = _callback => {
  _callback(null)
}

let getwallettopupbycreditnote = _callback => {
  let query = `
    Select storecode, sum(amount) as amount FROM payments 
    WHERE state = 'completed' AND mediatype LIKE '%Credit Note%' AND 
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}' 
    GROUP BY storecode;
  `
  mysql.query(query, [], (err, res) => {
    if (!err) {
      _.each(res, item => {
        let data = {
          INTERFACE_NAME: "WALLETTOPUPBYCREDITNOTEPOSTING",
          STORE_CODE: item.storecode,
          SAP_POST_ID: sapid,
          REQUEST_DATE: reqdate,
          NET_AMT: item.amount,
          DISCOUNT: item.amount,
          GROSS_AMT: item.amount,
          RETAIL_TOPUP_MEDIA: 'FUP7',
          PT_TYPE: 'FUP7',
          BANK_NAME: ''
        }
        txndata.push(data)
      })
    }
    _callback(err)
  })
}

let getwallettopupbyearlysalary = _callback => {
  let query = `
    Select storecode, sum(amount) as amount FROM payments 
    WHERE state = 'completed' AND channeltype IN ('ES') AND 
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}' 
    GROUP BY storecode;
  `
  mysql.query(query, [], (err, res) => {
    if (!err) {
      _.each(res, item => {
        let data = {
          INTERFACE_NAME: "WALLETTOPUPBYESPOSTING",
          STORE_CODE: item.storecode,
          SAP_POST_ID: sapid,
          REQUEST_DATE: reqdate,
          NET_AMT: item.amount,
          DISCOUNT: item.amount,
          GROSS_AMT: item.amount,
          RETAIL_TOPUP_MEDIA: 'FUP8',
          PT_TYPE: 'FUP8',
          BANK_NAME: ''
        }
        txndata.push(data)
      })
    }
    _callback(err)
  })
}

let preparereq = _callback => {
  tosend = {
    H_CONTROL_REC: {
      MANDT: "777",
      STATUS: "30",
      RCVPRN: "SRTCLIENT",
      DOCREL: "740",
      SNDPRT: "KU",
      SNDPOR: "WPUx",
      RCVPRT: "KU",
      MESTYP: "WPUFIB",
      RCVPOR: "SAPSQH",
      DIRECT: "2",
      IDOCTP: "WPUFIB01"
    },
    SAHAYOG_ITEM_MEDIAINFO: txndata,
    H_SAP_POST_ID: sapid,
    H_RETAIL_TOPUP_MEDIA: "FUP6"
  }
  _callback(null)
}

let posttossap = _callback => {
  let options = {
    uri: `${process.env.SAP_ENDPOINT}?apikey=${process.env.SAP_API_KEY}`,
    method: 'POST',
    json: tosend
  }

  request(options, (err, res, body) => {
    console.log("SAP RESPONSE: ", err, res, body)
    if (!err && res && res.statusCode == '200') {
      _callback(null)
    } else {
      _callback(err)
    }
  })
}

let createxlsx = _callback => {
  filename = `sap-posting-report-${Date.now()}.xlsx`
  filepath = '/tmp'
  let workbook = xl.createWorkbook(filepath, filename)
  let sheet = workbook.createSheet('Report', 50, 30000)
  let headers = ["Posting ID", "Store Code", "Transaction Date", "Media Type", "Amount", "Type", "Status", "Wallet ID", "Log Date"]

  _.each(headers, (header, index) => {
    sheet.set(index + 1, 1, header)
  })

  _.each(txndata, (item, index) => {
    let i = index + 2

    sheet.set(1, i, sapid)
    sheet.set(2, i, storecode)
    sheet.set(3, i, reqdate)
    sheet.set(4, i, item.RETAIL_TOPUP_MEDIA)
    sheet.set(5, i, item.PT_TYPE)
    sheet.set(6, i, "Success")
    sheet.set(6, i, 01)
    sheet.set(6, i, reqdate)
  })

  workbook.save(err => {
    if (err) {
      workbook.cancel()
      _callback(err)
    } else _callback(null)
  })

}

let updateawsconfig = _callback => {
  AWS.config.region = process.env.AWS_REGION_CONTENT
  let aws_config = {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
  }
  AWS.config.update(aws_config)
  _callback(null)
}

let uploadtos3 = _callback => {
  fs.readFile(`${filepath}/${filename}`, (err, data) => {
    if (err) {
      _callback('Error while reading file.')
    } else {
      let s3 = new AWS.S3()
      let base64data = new Buffer(data, 'binary')
      let aws_data_dict = {
        Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
        Key: `panel-reports/${moment().format('DD-MM-YYYY')}/${filename}`,
        Body: base64data,
        ContentType: filename.mimetype,
        ACL: process.env.AWS_ACL
      }

      s3.putObject(aws_data_dict, (_err, data) => {
        if (_err) _callback(_err)
        else _callback(null)
      })
    }
  })
}

async.waterfall([getwalletpayments, getwalletrecharges, getbbpcpayments, getpaybackpayments, getwalletcashbacks, getwalletexpiredcashbacks, getwallettopupbycreditnote, getwallettopupbyearlysalary, preparereq, createxlsx, updateawsconfig, uploadtos3], (err) => {
  if (err) {
    log(`Failed to generate SAP Posting report for ${moment().format('DD-MM-YYYY')} : ${err}`)
    process.exit()
  } else {
    console.log("Posting Data: ", txndata)
    log(`Successfully generated SAP Posting report for ${moment().format('DD-MM-YYYY')}`)
    process.exit()
  }
})