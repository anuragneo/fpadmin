const async = require('async')
const log = require('../sys/log')
const Login = require('../persistence/login')
const _ = require('lodash')

class LoginHelper {
  login(incoming, callback) {
    let clienterr, syserr, manager

    function validatedata(_callback) {
      if (incoming) {
        if (!incoming.loginid || `${incoming.loginid}`.trim().length < 2 || !isNaN(incoming.loginid))
          clienterr = syserr = `Enter Valid LoginId`
        else if (!incoming.password || `${incoming.password}`.trim().length < 8)
          clienterr = syserr = `Enter a valid 8 digit password.`
      } else clienterr = syserr = `Invalid request for login`

      _callback(clienterr)
    }

    function getmanager(_callback) {
      Login.getbyloginid(incoming.loginid, (err, _manager) => {
        manager = _manager

        if (err) {
          clienterr = `Login failed. Please try again.`
          syserr = err
        } else if (!manager) clienterr = syserr = `Login Id(${incoming.loginid}) does not exist. Please register.`

        _callback(clienterr)
      })
    }

    function verifylogin(_callback) {
      if (manager.status === process.env.ACCOUNT_LOCKED) {
        clienterr = syserr = `Your account is locked. Contact admin.`
        _callback(clienterr)
      } else {
        Login.verifylogin(manager, incoming.password, (err, loginsuccess, remaining) => {
          if (!loginsuccess) {
            if (remaining > 0) clienterr = syserr = `Invalid credentials. ${remaining} attempt(s) remaining.`
            else clienterr = syserr = `Your account is locked. Contact Admin.`
          } else if (err || !loginsuccess) {
            clienterr = `Login failed. Please try again.`
            syserr = err || `Login Failed for manager id ${manager.id}.`
          }
          _callback(clienterr)
        })
      }
    }

    async.waterfall([validatedata, getmanager, verifylogin], err => {
      if (err || syserr) {
        log(syserr || err, true)
        callback(err)
      } else {
        let admin = _.omit(manager, ["password"])
        callback(null, admin)
      }
    })
  }
}

module.exports = new LoginHelper()
