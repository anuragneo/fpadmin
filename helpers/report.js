const async = require('async')
const xl = require('msexcel-builder-colorfix')
const AWS = require('aws-sdk')
const _ = require('lodash')
const moment = require('moment')
const fs = require('fs')
const log = require('../sys/log')
const Report = require('../persistence/report')

class ReportHelper {
  gettotaluser(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.enddate) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let totaluserfrompersistence = _callback => {
      Report.gettotaluser(incoming.enddate, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching total no of users. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totaluserfrompersistence], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  getactiveinactiveuser(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let getactiveinactiveuser = _callback => {
      Report.getactiveinactiveuser(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching active users`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, getactiveinactiveuser], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  totalnewregistered(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let totalnewregistered = _callback => {
      Report.totalnewregistered(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching total no of new registered users. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totalnewregistered], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  registrationthroughsource(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let registrationthroughsource = _callback => {
      Report.registrationthroughsource(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching users registered through source. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, registrationthroughsource], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  getcustomerbydaterange(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let getcustomerbydaterange = _callback => {
      Report.getcustomerbydaterange(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, getcustomerbydaterange], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  getcustomerreport(incoming, callback) {
    let customers, filename, clienterr, syserr, filepath
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let getcustomerreport = _callback => {
      Report.getcustomerreport(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customer report. Please try again.`
        } else {
          customers = response
        }
        _callback(clienterr)
      })
    }

    let createxlsx = _callback => {
      filename = `customer-master-report-${Date.now()}.xlsx`
      filepath = '/tmp'
      let workbook = xl.createWorkbook(filepath, filename)
      let sheet = workbook.createSheet('Report', 50, 30000)

      // Set Header
      sheet.set(1, 1, 'Mobile')
      sheet.set(2, 1, 'FirstName')
      sheet.set(3, 1, 'LastName')
      sheet.set(4, 1, 'DOB')
      sheet.set(5, 1, 'Age')
      sheet.set(6, 1, 'Email')
      sheet.set(7, 1, 'Postal Code')
      sheet.set(8, 1, 'City Name')
      sheet.set(9, 1, 'State Name')
      sheet.set(10, 1, 'Date of Registration')
      sheet.set(11, 1, 'Device Platform')
      sheet.set(12, 1, 'Device Platform')
      sheet.set(13, 1, 'Time of Registration')
      sheet.set(14, 1, 'Device IMEI Platform')
      sheet.set(15, 1, 'OS Version')
      sheet.set(16, 1, 'Wallet Status')

      console.log(`started apending dtat to ram ${customers.length}`)

      _.each(customers, (data, index) => {
        let i = index + 2

        sheet.set(1, i, data.mobile)
        sheet.set(2, i, data.firstname)
        sheet.set(3, i, data.lastname)
        sheet.set(4, i, data.dob)
        sheet.set(5, i, data.age)
        sheet.set(6, i, data.email)
        sheet.set(7, i, data.pincode)
        sheet.set(8, i, data.city)
        sheet.set(9, i, data.state)
        sheet.set(10, i, data.txndate)
        sheet.set(11, i, '')
        sheet.set(12, i, '')
        sheet.set(13, i, data.txntime)
        sheet.set(14, i, data.deviceid)
        sheet.set(15, i, data.osversion)
        sheet.set(16, i, data.status)
      })

      console.log('started saving file')

      workbook.save(err => {
        if (err) {
          log(err, true)
          workbook.cancel()
          _callback(err)
        } else _callback(null)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadtos3 = _callback => {
      fs.readFile(`${filepath}/${filename}`, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          console.log(`done reading file from ${filepath}/${filename}`)
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: `reports/${filename}`,
            Body: base64data,
            ContentType: filename.mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, (_err, data) => {
            if (_err) {
              clienterr = 'Error occoured while generating report. Please try again.'
              syserr = _err
              _callback(clienterr)
            } else {
              console.log(`uploaded to s3 ${data}`)
              _callback(null, { url: `${process.env.AWS_BUCKET_URL_CONTENT}/reports/${filename}` })
            }
          })
        }
      })
    }

    async.waterfall([validateincoming, getcustomerreport, createxlsx, updateawsconfig, uploadtos3], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        console.log('customer report ===> All operations completed successfully')
        callback({ status: 'success', data: response })
      }
    })
  }

  totalspent(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let totalspent = _callback => {
      Report.totalspent(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totalspent], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  totaltopup(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let totaltopup = _callback => {
      Report.totaltopup(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totaltopup], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  totalcashback(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let totalcashback = _callback => {
      Report.totalcashback(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totalcashback], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  debitvolume(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let debitvolume = _callback => {
      Report.debitvolume(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, debitvolume], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  countgroupdebit(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let countgroupdebit = _callback => {
      Report.countgroupdebit(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, countgroupdebit], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  creditvolume(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let creditvolume = _callback => {
      Report.creditvolume(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, creditvolume], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  countgroupcredit(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let countgroupcredit = _callback => {
      Report.countgroupcredit(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, countgroupcredit], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  cashbackanalysis(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let cashbackanalysis = _callback => {
      Report.cashbackanalysis(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, cashbackanalysis], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  cashbackexperiable(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let cashbackexperiable = _callback => {
      Report.cashbackexperiable(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, cashbackexperiable], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  groupbycampaignname(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let groupbycampaignname = _callback => {
      Report.groupbycampaignname(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, groupbycampaignname], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  totalactiveuserwallet(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let totalactiveuserwallet = _callback => {
      Report.totalactiveuserwallet(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching customers. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totalactiveuserwallet], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  newbbpcpaybackregistered(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.type) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let newbbpcpaybackregistered = _callback => {
      Report.newbbpcpaybackregistered(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching new registered user. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, newbbpcpaybackregistered], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  totaldelinkingbbpcpayback(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.type) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let totaldelinkingbbpcpayback = _callback => {
      Report.totaldelinkingbbpcpayback(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching delinked user. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totaldelinkingbbpcpayback], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  totallinkedbbpcpayback(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.type) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let totallinkedbbpcpayback = _callback => {
      Report.totallinkedbbpcpayback(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching linked user. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totallinkedbbpcpayback], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  totaldeinkedbbpcpayback(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.type) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let totaldeinkedbbpcpayback = _callback => {
      Report.totaldeinkedbbpcpayback(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching delinked user. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, totaldeinkedbbpcpayback], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  bbpcreport(incoming, callback) {
    let bbpc, filename, clienterr, syserr, filepath
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let bbpcreport = _callback => {
      Report.bbpcreport(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching delinked user. Please try again.`
        } else {
          bbpc = response
        }
        _callback(clienterr)
      })
    }

    let createxlsx = _callback => {
      filename = `bbpc-report-${Date.now()}.xlsx`
      filepath = '/tmp'
      let workbook = xl.createWorkbook(filepath, filename)
      let sheet = workbook.createSheet('Report', 50, 30000)

      // Set Header
      sheet.set(1, 1, 'BBPC Membership ID')
      sheet.set(2, 1, 'Future Pay Account ID')
      sheet.set(3, 1, 'Mobile Number')
      sheet.set(4, 1, 'Card Type')
      sheet.set(5, 1, 'Card Number')
      sheet.set(6, 1, 'Customer Name')
      sheet.set(7, 1, 'Customer Mobile Number')

      _.each(bbpc, (data, index) => {
        let i = index + 2

        sheet.set(1, i, data.membershipid)
        sheet.set(2, i, data.fpaccountid)
        sheet.set(3, i, data.mobile)
        sheet.set(4, i, data.card_type)
        sheet.set(5, i, data.cardnumber)
        sheet.set(6, i, data.customer_name)
        sheet.set(7, i, data.mobile)
      })

      workbook.save(err => {
        if (err) {
          workbook.cancel()
          _callback(null)
        } else _callback(null)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadtos3 = _callback => {
      fs.readFile(`${filepath}/${filename}`, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: `reports/${filename}`,
            Body: base64data,
            ContentType: filename.mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, (_err, data) => {
            if (_err) {
              clienterr = 'Error occoured while generating report. Please try again.'
              syserr = _err
              _callback(clienterr)
            } else {
              _callback(null, { url: `${process.env.AWS_BUCKET_URL_CONTENT}/reports/${filename}` })
            }
          })
        }
      })
    }

    async.waterfall([validateincoming, bbpcreport, createxlsx, updateawsconfig, uploadtos3], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  paybackreport(incoming, callback) {
    let payback, filename, clienterr, syserr, filepath
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let paybackreport = _callback => {
      Report.paybackreport(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error creating payback report. Please try again.`
        } else {
          payback = response
        }
        _callback(clienterr)
      })
    }

    let createxlsx = _callback => {
      filename = `payback-report-${Date.now()}.xlsx`
      filepath = '/tmp'
      let workbook = xl.createWorkbook(filepath, filename)
      let sheet = workbook.createSheet('Report', 50, 30000)

      // Set Header
      sheet.set(1, 1, 'Payback Membership ID')
      sheet.set(2, 1, 'Future Pay Account ID')
      sheet.set(3, 1, 'Mobile Number')
      sheet.set(4, 1, 'Card Type')
      sheet.set(5, 1, 'Card Number')
      sheet.set(6, 1, 'Customer Name')
      sheet.set(7, 1, 'Customer Mobile Number')

      _.each(payback, (data, index) => {
        let i = index + 2

        sheet.set(1, i, data.cardnumber)
        sheet.set(2, i, data.fpaccountid)
        sheet.set(3, i, data.mobile)
        sheet.set(4, i, data.card_type)
        sheet.set(5, i, data.cardnumber)
        sheet.set(6, i, data.customer_name)
        sheet.set(7, i, data.mobile)
      })

      workbook.save(err => {
        if (err) {
          workbook.cancel()
          _callback(null)
        } else _callback(null)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadtos3 = _callback => {
      fs.readFile(`${filepath}/${filename}`, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: `reports/${filename}`,
            Body: base64data,
            ContentType: filename.mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, (_err, data) => {
            if (_err) {
              clienterr = 'Error occoured while generating report. Please try again.'
              syserr = _err
              _callback(clienterr)
            } else {
              _callback(null, { url: `${process.env.AWS_BUCKET_URL_CONTENT}/reports/${filename}` })
            }
          })
        }
      })
    }

    async.waterfall([validateincoming, paybackreport, createxlsx, updateawsconfig, uploadtos3], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  bbpctransactionreport(incoming, callback) {
    let bbpc, filename, clienterr, syserr, filepath
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let bbpctransactionreport = _callback => {
      Report.bbpctransactionreport(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error creating bbpc report. Please try again.`
        } else {
          bbpc = response
        }
        _callback(clienterr)
      })
    }

    let createxlsx = _callback => {
      filename = `bbpc-transaction-report-${Date.now()}.xlsx`
      filepath = '/tmp'
      let workbook = xl.createWorkbook(filepath, filename)
      let sheet = workbook.createSheet('Report', 50, 30000)

      // Set Header
      sheet.set(1, 1, 'Card Type')
      sheet.set(2, 1, 'ID')
      sheet.set(3, 1, 'Mobile Number')
      sheet.set(4, 1, 'Wallet ID')
      sheet.set(5, 1, 'Wallet Owner')
      sheet.set(6, 1, 'Store Code')
      sheet.set(7, 1, 'Store Name')
      sheet.set(8, 1, 'FG Txn Nmuber')
      sheet.set(9, 1, 'Reference Number')
      sheet.set(10, 1, 'Transac Type')
      sheet.set(11, 1, 'Transac Sub Type')
      sheet.set(12, 1, 'Transac Source')
      sheet.set(13, 1, 'Amount')
      sheet.set(14, 1, 'Transac Date')
      sheet.set(15, 1, 'Transac Status')
      sheet.set(16, 1, 'Cash Memo No')
      sheet.set(17, 1, 'Terminal ID')
      sheet.set(18, 1, 'Store Type')
      sheet.set(19, 1, 'Balance')
      sheet.set(20, 1, 'Linked Card')
      sheet.set(21, 1, 'Time Stamp Number')
      sheet.set(22, 1, 'PayU Id')

      _.each(bbpc, (data, index) => {
        let i = index + 2

        sheet.set(1, i, 'BBPC')
        sheet.set(2, i, '')
        sheet.set(3, i, data.mobile)
        sheet.set(4, i, data.walletid)
        sheet.set(5, i, data.wallet_owner)
        sheet.set(6, i, data.storecode)
        sheet.set(7, i, '')
        sheet.set(7, i, data.txnid)
        sheet.set(7, i, data.txnrefno)
        sheet.set(7, i, data.txntype)
        sheet.set(7, i, data.subtype)
        sheet.set(7, i, data.source)
        sheet.set(7, i, data.amount)
        sheet.set(7, i, data.txndate)
        sheet.set(7, i, data.txnstatus)
        sheet.set(7, i, data.cashmemono)
        sheet.set(7, i, data.terminalid)
        sheet.set(7, i, '')
        sheet.set(7, i, data.balance)
        sheet.set(7, i, '')
        sheet.set(7, i, data.timestampno)
        sheet.set(7, i, '')
      })

      workbook.save(err => {
        if (err) {
          workbook.cancel()
          _callback(null)
        } else _callback(null)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadtos3 = _callback => {
      fs.readFile(`${filepath}/${filename}`, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: `reports/${filename}`,
            Body: base64data,
            ContentType: filename.mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, (_err, data) => {
            if (_err) {
              clienterr = 'Error occoured while generating report. Please try again.'
              syserr = _err
              _callback(clienterr)
            } else {
              _callback(null, { url: `${process.env.AWS_BUCKET_URL_CONTENT}/reports/${filename}` })
            }
          })
        }
      })
    }

    async.waterfall(
      [validateincoming, bbpctransactionreport, createxlsx, updateawsconfig, uploadtos3],
      (err, response) => {
        if (err) {
          log(syserr || err, true)
          callback({ status: 'failed', message: clienterr })
        } else {
          callback({ status: 'success', data: response })
        }
      }
    )
  }

  paybacktransactionreport(incoming, callback) {
    let payback, filename, clienterr, syserr, filepath
    let validateincoming = _callback => {
      if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let paybacktransactionreport = _callback => {
      Report.paybacktransactionreport(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error creating payback report. Please try again.`
        } else {
          payback = response
        }
        _callback(clienterr)
      })
    }

    let createxlsx = _callback => {
      filename = `payback-transaction-report-${Date.now()}.xlsx`
      filepath = '/tmp'
      let workbook = xl.createWorkbook(filepath, filename)
      let sheet = workbook.createSheet('Report', 50, 30000)

      // Set Header
      sheet.set(1, 1, 'Card Type')
      sheet.set(2, 1, 'ID')
      sheet.set(3, 1, 'Mobile Number')
      sheet.set(4, 1, 'Wallet ID')
      sheet.set(5, 1, 'Wallet Owner')
      sheet.set(6, 1, 'Store Code')
      sheet.set(7, 1, 'Store Name')
      sheet.set(8, 1, 'FG Txn Nmuber')
      sheet.set(9, 1, 'Reference Number')
      sheet.set(10, 1, 'Transac Type')
      sheet.set(11, 1, 'Transac Sub Type')
      sheet.set(12, 1, 'Transac Source')
      sheet.set(13, 1, 'Amount')
      sheet.set(14, 1, 'Transac Date')
      sheet.set(15, 1, 'Transac Status')
      sheet.set(16, 1, 'Cash Memo No')
      sheet.set(17, 1, 'Terminal ID')
      sheet.set(18, 1, 'Store Type')
      sheet.set(19, 1, 'Balance')
      sheet.set(20, 1, 'Linked Card')
      sheet.set(21, 1, 'Time Stamp Number')
      sheet.set(22, 1, 'PayU Id')

      _.each(payback, (data, index) => {
        let i = index + 2

        sheet.set(1, i, 'Payback')
        sheet.set(2, i, '')
        sheet.set(3, i, data.mobile)
        sheet.set(4, i, data.walletid)
        sheet.set(5, i, data.wallet_owner)
        sheet.set(6, i, data.storecode)
        sheet.set(7, i, '')
        sheet.set(7, i, data.txnid)
        sheet.set(7, i, data.txnrefno)
        sheet.set(7, i, data.txntype)
        sheet.set(7, i, data.subtype)
        sheet.set(7, i, data.source)
        sheet.set(7, i, data.amount)
        sheet.set(7, i, data.txndate)
        sheet.set(7, i, data.txnstatus)
        sheet.set(7, i, data.cashmemono)
        sheet.set(7, i, data.terminalid)
        sheet.set(7, i, '')
        sheet.set(7, i, data.balance)
        sheet.set(7, i, '')
        sheet.set(7, i, data.timestampno)
        sheet.set(7, i, '')
      })

      workbook.save(err => {
        if (err) {
          workbook.cancel()
          _callback(null)
        } else _callback(null)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadtos3 = _callback => {
      fs.readFile(`${filepath}/${filename}`, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: `reports/${filename}`,
            Body: base64data,
            ContentType: filename.mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, (_err, data) => {
            if (_err) {
              clienterr = 'Error occoured while generating report. Please try again.'
              syserr = _err
              _callback(clienterr)
            } else {
              _callback(null, { url: `${process.env.AWS_BUCKET_URL_CONTENT}/reports/${filename}` })
            }
          })
        }
      })
    }

    async.waterfall(
      [validateincoming, paybacktransactionreport, createxlsx, updateawsconfig, uploadtos3],
      (err, response) => {
        if (err) {
          log(syserr || err, true)
          callback({ status: 'failed', message: clienterr })
        } else {
          callback({ status: 'success', data: response })
        }
      }
    )
  }

  successratiodebit(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let successratiodebit = _callback => {
      Report.successratiodebit(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching success ratio. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, successratiodebit], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  successratiocredit(incoming, callback) {
    let clienterr, syserr
    let validateincoming = _callback => {
      if (!incoming.walletid) clienterr = `Missing important data`
      else if (
        !moment(incoming.startdate, `DD/MM/YYYY`, true).isValid() ||
        !moment(incoming.enddate, `DD/MM/YYYY`, true).isValid()
      )
        clienterr = `Invalid date range to fetch history.`
      else {
        let start = moment(incoming.startdate, 'DD/MM/YYYY'),
          end = moment(incoming.enddate, 'DD/MM/YYYY')
        if (end.diff(start, 'days') < 0) clienterr = `Invalid date range to get history.`
      }
      _callback(clienterr)
    }

    let successratiocredit = _callback => {
      Report.successratiocredit(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching success ratio. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validateincoming, successratiocredit], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }
}

module.exports = new ReportHelper()
