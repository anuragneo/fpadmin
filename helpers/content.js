const async = require('async')
const log = require('../sys/log')
const fs = require('fs')
const AWS = require('aws-sdk')
const Content = require('../persistence/content')
const path = require('path')
const AdmZip = require('adm-zip')
const _ = require('lodash')
const mime = require('mime')

class ContentHelper {

  invalidatecache(incoming, callback) {
    let clienterr, syserr

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let invalidatecache = _callback => {
      let cloudfront = new AWS.CloudFront();
      let items = ['/static/*']
      let invalidations = items.map(encodeURI)
      let params = {
        DistributionId: process.env.AWS_CLOUDFRONT_S3_DISTRIBUTION_ID,
        InvalidationBatch: {
          CallerReference: '' + +new Date(),
          Paths: {
            Quantity: invalidations.length,
            Items: invalidations
          }
        }
      }
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) {
          syserr = err
          log(syserr || err, true)
        }
      })

      _callback(null)
    }

    async.waterfall([updateawsconfig, invalidatecache], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: 'S3 Cache invalidated successfully.' })
      }
    })


  }

  updateintroscreen(_incoming, file, callback) {
    let clienterr, syserr, incoming

    let formatincoming = _callback => {
      try {
        incoming = JSON.parse(_incoming)
      } catch (_error) {
        clienterr = 'Invalid request.'
        syserr = 'Invalid JSON ' + _incoming
      } finally {
        _callback(clienterr)
      }
    }

    let validateincoming = _callback => {
      if (!incoming.name || !incoming.id || !incoming.seqnumber || !incoming.sdesc || !incoming.ldesc) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let updateintroscreen = (_callback) => {
      Content.updateintroscreen(incoming, file, (err, res) => {
        if (err) {
          syserr = err
          clienterr = "Failed. Try again."
        }
        _callback(clienterr)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadtos3 = (_callback) => {
      if (!file) {
        _callback(null)
        return
      }

      let filename = `introscreens/${incoming.id}/${file.originalname}`

      fs.readFile(file.path, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: filename,
            Body: base64data,
            ContentType: mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, _err => {
            if (_err) {
              clienterr = 'Error occoured while uploading the file. Please try again.'
              syserr = _err
            }
            _callback(clienterr)
          })
        }
      })
    }

    let invalidatecache = _callback => {
      let cloudfront = new AWS.CloudFront();
      let items = ['/api/content/getintroscreens']
      let invalidations = items.map(encodeURI)
      let params = {
        DistributionId: process.env.AWS_CLOUDFRONT_DISTRIBUTION_ID,
        InvalidationBatch: {
          CallerReference: '' + +new Date(),
          Paths: {
            Quantity: invalidations.length,
            Items: invalidations
          }
        }
      }
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) {
          syserr = err
          log(syserr || err, true)
        }
      })

      _callback(null)
    }

    async.waterfall([formatincoming, validateincoming, updateintroscreen, updateawsconfig, uploadtos3, invalidatecache], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: 'Introscreen updated successfully.' })
      }
    })
  }

  deleteintroscreen(incoming, callback) {
    let clienterr, syserr

    let validateincoming = _callback => {
      if (!incoming.id) {
        clienterr = syserr = 'Missing improtant data'
      }
      _callback(clienterr)
    }

    let updatepersistance = _callback => {
      Content.deleteintroscreen(incoming.id, (err, res) => {
        if (err) {
          clienterr = 'Error while deleting introscreen. Please try again.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let invalidatecache = _callback => {
      let cloudfront = new AWS.CloudFront();
      let items = ['/api/content/getintroscreens']
      let invalidations = items.map(encodeURI)
      let params = {
        DistributionId: process.env.AWS_CLOUDFRONT_DISTRIBUTION_ID,
        InvalidationBatch: {
          CallerReference: '' + +new Date(),
          Paths: {
            Quantity: invalidations.length,
            Items: invalidations
          }
        }
      }
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) {
          syserr = err
          log(syserr || err, true)
        }
      })

      _callback(null)
    }

    async.waterfall([validateincoming, updatepersistance, updateawsconfig, invalidatecache], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { message: 'Introscreen deleted successfully.' } })
      }
    })
  }

  getintroscreens(incoming, callback) {
    let clienterr, syserr

    Content.getintroscreens((err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: res })
      }
    })
  }

  createintroscreens(_incoming, file, callback) {
    let clienterr, syserr, incoming, introscreenid
    let introscreen = null

    let formatincoming = _callback => {
      try {
        incoming = JSON.parse(_incoming)
      } catch (_error) {
        clienterr = 'Invalid request.'
        syserr = 'Invalid JSON ' + _incoming
      } finally {
        _callback(clienterr)
      }
    }

    let validateincoming = _callback => {
      if (!incoming.name || !incoming.seqnumber || !incoming.sdesc || !incoming.ldesc || !file) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let createintroscreen = (_callback) => {
      Content.createintroscreen(incoming, file, (err, res) => {
        if (err) {
          syserr = err
          clienterr = "Failed. Try again."
        } else {
          introscreenid = res.insertId
        }
        _callback(clienterr)
      })
    }

    let getintroscreen = (_callback) => {
      Content.getintroscreenbyid(introscreenid, (err, res) => {
        if (err) {
          syserr = err
          clienterr = "Failed. Try again."
        } else {
          introscreen = res[0]
        }
        _callback(null)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadtos3 = _callback => {
      let filename = `introscreens/${introscreenid}/${file.originalname}`

      fs.readFile(file.path, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: filename,
            Body: base64data,
            ContentType: file.mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, _err => {
            if (_err) {
              clienterr = 'Error occoured while uploading the file. Please try again.'
              syserr = _err
            }
            _callback(clienterr)
          })
        }
      })
    }

    let invalidatecache = _callback => {
      let cloudfront = new AWS.CloudFront();
      let items = ['/api/content/getintroscreens']
      let invalidations = items.map(encodeURI)
      let params = {
        DistributionId: process.env.AWS_CLOUDFRONT_DISTRIBUTION_ID,
        InvalidationBatch: {
          CallerReference: '' + +new Date(),
          Paths: {
            Quantity: invalidations.length,
            Items: invalidations
          }
        }
      }
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) {
          syserr = err
          log(syserr || err, true)
        }
      })

      _callback(null)
    }

    async.waterfall([formatincoming, validateincoming, createintroscreen, getintroscreen, updateawsconfig, uploadtos3, invalidatecache], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: 'Introscreen created successfully.', data: introscreen })
      }
    })
  }

  deletebanner(incoming, callback) {
    let clienterr, syserr

    let validateincoming = _callback => {
      if (!incoming.id) {
        clienterr = syserr = 'Missing improtant data'
      }
      _callback(clienterr)
    }

    let updatepersistance = _callback => {
      Content.deletebanner(incoming.id, (err, res) => {
        if (err) {
          clienterr = 'Error while deleting  banner. Please try again.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let invalidatecache = _callback => {
      let cloudfront = new AWS.CloudFront();
      let items = ['/api/content/getbanners']
      let invalidations = items.map(encodeURI)
      let params = {
        DistributionId: process.env.AWS_CLOUDFRONT_DISTRIBUTION_ID,
        InvalidationBatch: {
          CallerReference: '' + +new Date(),
          Paths: {
            Quantity: invalidations.length,
            Items: invalidations
          }
        }
      }
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) {
          syserr = err
          log(syserr || err, true)
        }
      })

      _callback(null)
    }

    async.waterfall([validateincoming, updatepersistance, updateawsconfig, invalidatecache], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { message: 'Banner deleted successfully.' } })
      }
    })
  }

  getbanners(incoming, callback) {
    let clienterr, syserr
    let banners = null

    Content.getbanners(incoming, (err, res) => {
      if (err) {
        clienterr = 'Failed. Try again.'
        syserr = err
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: res })
      }
    })
  }

  createbanner(_incoming, files, callback) {
    let clienterr, syserr, incoming, bannerid
    let banner = null
    let filestoupload = []

    let formatincoming = _callback => {
      try {
        incoming = JSON.parse(_incoming)
      } catch (_error) {
        clienterr = 'Invalid request.'
        syserr = 'Invalid JSON ' + _incoming
      } finally {
        _callback(clienterr)
      }
    }

    let validateincoming = _callback => {
      if (!incoming.name || !incoming.seqnumber || !incoming.formatid || !files) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let unzip = _callback => {
      let file = files.file[0]
      // Read zip archive
      let zip = new AdmZip(file.path);
      // Extract zip archive
      zip.extractAllTo(`/tmp/banners/${file.filename}/`, true)
      _callback(null)
    }

    let getfiles = _callback => {
      let file = files.file[0]
      let dirs = null

      try {
        dirs = fs.readdirSync(`/tmp/banners/${file.filename}/zip/`)
        dirs = dirs.filter(item => !(/(^|\/)\.[^\/\.]/g).test(item))
      } catch (e) {
        syserr = e
        clienterr = "Error while reading zip content."
      }

      if (dirs) {
        async.each(dirs, preparefiles, (err) => {
          clienterr = syserr = err
          _callback(clienterr)
        })
      } else {
        _callback(clienterr)
      }
    }

    let preparefiles = (dirname, _callback) => {
      let dirfiles = null
      let file = files.file[0]

      try {
        dirfiles = fs.readdirSync(`/tmp/banners/${file.filename}/zip/${dirname}`)
        dirfiles = dirfiles.filter(item => !(/(^|\/)\.[^\/\.]/g).test(item))
      } catch (e) {
        clienterr = "Error while reading zip content."
      }

      if (dirfiles) {
        switch (dirname) {
          case "homelink":
            filestoupload.push({
              fieldname: 'homelink',
              filename: dirfiles[0],
              path: `/tmp/banners/${file.filename}/zip/${dirname}/${dirfiles[0]}`
            })
            break;
          case "offerlink":
            filestoupload.push({
              fieldname: 'offerlink',
              filename: dirfiles[0],
              path: `/tmp/banners/${file.filename}/zip/${dirname}/${dirfiles[0]}`
            })
            break;
          case "htmllink":
            filestoupload.push({
              fieldname: 'htmllink',
              filename: dirfiles[0],
              path: `/tmp/banners/${file.filename}/zip/${dirname}/${dirfiles[0]}`
            })
            break;
          case "images":
            _.each(dirfiles, (dirfile) => {
              filestoupload.push({
                fieldname: 'images',
                filename: dirfile,
                path: `/tmp/banners/${file.filename}/zip/${dirname}/${dirfile}`
              })
            })
          default:
            break;
        }
        _callback(null)
      } else {
        _callback(clienterr)
      }

    }

    let createbanner = (_callback) => {
      Content.createbanner(incoming, filestoupload, (err, res) => {
        if (err) {
          syserr = err
          clienterr = "Failed. Try again."
        } else {
          bannerid = res.insertId
        }
        _callback(clienterr)
      })
    }

    let getbanner = (_callback) => {
      Content.getbannerbyid(bannerid, (err, res) => {
        if (err) {
          syserr = err
          clienterr = "Failed. Try again."
        } else {
          banner = res[0]
        }
        _callback(null)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadFiles = _callback => {
      async.each(filestoupload, uploadtos3, (err) => {
        if (err) {
          syserr = err
          clienterr = "Error while uploading files. Please try again."
        }
        _callback(clienterr)
      })
    }

    let uploadtos3 = (fileobj, _callback) => {
      let filename = null
      let file = files.file[0]

      if (fileobj.fieldname === 'homelink') {
        filename = `banners/${bannerid}/h-${fileobj.filename}`
      } else if (fileobj.fieldname === 'offerlink') {
        filename = `banners/${bannerid}/o-${fileobj.filename}`
      } else if (fileobj.fieldname === 'htmllink') {
        filename = `banners/${bannerid}/${fileobj.filename}`
      } else if (fileobj.fieldname === 'images') {
        filename = `banners/${bannerid}/images/${fileobj.filename}`
      }

      fs.readFile(fileobj.path, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let mimetype = mime.getType(fileobj.path)
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: filename,
            Body: base64data,
            ContentType: mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, _err => {
            if (_err) {
              clienterr = 'Error occoured while uploading the file. Please try again.'
              syserr = _err
            }
            _callback(clienterr)
          })
        }
      })
    }

    let invalidatecache = _callback => {
      let cloudfront = new AWS.CloudFront();
      let items = ['/api/content/getbanners']
      let invalidations = items.map(encodeURI)
      let params = {
        DistributionId: process.env.AWS_CLOUDFRONT_DISTRIBUTION_ID,
        InvalidationBatch: {
          CallerReference: '' + +new Date(),
          Paths: {
            Quantity: invalidations.length,
            Items: invalidations
          }
        }
      }
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) {
          syserr = err
          log(syserr || err, true)
        }
      })

      _callback(null)
    }

    async.waterfall([formatincoming, validateincoming, unzip, getfiles, createbanner, getbanner, updateawsconfig, uploadFiles, invalidatecache], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: 'Banner created successfully.', data: banner })
      }
    })
  }

  updatebanner(_incoming, files, callback) {
    let clienterr, syserr, incoming
    let filestoupload = []

    let formatincoming = _callback => {
      try {
        incoming = JSON.parse(_incoming)
      } catch (_error) {
        clienterr = 'Invalid request.'
        syserr = 'Invalid JSON ' + _incoming
      } finally {
        _callback(clienterr)
      }
    }

    let validateincoming = _callback => {
      if (!incoming.name || !incoming.id || !incoming.formatid) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let unzip = _callback => {
      let file = files.file[0]
      // Read zip archive
      let zip = new AdmZip(file.path);
      // Extract zip archive
      zip.extractAllTo(`/tmp/banners/${file.filename}/`, true)
      _callback(null)
    }

    let getfiles = _callback => {
      let file = files.file[0]
      let dirs = null

      try {
        dirs = fs.readdirSync(`/tmp/banners/${file.filename}/zip/`)
        dirs = dirs.filter(item => !(/(^|\/)\.[^\/\.]/g).test(item))
      } catch (e) {
        syserr = e
        clienterr = "Error while reading zip content."
      }

      if (dirs) {
        async.each(dirs, preparefiles, (err) => {
          clienterr = syserr = err
          _callback(clienterr)
        })
      } else {
        _callback(clienterr)
      }
    }

    let preparefiles = (dirname, _callback) => {
      let dirfiles = null
      let file = files.file[0]

      try {
        dirfiles = fs.readdirSync(`/tmp/banners/${file.filename}/zip/${dirname}`)
        dirfiles = dirfiles.filter(item => !(/(^|\/)\.[^\/\.]/g).test(item))
      } catch (e) {
        clienterr = "Error while reading zip content."
      }

      if (dirfiles) {
        switch (dirname) {
          case "homelink":
            filestoupload.push({
              fieldname: 'homelink',
              filename: dirfiles[0],
              path: `/tmp/banners/${file.filename}/zip/${dirname}/${dirfiles[0]}`
            })
            break;
          case "offerlink":
            filestoupload.push({
              fieldname: 'offerlink',
              filename: dirfiles[0],
              path: `/tmp/banners/${file.filename}/zip/${dirname}/${dirfiles[0]}`
            })
            break;
          case "htmllink":
            filestoupload.push({
              fieldname: 'htmllink',
              filename: dirfiles[0],
              path: `/tmp/banners/${file.filename}/zip/${dirname}/${dirfiles[0]}`
            })
            break;
          case "images":
            _.each(dirfiles, (dirfile) => {
              filestoupload.push({
                fieldname: 'images',
                filename: dirfile,
                path: `/tmp/banners/${file.filename}/zip/${dirname}/${dirfile}`
              })
            })
          default:
            break;
        }
        _callback(null)
      } else {
        _callback(clienterr)
      }

    }

    let updatebanner = (_callback) => {
      Content.updatebanner(incoming, filestoupload, (err, res) => {
        if (err) {
          syserr = err
          clienterr = "Failed. Try again."
        }
        _callback(clienterr)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadFiles = _callback => {
      let filestoupload = []

      if (files.homelink) filestoupload.push(files.homelink[0])
      if (files.offerlink) filestoupload.push(files.offerlink[0])
      if (files.htmllink) filestoupload.push(files.htmllink[0])

      async.each(filestoupload, uploadtos3, (err) => {
        if (err) {
          syserr = err
          clienterr = "Error while uploading files. Please try again."
        }
        _callback(clienterr)
      })
    }

    let uploadtos3 = (file, _callback) => {
      let filename = null

      if (file.fieldname === 'homelink') {
        filename = `banners/${incoming.id}/h-${file.originalname}`
      } else if (file.fieldname === 'offerlink') {
        filename = `banners/${incoming.id}/o-${file.originalname}`
      } else {
        filename = `banners/${incoming.id}/${file.originalname}`
      }

      fs.readFile(file.path, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME_CONTENT,
            Key: filename,
            Body: base64data,
            ContentType: file.mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, _err => {
            if (_err) {
              clienterr = 'Error occoured while uploading the file. Please try again.'
              syserr = _err
            }
            _callback(clienterr)
          })
        }
      })
    }

    let invalidatecache = _callback => {
      let cloudfront = new AWS.CloudFront();
      let items = ['/api/content/getbanners']
      let invalidations = items.map(encodeURI)
      let params = {
        DistributionId: process.env.AWS_CLOUDFRONT_DISTRIBUTION_ID,
        InvalidationBatch: {
          CallerReference: '' + +new Date(),
          Paths: {
            Quantity: invalidations.length,
            Items: invalidations
          }
        }
      }
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) {
          syserr = err
          log(syserr || err, true)
        }
      })

      _callback(null)
    }

    async.waterfall([formatincoming, validateincoming, unzip, getfiles, updatebanner, updateawsconfig, uploadFiles, invalidatecache], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: 'Banner updated successfully.' })
      }
    })
  }

  updatebannerposition(incoming, callback) {
    let clienterr, syserr

    let validateincoming = _callback => {
      if (!incoming.banners) {
        clienterr = syserr = 'Missing improtant data'
      }
      _callback(clienterr)
    }

    let updatepersistance = _callback => {
      async.each(incoming.banners, (banner, __callback) => {
        Content.updatebannerposition(banner, (_err, _res) => {
          __callback(_err)
        })
      }, (err) => {
        if (err) {
          clienterr = 'Error while updaating banner. Please try again.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION_CONTENT
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID_CONTENT,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_CONTENT
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let invalidatecache = _callback => {
      let cloudfront = new AWS.CloudFront();
      let items = ['/api/content/getbanners']
      let invalidations = items.map(encodeURI)
      let params = {
        DistributionId: process.env.AWS_CLOUDFRONT_DISTRIBUTION_ID,
        InvalidationBatch: {
          CallerReference: '' + +new Date(),
          Paths: {
            Quantity: invalidations.length,
            Items: invalidations
          }
        }
      }
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) {
          syserr = err
          log(syserr || err, true)
        }
      })

      _callback(null)
    }

    async.waterfall([validateincoming, updatepersistance, updateawsconfig, invalidatecache], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { message: 'Banner updated successfully.' } })
      }
    })
  }
}

module.exports = new ContentHelper()
