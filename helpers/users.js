const async = require('async')
const log = require('../sys/log')
const Users = require('../persistence/users')
const comms = require('../sys/comms')
const grpc = require('../sys/grpc')

class UsersHelper {
  create(incoming, callback) {
    let clienterr, syserr

    function validatedata(_callback) {
      let emailregex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
      if (incoming) {
        if (!incoming.loginid || `${incoming.loginid}`.trim().length < 2 || !isNaN(incoming.loginid))
          clienterr = syserr = 'Enter Valid LoginId'
        else if (!incoming.password || `${incoming.password}`.trim().length < 8)
          clienterr = syserr = `Please enter a valid 8 digit password.`
        else if (
          !incoming.firstname ||
          `${incoming.firstname}`.trim().length < 2 ||
          !isNaN(incoming.firstname) ||
          (!incoming.lastname || `${incoming.lastname}`.trim().length < 2 || !isNaN(incoming.lastname))
        )
          clienterr = syserr = `Enter Valid Name to create user`
        else if (!incoming.email || `${incoming.email}`.trim().length < 5 || !emailregex.test(incoming.email))
          clienterr = syserr = `Enter Valid Email to create user`
        else if (!incoming.mobile || !/^\d{10}$/.test(incoming.mobile))
          clienterr = syserr = `Enter Valid Mobile to create user`
        else if (
          incoming.adminactions === '' &&
          incoming.accountactions === '' &&
          incoming.customercare === '' &&
          incoming.offersactions === '' &&
          incoming.contentactions === '' &&
          incoming.commsactions === '' &&
          incoming.reporting === ''
        )
          clienterr = syserr = `Enter at least one action`
      } else clienterr = syserr = `Invalid request to create user`

      _callback(clienterr)
    }

    function checkexisting(_callback) {
      Users.getbymobile(incoming, (err, _exists) => {
        console.log(incoming)
        if (_exists) {
          console.log(incoming)
          if (incoming.mobile === _exists.mobile)
            clienterr = syserr = `Mobile number ${incoming.mobile} is already registered.`
          else if (incoming.loginid === _exists.loginid)
            clienterr = syserr = `LoginId ${incoming.loginid} is already registered.`
          else if (incoming.email === _exists.email)
            clienterr = syserr = `Email ${incoming.email} is already registered.`
        }
        _callback(clienterr)
      })
    }

    function createuser(_callback) {
      Users.create(incoming, err => {
        if (err) {
          syserr = err
          clienterr = `Error while creating an user. Please try again.`
        }
        _callback(clienterr)
      })
    }

    let publishevent = _callback => {
      let data = {
        firstname: incoming.firstname,
        loginid: incoming.loginid,
        password: incoming.password,
        email: incoming.email
      }
      let channelname = 'fpadmin.signup'
      log(`Called ${channelname}: ${JSON.stringify(data)}`)

      comms.publish(channelname, data)
      _callback(null)
    }

    async.waterfall([validatedata, checkexisting, createuser, publishevent], err => {
      if (err || syserr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: `User created successfully` })
      }
    })
  }

  get(callback) {
    Users.get((err, response) => {
      if (err) {
        log(err, true)
        callback({
          status: 'failed',
          message: 'Error while geting list of users'
        })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  update(incoming, callback) {
    let clienterr, syserr

    function validatedata(_callback) {
      let emailregex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
      if (incoming) {
        if (!incoming.id || `${incoming.id}`.trim() === '')
          clienterr = syserr = `Unable to authenticate user. Please try again.`
        else if (!incoming.loginid || `${incoming.loginid}`.trim().length < 2 || !isNaN(incoming.loginid))
          clienterr = syserr = 'Enter Valid LoginId'
        else if (
          !incoming.firstname ||
          `${incoming.firstname}`.trim().length < 2 ||
          !isNaN(incoming.firstname) ||
          (!incoming.lastname || `${incoming.lastname}`.trim().length < 2 || !isNaN(incoming.lastname))
        )
          clienterr = syserr = `Enter Valid Name to create user`
        else if (!incoming.email || `${incoming.email}`.trim().length < 5 || !emailregex.test(incoming.email))
          clienterr = syserr = `Enter Valid Email to create user`
        else if (!incoming.mobile || !/^\d{10}$/.test(incoming.mobile))
          clienterr = syserr = `Enter Valid Mobile to create user`
        else if (
          incoming.adminactions === '' &&
          incoming.accountactions === '' &&
          incoming.customercare === '' &&
          incoming.offersactions === '' &&
          incoming.contentactions === '' &&
          incoming.commsactions === '' &&
          incoming.reporting === ''
        )
          clienterr = syserr = `Enter at least one action`
      } else clienterr = syserr = `Invalid request to update user`

      _callback(clienterr)
    }

    function updateuser(_callback) {
      Users.update(incoming, err => {
        if (err) {
          syserr = err
          clienterr = `Error while updating the user. Please try again.`
        }
        _callback(clienterr)
      })
    }

    let publishevent = (_callback) => {
      let data = {
        firstname: incoming.firstname,
        loginid: incoming.loginid,
        password: incoming.password,
        email: incoming.email
      }
      let channelname = 'fpadmin.updatepassword'
      log(`Called ${channelname}: ${JSON.stringify(data)}`)

      comms.publish(channelname, data)
      _callback(null)
    }

    async.waterfall([validatedata, updateuser, publishevent], err => {
      if (err || syserr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: `User updated successfully` })
      }
    })
  }

  delete(incoming, callback) {
    let clienterr, syserr
    function validatedata(_callback) {
      if (incoming && incoming.id) _callback(null)
      else {
        clienterr = syserr = `Invalid request to delete user`
        _callback(clienterr)
      }
    }

    function deleteuser(_callback) {
      Users.delete(incoming, err => {
        if (err) {
          syserr = err
          clienterr = `Error while deleting the user. Please try again.`
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validatedata, deleteuser], err => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: `User deleted successfully` })
      }
    })
  }

  search(incoming, callback) {
    let clienterr, syserr
    function validatedata(_callback) {
      if (incoming.mobile && /^\d{10}$/.test(incoming.mobile)) _callback(null)
      else {
        clienterr = syserr = `Invalid mobile`
        _callback(clienterr)
      }
    }

    function searchuser(_callback) {
      Users.search(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching user details. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validatedata, searchuser], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }
}

module.exports = new UsersHelper()
