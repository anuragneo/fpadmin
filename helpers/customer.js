const async = require('async')
const log = require('../sys/log')
const comms = require('../sys/comms')
const grpc = require('../sys/grpc')

class CustomerHelper {
  getpaybackbalance(incoming, callback) {
    let clienterr, syserr
    let payback = null

    let validateincoming = _callback => {
      if (!incoming.fpaccountid) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let paybackbalance = _callback => {
      comms.request('payback/getbalanceforadmin', incoming, grpc.getMetaData(), (err, response) => {
        if (err) {
          clienterr = syserr = err.details
        } else {
          payback = response.payback
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, paybackbalance], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', data: { message: clienterr } })
      } else {
        callback({ status: 'success', data: payback })
      }
    })
  }

  getbbpcbalance(incoming, callback) {
    let clienterr, syserr
    let bbpc = null

    let validateincoming = _callback => {
      if (!incoming.fpaccountid || !incoming.mobile) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let bbpcbalance = _callback => {
      comms.request('bbpc/getbalanceforadmin', incoming, grpc.getMetaData(), (err, response) => {
        if (err) {
          clienterr = syserr = err.details
        } else {
          bbpc = response
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, bbpcbalance], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', data: { message: clienterr } })
      } else {
        callback({ status: 'success', data: bbpc })
      }
    })
  }

  getbbpctransactions(incoming, callback) {
    let clienterr, syserr
    let bbpc = null

    let validateincoming = _callback => {
      if (!incoming.mobile) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let bbpctransaction = _callback => {
      const metadata = grpc.getMetaData()
      if (process.env.NODE_ENV === 'development') {
        metadata.set('eastwest', 'true')
      }
      comms.request('bbpc/gettransactions', incoming, metadata, (err, response) => {
        if (err) {
          clienterr = syserr = err.details
        } else {
          bbpc = response
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, bbpctransaction], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', data: { message: clienterr } })
      } else {
        callback({ status: 'success', data: bbpc })
      }
    })
  }

  getprepaidbalance(incoming, callback) {
    let clienterr, syserr
    let prepaids = null

    let validateincoming = _callback => {
      if (!incoming.mobile || !incoming.managerid) {
        clienterr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let prepaidbalances = _callback => {
      comms.request('prepaid/getbalances', incoming, grpc.getMetaData(), (err, response) => {
        if (err) {
          clienterr = syserr = err.details
        } else {
          prepaids = response.details
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, prepaidbalances], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', data: { message: clienterr } })
      } else {
        callback({ status: 'success', data: { prepaids: prepaids, limit: process.env.MONTHLY_WALLET_LIMIT } })
      }
    })
  }
}

module.exports = new CustomerHelper()
