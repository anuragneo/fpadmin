const async = require('async')
const log = require('../sys/log')
const XLSX = require('xlsx')
const fs = require('fs')
const AWS = require('aws-sdk')
const Adminops = require('../persistence/adminops')
const User = require('../persistence/users')
const comms = require('../sys/comms')
const grpc = require('../sys/grpc')
const path = require('path')

class AccountHelper {
  getadminops(incoming, callback) {
    let clienterr, syserr
    let adminops = null

    let validateincoming = _callback => {
      if (!incoming.managerid) {
        clienterr = syserr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let getadminops = _callback => {
      Adminops.getadminops(incoming, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else {
          adminops = res
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, getadminops], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: adminops })
      }
    })
  }

  getadminopsformaker(incoming, callback) {
    let clienterr, syserr
    let adminops = null

    let validateincoming = _callback => {
      if (!incoming.managerid) {
        clienterr = syserr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let getadminops = _callback => {
      Adminops.getadminopsformaker(incoming, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else {
          adminops = res
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, getadminops], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: adminops })
      }
    })
  }

  approvetopup(incoming, callback) {
    let clienterr, syserr
    let adminop = null

    let validateincoming = _callback => {
      if (!incoming.managerid || !incoming.id) {
        clienterr = syserr = 'Missing improtant data'
      } else if (!incoming.otp && isNaN(incoming.otp)) {
        clienterr = syserr = 'Invalid otp'
      }
      _callback(clienterr)
    }

    let getmanagerbyid = _callback => {
      User.getbyid(incoming.managerid, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else if (res && res.length < 1) {
          clienterr = syserr = 'User not found.'
        } else if (res[0].status === process.env.ACCOUNT_BLOCKED) {
          clienterr = syserr = 'Your account is blocked. Please contact customer care.'
        } else {
          incoming.mobile = res[0].mobile
        }
        _callback(clienterr)
      })
    }

    let verifyotp = _callback => {
      this.verifyotp(incoming, response => {
        if (response.status === 'failed') {
          clienterr = syserr = response.message
        }
        _callback(clienterr)
      })
    }

    let getadminopsbyid = _callback => {
      Adminops.getadminopsbyid(incoming.id, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else if (res && res.length > 0) {
          adminop = res[0]
        } else {
          clienterr = syserr = 'Invalid request id'
        }
        _callback(clienterr)
      })
    }

    let sendrequest = _callback => {
      let tosend = {
        managerid: incoming.managerid
      }

      comms.request('customers/topup', tosend, grpc.getMetaData(), (err, response) => {
        if (err) {
          clienterr = syserr = err.details
        }
        _callback(clienterr)
      })
    }

    let updatepersistance = _callback => {
      let status = 'approved'
      let comments = incoming.comments ? incoming.comments : ''
      let checkerid = incoming.managerid

      Adminops.updateadminops(incoming.id, checkerid, status, comments, (err, res) => {
        if (err) {
          clienterr = 'Error while updating record.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, getmanagerbyid, verifyotp, getadminopsbyid, updatepersistance], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { message: 'Credit has been approved successfully.' } })
      }
    })
  }

  rejecttopup(incoming, callback) {
    let clienterr, syserr

    let validateincoming = _callback => {
      if (!incoming.managerid || !incoming.id) {
        clienterr = syserr = 'Missing improtant data'
      }
      _callback(clienterr)
    }

    let updatepersistance = _callback => {
      let status = 'rejected'
      let comments = incoming.comments ? incoming.comments : ''
      let checkerid = incoming.managerid

      Adminops.updateadminops(incoming.id, checkerid, status, comments, (err, res) => {
        if (err) {
          clienterr = 'Error while updating record.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, updatepersistance], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { message: 'Credit has been rejected successfully.' } })
      }
    })
  }

  topup(incoming, callback) {
    let clienterr, syserr

    let validateincoming = _callback => {
      if (!incoming.managerid || !incoming.mobile) {
        clienterr = 'Missing improtant data'
      } else if (!incoming.amount) {
        clienterr = 'Invalid amount'
      } else if (!incoming.walletid) {
        clienterr = 'Invalid or missing wallet id'
      }
      _callback(clienterr)
    }

    let topup = _callback => {
      incoming.status = 'pending'
      incoming.makerid = incoming.managerid
      incoming.comments = ''
      incoming.checkerid = null

      Adminops.createadminops(incoming, (err, res) => {
        if (err) {
          clienterr = 'Error while creating record.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, topup], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success' })
      }
    })
  }

  rejectbulkops(incoming, callback) {
    let clienterr, syserr

    let validateincoming = _callback => {
      if (!incoming.managerid || !incoming.id) {
        clienterr = syserr = 'Missing improtant data'
      }
      _callback(clienterr)
    }

    let updatepersistance = _callback => {
      let status = 'rejected'
      let comments = incoming.comments ? incoming.comments : ''
      let checkerid = incoming.managerid

      Adminops.updatebulkadminops(incoming.id, checkerid, status, comments, (err, res) => {
        if (err) {
          clienterr = 'Error while updating record.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, updatepersistance], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { message: 'File has been rejected successfully.' } })
      }
    })
  }

  approvebulkops(incoming, callback) {
    let clienterr, syserr
    let bulkadminop = null

    let validateincoming = _callback => {
      if (!incoming.managerid || !incoming.id) {
        clienterr = syserr = 'Missing improtant data'
      } else if (!incoming.otp && isNaN(incoming.otp)) {
        clienterr = syserr = 'Invalid otp'
      }
      _callback(clienterr)
    }

    let getmanagerbyid = _callback => {
      User.getbyid(incoming.managerid, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else if (res && res.length < 1) {
          clienterr = syserr = 'User not found.'
        } else if (res[0].status === process.env.ACCOUNT_BLOCKED) {
          clienterr = syserr = 'Your account is blocked. Please contact customer care.'
        } else {
          incoming.mobile = res[0].mobile
        }
        _callback(clienterr)
      })
    }

    let verifyotp = _callback => {
      this.verifyotp(incoming, response => {
        if (response.status === 'failed') {
          clienterr = syserr = response.message
        }
        _callback(clienterr)
      })
    }

    let getbulkadminopsbyid = _callback => {
      Adminops.getbulkadminopsbyid(incoming.id, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else if (res && res.length > 0) {
          bulkadminop = res[0]
        } else {
          clienterr = syserr = 'Invalid request id'
        }
        _callback(clienterr)
      })
    }

    let sendrequest = _callback => {
      let tosend = {
        managerid: incoming.managerid,
        filename: bulkadminop.filename,
        url: `${process.env.AWS_BUCKET_URL}/${bulkadminop.filename}`
      }

      comms.request('customers/approvebulkops', tosend, grpc.getMetaData(), (err, response) => {
        if (err) {
          clienterr = syserr = err.details
        }
        _callback(clienterr)
      })
    }

    let updatepersistance = _callback => {
      let status = 'approved'
      let comments = incoming.comments ? incoming.comments : ''
      let checkerid = incoming.managerid

      Adminops.updatebulkadminops(incoming.id, checkerid, status, comments, (err, res) => {
        if (err) {
          clienterr = 'Error while updating record.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, getmanagerbyid, verifyotp, getbulkadminopsbyid, updatepersistance], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { message: 'File has been approved successfully.' } })
      }
    })
  }

  verifyotp(incoming, callback) {
    let clienterr, syserr
    let platforms = ['android', 'ios', 'web']
    let otptypes = [process.env.OTP_TYPE_VERIFY_BULK_UPLOAD, process.env.OTP_TYPE_VERIFY_TOPUP]

    let validateincoming = _callback => {
      if (!incoming.mobile || `${incoming.mobile}`.trim().length !== 10 || !/^\d{10}$/.test(incoming.mobile))
        clienterr = syserr = `Invalid mobile number to verify OTP.`
      else if (!incoming.otptype || isNaN(incoming.otptype) || otptypes.indexOf(`${incoming.otptype}`) < 0)
        clienterr = syserr = `Invalid OTP operation initiated.`
      else if (!incoming.authtoken || `${incoming.authtoken}`.trim().length !== 16) {
        clienterr = `OTP verification failed. Please try again.`
        syserr = `Missing token to verify OTP.`
      } else if (!incoming.otp || `${incoming.otp}`.trim().length !== 6 || isNaN(incoming.otp))
        clienterr = syserr = `Please enter valid OTP which was sent on your mobile number.`
      else if (!incoming.platform || platforms.indexOf(incoming.platform) < 0)
        clienterr = syserr = `Invalid platform for OTP verification.`
      else if (incoming.platform === 'web') incoming.deviceid = incoming.authtoken

      _callback(clienterr)
    }

    let verify = _callback => {
      User.verifyotp(incoming, (err, isvalid) => {
        if (err) {
          clienterr = `Error while verifying OTP. Please try again.`
          syserr = err
        } else if (isvalid === null || typeof isvalid === 'undefined')
          clienterr = syserr = `Your OTP seems to have expired. Please request again.`
        else if (!isvalid) clienterr = syserr = `OTP verification failed. Please enter valid OTP received.`
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, verify], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { message: 'OTP verification successful.' } })
      }
    })
  }

  generateotp(incoming, callback) {
    let clienterr, syserr
    let platforms = ['android', 'ios', 'web']
    let otptypes = [process.env.OTP_TYPE_VERIFY_BULK_UPLOAD, process.env.OTP_TYPE_VERIFY_TOPUP]
    let authtoken = null

    let validateincoming = _callback => {
      if (!incoming.managerid) {
        clienterr = syserr = 'Missing important data'
      } else if (!incoming.platform || platforms.indexOf(incoming.platform) < 0) {
        clienterr = syserr = `Invalid platform for sending OTP.`
      } else if (!incoming.otptype || isNaN(incoming.otptype) || otptypes.indexOf(`${incoming.otptype}`) < 0) {
        clienterr = syserr = `Invalid OTP operation initiated.`
      }
      _callback(clienterr)
    }

    let getmanagerbyid = _callback => {
      User.getbyid(incoming.managerid, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else if (res && res.length < 1) {
          clienterr = syserr = 'User not found.'
        } else if (res[0].status === process.env.ACCOUNT_BLOCKED) {
          clienterr = syserr = 'Your account is blocked. Please contact customer care.'
        } else {
          incoming.mobile = res[0].mobile
        }
        _callback(clienterr)
      })
    }

    let setotp = _callback => {
      let otp = null

      User.setotp(incoming, (err, res) => {
        if (err) {
          clienterr = 'Error while generating OTP.'
          syserr = err
        } else {
          authtoken = res.authtoken
          otp = res.otp
        }
        _callback(clienterr, otp)
      })
    }

    let publishevent = (otp, _callback) => {
      let data = {
        mobile: incoming.mobile,
        otp
      }
      let channelname = ''

      if (incoming.otptype === Number(process.env.OTP_TYPE_VERIFY_BULK_UPLOAD))
        channelname = 'comms.otp.verifybulkupload'
      else if (incoming.otptype === Number(process.env.OTP_TYPE_VERIFY_TOPUP)) channelname = 'comms.otp.verifytopup'
      log(`Called getOTP ${channelname}: ${JSON.stringify(data)}`)

      comms.publish(channelname, data)
      _callback(null)
    }

    async.waterfall([validateincoming, getmanagerbyid, setotp, publishevent], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: { authtoken } })
      }
    })
  }

  getbulkops(incoming, callback) {
    let clienterr, syserr
    let bulkops = null

    let validateincoming = _callback => {
      if (!incoming.managerid) {
        clienterr = syserr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let getbulkops = _callback => {
      Adminops.getbulkops(incoming, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else {
          bulkops = res
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, getbulkops], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: bulkops })
      }
    })
  }

  getbulkopsformaker(incoming, callback) {
    let clienterr, syserr
    let bulkops = null

    let validateincoming = _callback => {
      if (!incoming.managerid) {
        clienterr = syserr = 'Missing important data'
      }
      _callback(clienterr)
    }

    let getbulkops = _callback => {
      Adminops.getbulkopsformaker(incoming, (err, res) => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        } else {
          bulkops = res
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validateincoming, getbulkops], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: bulkops })
      }
    })
  }

  bulkupload(_incoming, file, ip, callback) {
    let clienterr, syserr, incoming
    let rowscount = 0

    let formatincoming = _callback => {
      try {
        incoming = JSON.parse(_incoming)
      } catch (_error) {
        clienterr = 'Invalid request.'
        syserr = 'Invalid JSON ' + _incoming
      } finally {
        _callback(clienterr)
      }
    }

    let validateincoming = _callback => {
      if (!file) {
        clienterr = syserr = 'Please upload a valid file to proceed.'
      } else if (!incoming.managerid) {
        clienterr = syserr = 'Missing important data.'
      }
      _callback(clienterr)
    }

    let parsefile = _callback => {
      let workbook = XLSX.readFile(file.path).Sheets.Sheet1
      let parsedfile = XLSX.utils.sheet_to_json(workbook, { header: 1, defval: null, blankrows: false })
      let parseddata = parsedfile.slice(1)
      parseddata = parseddata.filter(e => e.length)
      rowscount = parseddata.length
      if (rowscount <= 0) {
        clienterr = syserr = 'Invalid or empty file.'
      }
      _callback(clienterr)
    }

    let updateawsconfig = _callback => {
      AWS.config.region = process.env.AWS_REGION
      let aws_config = {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
      }
      AWS.config.update(aws_config)
      _callback(null)
    }

    let uploadtos3 = _callback => {
      fs.readFile(file.path, (err, data) => {
        if (err) {
          clienterr = syserr = 'Error while reading file.'
          _callback(clienterr)
        } else {
          let s3 = new AWS.S3()
          let base64data = new Buffer(data, 'binary')
          let ext = path.extname(file.originalname)
          let key = file.filename + ext
          let aws_data_dict = {
            Bucket: process.env.AWS_BUCKET_NAME,
            Key: key,
            Body: base64data,
            ContentType: file.mimetype,
            ACL: process.env.AWS_ACL
          }

          s3.putObject(aws_data_dict, _err => {
            if (_err) {
              clienterr = 'Error occoured while uploading the file. Please try again.'
              syserr = _err
            }
            _callback(clienterr)
          })
        }
      })
    }

    let addtomysql = _callback => {
      let data = {
        filename: file.filename,
        originalname: file.originalname,
        recordscount: rowscount,
        makerid: incoming.managerid,
        ip: ip,
        status: 'pending',
        checkerid: null,
        comments: ''
      }

      Adminops.createbulkadminops(data, err => {
        if (err) {
          clienterr = 'Failed. Try again.'
          syserr = err
        }
        _callback(clienterr)
      })
    }

    async.waterfall([formatincoming, validateincoming, parsefile, updateawsconfig, uploadtos3, addtomysql], err => {
      if (err || clienterr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: 'File has been uploaded successfully.' })
      }
    })
  }
}

module.exports = new AccountHelper()
