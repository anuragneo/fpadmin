const async = require('async')
const log = require('../sys/log')
const Channel = require('../persistence/channel')

class ChannelHelper {
  create(incoming, callback) {
    let clienterr, syserr

    function validatedata(_callback) {
      if (incoming) {
        if (!incoming.name || `${incoming.name}`.trim().length < 2) clienterr = syserr = `Enter Valid Channel Type`
        else if (!incoming.source || `${incoming.source}`.trim().length < 2) clienterr = syserr = `Enter Valid Source`
        else if (!incoming.description || `${incoming.description}`.trim().length < 2)
          clienterr = syserr = `Enter Valid Description`
        else if (!incoming.businessformat || `${incoming.businessformat}`.trim().length < 1)
          clienterr = syserr = `Enter Valid Business Format`
      } else clienterr = syserr = `Invalid request to create channel`
      _callback(clienterr)
    }

    let getchannelbyname = (_callback) => {
      Channel.getbyname(incoming.name, (err, res) => {
        if (err) {
          clienterr = "Failed. try again."
          syserr = err
        } else if (res && res.length > 0) {
          clienterr = "Channel exists with the given name."
        }
        _callback(clienterr)
      })
    }

    function createchannel(_callback) {
      Channel.create(incoming, err => {
        if (err) {
          syserr = err
          clienterr = `Error while creating a channel. Please try again.`
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validatedata, getchannelbyname, createchannel], err => {
      if (err || syserr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: `Channel created successfully` })
      }
    })
  }

  get(callback) {
    Channel.get((err, response) => {
      if (err) {
        log(err, true)
        callback({
          status: 'failed',
          message: 'Error while geting list of users'
        })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }

  update(incoming, callback) {
    let clienterr, syserr

    function validatedata(_callback) {
      if (incoming) {
        if (!incoming.id || `${incoming.id}`.trim() === '')
          clienterr = syserr = `Unable to authenticate channel. Please try again.`
        else if (!incoming.name || `${incoming.name}`.trim().length < 2) clienterr = syserr = `Enter Valid Channel Type`
        else if (!incoming.source || `${incoming.source}`.trim().length < 2) clienterr = syserr = `Enter Valid Source`
        else if (!incoming.description || `${incoming.description}`.trim().length < 2)
          clienterr = syserr = `Enter Valid Description`
        else if (!incoming.businessformat || `${incoming.businessformat}`.trim().length < 1)
          clienterr = syserr = `Enter Valid Business Format`
      } else clienterr = syserr = `Invalid request to create channel`
      _callback(clienterr)
    }

    function updatechannel(_callback) {
      Channel.update(incoming, err => {
        if (err) {
          syserr = err
          clienterr = `Error while updating the Channel. Please try again.`
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validatedata, updatechannel], err => {
      if (err || syserr) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: `Channel updated successfully` })
      }
    })
  }

  delete(incoming, callback) {
    let clienterr, syserr
    function validatedata(_callback) {
      if (incoming && incoming.id) _callback(null)
      else {
        clienterr = syserr = `Invalid request to delete channel`
        _callback(clienterr)
      }
    }

    function deletechannel(_callback) {
      Channel.delete(incoming, err => {
        if (err) {
          syserr = err
          clienterr = `Error while deleting the channel. Please try again.`
        }
        _callback(clienterr)
      })
    }

    async.waterfall([validatedata, deletechannel], err => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', message: `Channel deleted successfully` })
      }
    })
  }

  search(incoming, callback) {
    let clienterr, syserr
    function validatedata(_callback) {
      if (incoming && incoming.name) _callback(null)
      else {
        clienterr = syserr = `Invalid data`
        _callback(clienterr)
      }
    }

    function searchchannel(_callback) {
      Channel.search(incoming, (err, response) => {
        if (err) {
          syserr = err
          clienterr = `Error fetching channel details. Please try again.`
        }
        _callback(clienterr, response)
      })
    }

    async.waterfall([validatedata, searchchannel], (err, response) => {
      if (err) {
        log(syserr || err, true)
        callback({ status: 'failed', message: clienterr })
      } else {
        callback({ status: 'success', data: response })
      }
    })
  }
}

module.exports = new ChannelHelper()
