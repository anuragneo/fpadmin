import axios from 'axios'
import * as types from './types'

const url = process.env.PUBLIC_URL

export function reset() {
  return {
    type: types.RESET
  }
}

export const login = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/login`, req)
  dispatch({ type: types.LOGIN, payload: res.data })
  callback()
}

export const getProfile = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/getprofile`, req)
  dispatch({ type: types.GET_PROFILE, payload: res.data })
  callback()
}

export const getPrepaidBalance = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/getprepaidbalance`, req)
  dispatch({ type: types.GET_PREPAID_BALANCE, payload: res.data })
}

export const getPaybackBalance = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/getpaybackbalance`, req)
  dispatch({ type: types.GET_PAYBACK_BALANCE, payload: res.data })
}

export const getBbpcBalance = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/getbbpcbalance`, req)
  dispatch({ type: types.GET_BBPC_BALANCE, payload: res.data })
}

export const getTransactions = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/transactions`, req)
  dispatch({ type: types.GET_TRANSACTIONS, payload: res.data })
}

export const getCommunications = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/communications`, req)
  dispatch({ type: types.GET_COMMUNICATIONS, payload: res.data })
}

export const getFeedbacks = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/feedbacks`, req)
  dispatch({ type: types.GET_FEEDBACK, payload: res.data })
}

export const getLoyalty = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/loyalty`, req)
  dispatch({ type: types.GET_LOYALTY, payload: res.data })
}

export const getPricematch = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/pricematch`, req)
  dispatch({ type: types.GET_PRICE_MATCH, payload: res.data })
}

export const getAdminUsers = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users`, req)
  dispatch({ type: types.ADMIN_USERS, payload: res.data })
}

export const createAdminUsers = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users/create`, req)
  dispatch({ type: types.USERS_CREATE, payload: res.data })
  callback()
}

export const updateAdminUsers = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users/update`, req)
  dispatch({ type: types.USERS_UPDATE, payload: res.data })
  callback()
}

export const removeAdminUsers = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users/delete`, req)
  dispatch({ type: types.USERS_REMOVE, payload: res.data })
  callback()
}

export const searchUser = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users/search`, req)
  dispatch({ type: types.ADMIN_USERS, payload: res.data })
}

export const getAdminChannel = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel`, req)
  dispatch({ type: types.ADMIN_CHANNEL, payload: res.data })
}

export const createAdminChannel = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel/create`, req)
  dispatch({ type: types.CHANNEL_CREATE, payload: res.data })
  callback()
}

export const updateAdminChannel = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel/update`, req)
  dispatch({ type: types.CHANNEL_UPDATE, payload: res.data })
  callback()
}

export const removeAdminChannel = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel/delete`, req)
  dispatch({ type: types.CHANNEL_REMOVE, payload: res.data })
  callback()
}

export const searchChannel = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel/search`, req)
  dispatch({ type: types.ADMIN_CHANNEL, payload: res.data })
}

export const getAdminWallet = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/wallet`, req)
  dispatch({ type: types.ADMIN_WALLET, payload: res.data })
}

export const createAdminWallet = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/wallet/create`, req)
  dispatch({ type: types.WALLET_CREATE, payload: res.data })
  callback()
}

export const updateAdminWallet = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/wallet/update`, req)
  dispatch({ type: types.WALLET_UPDATE, payload: res.data })
  callback()
}

export const searchWallet = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/wallet/search`, req)
  dispatch({ type: types.ADMIN_WALLET, payload: res.data })
}

export const getBlockUnblockCustomer = req => async dispatch => {
  const res = await axios.post(`${url}/api/account/getcustomer`, req)
  dispatch({ type: types.GET_BLOCK_UNBLOCK_CUSTOMER, payload: res.data })
}

export const getCreditCustomer = req => async dispatch => {
  const res = await axios.post(`${url}/api/account/getcustomer`, req)
  dispatch({ type: types.GET_CREDIT_CUSTOMER, payload: res.data })
}

export const blockCustomer = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/blockcustomer`, req)
  dispatch({ type: types.CUSTOMER_BLOCK, payload: res.data })
  callback()
}

export const unblockCustomer = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/activatecustomer`, req)
  dispatch({ type: types.CUSTOMER_UNBLOCK, payload: res.data })
  callback()
}

export const doCustomerTopUp = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/topup`, req)
  dispatch({ type: types.CUSTOMER_TOPUP, payload: res.data })
  callback()
}

export const doBulkCredit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/bulkupload`, req)
  dispatch({ type: types.BULK_CREDIT, payload: res.data })
  callback()
}

export const getBulkAdminOps = req => async dispatch => {
  const res = await axios.post(`${url}/api/account/getbulkadminops`, req)
  dispatch({ type: types.GET_BULK_ADMIN_OPS, payload: res.data })
}

export const getBulkAdminOpsMaker = req => async dispatch => {
  const res = await axios.post(`${url}/api/account/getbulkadminopsformaker`, req)
  dispatch({ type: types.GET_BULK_ADMIN_OPS_MAKER, payload: res.data })
}

export const getAdminOps = req => async dispatch => {
  const res = await axios.post(`${url}/api/account/getadminops`, req)
  dispatch({ type: types.GET_ADMIN_OPS, payload: res.data })
}

export const getAdminOpsMaker = req => async dispatch => {
  const res = await axios.post(`${url}/api/account/getadminopsformaker`, req)
  dispatch({ type: types.GET_ADMIN_OPS_MAKER, payload: res.data })
}

export const generateBulkCreditOTP = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/generateotp`, req)
  dispatch({ type: types.GENERATE_BULK_CREDIT_OTP, payload: res.data })
  callback()
}

export const generateManualCreditOTP = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/generateotp`, req)
  dispatch({ type: types.GENERATE_MANUAL_CREDIT_OTP, payload: res.data })
  callback()
}

export const approveBulkCredit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/approvebulkadminops`, req)
  dispatch({ type: types.APPROVE_BULK_CREDIT, payload: res.data })
  callback()
}

export const rejectBulkCredit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/rejectbulkadminops`, req)
  dispatch({ type: types.REJECT_BULK_CREDIT, payload: res.data })
  callback()
}

export const approveManualCredit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/approvetopup`, req)
  dispatch({ type: types.APPROVE_MANUAL_CREDIT, payload: res.data })
  callback()
}

export const rejectManualCredit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/account/rejecttopup`, req)
  dispatch({ type: types.REJECT_MANUAL_CREDIT, payload: res.data })
  callback()
}

export const commsList = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/list`, req)
  dispatch({ type: types.COMMS_LIST, payload: res.data })
}

export const commsSearch = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/search`, req)
  dispatch({ type: types.COMMS_LIST, payload: res.data })
}

export const getBanners = req => async dispatch => {
  const res = await axios.post(`${url}/api/content/getbanners`, req)
  dispatch({ type: types.GET_BANNERS, payload: res.data })
}

export const bannerSearch = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/content/getbanners`, req)
  dispatch({ type: types.GET_BANNERS, payload: res.data })
}
