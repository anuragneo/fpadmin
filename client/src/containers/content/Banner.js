import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Grid, Col, Row, Button, Glyphicon, Modal } from 'react-bootstrap'
import NewBanner from './NewBanner'
import Search from '../Search'
import BannerItem from './BannerItem'
import { getAdmin } from '../../helpers/app'

class Banner extends Component {
  constructor(props) {
    super(props)
    this.contentAccess = []
    this.state = {
      newBanner: false,
      showModal: false,
      modalText: '',
      updateDisable: true,
      cancelDisable: true
    }
  }

  componentDidMount() {
    let admin = getAdmin()
    this.contentAccess = admin.contentaccess.split('')
    this.props.getBanners()
  }

  handleClose = () => this.setState({ showModal: false })
  newBannerClose = () => {
    this.setState({
      newBanner: false
    })
  }

  showNewBanner = () => {
    this.setState({
      newBanner: true
    })
  }

  componentDidUpdate(prevProps) {
    if (this.props.contentReducer !== prevProps.contentReducer) {
      if (this.props.contentReducer.getBannersData && this.props.contentReducer.getBannersData.length > 1) {
        this.setState({
          updateDisable: false,
          cancelDisable: false
        })
      } else {
        this.setState({
          updateDisable: true,
          cancelDisable: true
        })
      }
      if (
        this.props.contentReducer.createBannerStatus &&
        this.props.contentReducer.createBannerStatus === 'success' &&
        this.state.newBanner
      )
        this.setState({
          showModal: true,
          modalText: this.props.contentReducer.createBannerMessage,
          newBanner: false
        })
    }
  }

  updatebanners = event => {
    event.preventDefault()
    this.props.updateBannerPosition(
      {
        banners: this.props.contentReducer.getBannersData
      },
      () => {
        if (this.props.contentReducer.updateBannerPositionStatus === 'success') {
          this.setState({
            showModal: true,
            modalText: `Banner position updated successfully.`,
            removeBannerScreen: false
          })

          this.props.getBanners()
        }
      }
    )
  }

  reset = event => {
    this.props.getBanners()
  }

  handleClose = () => this.setState({ showModal: false })

  showBanners() {
    if (this.props.contentReducer.getBannersStatus === 'success') {
      if (this.props.contentReducer.getBannersData.length > 0) {
        return this.props.contentReducer.getBannersData.map((banner, index) => (
          <Col key={index} xs={4} className="form-group">
            <BannerItem
              contentAccess={this.contentAccess}
              index={index}
              banner={banner}
              position={this.props.contentReducer.changeIndexData ? this.props.contentReducer.changeIndexData : null}
            />
          </Col>
        ))
      } else {
        return (
          <Col xs={12}>
            <div className="nobanner">No Banner Found</div>
          </Col>
        )
      }
    }
  }

  render() {
    return (
      <Col xs={10} className="scroll">
        <Grid fluid>
          <Row className="form-group">
            <form>
              <Search type="banner" placeHolderText="Enter text" />
              <Col xs={2} className="form-group">
                <Button
                  style={{ display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block' }}
                  type="button"
                  bsStyle="secondary"
                  onClick={this.showNewBanner}
                >
                  <Glyphicon glyph="glyphicon glyphicon-plus" /> ADD NEW
                </Button>
              </Col>
            </form>
          </Row>
          <Row className="form-group">{this.showBanners()}</Row>
          <Row style={{ display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block' }}>
            <Col xs={3} xsOffset={3}>
              <Button type="button" disabled={this.state.cancelDisable} bsStyle="secondary" onClick={this.reset}>
                CANCEL
              </Button>
            </Col>

            <Col xs={3}>
              <Button type="button" disabled={this.state.updateDisable} bsStyle="primary" onClick={this.updatebanners}>
                UPDATE
              </Button>
            </Col>
          </Row>
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
          <NewBanner show={this.state.newBanner} onHide={this.newBannerClose} />

          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
        </Grid>
      </Col>
    )
  }
}

function mapStateToProps({ contentReducer }) {
  return { contentReducer }
}

export default connect(
  mapStateToProps,
  actions
)(Banner)
