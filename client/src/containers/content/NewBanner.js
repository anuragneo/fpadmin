import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Glyphicon, Row, Col, FormGroup, FormControl, Button, ControlLabel, Modal } from 'react-bootstrap'
import HomePreview from './BannerPreview'
import HtmlPreview from './HtmlPreview'
import OfferPreview from './OfferPreview'

class NewBanner extends Component {
  constructor(props) {
    super(props)
    this.homePageFileRef = React.createRef()
    this.offerPageFileRef = React.createRef()
    this.htmlLinkFileRef = React.createRef()
    this.state = {
      bannerName: '',
      bannerId: '',
      businessFormat: '1',
      campaignId: '',
      tags: '',
      file: null,
      fileInputKey: '',
      filename: '',
      showModal: false,
      modalText: '',
      showHomePreview: false,
      showOfferPreview: false,
      showHtmlPreview: false,
      qaDisabled: false,
      homePreviewData: {},
      offerPreviewData: {},
      htmlPreviewData: {}
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show !== nextProps.show)
      this.setState({
        bannerName: '',
        bannerId: '',
        businessFormat: '1',
        campaignId: '',
        tags: '',
        file: null,
        fileInputKey: Math.random().toString(36),
        filename: '',
        showModal: false,
        modalText: '',
        showHomePreview: false,
        showOfferPreview: false,
        showHtmlPreview: false,
        qaDisabled: false,
        homePreviewData: {},
        offerPreviewData: {},
        htmlPreviewData: {}
      })
  }

  onChange = e => {
    switch (e.target.name) {
      case 'bannername':
        this.setState({ bannerName: e.target.value })
        break
      case 'bannerid':
        this.setState({ bannerId: e.target.value })
        break
      case 'businessfromat':
        this.setState({ businessFormat: e.target.value })
        break
      case 'campaignid':
        this.setState({ campaignId: e.target.value })
        break
      case 'tags':
        this.setState({ tags: e.target.value })
        break
      case 'filename':
        this.setState({ filename: e.target.value })
        break
      case 'file':
        this.setState({ file: e.target.files[0], filename: e.target.files[0].name })
        break
      default:
        break
    }
  }

  hideHomePreview = () => {
    this.setState({ showHomePreview: false })
  }

  hideOfferPreview = () => {
    this.setState({ showOfferPreview: false })
  }

  hideHtmlPreview = () => {
    this.setState({ showHtmlPreview: false })
  }

  openHomePreview = () => {
    this.setState({ showHomePreview: true })
  }

  openOfferPreview = () => {
    this.setState({ showOfferPreview: true })
  }

  openHtmlPreview = () => {
    this.setState({ showHtmlPreview: true })
  }

  handleClose = () => this.setState({ showModal: false })

  showFile = type => {
    if (type === 'home') this.homePageFileRef.current.click()
    else if (type === 'offer') this.offerPageFileRef.current.click()
    else if (type === 'html') this.htmlLinkFileRef.current.click()
  }

  saveForQA = event => {
    event.preventDefault()
    if (this.state.screenName === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Screen Name'
      })
    } else if (this.state.screenId === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Screen Id'
      })
    } else if (this.state.homePageFile === null) {
      this.setState({
        showModal: true,
        modalText: 'Select a file for home page'
      })
    } else if (this.state.offerPageFile === null) {
      this.setState({
        showModal: true,
        modalText: 'Select a file for offer page'
      })
    } else if (this.state.htmlLinkFile === null) {
      this.setState({
        showModal: true,
        modalText: 'Select a file for html link'
      })
    } else {
      const data = {
        name: this.state.bannerName,
        seqnumber: this.state.bannerId,
        formatid: this.state.businessFormat,
        campaignid: this.state.campaignId === '' ? undefined : this.state.campaignId,
        tags: this.state.tags,
        isactive: 'N'
      }

      let formData = new FormData()
      formData.append('data', JSON.stringify(data))
      formData.append('file', this.state.file)

      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }

      this.props.createBanner(formData, config, () => {
        if (this.props.contentReducer.createBannerStatus === 'success') {
          this.props.getBanners()
        } else if (this.props.contentReducer.createBannerStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.contentReducer.createBannerMessage
          })
        }
      })
    }
  }

  addBanner = event => {
    event.preventDefault()
    if (this.state.screenName === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Screen Name'
      })
    } else if (this.state.screenId === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Screen Id'
      })
    } else if (this.state.homePageFile === null) {
      this.setState({
        showModal: true,
        modalText: 'Select a file for home page'
      })
    } else if (this.state.offerPageFile === null) {
      this.setState({
        showModal: true,
        modalText: 'Select a file for offer page'
      })
    } else if (this.state.htmlLinkFile === null) {
      this.setState({
        showModal: true,
        modalText: 'Select a file for html link'
      })
    } else {
      const data = {
        name: this.state.bannerName,
        seqnumber: this.state.bannerId,
        formatid: this.state.businessFormat,
        campaignid: this.state.campaignId === '' ? undefined : this.state.campaignId,
        tags: this.state.tags
      }

      let formData = new FormData()
      formData.append('data', JSON.stringify(data))
      formData.append('file', this.state.file)

      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }
      this.props.createBanner(formData, config, () => {
        if (this.props.contentReducer.createBannerStatus === 'success') {
          this.props.getBanners()
          this.props.onHide()
        } else if (this.props.contentReducer.createBannerStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.contentReducer.createBannerMessage
          })
        }
      })
    }
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="medium" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title>ADD NEW</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <form>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Banner Name</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="bannername"
                    value={this.state.bannerName}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Banner ID</ControlLabel>
                  <FormControl
                    type="number"
                    min="0"
                    className="form-control"
                    name="bannerid"
                    value={this.state.bannerId}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Business Format</ControlLabel>
                  <FormControl
                    name="businessfromat"
                    onChange={this.onChange}
                    componentClass="select"
                    placeholder="select"
                    value={this.state.businessFormat}
                  >
                    <option value="1">Big Bazar</option>
                    <option value="2">Easy Day</option>
                    <option value="3">Brand Factory</option>
                    <option value="4">Central</option>
                    <option value="5">Early Salary</option>
                  </FormControl>
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Campaign ID</ControlLabel>
                  <FormControl
                    type="number"
                    min="0"
                    className="form-control"
                    name="campaignid"
                    value={this.state.campaignId}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Tags</ControlLabel>
                  <FormControl
                    className="form-control"
                    placeholder="Enter comma seperated tags"
                    name="tags"
                    value={this.state.tags}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Zip file</ControlLabel>
                  <FormControl
                    className="form-control"
                    placeholder="Zip File"
                    name="filename"
                    disabled={true}
                    value={this.state.filename}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup className="mt26">
                  <input
                    type="file"
                    name="file"
                    key={this.state.offerPageInputKey || ''}
                    ref={this.offerPageFileRef}
                    accept=".zip"
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button bsStyle="primary" onClick={this.showFile.bind(this, 'offer')}>
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button>
                </FormGroup>
              </Col>
            </Row>

            <Row className="form-group">
              <Col sm={10} smOffset={1}>
                <Button bsStyle="primary" disabled={this.state.qaDisabled} onClick={this.saveForQA}>
                  UPLOAD & SAVE IN QA
                </Button>
              </Col>
            </Row>
            <Row>
              <Col sm={5} smOffset={1}>
                <Button bsStyle="secondary" onClick={onHide}>
                  CANCEL
                </Button>
              </Col>
              <Col sm={5}>
                <Button bsStyle="primary" disabled={true} onClick={this.addBanner}>
                  ADD
                </Button>
              </Col>
            </Row>
          </form>
        </Modal.Body>

        <Modal.Footer />
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <HomePreview
          show={this.state.showHomePreview}
          onHide={this.hideHomePreview}
          data={this.state.homePreviewData}
        />
        <HtmlPreview
          show={this.state.showHtmlPreview}
          onHide={this.hideHtmlPreview}
          data={this.state.htmlPreviewData}
        />
        <OfferPreview
          show={this.state.showOfferPreview}
          onHide={this.hideOfferPreview}
          data={this.state.offerPreviewData}
        />
      </Modal>
    )
  }
}

function mapStateToProps({ contentReducer }) {
  return { contentReducer }
}

export default connect(
  mapStateToProps,
  actions
)(NewBanner)
