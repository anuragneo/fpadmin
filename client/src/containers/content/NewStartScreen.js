import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Glyphicon, Row, Col, FormGroup, FormControl, Button, ControlLabel, Modal } from 'react-bootstrap'
import * as actions from './actions'

class NewStartScreen extends Component {
  constructor(props) {
    super(props)
    this.imageFileRef = React.createRef()
    this.state = {
      fileToSend: null,
      screenName: '',
      screenId: '',
      title: '',
      description: '',
      image: '',
      showModal: false,
      modalText: '',
      showPreview: false,
      theInputKey: '',
      qaDisabled: false,
      addDisabled: true,
      updateId: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show !== nextProps.show)
      this.setState({
        theInputKey: Math.random().toString(36),
        fileToSend: null,
        screenName: '',
        screenId: '',
        title: '',
        description: '',
        image: '',
        showModal: false,
        modalText: '',
        showPreview: false,
        qaDisabled: false,
        addDisabled: true,
        updateId: ''
      })
  }

  handleClose = () => this.setState({ showModal: false })

  hidePreview = () => {
    this.setState({ showPreview: false })
  }

  openPreview = () => {
    this.setState({ showPreview: true })
  }

  showFile = () => {
    this.imageFileRef.current.click()
  }

  onChange = e => {
    switch (e.target.name) {
      case 'screenname':
        this.setState({ screenName: e.target.value })
        break
      case 'screenid':
        this.setState({ screenId: e.target.value })
        break
      case 'title':
        this.setState({ title: e.target.value })
        break
      case 'description':
        this.setState({ description: e.target.value })
        break
      case 'image':
        this.setState({ image: e.target.value })
        break
      case 'imagefile':
        this.setState({
          fileToSend: e.target.files[0],
          image: e.target.files[0].name,
          qaDisabled: false,
          addDisabled: true
        })
        break
      default:
        break
    }
  }

  saveForQA = event => {
    event.preventDefault()
    if (this.state.screenName === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Screen Name'
      })
    } else if (this.state.screenId === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Screen Id'
      })
    } else if (this.state.title === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Title'
      })
    } else if (this.state.description === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter description'
      })
    } else if (this.state.fileToSend === null) {
      this.setState({
        showModal: true,
        modalText: 'Select a file to upload'
      })
    } else {
      const data = {
        name: this.state.screenName,
        seqnumber: this.state.screenId,
        sdesc: this.state.title,
        ldesc: this.state.description,
        isactive: 'N'
      }

      let formData = new FormData()
      formData.append('data', JSON.stringify(data))
      formData.append('file', this.state.fileToSend)
      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }
      this.props.createIntro(formData, config, () => {
        if (this.props.contentReducer.createIntroStatus === 'success') {
          this.props.getIntro()
        } else if (this.props.contentReducer.createIntroStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.contentReducer.createIntroMessage
          })
        }
      })
    }
  }

  addStartScreen = event => {
    event.preventDefault()
    if (this.state.screenName === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Screen Name'
      })
    } else if (this.state.screenId === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Screen Id'
      })
    } else if (this.state.title === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Title'
      })
    } else if (this.state.description === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter description'
      })
    } else if (this.state.fileToSend === null) {
      this.setState({
        showModal: true,
        modalText: 'Select a file to upload'
      })
    } else {
      const data = {
        id: this.state.updateId,
        name: this.state.screenName,
        seqnumber: this.state.screenId,
        sdesc: this.state.title,
        ldesc: this.state.description
      }

      let formData = new FormData()
      formData.append('data', JSON.stringify(data))
      formData.append('file', this.state.fileToSend)
      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }
      this.props.updateBanner(formData, config, () => {
        if (this.props.contentReducer.updateBannerStatus === 'success') {
          this.props.getIntro()
        } else if (this.props.contentReducer.updateBannerStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.contentReducer.updateBannerMessage
          })
        }
      })
    }
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="medium" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title>ADD NEW</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Screen Name</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="screenname"
                    value={this.state.screenName}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Screen ID</ControlLabel>
                  <FormControl
                    type="number"
                    min="1"
                    className="form-control"
                    name="screenid"
                    value={this.state.screenId}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Title</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="title"
                    value={this.state.title}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Description</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="description"
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="form-group">
              <Col sm={6} className="form-group">
                <FormGroup>
                  <ControlLabel>Image</ControlLabel>
                  <FormControl
                    className="form-control"
                    placeholder="Upload Image"
                    name="image"
                    disabled="true"
                    value={this.state.image}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6} className="form-group">
                <FormGroup className="mt26">
                  <input
                    type="file"
                    name="imagefile"
                    key={this.state.theInputKey || ''}
                    ref={this.imageFileRef}
                    accept="image/*"
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button bsStyle="primary" onClick={this.showFile}>
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button>
                </FormGroup>
              </Col>
            </Row>
            <Row className="form-group">
              <Col sm={10} smOffset={1}>
                <Button bsStyle="primary" disabled={this.state.qaDisabled} onClick={this.saveForQA}>
                  UPLOAD & SAVE IN QA
                </Button>
              </Col>
            </Row>
            <Row>
              <Col sm={5} smOffset={1}>
                <Button bsStyle="secondary" onClick={onHide}>
                  CANCEL
                </Button>
              </Col>
              <Col sm={5}>
                <Button bsStyle="primary" disabled={this.state.addDisabled} onClick={this.addStartScreen}>
                  ADD
                </Button>
              </Col>
            </Row>
          </form>
        </Modal.Body>
        <Modal.Footer />
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </Modal>
    )
  }
}

function mapStateToProps({ contentReducer }) {
  return { contentReducer }
}

export default connect(
  mapStateToProps,
  actions
)(NewStartScreen)
