import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import {
  Well,
  Row,
  Col,
  FormGroup,
  FormControl,
  ControlLabel,
  ButtonGroup,
  Button,
  Glyphicon,
  Modal
} from 'react-bootstrap'
import RemoveBannerScreen from './RemoveBannerScreen'
import EditBanner from './EditBanner'
import findIndex from 'lodash/findIndex'

class BannerItem extends Component {
  constructor(props) {
    super(props)
    this.contentAccess = []
    this.state = {
      editBannerScreen: false,
      removeBannerScreen: false,
      showModal: false,
      modalText: '',
      position: this.props.banner.seqnumber
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.contentReducer !== prevProps.contentReducer) {
      this.setState({ position: this.props.banner.seqnumber })
      if (
        this.props.contentReducer.deleteBannerStatus &&
        this.props.contentReducer.deleteBannerStatus === 'success' &&
        this.state.removeBannerScreen
      ) {
        this.setState({
          showModal: true,
          modalText: `Banner screen deleted successfully`,
          removeBannerScreen: false
        })
      } else if (
        this.props.contentReducer.updateBannerStatus &&
        this.props.contentReducer.updateBannerStatus === 'success' &&
        this.state.editBannerScreen
      ) {
        this.setState({
          showModal: true,
          modalText: this.props.contentReducer.updateBannerMessage,
          editBannerScreen: false
        })
      }
    }
  }

  handleClose = () => this.setState({ showModal: false })

  removeBannerScreenClose = () => {
    this.setState({ removeBannerScreen: false })
  }

  removeBannerScreenOpen = () => {
    this.setState({ removeBannerScreen: true })
  }

  editBannerScreenClose = () => {
    this.setState({ editBannerScreen: false })
  }

  editBannerScreenOpen = () => {
    this.setState({ editBannerScreen: true })
  }

  renderOptions() {
    return this.props.contentReducer.getBannersData.map((banner, index) => {
      return (
        <option key={index + 1} value={index + 1}>
          {index + 1}
        </option>
      )
    })
  }

  onChange = e => {
    let banners = JSON.parse(JSON.stringify(this.props.contentReducer.getBannersData))
    let oldseqnumber = null

    banners.map(banner => {
      if (Number(banner.id) === Number(this.props.banner.id)) {
        oldseqnumber = banner.seqnumber
        banner.seqnumber = Number(e.target.value)
        return banner
      } else {
        banner.disabled = true
        return banner
      }
    })

    let index = findIndex(this.props.contentReducer.getBannersData, { seqnumber: Number(e.target.value) })
    if (index !== -1) {
      let banner = banners[index]
      banner.disabled = false
      banner.seqnumber = oldseqnumber
      banners[index] = banner
    }

    this.setState({ position: e.target.value })
    this.props.updateBanners(banners)
  }

  render() {
    let backgroundUrl = `${process.env.REACT_APP_S3_URL}${process.env.REACT_APP_S3_BUCKET_CONTENT}/banners/${
      this.props.banner.id
    }/h-${this.props.banner.homelink}`

    let htmlUrl = `${process.env.REACT_APP_S3_URL}${process.env.REACT_APP_S3_BUCKET_CONTENT}/banners/${
      this.props.banner.id
    }/${this.props.banner.htmllink}`
    return (
      <React.Fragment>
        <Well>
          <Row className="bluebg">
            <Col xs={6}>
              <div className="well-title">{`${this.props.banner.name}`}</div>
            </Col>
            <Col xs={6}>
              <FormGroup>
                <ControlLabel
                  style={{
                    display:
                      this.props.contentAccess[1] === '0' && this.props.contentAccess[2] === '0' ? 'none' : 'block'
                  }}
                >
                  Change Position
                </ControlLabel>
                <FormControl
                  style={{
                    display:
                      this.props.contentAccess[1] === '0' && this.props.contentAccess[2] === '0' ? 'none' : 'block'
                  }}
                  componentClass="select"
                  placeholder="select"
                  value={this.state.position}
                  onChange={this.onChange}
                >
                  {this.renderOptions()}
                </FormControl>
              </FormGroup>
            </Col>
          </Row>
          <div className="img_text" style={{ backgroundImage: `url(${backgroundUrl})`, backgroundSize: 'cover' }} />
          <Row className="well-footer">
            <Col xs={6}>
              <div className="link">
                <a target="_blank" href={`${htmlUrl}`}>
                  {this.props.banner.htmllink}
                </a>{' '}
              </div>
            </Col>
            <Col xs={6}>
              <ButtonGroup>
                <Button
                  style={{
                    display:
                      this.props.contentAccess[1] === '0' && this.props.contentAccess[2] === '0' ? 'none' : 'block'
                  }}
                  className="edit_btn"
                  onClick={this.editBannerScreenOpen}
                >
                  <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                </Button>
                <Button
                  style={{
                    display:
                      this.props.contentAccess[1] === '0' && this.props.contentAccess[2] === '0' ? 'none' : 'block'
                  }}
                  className="remove_btn"
                  onClick={this.removeBannerScreenOpen}
                >
                  <Glyphicon glyph=" glyphicon glyphicon-trash" /> Remove
                </Button>
              </ButtonGroup>
            </Col>
          </Row>
        </Well>
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <RemoveBannerScreen
          show={this.state.removeBannerScreen}
          onHide={this.removeBannerScreenClose}
          id={this.props.banner.id}
        />
        <EditBanner show={this.state.editBannerScreen} onHide={this.editBannerScreenClose} banner={this.props.banner} />
      </React.Fragment>
    )
  }
}

function mapStateToProps({ contentReducer }) {
  return { contentReducer }
}

export default connect(
  mapStateToProps,
  actions
)(BannerItem)
