import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Glyphicon, Row, Col, FormGroup, FormControl, Button, ControlLabel, Modal } from 'react-bootstrap'
import HomePreview from './BannerPreview'
import HtmlPreview from './HtmlPreview'
import OfferPreview from './OfferPreview'

class EditBanner extends Component {
  constructor(props) {
    super(props)
    this.homePageFileRef = React.createRef()
    this.fileRef = React.createRef()
    this.offerPageFileRef = React.createRef()
    this.htmlLinkFileRef = React.createRef()
    const { banner } = this.props
    this.state = {
      bannerName: banner.name,
      bannerId: banner.seqnumber,
      businessFormat: banner.formatid,
      campaignId: banner.campaignid ? banner.campaignid : '',
      homePageFile: null,
      offerPageFile: null,
      htmlLinkFile: null,
      homePageInputKey: Math.random().toString(36),
      offerPageInputKey: Math.random().toString(36),
      htmlLinkInputKey: Math.random().toString(36),
      offerimagename: banner.offerlink,
      homeimagename: banner.homelink,
      htmlimagename: banner.htmllink,
      showModal: false,
      modalText: '',
      showHomePreview: false,
      showOfferPreview: false,
      showHtmlPreview: false,
      qaDisabled: true,
      addDisabled: false,
      file: null,
      fileInputKey: Math.random().toString(36),
      filename: '',
      tags: banner.tags
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show !== nextProps.show) {
      const { banner } = nextProps
      this.setState({
        bannerName: banner.name,
        bannerId: banner.seqnumber,
        businessFormat: banner.formatid,
        campaignId: banner.campaignid ? banner.campaignid : '',
        homePageFile: null,
        offerPageFile: null,
        htmlLinkFile: null,
        homePageInputKey: Math.random().toString(36),
        offerPageInputKey: Math.random().toString(36),
        htmlLinkInputKey: Math.random().toString(36),
        offerimagename: banner.offerlink,
        homeimagename: banner.homelink,
        htmlimagename: banner.htmllink,
        showModal: false,
        modalText: '',
        showHomePreview: false,
        showOfferPreview: false,
        showHtmlPreview: false,
        qaDisabled: true,
        addDisabled: false,
        file: null,
        fileInputKey: Math.random().toString(36),
        filename: '',
        tags: banner.tags
      })
    }
  }

  onChange = e => {
    switch (e.target.name) {
      case 'bannername':
        this.setState({ bannerName: e.target.value })
        break
      case 'bannerid':
        this.setState({ bannerId: e.target.value })
        break
      case 'businessfromat':
        this.setState({ businessFormat: e.target.value })
        break
      case 'campaignid':
        this.setState({ campaignId: e.target.value })
        break
      case 'offerimagename':
        this.setState({ offerimagename: e.target.value })
        break
      case 'homeimagename':
        this.setState({ homeimagename: e.target.value })
        break
      case 'htmlimagename':
        this.setState({ htmlimagename: e.target.value })
        break
      case 'homeimagefile':
        this.setState({
          homePageFile: e.target.files[0],
          homeimagename: e.target.files[0].name,
          qaDisabled: false,
          addDisabled: true
        })
        break
      case 'offerimagefile':
        this.setState({
          offerPageFile: e.target.files[0],
          offerimagename: e.target.files[0].name,
          qaDisabled: false,
          addDisabled: true
        })
        break
      case 'htmlimagefile':
        this.setState({
          htmlLinkFile: e.target.files[0],
          htmlimagename: e.target.files[0].name,
          qaDisabled: false,
          addDisabled: true
        })
        break
      case 'file':
        this.setState({
          file: e.target.files[0],
          filename: e.target.files[0].name,
          qaDisabled: false,
          addDisabled: true
        })
        break
      case 'tags':
        this.setState({ tags: e.target.value })
        break
      default:
        break
    }
  }

  hideHomePreview = () => {
    this.setState({ showHomePreview: false })
  }

  hideOfferPreview = () => {
    this.setState({ showOfferPreview: false })
  }

  hideHtmlPreview = () => {
    this.setState({ showHtmlPreview: false })
  }

  openHomePreview = () => {
    this.setState({ showHomePreview: true })
  }

  openOfferPreview = () => {
    this.setState({ showOfferPreview: true })
  }

  openHtmlPreview = () => {
    this.setState({ showHtmlPreview: true })
  }

  handleClose = () => this.setState({ showModal: false })

  showFile = type => {
    if (type === 'home') this.homePageFileRef.current.click()
    else if (type === 'file') this.fileRef.current.click()
    else if (type === 'offer') this.offerPageFileRef.current.click()
    else if (type === 'html') this.htmlLinkFileRef.current.click()
  }

  saveForQA = event => {
    event.preventDefault()
    if (this.state.bannerName === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Banner Name'
      })
    } else if (this.state.bannerId === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Banner Id'
      })
    } else {
      const data = {
        id: this.props.banner.id,
        name: this.state.bannerName,
        seqnumber: this.state.bannerId,
        formatid: this.state.businessFormat,
        campaignid: this.state.campaignId === '' ? undefined : this.state.campaignId,
        isactive: 'N',
        tags: this.state.tags
      }

      let formData = new FormData()
      formData.append('data', JSON.stringify(data))
      formData.append('file', this.state.file)

      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }
      this.props.updateBanner(formData, config, () => {
        if (this.props.contentReducer.updateBannerStatus === 'success') {
          this.props.getBanners()
        } else if (this.props.contentReducer.updateBannerStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.contentReducer.updateBannerMessage
          })
        }
      })
    }
  }

  editBanner = event => {
    event.preventDefault()
    if (this.state.bannerName === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Banner Name'
      })
    } else if (this.state.bannerId === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Banner Id'
      })
    } else {
      const data = {
        id: this.props.banner.id,
        name: this.state.bannerName,
        seqnumber: this.state.bannerId,
        formatid: this.state.businessFormat,
        campaignid: this.state.campaignId === '' ? undefined : this.state.campaignId,
        tags: this.state.tags
      }

      let formData = new FormData()
      formData.append('data', JSON.stringify(data))
      formData.append('file', this.state.file)
      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }
      this.props.updateBanner(formData, config, () => {
        if (this.props.contentReducer.updateBannerStatus === 'success') {
          this.props.getBanners()
        } else if (this.props.contentReducer.updateBannerStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.contentReducer.updateBannerMessage
          })
        }
      })
    }
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="medium" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title>UPDATE</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <form>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Banner Name</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="bannername"
                    value={this.state.bannerName}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Banner ID</ControlLabel>
                  <FormControl
                    type="number"
                    min="1"
                    disabled={true}
                    className="form-control"
                    name="bannerid"
                    value={this.state.bannerId}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Business Format</ControlLabel>
                  <FormControl
                    name="businessfromat"
                    onChange={this.onChange}
                    componentClass="select"
                    placeholder="select"
                    value={this.state.businessFormat}
                  >
                    <option value="1">Big Bazar</option>
                    <option value="2">Easy Day</option>
                    <option value="3">Brand Factory</option>
                    <option value="4">Central</option>
                    <option value="5">Early Salary</option>
                  </FormControl>
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Campaign ID</ControlLabel>
                  <FormControl
                    type="number"
                    min="0"
                    className="form-control"
                    name="campaignid"
                    value={this.state.campaignId}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Tags</ControlLabel>
                  <FormControl
                    name="tags"
                    type="text"
                    onChange={this.onChange}
                    placeholder="Enter comma seperated tags"
                    value={this.state.tags}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Zip file</ControlLabel>
                  <FormControl
                    className="form-control"
                    placeholder="Upload Zip"
                    name="filename"
                    disabled={true}
                    value={this.state.filename}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup className="preview hom_off_prev">
                  <input
                    type="file"
                    name="file"
                    key={this.state.fileInputKey || ''}
                    ref={this.fileRef}
                    accept=".zip"
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button bsStyle="primary" onClick={this.showFile.bind(this, 'file')}>
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Home Page</ControlLabel>
                  <FormControl
                    className="form-control"
                    placeholder="Upload Image"
                    name="homeimagename"
                    disabled={true}
                    value={this.state.homeimagename}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup className="preview hom_off_prev">
                  <ControlLabel onClick={this.openHomePreview} style={{ cursor: 'pointer' }}>
                    <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                    PREVIEW
                  </ControlLabel>
                  {/* <input
                    type="file"
                    name="homeimagefile"
                    key={this.state.homePageInputKey || ''}
                    ref={this.homePageFileRef}
                    accept="image/*"
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button bsStyle="primary" onClick={this.showFile.bind(this, 'home')}>
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button> */}
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Offer Page</ControlLabel>
                  <FormControl
                    className="form-control"
                    placeholder="Upload Image"
                    name="offerimagename"
                    disabled={true}
                    value={this.state.offerimagename}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup className="preview hom_off_prev">
                  <ControlLabel onClick={this.openOfferPreview} style={{ cursor: 'pointer' }}>
                    <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                    PREVIEW
                  </ControlLabel>
                  {/* <input
                    type="file"
                    name="offerimagefile"
                    key={this.state.offerPageInputKey || ''}
                    ref={this.offerPageFileRef}
                    accept="image/*"
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button bsStyle="primary" onClick={this.showFile.bind(this, 'offer')}>
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button> */}
                </FormGroup>
              </Col>
            </Row>
            <Row className="form-group">
              <Col sm={6} className="form-group">
                <FormGroup>
                  <ControlLabel>HTML Link</ControlLabel>
                  <FormControl
                    className="form-control"
                    placeholder="Upload Image"
                    name="htmlimagename"
                    disabled={true}
                    value={this.state.htmlimagename}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6} className="form-group">
                <FormGroup className="preview hom_off_prev">
                  <ControlLabel onClick={this.openHtmlPreview} style={{ cursor: 'pointer' }}>
                    <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                    PREVIEW
                  </ControlLabel>
                  {/* <input
                    type="file"
                    name="htmlimagefile"
                    key={this.state.htmlLinkInputKey || ''}
                    ref={this.htmlLinkFileRef}
                    accept=".html"
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button bsStyle="primary" onClick={this.showFile.bind(this, 'html')}>
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button> */}
                </FormGroup>
              </Col>
            </Row>
            <Row className="form-group">
              <Col sm={10} smOffset={1}>
                <Button bsStyle="primary" disabled={this.state.qaDisabled} onClick={this.saveForQA}>
                  UPLOAD & SAVE IN QA
                </Button>
              </Col>
            </Row>
            <Row>
              <Col sm={5} smOffset={1}>
                <Button bsStyle="secondary" onClick={onHide}>
                  CANCEL
                </Button>
              </Col>
              <Col sm={5}>
                <Button bsStyle="primary" disabled={this.state.addDisabled} onClick={this.editBanner}>
                  UPDATE
                </Button>
              </Col>
            </Row>
          </form>
        </Modal.Body>

        <Modal.Footer />
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <HomePreview
          show={this.state.showHomePreview}
          onHide={this.hideHomePreview}
          data={{ id: this.props.banner.id, imagelink: this.props.banner.homelink }}
        />
        <HtmlPreview
          show={this.state.showHtmlPreview}
          onHide={this.hideHtmlPreview}
          data={{ id: this.props.banner.id, imagelink: this.props.banner.htmllink }}
        />
        <OfferPreview
          show={this.state.showOfferPreview}
          onHide={this.hideOfferPreview}
          data={{ id: this.props.banner.id, imagelink: this.props.banner.offerlink }}
        />
      </Modal>
    )
  }
}

function mapStateToProps({ contentReducer }) {
  return { contentReducer }
}

export default connect(
  mapStateToProps,
  actions
)(EditBanner)
