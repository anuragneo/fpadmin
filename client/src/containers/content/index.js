import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import Sidebar from '../../components/sidebar'
import StaticConent from './StaticContent'
import Banner from './Banner'
import StartScreen from './StartScreen'

export default class Content extends Component {
  render() {
    if (this.props.history.location.pathname === '/' || this.props.history.location.pathname === '/content')
      this.props.history.push('/content/banner')
    return (
      <div id="tabs-with-dropdown-pane-fifth">
        <Sidebar type="content" />
        <Route path="/content/banner" component={Banner} />
        <Route path="/content/start" component={StartScreen} />
        <Route path="/content/static" component={StaticConent} />
      </div>
    )
  }
}
