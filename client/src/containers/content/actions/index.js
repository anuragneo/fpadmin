import axios from 'axios'
import * as types from './types'
import store from '../'

const url = process.env.PUBLIC_URL

export const getBanners = req => async dispatch => {
  const res = await axios.post(`${url}/api/content/getbanners`, req)
  dispatch({ type: types.GET_BANNERS, payload: res.data })
}

export const createBanner = (req, config, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/content/createbanner`, req, config)
  dispatch({ type: types.CREATE_BANNER, payload: res.data })
  callback()
}

export const updateBanner = (req, config, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/content/updatebanner`, req, config)
  dispatch({ type: types.UPDATE_BANNER, payload: res.data })
  callback()
}

export const deleteBanner = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/content/deletebanner`, req)
  dispatch({ type: types.DELETE_BANNER, payload: res.data })
  callback()
}

export const getIntro = req => async dispatch => {
  const res = await axios.post(`${url}/api/content/getintroscreens`, req)
  dispatch({ type: types.GET_INTRO, payload: res.data })
}

export const createIntro = (req, config, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/content/createintroscreen`, req, config)
  dispatch({ type: types.CREATE_INTRO, payload: res.data })
  callback()
}

export const updateIntro = (req, config, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/content/updateintroscreen`, req, config)
  dispatch({ type: types.UPDATE_INTRO, payload: res.data })
  callback()
}

export const deleteIntro = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/content/deleteintroscreen`, req)
  dispatch({ type: types.DELETE_INTRO, payload: res.data })
  callback()
}

export const updateBannerPosition = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/content/updatebannerposition`, req)
  dispatch({ type: types.UPDATE_BANNER_POSITION, payload: res.data })
  callback()
}

export const invalidateCache = req => async dispatch => {
  const res = await axios.post(`${url}/api/content/invalidatecache`, req)
  dispatch({ type: types.INVALIDATE_CACHE, payload: res.data })
}

export function updateBanners(data) {
  return {
    type: types.UPDATE_BANNERS,
    payload: data
  }
}

export function changeIndex(data) {
  return {
    type: types.CHANGE_INDEX,
    payload: data
  }
}
