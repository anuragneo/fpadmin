export const GET_BANNERS = 'getbanners'
export const CREATE_BANNER = 'createbanner'
export const UPDATE_BANNER = 'updatebanner'
export const DELETE_BANNER = 'deletebanner'
export const GET_INTRO = 'getintro'
export const CREATE_INTRO = 'createintro'
export const UPDATE_INTRO = 'updateintro'
export const DELETE_INTRO = 'deleteintro'
export const CHANGE_INDEX = 'changeindex'
export const UPDATE_BANNERS = 'updatebanners'
export const UPDATE_BANNER_POSITION = 'updatebannerposition'
export const INVALIDATE_CACHE = 'invalidatecache'
