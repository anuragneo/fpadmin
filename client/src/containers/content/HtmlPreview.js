import React, { Component } from 'react'
import { Modal, Image } from 'react-bootstrap'
import logo from '../../images/fplogo.jpg'

class HtmlPreview extends Component {
  constructor(props) {
    super(props)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show != nextProps.show) {
    }
  }

  render() {
    const { show, onHide, data } = this.props
    let imageurl = `${process.env.REACT_APP_S3_URL}${process.env.REACT_APP_S3_BUCKET_CONTENT}/banners/${data.id}/${
      data.imagelink
    }`
    return (
      <Modal
        className="preview_inapp offers-bg"
        show={show}
        onHide={onHide}
        bsSize="small"
        aria-labelledby="contained-modal-title-lg"
      >
        <iframe src={imageurl} />
      </Modal>
    )
  }
}

export default HtmlPreview
