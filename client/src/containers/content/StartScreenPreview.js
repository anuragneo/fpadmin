import React, { Component } from 'react'
import { Modal, Image } from 'react-bootstrap'
import logo from '../../images/fplogo.jpg'

class StartScreenPreview extends Component {
  constructor(props) {
    super(props)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show != nextProps.show) {
    }
  }

  render() {
    const { show, onHide, data } = this.props
    let imageurl = `${process.env.REACT_APP_S3_URL}${process.env.REACT_APP_S3_BUCKET_CONTENT}/introscreens/${data.id}/${
      data.imagelink
    }`
    return (
      <Modal
        className="preview_inapp preview_inapp_start_screen"
        show={show}
        onHide={onHide}
        bsSize="small"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Body
          className="homebanner_p_image"
          style={{ backgroundImage: `url(${imageurl})`, backgroundSize: 'cover' }}
        >
          <div className="homebanner_preview_top_txt">{data.title}</div>
          <div className="homebanner_preview_top_subtxt">{data.desc}</div>
        </Modal.Body>
      </Modal>
    )
  }
}

export default StartScreenPreview
