import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Modal, Row, Col, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'

class RemoveBannerScreen extends Component {
  removeBannerScreen = () => {
    this.props.deleteBanner(
      {
        id: this.props.id
      },
      () => {
        if (this.props.contentReducer.deleteBannerStatus === 'success') {
          this.props.getBanners()
        }
      }
    )
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">Remove</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Row>
            <Col xs={12}>
              <FormGroup>
                <ControlLabel>Are you sure you want to remove the banner?</ControlLabel>
              </FormGroup>
            </Col>
          </Row>
        </Modal.Body>

        <Modal.Footer>
          <Row>
            <Col xs={6}>
              <Button bsClass="btn btn-secondary" onClick={onHide}>
                CANCEL
              </Button>
            </Col>
            <Col xs={6}>
              <Button bsStyle="primary" onClick={this.removeBannerScreen}>
                CONFIRM
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

function mapStateToProps({ contentReducer }) {
  return { contentReducer }
}

export default connect(
  mapStateToProps,
  actions
)(RemoveBannerScreen)
