import React, { Component } from 'react'
import { Modal, Image } from 'react-bootstrap'
import logo from '../../images/fplogo.jpg'

class HomePreview extends Component {
  constructor(props) {
    super(props)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show != nextProps.show) {
    }
  }

  render() {
    const { show, onHide, data } = this.props
    let imageurl = `${process.env.REACT_APP_S3_URL}${process.env.REACT_APP_S3_BUCKET_CONTENT}/banners/${data.id}/h-${
      data.imagelink
    }`
    return (
      <Modal
        className="preview_inapp homebanner_p_image"
        show={show}
        onHide={onHide}
        bsSize="small"
        aria-labelledby="contained-modal-title-lg"
      >
        <Image src={imageurl} />
      </Modal>
    )
  }
}

export default HomePreview
