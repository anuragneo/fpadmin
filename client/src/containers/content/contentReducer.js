import {
  GET_BANNERS,
  CHANGE_INDEX,
  CREATE_BANNER,
  UPDATE_BANNER,
  DELETE_BANNER,
  GET_INTRO,
  CREATE_INTRO,
  UPDATE_INTRO,
  DELETE_INTRO,
  UPDATE_BANNERS,
  UPDATE_BANNER_POSITION
} from './actions/types'

import sortBy from 'lodash/sortBy'


export default function (state = {}, action) {
  switch (action.type) {
    case UPDATE_BANNER_POSITION:
      return {
        ...state,
        updateBannerPositionStatus: action.payload.status,
        updateBannerPositionMessage: action.payload.message,
      }
    case UPDATE_BANNERS:
      return {
        ...state,
        getBannersData: sortBy(action.payload, ['seqnumber'])
      }
    case GET_BANNERS:
      return {
        ...state,
        getBannersStatus: action.payload.status,
        getBannersData: sortBy(action.payload.data, ['seqnumber']),
        getBannersMessage: action.payload.message
      }
    case CREATE_BANNER:
      return {
        ...state,
        createBannerStatus: action.payload.status,
        createBannerData: action.payload.data,
        createBannerMessage: action.payload.message
      }
    case UPDATE_BANNER:
      return {
        ...state,
        updateBannerStatus: action.payload.status,
        updateBannerData: action.payload.data,
        updateBannerMessage: action.payload.message
      }
    case DELETE_BANNER:
      return {
        ...state,
        deleteBannerStatus: action.payload.status,
        deleteBannerData: action.payload.data,
        deleteBannerMessage: action.payload.message
      }
    case GET_INTRO:
      return {
        ...state,
        getIntroStatus: action.payload.status,
        getIntroData: action.payload.data,
        getIntroMessage: action.payload.message
      }
    case CREATE_INTRO:
      return {
        ...state,
        createIntroStatus: action.payload.status,
        createIntroData: action.payload.data,
        createIntroMessage: action.payload.message
      }
    case UPDATE_INTRO:
      return {
        ...state,
        updateIntroStatus: action.payload.status,
        updateIntroData: action.payload.data,
        updateIntroMessage: action.payload.message
      }
    case DELETE_INTRO:
      return {
        ...state,
        deleteIntroStatus: action.payload.status,
        deleteIntroData: action.payload.data,
        deleteIntroMessage: action.payload.message
      }

    case CHANGE_INDEX:
      console.log('indexdata', action.payload)
      return {
        ...state,
        changeIndexData: action.payload
      }

    default:
      return state
  }
}
