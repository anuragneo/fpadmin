import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import aws from 'aws-sdk'
import { Grid, Col, Row, FormGroup, ControlLabel, Button, Glyphicon, PanelGroup, Panel } from 'react-bootstrap'
import { getAdmin } from '../../helpers/app'

class StaticContent extends Component {
  constructor(props) {
    super(props)
    this.contentAccess = []
    this.tncFileRef = React.createRef()
    this.privacyFileRef = React.createRef()
    this.faqFileRef = React.createRef()
    this.customerFileRef = React.createRef()
    this.pricematchtncFileRef = React.createRef()
    this.pricematchfaqFileRef = React.createRef()
    this.bbpctncFileRef = React.createRef()
    this.bbpcfaqFileRef = React.createRef()
    this.paybacktncFileRef = React.createRef()
    this.paybackfaqFileRef = React.createRef()

    this.state = {
      tncFilekey: '',
      privacyFileKey: '',
      faqFileKey: '',
      customerFileKey: '',
      pricematchtncFileKey: '',
      pricematchfaqFileKey: '',
      bbpctncFileKey: '',
      bbpcfaqFileKey: '',
      paybacktncFileKey: '',
      paybackfaqFileKey: ''
    }
  }

  componentWillMount() {
    let admin = getAdmin()
    this.contentAccess = admin.contentaccess.split('')
  }

  onChange = e => {
    switch (e.target.name) {
      case 'tncfile':
        this.uploadFile('tnc', e.target.files[0])
        break
      case 'privacyfile':
        this.uploadFile('privacy', e.target.files[0])
        break
      case 'faqfile':
        this.uploadFile('faq', e.target.files[0])
        break
      case 'customerfile':
        this.uploadFile('customer', e.target.files[0])
        break
      case 'pricematchtncfile':
        this.uploadFile('pricematchtnc', e.target.files[0])
        break
      case 'pricematchfaqfile':
        this.uploadFile('pricematchfaq', e.target.files[0])
        break
      case 'bbpctncfile':
        this.uploadFile('bbpctnc', e.target.files[0])
        break
      case 'bbpcfaqfile':
        this.uploadFile('bbpcfaq', e.target.files[0])
        break
      case 'paybacktncfile':
        this.uploadFile('paybacktnc', e.target.files[0])
        break
      case 'paybackfaqfile':
        this.uploadFile('paybackfaq', e.target.files[0])
        break
      default:
        break
    }
  }

  showFile = type => {
    if (type === 'tnc') this.tncFileRef.current.click()
    else if (type === 'privacy') this.privacyFileRef.current.click()
    else if (type === 'faq') this.faqFileRef.current.click()
    else if (type === 'customer') this.customerFileRef.current.click()
    else if (type === 'pricematchtnc') this.pricematchtncFileRef.current.click()
    else if (type === 'pricematchfaq') this.pricematchfaqFileRef.current.click()
    else if (type === 'bbpctnc') this.bbpctncFileRef.current.click()
    else if (type === 'bbpcfaq') this.bbpcfaqFileRef.current.click()
    else if (type === 'paybacktnc') this.paybacktncFileRef.current.click()
    else if (type === 'paybackfaq') this.paybackfaqFileRef.current.click()
  }

  uploadFile = (type, file) => {
    aws.config.update({
      accessKeyId: process.env.REACT_APP_S3_ACCESSKEY_CONTENT,
      secretAccessKey: process.env.REACT_APP_S3_ACCESSSECRET_CONTENT
    })

    let bucket = ''
    let key = ''
    if (type === 'tnc') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/fp/tnc.html'
    } else if (type === 'privacy') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/fp/privacy-policy.html'
    } else if (type === 'faq') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/fp/faq.html'
    } else if (type === 'customer') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/fp/customer-service-policy.html'
    } else if (type === 'pricematchtnc') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/pricematch/tnc.html'
    } else if (type === 'pricematchfaq') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/pricematch/faq.html'
    } else if (type === 'bbpctnc') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/bbpc/tnc.html'
    } else if (type === 'bbpcfaq') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/bbpc/faq.html'
    } else if (type === 'paybacktnc') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/paybac/tnc.html'
    } else if (type === 'paybackfaq') {
      bucket = process.env.REACT_APP_S3_BUCKET_CONTENT
      key = 'static/paybac/faq.html'
    }

    const s3bucket = new aws.S3({ params: { Bucket: bucket, region: 'ap-south-1' } })
    const params = {
      Key: key,
      ContentType: file.type,
      Body: file,
      ACL: 'public-read'
    }
    s3bucket.upload(params, (err, data) => {
      if (data && !err) {
        if (type === 'tnc') {
          this.setState({ tncFilekey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'privacy') {
          this.setState({ privacyFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'faq') {
          this.setState({ faqFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'customer') {
          this.setState({ customerFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'pricematchtnc') {
          this.setState({ pricematchtncFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'pricematchfaq') {
          this.setState({ pricematchfaqFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'bbpctnc') {
          this.setState({ bbpctncFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'bbpcfaq') {
          this.setState({ bbpcfaqFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'paybacktnc') {
          this.setState({ paybacktncFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        } else if (type === 'paybackfaq') {
          this.setState({ paybackfaqFileKey: Math.random().toString(36) })
          this.props.invalidateCache({})
        }
      }
    })
  }

  render() {
    return (
      <Col xs={10} className="scroll">
        <Grid fluid>
          <Row className="form-group">
            <Col xs={8}>
              <FormGroup>
                <h4 className="acct_manage_hist_label form-group">Wallet Static Content</h4>
              </FormGroup>
            </Col>
          </Row>
          <Row className="form-group">
            <Col xs={3}>
              <FormGroup>
                <ControlLabel>Terms and Conditions</ControlLabel>
                <input
                  type="file"
                  name="tncfile"
                  key={this.state.tncFilekey || ''}
                  ref={this.tncFileRef}
                  accept=".html"
                  style={{ display: 'none' }}
                  onChange={this.onChange}
                />
              </FormGroup>
            </Col>
            <Col xs={7}>
              <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/fp/tnc.html`}>
                <b>Click to view current version </b>
              </a>
            </Col>
            <Col xs={2}>
              <Button
                style={{
                  display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                }}
                bsStyle="primary"
                onClick={this.showFile.bind(this, 'tnc')}
              >
                <Glyphicon glyph="glyphicon glyphicon-open" />
                Upload
              </Button>
            </Col>
          </Row>
          <Row className="form-group">
            <Col xs={3}>
              <FormGroup>
                <ControlLabel>Privacy Policy</ControlLabel>
                <input
                  type="file"
                  name="privacyfile"
                  key={this.state.privacyFileKey || ''}
                  ref={this.privacyFileRef}
                  accept=".html"
                  style={{ display: 'none' }}
                  onChange={this.onChange}
                />
              </FormGroup>
            </Col>
            <Col xs={7}>
              <a
                href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/fp/privacy-
              policy.html`}
              >
                <b>Click to view current version </b>
              </a>
            </Col>
            <Col xs={2}>
              <Button
                style={{
                  display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                }}
                bsStyle="primary"
                onClick={this.showFile.bind(this, 'privacy')}
              >
                <Glyphicon glyph="glyphicon glyphicon-open" />
                Upload
              </Button>
            </Col>
          </Row>
          <Row className="form-group">
            <Col xs={3}>
              <FormGroup>
                <ControlLabel>FAQ</ControlLabel>
                <input
                  type="file"
                  name="faqfile"
                  key={this.state.faqFileKey || ''}
                  ref={this.faqFileRef}
                  accept=".html"
                  style={{ display: 'none' }}
                  onChange={this.onChange}
                />
              </FormGroup>
            </Col>
            <Col xs={7}>
              <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/fp/faq.html`}>
                <b>Click to view current version </b>
              </a>
            </Col>
            <Col xs={2}>
              <Button
                style={{
                  display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                }}
                bsStyle="primary"
                onClick={this.showFile.bind(this, 'faq')}
              >
                <Glyphicon glyph="glyphicon glyphicon-open" />
                Upload
              </Button>
            </Col>
          </Row>
          <Row className="form-group">
            <Col xs={3} className="form-group">
              <FormGroup>
                <ControlLabel>Customer Service Policy</ControlLabel>
                <input
                  type="file"
                  name="customerfile"
                  key={this.state.customerFileKey || ''}
                  ref={this.customerFileRef}
                  accept=".html"
                  style={{ display: 'none' }}
                  onChange={this.onChange}
                />
              </FormGroup>
            </Col>
            <Col xs={7}>
              <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/fp/customer-service-policy.html`}>
                <b>Click to view current version </b>
              </a>
            </Col>
            <Col xs={2} className="form-group">
              <Button
                style={{
                  display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                }}
                bsStyle="primary"
                onClick={this.showFile.bind(this, 'customer')}
              >
                <Glyphicon glyph="glyphicon glyphicon-open" />
                Upload
              </Button>
            </Col>
          </Row>
          <FormGroup>
            <h4 className="acct_manage_hist_label form-group">Add on Module/Features</h4>
          </FormGroup>
          <PanelGroup accordion id="accordion-example">
            <Panel eventKey="1">
              <Panel.Heading>
                <Panel.Title toggle>
                  Price Match
                  <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
                  <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
                </Panel.Title>
              </Panel.Heading>

              <Panel.Body collapsible>
                <Row className="form-group">
                  <Col xs={3}>
                    <FormGroup>
                      <ControlLabel>Terms and Conditions</ControlLabel>
                      <input
                        type="file"
                        name="pricematchtncfile"
                        key={this.state.pricematchtncFileKey || ''}
                        ref={this.pricematchtncFileRef}
                        accept=".html"
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={7}>
                    <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/pricematch/tnc.html`}>
                      <b>Click to view current version </b>
                    </a>
                  </Col>
                  <Col xs={2}>
                    <Button
                      style={{
                        display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                      }}
                      bsStyle="primary"
                      onClick={this.showFile.bind(this, 'pricematchtnc')}
                    >
                      <Glyphicon glyph="glyphicon glyphicon-open" />
                      Upload
                    </Button>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col xs={3}>
                    <FormGroup>
                      <ControlLabel>FAQ</ControlLabel>
                      <input
                        type="file"
                        name="pricematchfaqfile"
                        key={this.state.pricematchfaqFileKey || ''}
                        ref={this.pricematchfaqFileRef}
                        accept=".html"
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={7}>
                    <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/pricematch/faq.html`}>
                      <b>Click to view current version </b>
                    </a>
                  </Col>
                  <Col xs={2}>
                    <Button
                      style={{
                        display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                      }}
                      bsStyle="primary"
                      onClick={this.showFile.bind(this, 'pricematchfaq')}
                    >
                      <Glyphicon glyph="glyphicon glyphicon-open" />
                      Upload
                    </Button>
                  </Col>
                </Row>
              </Panel.Body>
            </Panel>
            <Panel eventKey="2">
              <Panel.Heading>
                <Panel.Title toggle>
                  BBPC
                  <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
                  <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
                </Panel.Title>
              </Panel.Heading>

              <Panel.Body collapsible>
                <Row className="form-group">
                  <Col xs={3}>
                    <FormGroup>
                      <ControlLabel>Terms and Conditions</ControlLabel>
                      <input
                        type="file"
                        name="bbpctncfile"
                        key={this.state.bbpctncFileKey || ''}
                        ref={this.bbpctncFileRef}
                        accept=".html"
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={7}>
                    <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/bbpc/tnc.html`}>
                      <b>Click to view current version </b>
                    </a>
                  </Col>
                  <Col xs={2}>
                    <Button
                      style={{
                        display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                      }}
                      bsStyle="primary"
                      onClick={this.showFile.bind(this, 'bbpctnc')}
                    >
                      <Glyphicon glyph="glyphicon glyphicon-open" />
                      Upload
                    </Button>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col xs={3}>
                    <FormGroup>
                      <ControlLabel>FAQ</ControlLabel>
                      <input
                        type="file"
                        name="bbpcfaqfile"
                        key={this.state.bbpcfaqFileKey || ''}
                        ref={this.bbpcfaqFileRef}
                        accept=".html"
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={7}>
                    <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/bbpc/faq.html`}>
                      <b>Click to view current version </b>
                    </a>
                  </Col>
                  <Col xs={2}>
                    <Button
                      style={{
                        display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                      }}
                      bsStyle="primary"
                      onClick={this.showFile.bind(this, 'bbpcfaq')}
                    >
                      <Glyphicon glyph="glyphicon glyphicon-open" />
                      Upload
                    </Button>
                  </Col>
                </Row>
              </Panel.Body>
            </Panel>
            <Panel eventKey="3">
              <Panel.Heading>
                <Panel.Title toggle>
                  Payback
                  <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
                  <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
                </Panel.Title>
              </Panel.Heading>

              <Panel.Body collapsible>
                <Row className="form-group">
                  <Col xs={3}>
                    <FormGroup>
                      <ControlLabel>Terms and Conditions</ControlLabel>
                      <input
                        type="file"
                        name="paybacktncfile"
                        key={this.state.paybacktncFileKey || ''}
                        ref={this.paybacktncFileRef}
                        accept=".html"
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={7}>
                    <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/payback/tnc.html`}>
                      <b>Click to view current version </b>
                    </a>
                  </Col>
                  <Col xs={2}>
                    <Button
                      style={{
                        display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                      }}
                      bsStyle="primary"
                      onClick={this.showFile.bind(this, 'paybacktnc')}
                    >
                      <Glyphicon glyph="glyphicon glyphicon-open" />
                      Upload
                    </Button>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col xs={3}>
                    <FormGroup>
                      <ControlLabel>FAQ</ControlLabel>
                      <input
                        type="file"
                        name="paybackfaqfile"
                        key={this.state.paybackfaqFileKey || ''}
                        ref={this.paybackfaqFileRef}
                        accept=".html"
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={7}>
                    <a href={`${process.env.REACT_APP_AWS_BUCKET_URL_CONTENT}/static/payback/faq.html`}>
                      <b>Click to view current version </b>
                    </a>
                  </Col>
                  <Col xs={2}>
                    <Button
                      style={{
                        display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                      }}
                      bsStyle="primary"
                      onClick={this.showFile.bind(this, 'paybackfaq')}
                    >
                      <Glyphicon glyph="glyphicon glyphicon-open" />
                      Upload
                    </Button>
                  </Col>
                </Row>
              </Panel.Body>
            </Panel>
          </PanelGroup>
        </Grid>
      </Col>
    )
  }
}

export default connect(
  null,
  actions
)(StaticContent)
