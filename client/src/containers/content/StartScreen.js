import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Grid, Col, Row, FormGroup, FormControl, Button, Glyphicon, Well, ButtonGroup, Modal } from 'react-bootstrap'
import * as actions from './actions'
import NewStartScreen from './NewStartScreen'
import EditStartScreen from './EditStartScreen'
import RemoveStartScreen from './RemoveStartScreen'
import { getAdmin } from '../../helpers/app'

class StartScreen extends Component {
  constructor(props) {
    super(props)
    this.contentAccess = []
    this.state = {
      newStartScreen: false,
      editStartScreen: false,
      removeStartScreen: false,
      editScreen: {},
      removeScreen: {},
      showModal: false,
      modalText: ''
    }
  }

  componentDidMount() {
    let admin = getAdmin()
    this.contentAccess = admin.contentaccess.split('')
    this.props.getIntro()
  }

  handleClose = () => this.setState({ showModal: false })

  newStartScreenClose = () => {
    this.setState({
      newStartScreen: false
    })
  }

  editStartScreenClose = () => {
    this.setState({
      editStartScreen: false
    })
  }

  removeStartScreenClose = () => {
    this.setState({
      removeStartScreen: false
    })
  }

  showNewStartScreen = () => {
    this.setState({
      newStartScreen: true
    })
  }

  showEditStartScreen(screen) {
    this.setState({
      editStartScreen: true,
      editScreen: screen
    })
  }

  showRemoveStartScreen(screen) {
    this.setState({
      removeStartScreen: true,
      removeScreen: screen
    })
  }

  componentDidUpdate(prevProps) {
    if (this.props.contentReducer !== prevProps.contentReducer) {
      if (
        this.props.contentReducer.deleteIntroStatus &&
        this.props.contentReducer.deleteIntroStatus === 'success' &&
        this.state.removeStartScreen
      ) {
        this.setState({
          showModal: true,
          modalText: `Start screen deleted successfully`,
          removeStartScreen: false
        })
      } else if (
        this.props.contentReducer.createIntroStatus &&
        this.props.contentReducer.createIntroStatus === 'success' &&
        this.state.newStartScreen
      ) {
        this.setState({
          showModal: true,
          modalText: this.props.contentReducer.createIntroMessage,
          newStartScreen: false
        })
      } else if (
        this.props.contentReducer.updateIntroStatus &&
        this.props.contentReducer.updateIntroStatus === 'success' &&
        this.state.editStartScreen
      ) {
        this.setState({
          showModal: true,
          modalText: this.props.contentReducer.updateIntroMessage,
          editStartScreen: false
        })
      }
    }
  }

  showIntroScreens() {
    if (this.props.contentReducer.getIntroStatus === 'success') {
      return this.props.contentReducer.getIntroData.map((intro, index) => {
        let backgroundUrl = `${process.env.REACT_APP_S3_URL}${process.env.REACT_APP_S3_BUCKET_CONTENT}/introscreens/${
          intro.id
        }/${intro.imagelink}`
        return (
          <Col key={index} xs={4} className="form-group">
            <Well>
              <Row className="bluebg">
                <Col xs={6}>
                  <div className="well-title">{intro.name}</div>
                </Col>
              </Row>
              <div className="img_text" style={{ backgroundImage: `url(${backgroundUrl})`, backgroundSize: 'cover' }} />
              <Row className="well-footer">
                <Col xsOffset={6}>
                  <ButtonGroup>
                    <Button
                      style={{
                        display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                      }}
                      className="edit_btn"
                      onClick={this.showEditStartScreen.bind(this, intro)}
                    >
                      <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                    </Button>
                    <Button
                      style={{
                        display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block'
                      }}
                      className="remove_btn"
                      onClick={this.showRemoveStartScreen.bind(this, intro)}
                    >
                      <Glyphicon glyph=" glyphicon glyphicon-trash" /> Remove
                    </Button>
                  </ButtonGroup>
                </Col>
              </Row>
            </Well>
          </Col>
        )
      })
    }
  }

  render() {
    return (
      <Col xs={10} className="scroll">
        <Grid fluid>
          <Row className="form-group">
            <Col xs={2} className="form-group">
              <Button
                style={{ display: this.contentAccess[1] === '0' && this.contentAccess[2] === '0' ? 'none' : 'block' }}
                type="button"
                bsStyle="secondary"
                onClick={this.showNewStartScreen}
              >
                <Glyphicon glyph="glyphicon glyphicon-plus" /> ADD NEW
              </Button>
            </Col>
          </Row>
          <Row>{this.showIntroScreens()}</Row>
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
          <NewStartScreen show={this.state.newStartScreen} onHide={this.newStartScreenClose} />
          <EditStartScreen
            show={this.state.editStartScreen}
            onHide={this.editStartScreenClose}
            screen={this.state.editScreen}
          />
          <RemoveStartScreen
            show={this.state.removeStartScreen}
            onHide={this.removeStartScreenClose}
            screen={this.state.removeScreen}
          />
        </Grid>
      </Col>
    )
  }
}

function mapStateToProps({ contentReducer }) {
  return { contentReducer }
}

export default connect(
  mapStateToProps,
  actions
)(StartScreen)
