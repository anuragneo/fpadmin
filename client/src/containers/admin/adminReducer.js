import {
  ADMIN_USERS,
  ADMIN_CHANNEL,
  USERS_CREATE,
  USERS_REMOVE,
  USERS_UPDATE,
  CHANNEL_CREATE,
  CHANNEL_REMOVE,
  CHANNEL_UPDATE,
  ADMIN_WALLET,
  WALLET_CREATE,
  WALLET_UPDATE,
  LOGIN
} from './actions/types'

export default function (state = {}, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loginStatus: action.payload.status,
        loginData: action.payload.data,
        loginMessage: action.payload.message
      }
    case ADMIN_USERS:
      return {
        ...state,
        usersStatus: action.payload.status,
        usersData: action.payload.data
      }
    case USERS_CREATE:
      return {
        ...state,
        createStatus: action.payload.status,
        updateStatus: '',
        removeStatus: '',
        createData: action.payload.data,
        createMessage: action.payload.message
      }
    case USERS_UPDATE:
      return {
        ...state,
        updateStatus: action.payload.status,
        updateData: action.payload.data,
        createStatus: '',
        removeStatus: '',
        updateMessage: action.payload.message
      }
    case USERS_REMOVE:
      return {
        ...state,
        removeStatus: action.payload.status,
        createStatus: '',
        updateStatus: '',
        removeMessage: action.payload.message
      }
    case ADMIN_CHANNEL:
      return {
        ...state,
        channelStatus: action.payload.status,
        channelData: action.payload.data
      }
    case CHANNEL_CREATE:
      return {
        ...state,
        channelCreateStatus: action.payload.status,
        channelRemoveStatus: '',
        channelUpdateStatus: '',
        channelCreateMessage: action.payload.message
      }
    case CHANNEL_UPDATE:
      return {
        ...state,
        channelUpdateStatus: action.payload.status,
        channelCreateStatus: '',
        channelRemoveStatus: '',
        channelUpdateMessage: action.payload.message
      }
    case CHANNEL_REMOVE:
      return {
        ...state,
        channelRemoveStatus: action.payload.status,
        channelCreateStatus: '',
        channelUpdateStatus: '',
        channelRemoveMessage: action.payload.message
      }
    case ADMIN_WALLET:
      return {
        ...state,
        walletStatus: action.payload.status,
        walletData: action.payload.data
      }
    case WALLET_CREATE:
      return {
        ...state,
        walletCreateStatus: action.payload.status,
        walletUpdateStatus: '',
        walletCreateMessage: action.payload.message
      }
    case WALLET_UPDATE:
      return {
        ...state,
        walletUpdateStatus: action.payload.status,
        walletCreateStatus: '',
        walletUpdateData: action.payload.data,
        walletUpdateMessage: action.payload.message
      }
    default:
      return state
  }
}
