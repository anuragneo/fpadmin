import axios from 'axios'
import * as types from './types'

const url = process.env.PUBLIC_URL

export const getAdminUsers = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users`, req)
  dispatch({ type: types.ADMIN_USERS, payload: res.data })
}

export const createAdminUsers = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users/create`, req)
  dispatch({ type: types.USERS_CREATE, payload: res.data })
  callback()
}

export const updateAdminUsers = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users/update`, req)
  dispatch({ type: types.USERS_UPDATE, payload: res.data })
  callback()
}

export const removeAdminUsers = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users/delete`, req)
  dispatch({ type: types.USERS_REMOVE, payload: res.data })
  callback()
}

export const searchUser = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/users/search`, req)
  dispatch({ type: types.ADMIN_USERS, payload: res.data })
}

export const getAdminChannel = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel`, req)
  dispatch({ type: types.ADMIN_CHANNEL, payload: res.data })
}

export const createAdminChannel = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel/create`, req)
  dispatch({ type: types.CHANNEL_CREATE, payload: res.data })
  callback()
}

export const updateAdminChannel = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel/update`, req)
  dispatch({ type: types.CHANNEL_UPDATE, payload: res.data })
  callback()
}

export const removeAdminChannel = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel/delete`, req)
  dispatch({ type: types.CHANNEL_REMOVE, payload: res.data })
  callback()
}

export const searchChannel = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/channel/search`, req)
  dispatch({ type: types.ADMIN_CHANNEL, payload: res.data })
}

export const getAdminWallet = req => async dispatch => {
  const res = await axios.post(`${url}/api/admin/wallet`, req)
  dispatch({ type: types.ADMIN_WALLET, payload: res.data })
}

export const createAdminWallet = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/wallet/create`, req)
  dispatch({ type: types.WALLET_CREATE, payload: res.data })
  callback()
}

export const updateAdminWallet = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/admin/wallet/update`, req)
  dispatch({ type: types.WALLET_UPDATE, payload: res.data })
  callback()
}
