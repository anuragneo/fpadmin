import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import Sidebar from '../../components/sidebar'
import ChannelType from './channels/ChannelType'
import WalletConfiguration from './wallets/WalletConfiguration'
import UserManagement from './users/UserManagement'

export default class Admin extends Component {

  render() {
    if (this.props.history.location.pathname === '/' || this.props.history.location.pathname === '/admin')
      this.props.history.push('/admin/user');
    return (
      <div>
        <Sidebar type="admin" />
        <Route path="/admin/user" component={UserManagement} />
        <Route path="/admin/channel" component={ChannelType} />
        <Route path="/admin/wallet" component={WalletConfiguration} />
      </div>
    )
  }
}
