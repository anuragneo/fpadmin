import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { Grid, Row, Col, Button, ButtonGroup, Glyphicon, Table, Modal } from 'react-bootstrap'
import NewUser from './NewUser'
import RemoveUser from './RemoveUser'
import EditUser from './EditUser'
import { FieldBase } from 'protobufjs'
import Search from '../../Search'
import { getAdmin } from '../../../helpers/app'

class UserManagement extends Component {
  constructor(props, context) {
    super(props, context)
    this.adminAccess = []

    this.state = {
      showModal: false,
      modalText: '',
      newUserModal: false,
      removeUserModal: false,
      editUserModal: false,
      removeUser: {},
      editUser: {},
      searchtext: '',
      clickEdit: false
    }
  }

  componentDidMount() {
    let admin = getAdmin()
    this.adminAccess = admin.adminaccess.split('')
    this.props.getAdminUsers()
  }

  newUserClose = () => this.setState({ newUserModal: false, showModal: false })
  removeUserClose = () => this.setState({ removeUserModal: false, showModal: FieldBase })
  editUserClose = () => {
    this.setState({ editUserModal: false, showModal: false })
  }
  handleClose = () => this.setState({ showModal: false, removeUserModal: false, editUserModal: false, modalText: '' })

  showRemoveModal(user) {
    this.setState({
      removeUserModal: true,
      removeUser: user
    })
  }

  showEditModal(user) {
    this.setState({
      editUserModal: true,
      editUser: user,
      clickEdit: true
    })
  }

  componentDidUpdate(prevProps) {
    if (this.props.admin !== prevProps.admin) {
      if (this.props.admin.createStatus && this.props.admin.createStatus === 'success' && this.state.newUserModal)
        this.setState({
          showModal: true,
          modalText: `User created successfully`,
          newUserModal: false
        })
      else if (
        this.props.admin.removeStatus &&
        this.props.admin.removeStatus === 'success' &&
        this.state.removeUserModal
      ) {
        this.setState({
          showModal: true,
          modalText: this.props.admin.removeMessage,
          removeUserModal: false
        })
      } else if (
        this.props.admin.updateStatus &&
        this.props.admin.updateStatus === 'success' &&
        this.state.editUserModal
      ) {
        this.setState({
          showModal: true,
          modalText: this.props.admin.updateMessage,
          editUserModal: false
        })
      }
    }
  }

  renderUserData() {
    if (this.props.admin.usersStatus === 'success') {
      if (this.props.admin.usersData.length > 0)
        return this.props.admin.usersData.map(user => {
          return (
            <tr key={user.id}>
              <td>{user.loginid}</td>
              <td>{user.firstname}</td>
              <td>{user.lastname}</td>
              <td>{user.email}</td>
              <td>{user.mobile}</td>
              <td>
                <ButtonGroup>
                  <Button
                    style={{ display: this.adminAccess[2] === '0' ? 'none' : 'block' }}
                    className="edit_btn"
                    onClick={this.showEditModal.bind(this, user)}
                  >
                    <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                  </Button>
                  <Button
                    style={{ display: this.adminAccess[3] === '0' ? 'none' : 'block' }}
                    className="remove_btn"
                    onClick={this.showRemoveModal.bind(this, user)}
                  >
                    <Glyphicon glyph=" glyphicon glyphicon-trash" /> Remove
                  </Button>
                </ButtonGroup>
              </td>
            </tr>
          )
        })
      else
        return (
          <tr>
            <td align="center" colSpan="6">
              No User Found
            </td>
          </tr>
        )
    }
  }

  render() {
    return (
      <div>
        <Col xs="10">
          <Grid fluid className="scroll">
            <Row>
              <Col xs={12}>
                <form className="searchbar">
                  <Row className="form-group">
                    <Search type="user" placeHolderText="Mobile" />
                    <Col xs={2}>
                      <Button
                        style={{ display: this.adminAccess[1] === '0' ? 'none' : 'block' }}
                        type="button"
                        bsClass="btn btn-secondary"
                        onClick={() => this.setState({ newUserModal: true })}
                      >
                        <Glyphicon glyph="glyphicon glyphicon-plus" /> Add New
                      </Button>
                    </Col>
                  </Row>
                </form>
                <Table responsive striped bordered condensed hover>
                  <thead>
                    <tr>
                      <th>Login ID</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Email ID</th>
                      <th>Mobile Number</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderUserData()}</tbody>
                </Table>
              </Col>
            </Row>
          </Grid>
        </Col>
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <NewUser show={this.state.newUserModal} onHide={this.newUserClose} />

        <EditUser show={this.state.editUserModal} onHide={this.editUserClose} user={this.state.editUser} />

        <RemoveUser show={this.state.removeUserModal} onHide={this.removeUserClose} user={this.state.removeUser} />
      </div>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}
export default connect(
  mapStateToProps,
  actions
)(UserManagement)
