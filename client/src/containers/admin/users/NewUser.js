import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { Modal, FormControl, FormGroup, ControlLabel, Checkbox, Row, Col, Button } from 'react-bootstrap'

class NewUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      modalText: '',
      loginid: '',
      password: '',
      firstname: '',
      lastname: '',
      mobile: '',
      email: '',
      adminiview: false,
      admincreate: false,
      adminupdate: false,
      admindelete: false,
      accview: false,
      accchecker: false,
      accmaker: false,
      customercare: false,
      offerview: false,
      offermaker: false,
      offerchecker: false,
      contentview: false,
      contentmaker: false,
      contentchecker: false,
      commview: false,
      commmaker: false,
      commchecker: false,
      reporting: false
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show !== nextProps.show)
      this.setState({
        showModal: false,
        modalText: '',
        loginid: '',
        password: '',
        firstname: '',
        lastname: '',
        mobile: '',
        email: '',
        adminiview: false,
        admincreate: false,
        adminupdate: false,
        admindelete: false,
        accview: false,
        accchecker: false,
        accmaker: false,
        customercare: false,
        offerview: false,
        offermaker: false,
        offerchecker: false,
        contentview: false,
        contentmaker: false,
        contentchecker: false,
        commview: false,
        commmaker: false,
        commchecker: false,
        reporting: false
      })
  }

  handleClose = () => this.setState({ showModal: false })

  onChange = event => {
    switch (event.target.name) {
      case 'loginid':
        this.setState({ loginid: event.target.value })
        break
      case 'password':
        this.setState({ password: event.target.value })
        break
      case 'firstname':
        this.setState({ firstname: event.target.value })
        break
      case 'lastname':
        this.setState({ lastname: event.target.value })
        break
      case 'email':
        this.setState({ email: event.target.value })
        break
      case 'mobile':
        this.setState({ mobile: event.target.value })
        break
      case 'adminview':
        this.setState({ adminiview: event.target.checked })
        break
      case 'admincreate':
        this.setState({ admincreate: event.target.checked })
        break
      case 'adminupdate':
        this.setState({ adminupdate: event.target.checked })
        break
      case 'admindelete':
        this.setState({ admindelete: event.target.checked })
        break
      case 'accview':
        this.setState({ accview: event.target.checked })
        break
      case 'accmaker':
        if (this.state.accchecker)
          this.setState({ accchecker: false })
        this.setState({ accmaker: event.target.checked })
        break
      case 'accchecker':
        if (this.state.accmaker)
          this.setState({ accmaker: false })
        this.setState({ accchecker: event.target.checked })
        break
      case 'customercare':
        this.setState({ customercare: event.target.checked })
        break
      case 'offerview':
        this.setState({ offerview: event.target.checked })
        break
      case 'offermaker':
        if (this.state.offerchecker)
          this.setState({ offerchecker: false })
        this.setState({ offermaker: event.target.checked })
        break
      case 'offerchecker':
        if (this.state.offermaker)
          this.setState({ offermaker: false })
        this.setState({ offerchecker: event.target.checked })
        break
      case 'contentview':
        this.setState({ contentview: event.target.checked })
        break
      case 'contentmaker':
        if (this.state.contentchecker)
          this.setState({ contentchecker: false })
        this.setState({ contentmaker: event.target.checked })
        break
      case 'contentchecker':
        if (this.state.contentmaker)
          this.setState({ contentmaker: false })
        this.setState({ contentchecker: event.target.checked })
        break
      case 'commview':
        this.setState({ commview: event.target.checked })
        break
      case 'commmaker':
        if (this.state.commchecker)
          this.setState({ commchecker: false })
        this.setState({ commmaker: event.target.checked })
        break
      case 'commchecker':
        if (this.state.commmaker)
          this.setState({ commmaker: false })
        this.setState({ commchecker: event.target.checked })
        break
      case 'reporting':
        this.setState({ reporting: event.target.checked })
        break
      default:
        break
    }
  }

  createuser = () => {
    const regxp = new RegExp(/^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)
    if (this.state.loginid === '') {
      this.setState({
        showModal: true,
        modalText: `Enter Valid LoginId`
      })
    } else if (this.state.password === '' || this.state.password.length < 8) {
      this.setState({
        showModal: true,
        modalText: `Enter 8 digit password`
      })
    } else if (this.state.firstname === '') {
      this.setState({
        showModal: true,
        modalText: `Enter FirstName`
      })
    } else if (this.state.lastname === '') {
      this.setState({
        showModal: true,
        modalText: `Enter LastName`
      })
    } else if (!regxp.test(this.state.email)) {
      this.setState({
        showModal: true,
        modalText: `Enter Valid Email`
      })
    } else if (this.state.mobile === '' || this.state.mobile.length < 10) {
      this.setState({
        showModal: true,
        modalText: `Enter Valid Mobile`
      })
    } else if (
      !this.state.adminiview &&
      !this.state.admincreate &&
      !this.state.adminupdate &&
      !this.state.addmindelete &&
      !this.state.accview &&
      !this.state.accmaker &&
      !this.state.accchecker &&
      !this.state.customercare &&
      !this.state.offerview &&
      !this.state.offermaker &&
      !this.state.offerchecker &&
      !this.state.contentview &&
      !this.state.contentmaker &&
      !this.state.contentchecker &&
      !this.state.commview &&
      !this.state.commmaker &&
      !this.state.commchecker &&
      !this.state.reporting
    )
      this.setState({
        showModal: true,
        modalText: `Enter at least one role for user`
      })
    else
      this.props.createAdminUsers(
        {
          loginid: this.state.loginid,
          password: this.state.password,
          firstname: this.state.firstname,
          lastname: this.state.lastname,
          email: this.state.email,
          mobile: this.state.mobile,
          adminactions: `${this.state.adminiview ? '1' : '0'}${this.state.admincreate ? '1' : '0'}${
            this.state.adminupdate ? '1' : '0'}${this.state.admindelete ? '1' : '0'}`,
          accountactions: `${this.state.accview ? '1' : '0'}${this.state.accmaker ? '1' : '0'}${
            this.state.accchecker ? '1' : '0'
            }`,
          customercare: `${this.state.customercare ? '1' : '0'}`,
          offersactions: `${this.state.offerview ? '1' : '0'}${this.state.offermaker ? '1' : '0'}${
            this.state.offerchecker ? '1' : '0'
            }`,
          contentactions: `${this.state.contentview ? '1' : '0'}${this.state.contentmaker ? '1' : '0'}${
            this.state.contentchecker ? '1' : '0'
            }`,
          commsactions: `${this.state.commview ? '1' : '0'}${this.state.commmaker ? '1' : '0'}${
            this.state.commchecker ? '1' : '0'
            }`,
          reporting: `${this.state.reporting ? '1' : '0'}`
        },
        () => {
          if (this.props.admin.createStatus === 'success') {
            this.props.getAdminUsers()
          } else if (this.props.admin.createStatus === 'failed') {
            this.setState({
              showModal: true,
              modalText: this.props.admin.createMessage
            })
          }
        }
      )
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">ADD NEW</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Login ID</ControlLabel>
                  <FormControl name="loginid" value={this.state.loginid} onChange={this.onChange} />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Password</ControlLabel>
                  <FormControl type="password" name="password" value={this.state.password} onChange={this.onChange} />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>First Name</ControlLabel>
                  <FormControl name="firstname" value={this.state.firstname} onChange={this.onChange} />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Last Name</ControlLabel>
                  <FormControl name="lastname" value={this.state.lastname} onChange={this.onChange} />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Email ID</ControlLabel>
                  <FormControl name="email" value={this.state.email} onChange={this.onChange} />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Mobile Number</ControlLabel>
                  <FormControl name="mobile" value={this.state.mobile} onChange={this.onChange} />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <ControlLabel>Roles</ControlLabel>
            </FormGroup>
            <Row>
              <Col xs={12}>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Administration</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox name="adminview" checked={this.state.adminiview} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Create</span>
                        <Checkbox name="admincreate" checked={this.state.admincreate} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Update</span>
                        <Checkbox name="adminupdate" checked={this.state.adminupdate} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Delete</span>
                        <Checkbox name="admindelete" checked={this.state.addmindelete} onChange={this.onChange} />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Account Management</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox name="accview" checked={this.state.accview} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Maker</span>
                        <Checkbox name="accmaker" checked={this.state.accmaker} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Checker</span>
                        <Checkbox name="accchecker" checked={this.state.accchecker} onChange={this.onChange} />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Customer Care</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox name="customercare" checked={this.state.customercare} onChange={this.onChange} />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Campaigns & Offers</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox name="offerview" checked={this.state.offerview} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Maker</span>
                        <Checkbox name="offermaker" checked={this.state.offermaker} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Checker</span>
                        <Checkbox name="offerchecker" checked={this.state.offerchecker} onChange={this.onChange} />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Content Management</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox name="contentview" checked={this.state.contentview} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Maker</span>
                        <Checkbox name="contentmaker" checked={this.state.contentmaker} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Checker</span>
                        <Checkbox name="contentchecker" checked={this.state.contentchecker} onChange={this.onChange} />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Communication</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox name="commview" checked={this.state.commview} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Maker</span>
                        <Checkbox name="commmaker" checked={this.state.commmaker} onChange={this.onChange} />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Checker</span>
                        <Checkbox name="commchecker" checked={this.state.commchecker} onChange={this.onChange} />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Reporting</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox name="reporting" checked={this.state.reporting} onChange={this.onChange} />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Row>
          </form>
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
        </Modal.Body>
        <Modal.Footer>
          <Row className="form-group">
            <Col xs={4} xsOffset={2}>
              <Button bsClass="btn btn-secondary" onClick={onHide}>
                CANCEL
              </Button>
            </Col>
            <Col xs={4}>
              <Button bsStyle="primary" onClick={this.createuser}>
                ADD
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}

export default connect(
  mapStateToProps,
  actions
)(NewUser)
