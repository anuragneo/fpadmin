import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { Modal, FormControl, FormGroup, ControlLabel, Checkbox, Row, Col, Button, Glyphicon } from 'react-bootstrap'

class EditUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      modalText: '',
      loginid: '',
      firstname: '',
      lastname: '',
      mobile: '',
      email: '',
      password: '',
      adminiview: false,
      admincreate: false,
      adminupdate: false,
      admindelete: false,
      accview: false,
      accchecker: false,
      accmaker: false,
      customercare: false,
      offerview: false,
      offermaker: false,
      offerchecker: false,
      contentview: false,
      contentmaker: false,
      contentchecker: false,
      commview: false,
      commmaker: false,
      commchecker: false,
      reporting: false,
      editloginid: true,
      editfirstname: true,
      editlastname: true,
      editmobile: true,
      editemail: true,
      editpassword: true,
      editadminiview: true,
      editadmincreate: true,
      editadminupdate: true,
      editadmindelete: true,
      editaccview: true,
      editaccchecker: true,
      editaccmaker: true,
      editcustomercare: true,
      editofferview: true,
      editoffermaker: true,
      editofferchecker: true,
      editcontentview: true,
      editcontentmaker: true,
      editcontentchecker: true,
      editcommview: true,
      editcommmaker: true,
      editcommchecker: true,
      editreporting: true
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show !== nextProps.show) {
      const { user } = nextProps
      this.setState({
        showModal: false,
        modalText: '',
        loginid: user.loginid,
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        mobile: user.mobile,
        password: '',
        editloginid: true,
        editfirstname: true,
        editlastname: true,
        editmobile: true,
        editemail: true,
        editpassword: true,
        editadminiview: true,
        editadmincreate: true,
        editadminupdate: true,
        editadmindelete: true,
        editaccview: true,
        editaccchecker: true,
        editaccmaker: true,
        editcustomercare: true,
        editofferview: true,
        editoffermaker: true,
        editofferchecker: true,
        editcontentview: true,
        editcontentmaker: true,
        editcontentchecker: true,
        editcommview: true,
        editcommmaker: true,
        editcommchecker: true,
        editreporting: true
      })

      if (user.adminaccess) {
        let adminaccess = user.adminaccess.split('')
        console.log("Hello", adminaccess[3] === '1')
        this.setState({
          adminiview: adminaccess[0] === '1',
          admincreate: adminaccess[1] === '1',
          adminupdate: adminaccess[2] === '1',
          admindelete: adminaccess[3] === '1'
        })
      }

      if (user.accountaccess) {
        let accountaccess = user.accountaccess.split('')
        this.setState({
          accview: accountaccess[0] === '1',
          accmaker: accountaccess[1] === '1',
          accchecker: accountaccess[2] === '1'
        })
      }

      if (user.commsaccess) {
        let commsaccess = user.commsaccess.split('')
        this.setState({
          commview: commsaccess[0] === '1',
          commmaker: commsaccess[1] === '1',
          commchecker: commsaccess[2] === '1'
        })
      }

      if (user.contentaccess) {
        let contentaccess = user.contentaccess.split('')
        this.setState({
          contentview: contentaccess[0] === '1',
          contentmaker: contentaccess[1] === '1',
          contentchecker: contentaccess[2] === '1'
        })
      }

      if (user.offersaccess) {
        let offersaccess = user.offersaccess.split('')
        this.setState({
          offerview: offersaccess[0] === '1',
          offermaker: offersaccess[1] === '1',
          offerchecker: offersaccess[2] === '1'
        })
      }

      if (user.reporting) {
        this.setState({ reporting: user.reporting === '1' })
      }

      if (user.customercare) {
        this.setState({ customercare: user.customercare === '1' })
      }
    }
  }

  handleClose = () => this.setState({ showModal: false })

  enableloginid = () => {
    this.setState({
      editloginid: false
    })
  }

  enablefirstname = () => {
    this.setState({
      editfirstname: false
    })
  }

  enablelastname = () => {
    this.setState({
      editlastname: false
    })
  }

  enableemail = () => {
    this.setState({
      editemail: false
    })
  }

  enablepassword = () => {
    this.setState({
      editpassword: false
    })
  }

  enablemobile = () => {
    this.setState({
      editmobile: false
    })
  }

  enableactions = () => {
    this.setState({
      editadminiview: false,
      editadmincreate: false,
      editadminupdate: false,
      editadmindelete: false,
      editaccview: false,
      editaccchecker: false,
      editaccmaker: false,
      editcustomercare: false,
      editofferview: false,
      editoffermaker: false,
      editofferchecker: false,
      editcontentview: false,
      editcontentmaker: false,
      editcontentchecker: false,
      editcommview: false,
      editcommmaker: false,
      editcommchecker: false,
      editreporting: false
    })
  }

  onChange = event => {
    switch (event.target.name) {
      case 'loginid':
        this.setState({ loginid: event.target.value })
        break
      case 'firstname':
        this.setState({ firstname: event.target.value })
        break
      case 'lastname':
        this.setState({ lastname: event.target.value })
        break
      case 'email':
        this.setState({ email: event.target.value })
        break
      case 'password':
        this.setState({ password: event.target.value })
        break
      case 'mobile':
        this.setState({ mobile: event.target.value })
        break
      case 'adminview':
        this.setState({ adminiview: event.target.checked })
        break
      case 'admincreate':
        this.setState({ admincreate: event.target.checked })
        break
      case 'adminupdate':
        this.setState({ adminupdate: event.target.checked })
        break
      case 'admindelete':
        this.setState({ admindelete: event.target.checked })
        break
      case 'accview':
        this.setState({ accview: event.target.checked })
        break
      case 'accmaker':
        if (this.state.accchecker)
          this.setState({ accchecker: false })
        this.setState({ accmaker: event.target.checked })
        break
      case 'accchecker':
        if (this.state.accmaker)
          this.setState({ accmaker: false })
        this.setState({ accchecker: event.target.checked })
        break
      case 'customercare':
        this.setState({ customercare: event.target.checked })
        break
      case 'offerview':
        this.setState({ offerview: event.target.checked })
        break
      case 'offermaker':
        if (this.state.offerchecker)
          this.setState({ offerchecker: false })
        this.setState({ offermaker: event.target.checked })
        break
      case 'offerchecker':
        if (this.state.offermaker)
          this.setState({ offermaker: false })
        this.setState({ offerchecker: event.target.checked })
        break
      case 'contentview':
        this.setState({ contentview: event.target.checked })
        break
      case 'contentmaker':
        if (this.state.contentchecker)
          this.setState({ contentchecker: false })
        this.setState({ contentmaker: event.target.checked })
        break
      case 'contentchecker':
        if (this.state.contentmaker)
          this.setState({ contentmaker: false })
        this.setState({ contentchecker: event.target.checked })
        break
      case 'commview':
        this.setState({ commview: event.target.checked })
        break
      case 'commmaker':
        if (this.state.commchecker)
          this.setState({ commchecker: false })
        this.setState({ commmaker: event.target.checked })
        break
      case 'commchecker':
        if (this.state.commmaker)
          this.setState({ commmaker: false })
        this.setState({ commchecker: event.target.checked })
        break
      case 'reporting':
        this.setState({ reporting: event.target.checked })
        break
      default:
        break
    }
  }

  createuser = () => {
    const regxp = new RegExp(/^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)
    if (this.state.loginid === '') {
      this.setState({
        showModal: true,
        modalText: `Enter Valid LoginId`
      })
    } else if (this.state.firstname === '') {
      this.setState({
        showModal: true,
        modalText: `Enter FirstName`
      })
    } else if (this.state.lastname === '') {
      this.setState({
        showModal: true,
        modalText: `Enter LastName`
      })
    } else if (!regxp.test(this.state.email)) {
      this.setState({
        showModal: true,
        modalText: `Enter Valid Email`
      })
    } else if (!this.state.editpassword && (this.state.password === '' || this.state.password.length < 8)) {
      this.setState({
        showModal: true,
        modalText: `Enter 8 digit password`
      })
    } else if (this.state.mobile === '' || this.state.mobile.length < 10) {
      this.setState({
        showModal: true,
        modalText: `Enter Valid Mobile`
      })
    } else if (
      !this.state.adminiview &&
      !this.state.admincreate &&
      !this.state.adminupdate &&
      !this.state.addmindelete &&
      !this.state.accview &&
      !this.state.accmaker &&
      !this.state.accchecker &&
      !this.state.customercare &&
      !this.state.offerview &&
      !this.state.offermaker &&
      !this.state.offerchecker &&
      !this.state.contentview &&
      !this.state.contentmaker &&
      !this.state.contentchecker &&
      !this.state.commview &&
      !this.state.commmaker &&
      !this.state.commchecker &&
      !this.state.reporting
    )
      this.setState({
        showModal: true,
        modalText: `Enter at least one role for user`
      })
    else
      this.props.updateAdminUsers(
        {
          id: this.props.user.id,
          loginid: this.state.loginid,
          firstname: this.state.firstname,
          lastname: this.state.lastname,
          email: this.state.email,
          mobile: this.state.mobile,
          password: this.state.password,
          adminactions: `${this.state.adminiview ? '1' : '0'}${this.state.admincreate ? '1' : '0'}${
            this.state.adminupdate ? '1' : '0'
            }${this.state.admindelete ? '1' : '0'}`,
          accountactions: `${this.state.accview ? '1' : '0'}${this.state.accmaker ? '1' : '0'}${
            this.state.accchecker ? '1' : '0'
            }`,
          customercare: `${this.state.customercare ? '1' : '0'}`,
          offersactions: `${this.state.offerview ? '1' : '0'}${this.state.offermaker ? '1' : '0'}${
            this.state.offerchecker ? '1' : '0'
            }`,
          contentactions: `${this.state.contentview ? '1' : '0'}${this.state.contentmaker ? '1' : '0'}${
            this.state.contentchecker ? '1' : '0'
            }`,
          commsactions: `${this.state.commview ? '1' : '0'}${this.state.commmaker ? '1' : '0'}${
            this.state.commchecker ? '1' : '0'
            }`,
          reporting: `${this.state.reporting ? '1' : '0'}`
        },
        () => {
          if (this.props.admin.updateStatus === 'success') {
            this.props.getAdminUsers()
          } else {
            this.setState({
              showModal: true,
              modalText: this.props.admin.updateMessage
            })
          }
        }
      )
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">EDIT</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>
                    Login ID
                    <Button className="edit_btn" onClick={this.enableloginid}>
                      <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                    </Button>
                  </ControlLabel>
                  <FormControl
                    disabled={this.state.editloginid}
                    name="loginid"
                    value={this.state.loginid}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>
                    First Name
                    <Button className="edit_btn" onClick={this.enablefirstname}>
                      <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                    </Button>
                  </ControlLabel>
                  <FormControl
                    disabled={this.state.editfirstname}
                    name="firstname"
                    value={this.state.firstname}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>
                    Last Name
                    <Button className="edit_btn" onClick={this.enablelastname}>
                      <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                    </Button>
                  </ControlLabel>
                  <FormControl
                    disabled={this.state.editlastname}
                    name="lastname"
                    value={this.state.lastname}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>
                    Email ID
                    <Button className="edit_btn" onClick={this.enableemail}>
                      <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                    </Button>
                  </ControlLabel>
                  <FormControl
                    disabled={this.state.editemail}
                    name="email"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>
                    Password
                    <Button className="edit_btn" onClick={this.enablepassword}>
                      <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                    </Button>
                  </ControlLabel>
                  <FormControl
                    disabled={this.state.editpassword}
                    name="password"
                    type="password"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>
                    Mobile Number
                    <Button className="edit_btn" onClick={this.enablemobile}>
                      <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                    </Button>
                  </ControlLabel>
                  <FormControl
                    disabled={this.state.editmobile}
                    name="mobile"
                    value={this.state.mobile}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <ControlLabel>
                Roles
                <Button className="edit_btn" onClick={this.enableactions}>
                  <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                </Button>
              </ControlLabel>
            </FormGroup>
            <Row>
              <Col xs={12}>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Administration</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox
                          disabled={this.state.editadminiview}
                          name="adminview"
                          checked={this.state.adminiview}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Create</span>
                        <Checkbox
                          disabled={this.state.editadmincreate}
                          name="admincreate"
                          checked={this.state.admincreate}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Update</span>
                        <Checkbox
                          disabled={this.state.editadminupdate}
                          name="adminupdate"
                          checked={this.state.adminupdate}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Delete</span>
                        <Checkbox
                          disabled={this.state.editadmindelete}
                          name="admindelete"
                          checked={this.state.admindelete}
                          onChange={this.onChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Account Management</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox
                          disabled={this.state.editaccview}
                          name="accview"
                          checked={this.state.accview}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Maker</span>
                        <Checkbox
                          disabled={this.state.editaccmaker}
                          name="accmaker"
                          checked={this.state.accmaker}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Checker</span>
                        <Checkbox
                          disabled={this.state.editaccchecker}
                          name="accchecker"
                          checked={this.state.accchecker}
                          onChange={this.onChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Customer Care</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox
                          disabled={this.state.editcustomercare}
                          name="customercare"
                          checked={this.state.customercare}
                          onChange={this.onChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Campaigns & Offers</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox
                          disabled={this.state.editofferview}
                          name="offerview"
                          checked={this.state.offerview}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Maker</span>
                        <Checkbox
                          disabled={this.state.editoffermaker}
                          name="offermaker"
                          checked={this.state.offermaker}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Checker</span>
                        <Checkbox
                          disabled={this.state.editofferchecker}
                          name="offerchecker"
                          checked={this.state.offerchecker}
                          onChange={this.onChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Content Management</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox
                          disabled={this.state.editcontentview}
                          name="contentview"
                          checked={this.state.contentview}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Maker</span>
                        <Checkbox
                          disabled={this.state.editcontentmaker}
                          name="contentmaker"
                          checked={this.state.contentmaker}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Checker</span>
                        <Checkbox
                          disabled={this.state.editcontentchecker}
                          name="contentchecker"
                          checked={this.state.contentchecker}
                          onChange={this.onChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Communication</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox
                          disabled={this.state.editcommview}
                          name="commview"
                          checked={this.state.commview}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Maker</span>
                        <Checkbox
                          disabled={this.state.editcommmaker}
                          name="commmaker"
                          checked={this.state.commmaker}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="checkbox-inline  text-center">
                        <span>Checker</span>
                        <Checkbox
                          disabled={this.state.editcommchecker}
                          name="commchecker"
                          checked={this.state.commchecker}
                          onChange={this.onChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col sm={6}>
                      <div className="mar-l-15">Reporting</div>
                    </Col>
                    <Col sm={6}>
                      <div className="checkbox-inline  text-center">
                        <span>View</span>
                        <Checkbox
                          disabled={this.state.editreporting}
                          name="reporting"
                          checked={this.state.reporting}
                          onChange={this.onChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Row>
          </form>
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
        </Modal.Body>
        <Modal.Footer>
          <Row className="form-group">
            <Col xs={4} xsOffset={2}>
              <Button bsClass="btn btn-secondary" onClick={onHide}>
                CANCEL
              </Button>
            </Col>
            <Col xs={4}>
              <Button bsStyle="primary" onClick={this.createuser}>
                UPDATE
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}

export default connect(
  mapStateToProps,
  actions
)(EditUser)
