import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { Modal, Row, Col, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'

class RemoveChannel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      comments: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      comments: ''
    })
  }

  onchange = event => {
    this.setState({
      comments: event.target.value
    })
  }

  removechannel = () => {
    this.props.removeAdminChannel(
      {
        id: this.props.channel.id,
        comments: this.state.comments
      },
      () => {
        if (this.props.admin.channelRemoveStatus === 'success') {
          this.props.getAdminChannel()
        } else if (this.props.admin.channelRemoveStatus === 'failed') {
        }
      }
    )
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">Remove</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Row>
            <Col xs={12}>
              <FormGroup>
                <ControlLabel>Comments</ControlLabel>
                <FormControl name="comments" value={this.state.comments} onChange={this.onchange} />
              </FormGroup>
            </Col>
          </Row>
        </Modal.Body>

        <Modal.Footer>
          <Row>
            <Col xs={6}>
              <Button bsStyle="secondary" onClick={onHide}>
                CANCEL
              </Button>
            </Col>
            <Col xs={6}>
              <Button bsStyle="primary" onClick={this.removechannel}>
                SUBMIT
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}

export default connect(
  mapStateToProps,
  actions
)(RemoveChannel)
