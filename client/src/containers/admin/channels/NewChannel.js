import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { Modal, Row, Col, FormGroup, ControlLabel, FormControl, Radio, Button } from 'react-bootstrap'

class NewChannel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedOption: 'POS',
      name: '',
      description: '',
      businessFormat: '1',
      showModal: false,
      modalText: ''
    }
  }

  handleClose = () => this.setState({ showModal: false })

  componentWillReceiveProps(nextProps) {
    if (this.props.show !== nextProps.show)
      this.setState({
        selectedOption: 'POS',
        name: '',
        description: '',
        businessFormat: '1',
        showModal: false,
        modalText: ''
      })
  }

  handleOptionChange = event => {
    this.setState({
      selectedOption: event.target.value
    })
  }

  onchange = event => {
    switch (event.target.name) {
      case 'name':
        this.setState({ name: event.target.value })
        break
      case 'description':
        this.setState({ description: event.target.value })
        break
      case 'businessfromat':
        this.setState({ businessFormat: event.target.value })
        break
      default:
        break
    }
  }

  addchannel = () => {
    if (this.state.name === '') {
      this.setState({
        showModal: true,
        modalText: `Enter Valid Channel Type`
      })
    } else if (this.state.description === '') {
      this.setState({
        showModal: true,
        modalText: `Enter Valid Channel Description`
      })
    } else {
      this.props.createAdminChannel(
        {
          name: this.state.name,
          source: this.state.selectedOption,
          description: this.state.description,
          businessformat: this.state.businessFormat
        },
        () => {
          if (this.props.admin.channelCreateStatus === 'success') {
            this.props.getAdminChannel()
          } else if (this.props.admin.channelCreateStatus === 'failed') {
            this.setState({
              showModal: true,
              modalText: this.props.admin.channelCreateMessage
            })
          }
        }
      )
    }
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="medium" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">ADD NEW</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Channel Type</ControlLabel>
                  <FormControl name="name" value={this.state.name} onChange={this.onchange} />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <label>Source</label>
                <FormGroup>
                  <Row>
                    <Col xs={3}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="POS"
                        checked={this.state.selectedOption === 'POS'}
                        onChange={this.handleOptionChange}
                      >
                        POS
                      </Radio>
                    </Col>
                    <Col xs={3}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="mPOS"
                        checked={this.state.selectedOption === 'mPOS'}
                        onChange={this.handleOptionChange}
                      >
                        mPOS
                      </Radio>
                    </Col>
                    <Col xs={3}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="Online"
                        checked={this.state.selectedOption === 'Online'}
                        onChange={this.handleOptionChange}
                      >
                        Online
                      </Radio>
                    </Col>
                    <Col xs={3}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="Third Party"
                        checked={this.state.selectedOption === 'Third Party'}
                        onChange={this.handleOptionChange}
                      >
                        Third Party
                      </Radio>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <FormGroup>
                  <ControlLabel>Channel Description</ControlLabel>
                  <FormControl name="description" value={this.state.description} onChange={this.onchange} />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Business Format</ControlLabel>
                  <FormControl
                    name="businessfromat"
                    onChange={this.onchange}
                    componentClass="select"
                    placeholder="select"
                  >
                    <option value="1">Big Bazar</option>
                    <option value="2">Easy Day</option>
                    <option value="3">Brand Factory</option>
                    <option value="4">Central</option>
                    <option value="5">Early Salary</option>
                  </FormControl>
                </FormGroup>
              </Col>
            </Row>
          </form>
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
        </Modal.Body>
        <Modal.Footer>
          <Row className="form-group">
            <Col xs={4} xsOffset={2}>
              <Button bsStyle="secondary" onClick={onHide}>
                CANCEL
              </Button>
            </Col>
            <Col xs={4}>
              <Button bsStyle="primary" onClick={this.addchannel}>
                ADD
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}

export default connect(
  mapStateToProps,
  actions
)(NewChannel)
