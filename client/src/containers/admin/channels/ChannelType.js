import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { Grid, Row, Col, Button, Glyphicon, ButtonGroup, Table, Modal } from 'react-bootstrap'
import moment from 'moment'
import NewChannel from './NewChannel'
import RemoveChannel from './RemoveChannel'
import EditChannel from './EditChannel'
import Search from '../../Search'
import { getAdmin } from '../../../helpers/app'

class ChannelType extends Component {
  constructor(props, context) {
    super(props, context)
    this.adminAccess = []
    this.state = {
      showModal: false,
      modalText: '',
      searchtext: '',
      newChannel: false,
      editChannelModal: false,
      removeChannelModal: false,
      editChannel: {},
      removeChannel: {}
    }
  }

  componentDidMount() {
    let admin = getAdmin()
    this.adminAccess = admin.adminaccess.split('')
    this.props.getAdminChannel()
  }

  handleClose = () => this.setState({ showModal: false })
  newChannelClose = () => this.setState({ newChannel: false })
  editChannelClose = () => this.setState({ editChannelModal: false })
  removeChannelClose = () => this.setState({ removeChannelModal: false })

  showRemoveModal(channel) {
    this.setState({
      removeChannelModal: true,
      removeChannel: channel
    })
  }

  showEditModal(channel) {
    this.setState({
      editChannelModal: true,
      editChannel: channel
    })
  }

  componentDidUpdate(prevProps) {
    if (this.props.admin !== prevProps.admin) {
      if (
        this.props.admin.channelCreateStatus &&
        this.props.admin.channelCreateStatus === 'success' &&
        this.state.newChannel
      ) {
        this.setState({
          showModal: true,
          modalText: `Channel created successfully`,
          newChannel: false
        })
      } else if (
        this.props.admin.channelRemoveStatus &&
        this.props.admin.channelRemoveStatus === 'success' &&
        this.state.removeChannelModal
      ) {
        this.setState({
          showModal: true,
          modalText: this.props.admin.channelRemoveMessage,
          removeChannelModal: false,
          searchtext: ''
        })
      } else if (
        this.props.admin.channelUpdateStatus &&
        this.props.admin.channelUpdateStatus === 'success' &&
        this.state.editChannelModal
      ) {
        this.setState({
          showModal: true,
          modalText: this.props.admin.channelUpdateMessage,
          editChannelModal: false
        })
      }
    }
  }

  renderChannelData() {
    if (this.props.admin.channelStatus === 'success') {
      if (this.props.admin.channelData.length > 0)
        return this.props.admin.channelData.map(channel => {
          let businessformat = ''
          if (channel.businessformat === '1') businessformat = 'Big Bazar'
          else if (channel.businessformat === '2') businessformat = 'Easy Day'
          else if (channel.businessformat === '3') businessformat = 'Brand Factory'
          else if (channel.businessformat === '4') businessformat = 'Central'
          else if (channel.businessformat === '5') businessformat = 'Early Salary'
          return (
            <tr key={channel.id}>
              <td>{channel.name}</td>
              <td>{channel.source}</td>
              <td>{channel.description}</td>
              <td>{businessformat}</td>
              <td>{moment(channel.created_at).format('DD/MM/YYYY')}</td>
              <td>{moment(channel.updated_at).format('DD/MM/YYYY')}</td>
              <td>
                <ButtonGroup>
                  <Button
                    style={{ display: this.adminAccess[2] === '0' ? 'none' : 'block' }}
                    className="edit_btn"
                    onClick={this.showEditModal.bind(this, channel)}
                  >
                    <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                  </Button>
                  <Button
                    style={{ display: this.adminAccess[3] === '0' ? 'none' : 'block' }}
                    className="remove_btn"
                    onClick={this.showRemoveModal.bind(this, channel)}
                  >
                    <Glyphicon glyph=" glyphicon glyphicon-trash" /> Remove
                  </Button>
                </ButtonGroup>
              </td>
            </tr>
          )
        })
      else
        return (
          <tr>
            <td align="center" colSpan="7">
              No Channel Found
            </td>
          </tr>
        )
    }
  }

  render() {
    return (
      <div>
        <Col xs="10">
          <Grid fluid className="scroll">
            <Row>
              <Col xs={12}>
                <form className="searchbar">
                  <Row className="form-group">
                    <Search type="channel" placeHolderText="Channel Type" />
                    <Col xs={2}>
                      <Button
                        style={{ display: this.adminAccess[1] === '0' ? 'none' : 'block' }}
                        type="button"
                        bsStyle="secondary"
                        onClick={() => this.setState({ newChannel: true })}
                      >
                        <Glyphicon glyph="glyphicon glyphicon-plus" /> Add New
                      </Button>
                    </Col>
                  </Row>
                </form>
                <Table responsive striped bordered condensed hover>
                  <thead>
                    <tr>
                      <th>Channel Type</th>
                      <th>Source</th>
                      <th>Channel Description</th>
                      <th>Business Format</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderChannelData()}</tbody>
                </Table>
              </Col>
            </Row>
          </Grid>
        </Col>
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <NewChannel show={this.state.newChannel} onHide={this.newChannelClose} />
        <EditChannel
          show={this.state.editChannelModal}
          onHide={this.editChannelClose}
          channel={this.state.editChannel}
        />
        <RemoveChannel
          show={this.state.removeChannelModal}
          onHide={this.removeChannelClose}
          channel={this.state.removeChannel}
        />
      </div>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}

export default connect(
  mapStateToProps,
  actions
)(ChannelType)
