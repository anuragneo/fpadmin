import React, { Component } from 'react'
import Select from 'react-select'
import { connect } from 'react-redux'
import _ from 'lodash'
import { Modal, Row, Col, FormGroup, ControlLabel, FormControl, Button, Radio } from 'react-bootstrap'
import * as actions from '../actions'

class NewWallet extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedWallet: '',
      selectedTransaction: '',
      selectedPrice: '',
      channelTypes: '',
      applicableWith: '',
      walletId: '',
      walletName: '',
      showModal: false,
      modalText: ''
    }
  }

  componentDidMount() {
    this.props.getAdminChannel()
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show !== nextProps.show)
      this.setState({
        selectedWallet: '',
        selectedTransaction: '',
        selectedPrice: '',
        channelTypes: '',
        applicableWith: '',
        walletId: '',
        walletName: '',
        showModal: false,
        modalText: ''
      })
  }

  handleClose = () => this.setState({ showModal: false })

  handleWalletChange = event => {
    this.setState({
      selectedWallet: event.target.value
    })
  }

  handleTransactionChange = event => {
    this.setState({
      selectedTransaction: event.target.value
    })
  }

  handlePriceChange = event => {
    this.setState({
      selectedPrice: event.target.value
    })
  }

  handleMultiSelect = selectedOption => {
    let values = []
    selectedOption.forEach(element => {
      values.push(element.value)
    })
    this.setState({ applicableWith: values })
  }

  handleMultiChannelSelect = selectedOption => {
    let values = []
    selectedOption.forEach(element => {
      values.push(element.value)
    })
    this.setState({ channelTypes: values })
  }

  getOptions() {
    if (this.props.admin.walletStatus === 'success') {
      let alloptions = _.filter(
        this.props.admin.walletData.details,
        wallet => wallet.wallettype && wallet.wallettype === 'Closed'
      )

      return alloptions.map(wallet => {
        return {
          value: wallet.name,
          label: wallet.name
        }
      })
    }
  }

  getChannel() {
    if (this.props.admin.channelStatus === 'success') {
      return this.props.admin.channelData.map(channel => {
        return {
          value: channel.name,
          label: channel.name
        }
      })
    }
  }

  handleClose = () => this.setState({ showModal: false })

  onchange = event => {
    switch (event.target.name) {
      case 'walletid':
        if (event.target.validity.valid) this.setState({ walletId: event.target.value })
        else event.preventDefault()
        break
      case 'walletname':
        this.setState({ walletName: event.target.value })
        break
      default:
        break
    }
  }

  addWallet = () => {
    if (this.state.walletId === '' && this.state.walletId.length < 2) {
      this.setState({
        showModal: true,
        modalText: `Enter two digit Wallet Id`
      })
    } else if (this.state.walletName === '') {
      this.setState({
        showModal: true,
        modalText: `Enter wallet name`
      })
    } else if (this.state.selectedWallet === '') {
      this.setState({
        showModal: true,
        modalText: `Select wallet type`
      })
    } else if (this.state.channelTypes === '') {
      this.setState({
        showModal: true,
        modalText: `Select channel type`
      })
    } else if (this.state.selectedWallet === 'Closed' && this.state.selectedPrice === '') {
      this.setState({
        showModal: true,
        modalText: `Select pricematch`
      })
    } else if (this.state.selectedWallet === 'Closed' && this.state.selectedTransaction === '') {
      this.setState({
        showModal: true,
        modalText: `Select supported transaction`
      })
    } else
      this.props.createAdminWallet(
        {
          channels: this.state.channelTypes,
          name: this.state.walletName,
          pricematch: this.state.selectedPrice === 'Yes' ? true : false,
          txnsupported: this.state.selectedTransaction === '' ? '2' : this.state.selectedTransaction,
          walletid: this.state.walletId,
          business: this.state.applicableWith === '' ? undefined : this.state.applicableWith,
          wallettype: this.state.selectedWallet
        },
        () => {
          if (this.props.admin.walletCreateStatus === 'success') {
            this.props.getAdminWallet()
          } else {
            this.setState({
              showModal: true,
              modalText: this.props.admin.walletCreateMessage
            })
          }
        }
      )
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="medium" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">ADD NEW</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Wallet ID</ControlLabel>
                  <FormControl
                    pattern="[0-9]*"
                    maxLength="2"
                    name="walletid"
                    value={this.state.walletId}
                    onChange={this.onchange}
                  />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Wallet Name</ControlLabel>
                  <FormControl name="walletname" value={this.state.walletName} onChange={this.onchange} />
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xs={12}>
                <label>Wallet Type</label>
                <FormGroup>
                  <Row>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="Closed"
                        checked={this.state.selectedWallet === 'Closed'}
                        onChange={this.handleWalletChange}
                      >
                        Closed
                      </Radio>
                    </Col>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="Loyalty"
                        checked={this.state.selectedWallet === 'Loyalty'}
                        onChange={this.handleWalletChange}
                      >
                        Loyalty
                      </Radio>
                    </Col>
                    <Col xs={3}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="Semi-closed"
                        checked={this.state.selectedWallet === 'Semi-closed'}
                        onChange={this.handleWalletChange}
                      >
                        Semi-closed
                      </Radio>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <FormGroup>
                  <ControlLabel>Channel Type</ControlLabel>
                  <Select
                    closeMenuOnSelect={true}
                    isMulti
                    options={this.getChannel()}
                    onChange={this.handleMultiChannelSelect}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <label>Supported Transaction</label>
                <FormGroup>
                  <Row>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup1"
                        inline
                        value="1"
                        disabled={this.state.selectedWallet === 'Loyalty'}
                        checked={this.state.selectedTransaction === '1'}
                        onChange={this.handleTransactionChange}
                      >
                        Top Up
                      </Radio>
                    </Col>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup1"
                        inline
                        value="2"
                        checked={this.state.selectedTransaction === '2'}
                        onChange={this.handleTransactionChange}
                      >
                        Spend
                      </Radio>
                    </Col>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup1"
                        inline
                        value="3"
                        disabled={this.state.selectedWallet === 'Loyalty'}
                        checked={this.state.selectedTransaction === '3'}
                        onChange={this.handleTransactionChange}
                      >
                        Both
                      </Radio>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <label>PriceMatch Allowed</label>
                <FormGroup>
                  <Row>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup2"
                        inline
                        value="Yes"
                        disabled={
                          this.state.selectedWallet === 'Loyalty' || this.state.selectedWallet === 'Semi-closed'
                        }
                        checked={this.state.selectedPrice === 'Yes'}
                        onChange={this.handlePriceChange}
                      >
                        Yes
                      </Radio>
                    </Col>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup2"
                        inline
                        value="No"
                        disabled={
                          this.state.selectedWallet === 'Loyalty' || this.state.selectedWallet === 'Semi-closed'
                        }
                        checked={this.state.selectedPrice === 'No'}
                        onChange={this.handlePriceChange}
                      >
                        No
                      </Radio>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Applicable With</ControlLabel>
                  <Select
                    isDisabled={this.state.selectedWallet === 'Closed'}
                    closeMenuOnSelect={true}
                    isMulti
                    options={this.getOptions()}
                    onChange={this.handleMultiSelect}
                  />
                </FormGroup>
              </Col>
            </Row>
          </div>
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
        </Modal.Body>
        <Modal.Footer>
          <Row className="form-group">
            <Col xs={4} xsOffset={2}>
              <Button bsStyle="secondary" onClick={onHide}>
                CANCEL
              </Button>
            </Col>
            <Col xs={4}>
              <Button bsStyle="primary" onClick={this.addWallet}>
                ADD
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}

export default connect(
  mapStateToProps,
  actions
)(NewWallet)
