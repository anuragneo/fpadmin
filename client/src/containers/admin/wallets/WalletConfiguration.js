import React, { Component } from 'react'
import { Grid, Row, Col, Button, Glyphicon, Table, Modal } from 'react-bootstrap'
import { connect } from 'react-redux'
import * as actions from '../actions'
import NewWallet from './NewWallet'
import EditWallet from './EditWallet'
import Search from '../../Search'
import { getAdmin } from '../../../helpers/app'

class WalletConfiguration extends Component {
  constructor(props, context) {
    super(props, context)
    this.adminAccess = []
    this.state = {
      showModal: false,
      modalText: '',
      newWallet: false,
      removeWalletModal: false,
      editWalletModal: false,
      editWallet: {},
      removeWallet: {}
    }
  }

  componentDidMount() {
    let admin = getAdmin()
    this.adminAccess = admin.adminaccess.split('')
    this.props.getAdminWallet()
  }

  newWalletClose = () => this.setState({ newWallet: false })
  editWalletClose = () => this.setState({ editWalletModal: false })
  removeWalletClose = () => this.setState({ removeWalletModal: false })
  handleClose = () => this.setState({ showModal: false })

  showEditModal(wallet) {
    this.setState({
      editWalletModal: true,
      editWallet: wallet
    })
  }

  showRemoveModal(wallet) {
    this.setState({
      removeWalletModal: true,
      removeWallet: wallet
    })
  }

  componentDidUpdate(prevProps) {
    if (this.props.admin !== prevProps.admin) {
      if (
        this.props.admin.walletCreateStatus &&
        this.props.admin.walletCreateStatus === 'success' &&
        this.state.newWallet
      ) {
        this.setState({
          showModal: true,
          modalText: `Wallet created successfully`,
          newWallet: false
        })
      } else if (
        this.props.admin.walletUpdateStatus &&
        this.props.admin.walletUpdateStatus === 'success' &&
        this.state.editWalletModal
      ) {
        this.setState({
          showModal: true,
          modalText: this.props.admin.walletUpdateData.msg,
          editWalletModal: false
        })
      }
    }
  }

  searchWallet = event => {
    event.preventDefault()
  }

  renderWalletData() {
    if (this.props.admin.walletStatus === 'success') {
      if (this.props.admin.walletData.details.length > 0)
        return this.props.admin.walletData.details.map(wallet => {
          let transactions
          if (wallet.txnsupported === 1) {
            transactions = 'Top Up'
          } else if (wallet.txnsupported === 2) {
            transactions = 'Spend'
          } else if (wallet.txnsupported === 3) {
            transactions = 'Top Up, Spend'
          }
          return (
            <tr key={`${wallet.id}${wallet.wallettype}`}>
              <td>{wallet.walletid}</td>
              <td>{wallet.name}</td>
              <td>{wallet.wallettype}</td>
              <td>{wallet.channels.join(',')}</td>
              <td>{transactions}</td>
              <td>{wallet.pricematch ? `Yes` : `No`}</td>
              <td>{wallet.business && wallet.business.length > 0 ? wallet.business.join(',') : `N/A`}</td>
              <td>{wallet.created}</td>
              <td>{wallet.updated}</td>
              <td>{wallet.isactive ? `Active` : `Deactivated`}</td>
              <td>
                <Button
                  style={{ display: this.adminAccess[2] === '0' ? 'none' : 'block' }}
                  className="edit_btn"
                  onClick={this.showEditModal.bind(this, wallet)}
                >
                  <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                </Button>
              </td>
            </tr>
          )
        })
      else
        return (
          <tr>
            <td align="center" colSpan="11">
              No Wallet Found
            </td>
          </tr>
        )
    }
  }

  render() {
    return (
      <div>
        <Col xs="10">
          <Grid fluid className="scroll">
            <Row>
              <Col xs={12}>
                <form className="searchbar">
                  <Row className="form-group">
                    <Search type="wallet" placeHolderText="Enter Wallet Name" />
                    <Col xs={2}>
                      <Button
                        style={{ display: this.adminAccess[1] === '0' ? 'none' : 'block' }}
                        type="button"
                        bsStyle="secondary"
                        onClick={() => this.setState({ newWallet: true })}
                      >
                        <Glyphicon glyph="glyphicon glyphicon-plus" /> Add New
                      </Button>
                    </Col>
                  </Row>
                </form>
                <Table responsive striped bordered condensed hover>
                  <thead>
                    <tr>
                      <th>Wallet ID</th>
                      <th>Wallet Name</th>
                      <th>Wallet Type</th>
                      <th>Channel Types</th>
                      <th>Supported Txns</th>
                      <th>Price Match</th>
                      <th>Applicable With</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderWalletData()}</tbody>
                </Table>
              </Col>
            </Row>
          </Grid>
        </Col>
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <NewWallet show={this.state.newWallet} onHide={this.newWalletClose} />
        <EditWallet show={this.state.editWalletModal} onHide={this.editWalletClose} wallet={this.state.editWallet} />
      </div>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}

export default connect(
  mapStateToProps,
  actions
)(WalletConfiguration)
