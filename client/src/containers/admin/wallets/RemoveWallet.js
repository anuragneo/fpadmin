import React, { Component } from 'react'
import { Modal } from 'react-bootstrap'

class RemoveWallet extends Component {
  render() {
    console.log(this.props.user)
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body />
        <Modal.Footer />
      </Modal>
    )
  }
}

export default RemoveWallet
