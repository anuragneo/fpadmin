import React, { Component } from 'react'
import { Col, FormControl, FormGroup, Button, Glyphicon, Modal } from 'react-bootstrap'
import * as actions from '../actions'
import { connect } from 'react-redux'

class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchtext: '',
      showModal: false,
      modalText: ''
    }
  }

  onchange = e => {
    this.setState({ searchtext: e.target.value }, () => {
      if (this.state.searchtext.length === 0) {
        if (this.props.type === 'user') this.props.getAdminUsers()
        else if (this.props.type === 'channel') this.props.getAdminChannel()
        else if (this.props.type === 'wallet') this.props.getAdminWallet()
        else if (this.props.type === 'communication') this.props.commsList({ count: 10, pagenumber: this.props.page })
        else if (this.props.type === 'banner') this.props.getBanners()
      }
    })
  }

  handleClose = () => this.setState({ showModal: false })

  searchuser = e => {
    e.preventDefault()
    if (this.props.type === 'user') {
      if (this.state.searchtext.length === 10) {
        this.props.searchUser({
          mobile: this.state.searchtext
        })
      } else {
        this.setState({
          showModal: true,
          modalText: 'Enter Valid Mobile'
        })
      }
    } else if (this.props.type === 'channel') {
      if (this.state.searchtext.length < 3)
        this.setState({
          showModal: true,
          modalText: `Enter Valid Channel Type`
        })
      else
        this.props.searchChannel({
          name: this.state.searchtext
        })
    } else if (this.props.type === 'wallet') {
      if (this.state.searchtext.length < 3)
        this.setState({
          showModal: true,
          modalText: `Enter Valid Wallet Name`
        })
      else
        this.props.searchWallet({
          name: this.state.searchtext
        })
    } else if (this.props.type === 'communication') {
      if (this.state.searchtext.length < 3)
        this.setState({
          showModal: true,
          modalText: `Enter Valid Event Name`
        })
      else
        this.props.commsSearch({
          text: this.state.searchtext
        })
    } else if (this.props.type === 'banner') {
      if (this.state.searchtext.length < 3)
        this.setState({
          showModal: true,
          modalText: `Enter Valid banner Name`
        })
      else
        this.props.bannerSearch({
          name: this.state.searchtext
        })
    }
  }

  render() {
    return (
      <React.Fragment>
        <Col xs={this.props.type === 'communication' ? 9 : 8}>
          <FormGroup>
            <FormControl
              type="text"
              placeholder={this.props.placeHolderText}
              onChange={this.onchange}
              value={this.state.searchtext}
            />
          </FormGroup>
        </Col>
        <Col xs={this.props.type === 'communication' ? 3 : 2}>
          <Button type="submit" bsStyle="primary" onClick={this.searchuser}>
            <Glyphicon glyph="glyphicon glyphicon-search" /> Search
          </Button>
        </Col>
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    )
  }
}

export default connect(
  null,
  actions
)(Search)
