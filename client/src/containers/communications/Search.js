import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import Search from '../Search'
import ChangeComm from './ChangeComm'
import RemoveComm from './RemoveComm'
import EditComm from './EditCommunication'
import { getAdmin } from '../../helpers/app'
import { Glyphicon, Col, PageHeader, Grid, Row, Table, Button, Modal, Pager } from 'react-bootstrap'

class CommunicationsSearch extends Component {
  constructor(props) {
    super(props)
    this.commAccess = []
    this.state = {
      toggleComm: false,
      togglecomm: {},
      removeComm: false,
      removecomm: {},
      editComm: false,
      editcomm: {},
      showModal: false,
      modalText: '',
      pagenumber: 0
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.commsReducer !== prevProps.commsReducer) {
      if (
        this.props.commsReducer.toggleCommStatus &&
        this.props.commsReducer.toggleCommStatus === 'success' &&
        this.state.toggleComm
      ) {
        this.setState({
          toggleComm: false,
          showModal: true,
          modalText: 'Status of the communication changed successfully'
        })
      } else if (
        this.props.commsReducer.updateCommStatus &&
        this.props.commsReducer.updateCommStatus === 'success' &&
        this.state.editComm
      ) {
        this.setState({
          pagenumber: 0,
          editComm: false,
          showModal: true,
          modalText: 'Communication updated successfully'
        })
      }
    }
  }

  handleClose = () => this.setState({ showModal: false })
  toggleCommClose = () => this.setState({ toggleComm: false })
  removeCommClose = () => this.setState({ removeComm: false })
  editCommClose = () => this.setState({ editComm: false })

  showtoggle(comm) {
    this.setState({
      toggleComm: true,
      togglecomm: comm
    })
  }

  showremove(comm) {
    this.setState({
      removeComm: true,
      removecomm: comm
    })
  }

  showedit(comm) {
    this.setState({
      editComm: true,
      editcomm: comm
    })
  }

  componentDidMount() {
    let admin = getAdmin()
    this.commAccess = admin.commsaccess.split('')
    this.props.commsList({ count: 10, pagenumber: 0 })
  }

  callComms = type => {
    if (type === 'previous') {
      if (this.state.pagenumber > 0) {
        this.setState(
          {
            pagenumber: this.state.pagenumber - 1
          },
          () => this.props.commsList({ count: 10, pagenumber: this.state.pagenumber })
        )
      }
    } else if (type === 'next') {
      if (this.state.pagenumber >= 0) {
        this.setState(
          {
            pagenumber: this.state.pagenumber + 1
          },
          () => this.props.commsList({ count: 10, pagenumber: this.state.pagenumber })
        )
      }
    }
  }

  renderCommsTable() {
    if (this.props.commsReducer.commsListStatus === 'success') {
      if (this.props.commsReducer.commsListData.comms.length > 0)
        return this.props.commsReducer.commsListData.comms.map(comm => {
          return (
            <tr key={comm.id}>
              <td>{comm.id}</td>
              <td>{comm.description}</td>
              <td>{comm.event === '' ? `Manual` : `Event`}</td>
              <td>
                {/* <ButtonGroup className="coms-search_btn"> */}
                {
                  <Button
                    bsStyle="primary"
                    disabled={this.commAccess[1] === '0' && this.commAccess[2] === '0' ? true : false}
                    className={comm.status === 'active' ? 'btn-primary-small top-up' : 'btn-primary-small'}
                    onClick={
                      comm.status === 'active' || comm.status === 'inactive' || comm.status === 'scheduled'
                        ? this.showtoggle.bind(this, comm)
                        : undefined
                    }
                  >
                    {comm.status}
                  </Button>
                }
                {/* </ButtonGroup> */}
              </td>
              <td>{comm.created_at}</td>
              <td>{comm.updated_at}</td>
              <td>
                <Button
                  style={{ display: this.commAccess[1] === '0' && this.commAccess[2] === '0' ? 'none' : 'block' }}
                  className="edit_btn"
                  onClick={this.showedit.bind(this, comm)}
                >
                  <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                </Button>
              </td>
            </tr>
          )
        })
      else
        return (
          <tr>
            <td align="center" colSpan="7">
              No Communication Found
            </td>
          </tr>
        )
    }
  }

  render() {
    return (
      <Col xs="10" className="scroll">
        <PageHeader>SEARCH</PageHeader>
        <Grid fluid className="coms_search">
          <Row>
            <Col xs={12}>
              <form className="searchbar form-group">
                <Row>
                  <Search type="communication" placeHolderText="Enter text" page={this.state.pagenumber} />
                </Row>
              </form>
              <Table responsive striped bordered condensed hover>
                <thead>
                  <tr>
                    <th>
                      Communica-
                      <br />
                      tions ID
                    </th>
                    <th>
                      Communica-
                      <br />
                      tions Name
                    </th>
                    <th>Event/Manual Trigger</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>{this.renderCommsTable()}</tbody>
              </Table>
              <Pager>
                <Pager.Item
                  previous
                  disabled={this.state.pagenumber === 0}
                  onClick={this.callComms.bind(this, 'previous')}
                >
                  &larr; Previous
                </Pager.Item>
                <Pager.Item next onClick={this.callComms.bind(this, 'next')}>
                  Next &rarr;
                </Pager.Item>
              </Pager>
            </Col>
          </Row>
        </Grid>
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <ChangeComm
          show={this.state.toggleComm}
          onHide={this.toggleCommClose}
          comm={this.state.togglecomm}
          page={this.state.pagenumber}
        />
        <RemoveComm show={this.state.removeComm} onHide={this.removeCommClose} comm={this.state.removecomm} />
        <EditComm show={this.state.editComm} onHide={this.editCommClose} comm={this.state.editcomm} />
      </Col>
    )
  }
}

function mapStateToProps({ commsReducer }) {
  return { commsReducer }
}

export default connect(
  mapStateToProps,
  actions
)(CommunicationsSearch)
