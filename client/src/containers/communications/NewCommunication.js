import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import aws from 'aws-sdk'
import uuidv1 from 'uuid/v1'
import * as actions from './actions'
import {
  Glyphicon,
  Row,
  Col,
  Grid,
  FormGroup,
  FormControl,
  Button,
  PageHeader,
  ControlLabel,
  Checkbox,
  Radio,
  Modal
} from 'react-bootstrap'
import 'react-datepicker/dist/react-datepicker.css'
import PushPreview from './PushPreview'
import InAppPreview from './InAppPreview'

const renderField = ({ input, disabled, checked, type, className, min, meta: { touched, error } }) => (
  <div className="text-danger">
    <input {...input} disabled={disabled} type={type} min={min} className={className} checked={checked} />
    {touched && (error && <span>{error}</span>)}
  </div>
)

const renderTextArea = ({ input, disabled, className, meta: { touched, error } }) => (
  <div className="text-danger">
    <textarea {...input} disabled={disabled} className={className} rows="10" cols="40" />
    {touched && (error && <span>{error}</span>)}
  </div>
)

const required = value => (value ? undefined : 'This field is required')
const phoneNumber = value => (value && !/^[0-9]*$/i.test(value) ? 'Must be a number' : undefined)

class NewCommunication extends Component {
  constructor(props) {
    super(props)
    this.manualFileRef = React.createRef()
    this.emailFileRef = React.createRef()
    this.pushFileRef = React.createRef()
    this.inAppFileRef = React.createRef()
    this.state = {
      commsevent: '',
      deeplink: '{"screen_id":1}',
      inAppDeeplink: '{"screen_id":1}',
      typePush: false,
      typeEmail: false,
      typeInApp: false,
      typeSms: false,
      emailImmediate: true,
      smsImmediate: true,
      pushImmediate: true,
      emailSchedule: false,
      pushSchedule: false,
      smsSchedule: false,
      selectedOption: 'event',
      emailOption: 'url',
      pushStartDate: moment(),
      pushStartTime: moment(),
      smsStartDate: moment(),
      smsStartTime: moment(),
      emailStartDate: moment(),
      emailStartTime: moment(),
      fileToSend: null,
      s3url: '',
      showModal: false,
      modalText: '',
      showVariableModal: false,

      //preview
      previewData: {},
      showPreview: false,
      showInAppPreview: false,
      pushHeader: '',
      pushContent: '',
      smsText: '',
      emailcontent: '',
      emailUrl: '',
      pushUrl: '',
      inAppUrl: '',

      manualFilekey: '',
      pushFileKey: '',
      inAppFileKey: '',
      emailFileKey: ''
    }
  }

  handleClose = () => {
    this.setState({ showModal: false })
  }
  variableModalClose = () => {
    this.setState({ showVariableModal: false })
  }
  showPreviewClose = () => {
    this.setState({ showPreview: false })
  }

  inAppPreviewClose = () => {
    this.setState({ showInAppPreview: false })
  }

  emailPreview = () => {
    let newWindow = window.open()
    newWindow.document.write(this.state.emailcontent)
  }

  openInAppPreview = () => {
    this.setState({
      previewData: {
        header: this.state.inAppHeader,
        content: this.state.inAppContent,
        multimedia: this.state.inAppUrl
      },
      showInAppPreview: true
    })
  }

  openPreview = type => {
    if (type === 'push') {
      this.setState({
        previewData: {
          header: this.state.pushHeader,
          content: this.state.pushContent
        }
      })
    } else if (type === 'sms') {
      this.setState({
        previewData: {
          header: '',
          content: this.state.smsText
        }
      })
    }
    this.setState({ showPreview: true })
  }

  showVariable = () => {
    this.setState({ showVariableModal: true })
  }

  onChange = e => {
    switch (e.target.name) {
      case 'pushcheck':
        this.setState({ typePush: e.target.checked })
        break
      case 'smscheck':
        this.setState({ typeSms: e.target.checked })
        break
      case 'inappcheck':
        this.setState({ typeInApp: e.target.checked })
        break
      case 'emailcheck':
        this.setState({ typeEmail: e.target.checked })
        break
      case 'emailimmediate':
        this.setState({ emailImmediate: e.target.checked }, () => {
          this.state.emailImmediate
            ? this.setState({
                emailSchedule: false
              })
            : this.setState({
                emailSchedule: true
              })
        })
        break
      case 'pushimmediate':
        this.setState({ pushImmediate: e.target.checked }, () => {
          this.state.pushImmediate
            ? this.setState({
                pushSchedule: false
              })
            : this.setState({
                pushSchedule: true
              })
        })
        break
      case 'smsimmediate':
        this.setState({ smsImmediate: e.target.checked }, () => {
          this.state.smsImmediate
            ? this.setState({
                smsSchedule: false
              })
            : this.setState({
                smsSchedule: true
              })
        })
        break
      case 'emailschedule':
        this.setState({ emailSchedule: e.target.checked }, () => {
          this.state.emailSchedule
            ? this.setState({
                emailImmediate: false
              })
            : this.setState({
                emailImmediate: true
              })
        })
        break
      case 'pushschedule':
        this.setState({ pushSchedule: e.target.checked }, () => {
          this.state.pushSchedule
            ? this.setState({
                pushImmediate: false
              })
            : this.setState({
                pushImmediate: true
              })
        })
        break
      case 'smsschedule':
        this.setState({ smsSchedule: e.target.checked }, () => {
          this.state.smsSchedule
            ? this.setState({
                smsImmediate: false
              })
            : this.setState({
                smsImmediate: true
              })
        })
        break
      case 'eventselect':
        this.setState({ commsevent: e.target.value }, () => {
          this.props.getEvent({ name: this.state.commsevent })
          this.props.checkEvent(
            {
              text: this.state.commsevent
            },
            () => {
              if (this.props.commsReducer.checkEventsStatus === 'success') {
                if (this.props.commsReducer.checkEventsData.comms.length > 0) {
                  let existEvent = this.props.commsReducer.checkEventsData.comms.find(function(element) {
                    return element.status === 'active'
                  })

                  if (existEvent) {
                    this.setState({
                      showModal: true,
                      modalText: `There is already an active communication for this event. Name: ${
                        existEvent.description
                      },
                      event: ${existEvent.event}, id: ${existEvent.id}`
                    })
                  }
                }
              }
            }
          )
        })
        break
      case 'deeplink':
        this.setState({ deeplink: e.target.value })
        break
      case 'inappdeeplink':
        this.setState({ inAppDeeplink: e.target.value })
        break
      case 'userfile':
        this.uploadMultiMedia('manual', e.target.files[0])
        break
      case 'fileforEmail':
        this.uploadMultiMedia('email', e.target.files[0])
        break
      case 'fileforInApp':
        this.uploadMultiMedia('inapp', e.target.files[0])
        break
      case 'fileforPush':
        this.uploadMultiMedia('push', e.target.files[0])
        break
      case 'header':
        this.setState({ pushHeader: e.target.value })
        break
      case 'content':
        this.setState({ pushContent: e.target.value })
        break
      case 'inappheader':
        this.setState({ inAppHeader: e.target.value })
        break
      case 'inappcontent':
        this.setState({ inAppContent: e.target.value })
        break
      case 'smscontent':
        this.setState({ smsText: e.target.value })
        break
      case 'emailurl':
        this.setState({ emailUrl: e.target.value })
        break
      case 'emailcontent':
        this.setState({ emailcontent: e.target.value })
        break
      case 'multimediaurl':
        this.setState({ pushUrl: e.target.value })
        break
      case 'inappmultimediaurl':
        this.setState({ inAppUrl: e.target.value })
        break
      case 's3url':
        this.setState({ s3url: e.target.value })
        break
      default:
        break
    }
  }

  uploadMultiMedia = (type, file) => {
    aws.config.update({
      accessKeyId: 'AKIAICTUEHDGBAQZNNRA',
      secretAccessKey: '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'
    })

    const s3bucket = new aws.S3({ params: { Bucket: 'fpstaging' } })
    const params = {
      Key: uuidv1(),
      ContentType: file.type,
      Body: file
    }
    s3bucket.upload(params, (err, data) => {
      if (data && !err) {
        if (type === 'email') this.setState({ emailUrl: data.Location })
        else if (type === 'push') this.setState({ pushUrl: data.Location })
        else if (type === 'inapp') this.setState({ inAppUrl: data.Location })
        else if (type === 'manual') this.setState({ s3url: data.key })
      }
    })
  }

  handleOptionChange = event => {
    this.setState({
      selectedOption: event.target.value
    })
  }

  handleEmailOptionChange = event => {
    this.setState({
      emailOption: event.target.value,
      emailUrl: '',
      emailcontent: ''
    })
  }

  componentDidMount() {
    this.props.eventList({}, () => {
      if (this.props.commsReducer.eventListStatus === 'success') {
        // this.setState({
        //   commsevent: this.props.commsReducer.eventListData.event[0]
        // })
        // this.props.getEvent({ name: this.props.commsReducer.eventListData.event[0] })
      }
    })
  }

  smsDateChange = date => {
    this.setState({
      smsStartDate: date
    })
  }

  smsTimeChange = date => {
    this.setState({
      smsStartTime: date
    })
  }

  emailDateChange = date => {
    this.setState({
      emailStartDate: date
    })
  }

  emailTimeChange = date => {
    this.setState({
      emailStartTime: date
    })
  }

  pushDateChange = date => {
    this.setState({
      pushStartDate: date
    })
  }

  pushTimeChange = date => {
    this.setState({
      pushStartTime: date
    })
  }

  renderEventData() {
    if (this.props.commsReducer.eventListStatus === 'success') {
      return this.props.commsReducer.eventListData.event.map((event, pos) => {
        return (
          <option key={pos} value={event}>
            {event}
          </option>
        )
      })
    }
  }

  unixTime(date, time) {
    const totalTime = `${date} ${time}`
    return moment(totalTime, 'MM.DD.YYYY HH:mm')
      .utc()
      .valueOf()
      .toString()
  }

  onSubmit = values => {
    if (this.state.selectedOption === 'event' && this.state.commsevent === '') {
      this.setState({
        showModal: true,
        modalText: 'Select one event'
      })
    } else if (!this.state.typePush && !this.state.typeInApp && !this.state.typeSms && !this.state.typeEmail) {
      this.setState({
        showModal: true,
        modalText: 'Select at least one Channel'
      })
    } else if (this.state.selectedOption === 'manual' && this.state.s3url === '') {
      this.setState({
        showModal: true,
        modalText: 'Uplaod file for manual communication'
      })
    } else {
      const pushData = {
        header: values.header,
        multimediaurl: this.state.pushUrl,
        content: values.content,
        deeplink: JSON.parse(this.state.deeplink),
        daystolive: parseInt(values.daystolive, 10)
      }
      const inAppData = {
        header: values.inappheader,
        multimediaurl: this.state.inAppUrl,
        content: values.inappcontent,
        deeplink: JSON.parse(this.state.inAppDeeplink),
        daystolive: parseInt(values.inappdaystolive, 10)
      }
      const emailData = {
        content: this.state.emailOption === 'content' ? this.state.emailcontent : undefined,
        url: this.state.emailOption === 'url' ? this.state.emailUrl : undefined,
        subject: values.emailsubject
      }
      const data = {
        event: this.state.selectedOption === 'event' ? this.state.commsevent : '',
        description: values.description,
        smscontent: this.state.typeSms ? values.smscontent : undefined,
        pushcontent: this.state.typePush ? pushData : undefined,
        inappcontent: this.state.typeInApp ? inAppData : undefined,
        emailercontent: this.state.typeEmail ? emailData : undefined,
        smsstarttime:
          this.state.smsSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.smsStartDate.format('MM.DD.YYYY'), this.state.smsStartTime.format('HH:mm'))
            : undefined,
        emailstarttime:
          this.state.emailSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.emailStartDate.format('MM.DD.YYYY'), this.state.emailStartTime.format('HH:mm'))
            : undefined,
        pushstarttime:
          this.state.pushSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.pushStartDate.format('MM.DD.YYYY'), this.state.pushStartTime.format('HH:mm'))
            : undefined,
        referenceid: values.referenceid,
        referencename: values.referencename,
        s3url: this.state.s3url
      }
      this.props.createComm(data, () => {
        if (this.props.commsReducer.newCommStatus === 'success') {
          this.props.reset()
          this.setState({
            showModal: true,
            modalText: 'Communication created successfully',
            manualFilekey: Math.random().toString(36),
            pushFileKey: Math.random().toString(36),
            emailFileKey: Math.random().toString(36),
            inAppFileKey: Math.random().toString(36),
            typePush: false,
            typeEmail: false,
            typeInApp: false,
            typeSms: false,
            emailImmediate: true,
            smsImmediate: true,
            pushImmediate: true,
            emailSchedule: false,
            pushSchedule: false,
            smsSchedule: false,
            selectedOption: 'event',
            deeplink: '{"screen_id":1}',
            inAppDeeplink: '{"screen_id":1}',
            commsevent: this.props.commsReducer.eventListData.event[0],
            pushUrl: '',
            emailUrl: '',
            inAppUrl: '',
            s3url: ''
          })
        } else if (this.props.commsReducer.newCommStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.commsReducer.newCommMessage
          })
        }
      })
    }
  }

  onSaveSubmit = values => {
    if (this.state.selectedOption === 'event' && this.state.commsevent === '') {
      this.setState({
        showModal: true,
        modalText: 'Select one event'
      })
    } else if (!this.state.typePush && !this.state.typeInApp && !this.state.typeSms && !this.state.typeEmail) {
      this.setState({
        showModal: true,
        modalText: 'Select at least one Channel'
      })
    } else if (this.state.selectedOption === 'manual' && this.state.s3url === '') {
      this.setState({
        showModal: true,
        modalText: 'Uplaod file for manual communication'
      })
    } else {
      const pushData = {
        header: values.header,
        multimediaurl: this.state.pushUrl,
        content: values.content,
        deeplink: JSON.parse(this.state.deeplink),
        daystolive: parseInt(values.daystolive, 10)
      }
      const inAppData = {
        header: values.inappheader,
        multimediaurl: this.state.inAppUrl,
        content: values.inappcontent,
        deeplink: JSON.parse(this.state.inAppDeeplink),
        daystolive: parseInt(values.inappdaystolive, 10)
      }
      const emailData = {
        content: this.state.emailOption === 'content' ? this.state.emailcontent : undefined,
        url: this.state.emailOption === 'url' ? this.state.emailUrl : undefined,
        subject: values.emailsubject
      }
      const data = {
        event: this.state.selectedOption === 'event' ? this.state.commsevent : '',
        description: values.description,
        smscontent: this.state.typeSms ? values.smscontent : undefined,
        pushcontent: this.state.typePush ? pushData : undefined,
        inappcontent: this.state.typeInApp ? inAppData : undefined,
        emailercontent: this.state.typeEmail ? emailData : undefined,
        smsstarttime:
          this.state.smsSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.smsStartDate.format('MM.DD.YYYY'), this.state.smsStartTime.format('HH:mm'))
            : undefined,
        emailstarttime:
          this.state.emailSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.emailStartDate.format('MM.DD.YYYY'), this.state.emailStartTime.format('HH:mm'))
            : undefined,
        pushstarttime:
          this.state.pushSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.pushStartDate.format('MM.DD.YYYY'), this.state.pushStartTime.format('HH:mm'))
            : undefined,
        referenceid: values.referenceid,
        referencename: values.referencename,
        s3url: this.state.s3url,
        status: 'pending'
      }
      this.props.createComm(data, () => {
        if (this.props.commsReducer.newCommStatus === 'success') {
          this.props.reset()
          this.setState({
            showModal: true,
            modalText: 'Communication created successfully',
            manualFilekey: Math.random().toString(36),
            pushFileKey: Math.random().toString(36),
            emailFileKey: Math.random().toString(36),
            inAppFileKey: Math.random().toString(36),
            typePush: false,
            typeEmail: false,
            typeInApp: false,
            typeSms: false,
            emailImmediate: true,
            smsImmediate: true,
            pushImmediate: true,
            emailSchedule: false,
            pushSchedule: false,
            smsSchedule: false,
            selectedOption: 'event',
            deeplink: '{"screen_id":1}',
            inAppDeeplink: '{"screen_id":1}',
            commsevent: this.props.commsReducer.eventListData.event[0],
            pushUrl: '',
            emailUrl: '',
            inAppUrl: '',
            s3url: ''
          })
        } else if (this.props.commsReducer.newCommStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.commsReducer.newCommMessage
          })
        }
      })
    }
  }

  reset = () => {
    this.props.reset()
    this.setState({
      manualFilekey: Math.random().toString(36),
      pushFileKey: Math.random().toString(36),
      emailFileKey: Math.random().toString(36),
      inAppFileKey: Math.random().toString(36),
      typePush: false,
      typeEmail: false,
      typeInApp: false,
      typeSms: false,
      emailImmediate: true,
      smsImmediate: true,
      pushImmediate: true,
      emailSchedule: false,
      pushSchedule: false,
      smsSchedule: false,
      selectedOption: 'event',
      deeplink: '{"screen_id":1}',
      pushUrl: '',
      emailUrl: '',
      inAppUrl: '',
      s3url: ''
    })
  }

  showFile = type => {
    if (type === 'email') this.emailFileRef.current.click()
    else if (type === 'inapp') this.inAppFileRef.current.click()
    else if (type === 'push') this.pushFileRef.current.click()
    else if (type === 'manual') this.manualFileRef.current.click()
  }

  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props
    return (
      <Col xs="10" className="scroll">
        <PageHeader>ADD</PageHeader>
        <Grid fluid>
          <Row>
            <Col xs={10} xsOffset={1}>
              <form>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Communication Name</ControlLabel>
                      <Field
                        className="form-control"
                        name="description"
                        type="text"
                        component={renderField}
                        validate={[required]}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Reference ID</ControlLabel>
                      <Field
                        className="form-control"
                        name="referenceid"
                        type="text"
                        component={renderField}
                        validate={[required]}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Reference Name</ControlLabel>
                      <Field
                        className="form-control"
                        name="referencename"
                        type="text"
                        component={renderField}
                        validate={[required]}
                      />
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col xs={12}>
                    <label className="form-group">Communication Trigger</label>
                    <FormGroup>
                      <Row>
                        <Col xs={2}>
                          <Radio
                            name="radioGroup"
                            inline
                            value="event"
                            checked={this.state.selectedOption === 'event'}
                            onChange={this.handleOptionChange}
                          >
                            Event
                          </Radio>
                        </Col>
                        <Col xs={2}>
                          <Radio
                            name="radioGroup"
                            inline
                            value="manual"
                            checked={this.state.selectedOption === 'manual'}
                            onChange={this.handleOptionChange}
                          >
                            Manual
                          </Radio>
                        </Col>
                        <Col xs={4} xsOffset={2}>
                          <input
                            type="text"
                            name="s3url"
                            className="form-control"
                            value={this.state.s3url}
                            disabled={this.state.selectedOption === 'event'}
                            onChange={this.onChange}
                          />
                        </Col>
                        <Col xs={2}>
                          <input
                            type="file"
                            name="userfile"
                            key={this.state.manualFilekey || ''}
                            ref={this.manualFileRef}
                            accept=".xlsx, .xls, .csv"
                            style={{ display: 'none' }}
                            onChange={this.onChange}
                          />
                          <Button
                            bsStyle="primary"
                            onClick={this.showFile.bind(this, 'manual')}
                            disabled={this.state.selectedOption === 'event'}
                          >
                            <Glyphicon glyph="glyphicon glyphicon-open" />
                            Upload
                          </Button>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs={6}>
                    <FormGroup>
                      <ControlLabel>Select Event</ControlLabel>
                      <select
                        className="form-control"
                        name="eventselect"
                        onChange={this.onChange}
                        value={this.state.commsevent}
                      >
                        <option value="">Select</option>
                        {this.renderEventData()}
                      </select>
                    </FormGroup>
                  </Col>
                  <Col xs={6}>
                    <h3 className="personalize_var" onClick={this.showVariable}>
                      Personalization Variable
                      <Glyphicon glyph="glyphicon glyphicon-info-sign" />
                    </h3>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <label>Select Channel</label>
                    <FormGroup>
                      <Row>
                        <Col xs={2}>
                          <Checkbox name="pushcheck" inline checked={this.state.typePush} onChange={this.onChange}>
                            Push
                          </Checkbox>
                        </Col>
                        <Col xs={2}>
                          <Checkbox name="inappcheck" inline checked={this.state.typeInApp} onChange={this.onChange}>
                            In App
                          </Checkbox>
                        </Col>
                        <Col xs={2}>
                          <Checkbox name="smscheck" inline checked={this.state.typeSms} onChange={this.onChange}>
                            SMS
                          </Checkbox>
                        </Col>
                        <Col xs={2}>
                          <Checkbox name="emailcheck" inline checked={this.state.typeEmail} onChange={this.onChange}>
                            Email
                          </Checkbox>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <Row>
                  <Col xs={6}>
                    <FormGroup>
                      <h4 className="acct_manage_hist_label">SMS</h4>{' '}
                    </FormGroup>
                  </Col>
                  <Col xs={6}>
                    <FormGroup className="preview">
                      <ControlLabel onClick={this.openPreview.bind(this, 'sms')}>
                        <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                        PREVIEW
                      </ControlLabel>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <FormGroup>
                      <ControlLabel>Text</ControlLabel>
                      <Field
                        className="form-control"
                        name="smscontent"
                        type="text"
                        component={renderField}
                        value={this.state.smsText}
                        onChange={this.onChange}
                        validate={this.state.typeSms ? [required] : undefined}
                        disabled={!this.state.typeSms}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Sender ID</ControlLabel>
                      <FormControl disabled={!this.state.typeSms} />
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <h4 className="acct_manage_hist_label">Push</h4>
                    </FormGroup>
                  </Col>
                  <Col xs={6}>
                    <FormGroup className="preview">
                      <ControlLabel onClick={this.openPreview.bind(this, 'push')}>
                        <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                        PREVIEW
                      </ControlLabel>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <FormGroup>
                      <ControlLabel>Header</ControlLabel>
                      <Field
                        className="form-control"
                        name="header"
                        type="text"
                        value={this.state.pushHeader}
                        onChange={this.onChange}
                        component={renderField}
                        validate={this.state.typePush ? [required] : undefined}
                        disabled={!this.state.typePush}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={10}>
                    <FormGroup>
                      <ControlLabel>Multimedia URL</ControlLabel>
                      <input
                        className="form-control"
                        name="multimediaurl"
                        type="text"
                        value={this.state.pushUrl}
                        onChange={this.onChange}
                        disabled={!this.state.typePush}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={2}>
                    <FormGroup>
                      <input
                        type="file"
                        accept="image/*"
                        name="fileforPush"
                        key={this.state.pushFileKey || ''}
                        ref={this.pushFileRef}
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                      <Button
                        bsStyle="primary"
                        className="mt26"
                        onClick={this.showFile.bind(this, 'push')}
                        disabled={!this.state.typePush}
                      >
                        <Glyphicon glyph="glyphicon glyphicon-open" />
                        Upload
                      </Button>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <FormGroup>
                      <ControlLabel>Content</ControlLabel>
                      <Field
                        className="form-control"
                        name="content"
                        type="text"
                        value={this.state.pushContent}
                        onChange={this.onChange}
                        component={renderField}
                        validate={this.state.typePush ? [required] : undefined}
                        disabled={!this.state.typePush}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <FormGroup>
                      <ControlLabel>Deeplink Screen</ControlLabel>
                      <select
                        className="form-control"
                        name="deeplink"
                        value={this.state.deeplink}
                        onChange={this.onChange}
                        disabled={!this.state.typePush}
                      >
                        <option value='{"screen_id":1}'>Dashboard</option>
                        <option value='{"screen_id":2}'>Instant OTP</option>
                        <option value='{"screen_id":3, "index":1}'>Load Money BB</option>
                        <option value='{"screen_id":3, "index":2}'>Load Money CW</option>
                        <option value='{"screen_id":3, "index":3}'>Load Money BF</option>
                        <option value='{"screen_id":3, "index":4}'>Load Money ED</option>
                        <option value='{"screen_id":3, "index":5}'>Load Money OW</option>
                        <option value='{"screen_id":4}'>Link Payback</option>
                        <option value='{"screen_id":5}'>Link BBPC</option>
                        <option value='{"screen_id":6}'>Price Match</option>
                        <option value='{"screen_id":7, "index":1}'>My Wallet</option>
                        <option value='{"screen_id":7, "index":2}'>My Wallet BB</option>
                        <option value='{"screen_id":7, "index":3}'>My Wallet CW</option>
                        <option value='{"screen_id":7, "index":4}'>My Wallet BF</option>
                        <option value='{"screen_id":7, "index":5}'>My Wallet ED</option>
                      </select>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Notification Valid For (No. of Days)</ControlLabel>
                      <Field
                        className="form-control"
                        name="daystolive"
                        type="number"
                        min="1"
                        component={renderField}
                        validate={this.state.typePush ? [required, phoneNumber] : undefined}
                        disabled={!this.state.typePush}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <h4 className="acct_manage_hist_label">In App</h4>
                    </FormGroup>
                  </Col>
                  <Col xs={6}>
                    <FormGroup className="preview">
                      <ControlLabel onClick={this.openInAppPreview}>
                        <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                        PREVIEW
                      </ControlLabel>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <FormGroup>
                      <ControlLabel>Header</ControlLabel>
                      <Field
                        className="form-control"
                        name="inappheader"
                        type="text"
                        component={renderField}
                        value={this.state.inAppHeader}
                        onChange={this.onChange}
                        validate={this.state.typeInApp ? [required] : undefined}
                        disabled={!this.state.typeInApp}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={10}>
                    <FormGroup>
                      <ControlLabel>Multimedia URL</ControlLabel>
                      <input
                        className="form-control"
                        name="inappmultimediaurl"
                        type="text"
                        value={this.state.inAppUrl}
                        onChange={this.onChange}
                        disabled={!this.state.typeInApp}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={2}>
                    <FormGroup>
                      <input
                        type="file"
                        accept="image/*"
                        name="fileforInApp"
                        key={this.state.inAppFileKey || ''}
                        ref={this.inAppFileRef}
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                      <Button
                        bsStyle="primary"
                        className="mt26"
                        onClick={this.showFile.bind(this, 'inapp')}
                        disabled={!this.state.typeInApp}
                      >
                        <Glyphicon glyph="glyphicon glyphicon-open" />
                        Upload
                      </Button>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <FormGroup>
                      <ControlLabel>Content</ControlLabel>
                      <Field
                        className="form-control"
                        name="inappcontent"
                        type="text"
                        component={renderField}
                        value={this.state.inAppContent}
                        onChange={this.onChange}
                        validate={this.state.typeInApp ? [required] : undefined}
                        disabled={!this.state.typeInApp}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <FormGroup>
                      <ControlLabel>Deeplink Screen</ControlLabel>
                      <select
                        className="form-control"
                        name="inappdeeplink"
                        value={this.state.inAppDeeplink}
                        onChange={this.onChange}
                        disabled={!this.state.typeInApp}
                      >
                        <option value='{"screen_id":1}'>Dashboard</option>
                        <option value='{"screen_id":2}'>Instant OTP</option>
                        <option value='{"screen_id":3, "index":1}'>Load Money BB</option>
                        <option value='{"screen_id":3, "index":2}'>Load Money CW</option>
                        <option value='{"screen_id":3, "index":3}'>Load Money BF</option>
                        <option value='{"screen_id":3, "index":4}'>Load Money ED</option>
                        <option value='{"screen_id":3, "index":5}'>Load Money OW</option>
                        <option value='{"screen_id":4}'>Link Payback</option>
                        <option value='{"screen_id":5}'>Link BBPC</option>
                        <option value='{"screen_id":6}'>Price Match</option>
                        <option value='{"screen_id":7, "index":1}'>My Wallet</option>
                        <option value='{"screen_id":7, "index":2}'>My Wallet BB</option>
                        <option value='{"screen_id":7, "index":3}'>My Wallet CW</option>
                        <option value='{"screen_id":7, "index":4}'>My Wallet BF</option>
                        <option value='{"screen_id":7, "index":5}'>My Wallet ED</option>
                      </select>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Notification Valid For (No. of Days)</ControlLabel>
                      <Field
                        className="form-control"
                        name="inappdaystolive"
                        type="number"
                        min="1"
                        component={renderField}
                        validate={this.state.typeInApp ? [required, phoneNumber] : undefined}
                        disabled={!this.state.typeInApp}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <h4 className="acct_manage_hist_label">Email</h4>
                    </FormGroup>
                  </Col>
                  <Col xs={6}>
                    <FormGroup className="preview">
                      <ControlLabel onClick={this.emailPreview}>
                        <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                        PREVIEW
                      </ControlLabel>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Subject</ControlLabel>
                      <Field
                        className="form-control"
                        name="emailsubject"
                        type="text"
                        component={renderField}
                        validate={this.state.typeEmail ? [required] : undefined}
                        disabled={!this.state.typeEmail}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <FormGroup>
                      <Row>
                        <Col xs={2}>
                          <Radio
                            name="radioGroupemail"
                            inline
                            value="url"
                            checked={this.state.emailOption === 'url'}
                            onChange={this.handleEmailOptionChange}
                            disabled={!this.state.typeEmail}
                          >
                            URL
                          </Radio>
                        </Col>
                        <Col xs={2}>
                          <Radio
                            name="radioGroupemail"
                            inline
                            value="content"
                            checked={this.state.emailOption === 'content'}
                            onChange={this.handleEmailOptionChange}
                            disabled={!this.state.typeEmail}
                          >
                            Content
                          </Radio>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>HTML URL</ControlLabel>
                      <input
                        className="form-control"
                        name="emailurl"
                        type="text"
                        value={this.state.emailUrl}
                        onChange={this.onChange}
                        disabled={!this.state.typeEmail || this.state.emailOption !== 'url'}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={2}>
                    <FormGroup>
                      <input
                        type="file"
                        name="fileforEmail"
                        key={this.state.emailFileKey || ''}
                        ref={this.emailFileRef}
                        accept=".html"
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                      <Button
                        bsStyle="primary"
                        className="mt26"
                        onClick={this.showFile.bind(this, 'email')}
                        disabled={!this.state.typeEmail || this.state.emailOption !== 'url'}
                      >
                        <Glyphicon glyph="glyphicon glyphicon-open" />
                        Upload
                      </Button>
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col sm={6}>
                    <ControlLabel>HTML Content</ControlLabel>
                    <FormGroup>
                      <textarea
                        name="emailcontent"
                        rows="10"
                        className="form-control"
                        cols="40"
                        value={this.state.emailcontent}
                        onChange={this.onChange}
                        disabled={!this.state.typeEmail || this.state.emailOption !== 'content'}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                {this.state.selectedOption === 'manual' ? (
                  <React.Fragment>
                    <hr />
                    <Row>
                      <Col sm={6}>
                        <FormGroup>
                          <ControlLabel>SMS</ControlLabel>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={2}>
                        <Checkbox
                          className="mt28"
                          name="smsimmediate"
                          inline
                          checked={this.state.smsImmediate}
                          onChange={this.onChange}
                        >
                          Immediate
                        </Checkbox>
                      </Col>
                      <Col xs={2}>
                        <Checkbox
                          className="mt28"
                          name="smsschedule"
                          inline
                          checked={this.state.smsSchedule}
                          onChange={this.onChange}
                        >
                          Schedule
                        </Checkbox>
                      </Col>
                      <Col xs={4}>
                        <FormGroup>
                          <ControlLabel>Start Date</ControlLabel>
                          <DatePicker
                            className="form-control"
                            dateFormat="DD/MM/YYYY"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            selected={this.state.smsStartDate}
                            onChange={this.smsDateChange}
                            disabled={!this.state.smsSchedule}
                          />
                        </FormGroup>
                      </Col>
                      <Col xs={4}>
                        <FormGroup>
                          <ControlLabel>Start Time</ControlLabel>
                          <DatePicker
                            className="form-control"
                            selected={this.state.smsStartTime}
                            onChange={this.smsTimeChange}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            dateFormat="LT"
                            timeCaption="Time"
                            disabled={!this.state.smsSchedule}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm={6}>
                        <FormGroup>
                          <ControlLabel>Push</ControlLabel>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={2}>
                        <Checkbox
                          className="mt28"
                          name="pushimmediate"
                          inline
                          checked={this.state.pushImmediate}
                          onChange={this.onChange}
                        >
                          Immediate
                        </Checkbox>
                      </Col>
                      <Col xs={2}>
                        <Checkbox
                          className="mt28"
                          name="pushschedule"
                          inline
                          checked={this.state.pushSchedule}
                          onChange={this.onChange}
                        >
                          Schedule
                        </Checkbox>
                      </Col>
                      <Col xs={4}>
                        <FormGroup>
                          <ControlLabel>Start Date</ControlLabel>
                          <DatePicker
                            className="form-control"
                            dateFormat="DD/MM/YYYY"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            selected={this.state.pushStartDate}
                            onChange={this.pushDateChange}
                            disabled={!this.state.pushSchedule}
                          />
                        </FormGroup>
                      </Col>
                      <Col xs={4}>
                        <FormGroup>
                          <ControlLabel>Start Time</ControlLabel>
                          <DatePicker
                            className="form-control"
                            selected={this.state.pushStartTime}
                            onChange={this.pushTimeChange}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            dateFormat="LT"
                            timeCaption="Time"
                            disabled={!this.state.pushSchedule}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm={6}>
                        <FormGroup>
                          <ControlLabel>Email</ControlLabel>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row className="form-group">
                      <Col xs={2} className="form-group">
                        <Checkbox
                          className="mt28"
                          name="emailimmediate"
                          inline
                          checked={this.state.emailImmediate}
                          onChange={this.onChange}
                        >
                          Immediate
                        </Checkbox>
                      </Col>
                      <Col xs={2} className="form-group">
                        <Checkbox
                          className="mt28"
                          name="emailschedule"
                          inline
                          checked={this.state.emailSchedule}
                          onChange={this.onChange}
                        >
                          Schedule
                        </Checkbox>
                      </Col>
                      <Col xs={4} className="form-group">
                        <FormGroup>
                          <ControlLabel>Start Date</ControlLabel>
                          <DatePicker
                            className="form-control"
                            dateFormat="DD/MM/YYYY"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            selected={this.state.emailStartDate}
                            onChange={this.emailDateChange}
                            disabled={!this.state.emailSchedule}
                          />
                        </FormGroup>
                      </Col>
                      <Col xs={4} className="form-group">
                        <FormGroup>
                          <ControlLabel>Start Time</ControlLabel>
                          <DatePicker
                            className="form-control"
                            selected={this.state.emailStartTime}
                            onChange={this.emailTimeChange}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            dateFormat="LT"
                            timeCaption="Time"
                            disabled={!this.state.emailSchedule}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </React.Fragment>
                ) : null}
                <Row>
                  <Col sm={4}>
                    <Button type="button" bsStyle="secondary" disabled={pristine || submitting} onClick={this.reset}>
                      <Glyphicon glyph="glyphicon glyphicon-repeat" /> Reset
                    </Button>
                  </Col>
                  <Col sm={4}>
                    <Button bsStyle="primary" onClick={handleSubmit(this.onSaveSubmit)}>
                      SAVE
                    </Button>
                  </Col>
                  <Col sm={4}>
                    <Button bsStyle="primary" type="submit" disabled={submitting} onClick={handleSubmit(this.onSubmit)}>
                      SUBMIT
                    </Button>
                  </Col>
                </Row>
              </form>
              <Modal show={this.state.showVariableModal} onHide={this.variableModalClose}>
                <Modal.Header closeButton />
                <Modal.Body>
                  {this.props.commsReducer.eventData && this.props.commsReducer.eventData.event.length > 0 ? (
                    <div>
                      {this.props.commsReducer.eventData.event.map((ev, index) => {
                        return <h4 key={index}>{`$${ev}`}</h4>
                      })}
                    </div>
                  ) : (
                    <p>No variable found</p>
                  )}
                </Modal.Body>
              </Modal>
              <Modal show={this.state.showModal} onHide={this.handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title />
                </Modal.Header>
                <Modal.Body>
                  <p>{this.state.modalText}</p>
                </Modal.Body>
                <Modal.Footer>
                  <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                    Cancel
                  </Button>
                </Modal.Footer>
              </Modal>
              <PushPreview show={this.state.showPreview} onHide={this.showPreviewClose} data={this.state.previewData} />
              <InAppPreview
                show={this.state.showInAppPreview}
                onHide={this.inAppPreviewClose}
                data={this.state.previewData}
              />
            </Col>
          </Row>
        </Grid>
      </Col>
    )
  }
}

function mapStateToProps({ commsReducer }) {
  return { commsReducer }
}

export default reduxForm({
  form: 'newcommunication'
})(
  connect(
    mapStateToProps,
    actions
  )(NewCommunication)
)
