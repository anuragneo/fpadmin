import React, { Component } from 'react'
import { Modal, Image } from 'react-bootstrap'
import logo from '../../images/fplogo.jpg'

class PushPreview extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
      content: '',
      type: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show != nextProps.show) {
      this.setState({
        title: nextProps.data.header,
        content: nextProps.data.content
      })
    }
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal
        className="preview_push_sms"
        show={show}
        onHide={onHide}
        bsSize="small"
        aria-labelledby="contained-modal-title-lg"
      >
        <div className="cust_not">
          <Image src={logo} />
          <div className="not_text">
            <div className="not_title">{this.state.title}</div>
            <div className="not-sub_title">{this.state.content}</div>
          </div>
        </div>
      </Modal>
    )
  }
}

export default PushPreview
