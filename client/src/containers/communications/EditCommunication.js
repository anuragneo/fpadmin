import React, { Component } from 'react'
import { connect } from 'react-redux'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import aws from 'aws-sdk'
import uuidv1 from 'uuid/v1'
import * as actions from './actions'
import PushPreview from './PushPreview'
import InAppPreview from './InAppPreview'
import {
  Glyphicon,
  Row,
  Col,
  FormGroup,
  FormControl,
  Button,
  ControlLabel,
  Checkbox,
  Radio,
  Modal
} from 'react-bootstrap'
import 'react-datepicker/dist/react-datepicker.css'

class EditComm extends Component {
  constructor(props) {
    super(props)
    this.manualFileRef = React.createRef()
    this.emailFileRef = React.createRef()
    this.pushFileRef = React.createRef()
    this.inAppFileRef = React.createRef()
    this.state = {
      commsevent: '',
      commId: '',
      commName: '',
      refId: '',
      refName: '',
      typePush: false,
      typeEmail: false,
      typeInApp: false,
      typeSms: false,
      emailImmediate: true,
      smsImmediate: true,
      pushImmediate: true,
      emailSchedule: false,
      pushSchedule: false,
      smsSchedule: false,
      smscontent: '',
      emailcontent: '',
      emailsubject: '',
      emailhtml: '',
      header: '',
      content: '',
      pushUrl: '',
      deeplink: '',
      daystolive: '',
      inappheader: '',
      inappcontent: '',
      inappdeeplink: '',
      inappdaystolive: '',
      inAppUrl: '',
      emailUrl: '',

      emailOption: '',
      emailSchedule: false,
      pushSchedule: false,
      smsSchedule: false,
      selectedOption: '',
      pushStartDate: moment(),
      pushStartTime: moment(),
      smsStartDate: moment(),
      smsStartTime: moment(),
      emailStartDate: moment(),
      emailStartTime: moment(),
      fileToSend: null,
      s3url: '',
      showModal: false,
      modalText: '',
      showVariableModal: false,
      selectedStatus: '',

      //preview
      previewData: {},
      showPreview: false,
      showInAppPreview: false,

      manualFilekey: '',
      pushFileKey: '',
      inAppFileKey: '',
      emailFileKey: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.show !== nextProps.show) {
      const { comm } = nextProps
      if (nextProps.show) {
        this.props.getEvent({ name: comm.event === '' ? this.props.commsReducer.eventListData.event[0] : comm.event })
      }
      let emailOption = 'url'
      if (comm.emailercontent && comm.emailercontent.url !== '') {
        emailOption = 'url'
      } else if (comm.emailercontent && comm.emailercontent.content !== '') {
        emailOption = 'content'
      } else {
        emailOption = 'url'
      }
      this.setState({
        showVariableModal: false,
        selectedOption: comm.event === '' ? 'manual' : 'event',
        selectedStatus: comm.status,
        commsevent: comm.event !== '' ? comm.event : this.props.commsReducer.eventListData.event[0],
        commId: comm.id,
        commName: comm.description,
        refId: comm.referenceid,
        refName: comm.referencename,
        emailImmediate: comm.emailstarttime === '' ? true : false,
        smsImmediate: comm.smsstarttime === '' ? true : false,
        pushImmediate: comm.pushstarttime === '' ? true : false,
        typePush: comm.pushcontent ? true : false,
        typeEmail: comm.emailercontent ? true : false,
        typeInApp: comm.inappcontent ? true : false,
        typeSms: comm.smscontent === '' ? false : true,
        smscontent: comm.smscontent,

        emailcontent: comm.emailercontent ? comm.emailercontent.content : '',
        emailsubject: comm.emailercontent ? comm.emailercontent.subject : '',
        emailUrl: comm.emailercontent ? comm.emailercontent.url : '',

        emailOption: emailOption,
        header: comm.pushcontent ? comm.pushcontent.header : '',
        content: comm.pushcontent ? comm.pushcontent.content : '',
        daystolive: comm.pushcontent ? comm.pushcontent.daystolive : '',
        deeplink: comm.pushcontent ? JSON.stringify(comm.pushcontent.deeplink) : '{"screen_id":1}',
        pushUrl: comm.pushcontent ? comm.pushcontent.multimediaurl : '',

        inappheader: comm.inappcontent ? comm.inappcontent.header : '',
        inappcontent: comm.inappcontent ? comm.inappcontent.content : '',
        inappdeeplink: comm.inappcontent ? JSON.stringify(comm.inappcontent.deeplink) : '{"screen_id":1}',
        inappdaystolive: comm.inappcontent ? comm.inappcontent.daystolive : '',
        inAppUrl: comm.inappcontent ? comm.inappcontent.multimediaurl : '',

        emailSchedule: comm.emailstarttime === '' ? false : true,
        pushSchedule: comm.pushstarttime === '' ? false : true,
        smsSchedule: comm.smsstarttime === '' ? false : true,

        fileToSend: null,
        s3url: comm.s3url,
        //preview
        previewData: {},
        showPreview: false,

        pushStartDate: comm.pushstarttime === '' ? moment() : moment(comm.pushstarttime, 'DD/MM/YYYY'),
        pushStartTime: comm.pushstarttime === '' ? moment() : moment(comm.pushstarttime, 'hh:mm'),
        smsStartDate: comm.smsstarttime === '' ? moment() : moment(comm.smsstarttime, 'DD/MM/YYYY'),
        smsStartTime: comm.smsstarttime === '' ? moment() : moment(comm.smsstarttime, 'hh:mm'),
        emailStartDate: comm.emailstarttime === '' ? moment() : moment(comm.emailstarttime, 'DD/MM/YYYY'),
        emailStartTime: comm.emailstarttime === '' ? moment() : moment(comm.emailstarttime, 'hh:mm'),

        manualFilekey: Math.random().toString(36),
        pushFileKey: Math.random().toString(36),
        emailFileKey: Math.random().toString(36),
        inAppFileKey: Math.random().toString(36)
      })
    }
  }

  handleClose = () => {
    this.setState({ showModal: false })
  }
  variableModalClose = () => {
    this.setState({ showVariableModal: false })
  }
  showPreviewClose = () => {
    this.setState({ showPreview: false })
  }

  emailPreview = () => {
    let newWindow = window.open()
    newWindow.document.write(this.state.emailcontent)
  }

  inAppPreviewClose = () => {
    this.setState({ showInAppPreview: false })
  }

  openInAppPreview = () => {
    this.setState({
      previewData: {
        header: this.state.inappheader,
        content: this.state.inappcontent,
        multimedia: this.state.inAppUrl
      },
      showInAppPreview: true
    })
  }

  openPreview = type => {
    if (type === 'push') {
      this.setState({
        previewData: {
          header: this.state.header,
          content: this.state.content
        }
      })
    } else if (type === 'sms') {
      this.setState({
        previewData: {
          header: '',
          content: this.state.smscontent
        }
      })
    }
    this.setState({ showPreview: true })
  }

  showVariable = () => {
    this.setState({ showVariableModal: true })
  }

  onChange = e => {
    switch (e.target.name) {
      case 'pushcheck':
        this.setState({ typePush: e.target.checked })
        break
      case 'smscheck':
        this.setState({ typeSms: e.target.checked })
        break
      case 'inappcheck':
        this.setState({ typeInApp: e.target.checked })
        break
      case 'emailcheck':
        this.setState({ typeEmail: e.target.checked })
        break
      case 'emailimmediate':
        this.setState({ emailImmediate: e.target.checked }, () => {
          this.state.emailImmediate
            ? this.setState({
                emailSchedule: false
              })
            : this.setState({
                emailSchedule: true
              })
        })
        break
      case 'pushimmediate':
        this.setState({ pushImmediate: e.target.checked }, () => {
          this.state.pushImmediate
            ? this.setState({
                pushSchedule: false
              })
            : this.setState({
                pushSchedule: true
              })
        })
        break
      case 'smsimmediate':
        this.setState({ smsImmediate: e.target.checked }, () => {
          this.state.smsImmediate
            ? this.setState({
                smsSchedule: false
              })
            : this.setState({
                smsSchedule: true
              })
        })
        break
      case 'emailschedule':
        this.setState({ emailSchedule: e.target.checked }, () => {
          this.state.emailSchedule
            ? this.setState({
                emailImmediate: false
              })
            : this.setState({
                emailImmediate: true
              })
        })
        break
      case 'pushschedule':
        this.setState({ pushSchedule: e.target.checked }, () => {
          this.state.pushSchedule
            ? this.setState({
                pushImmediate: false
              })
            : this.setState({
                pushImmediate: true
              })
        })
        break
      case 'smsschedule':
        this.setState({ smsSchedule: e.target.checked }, () => {
          this.state.smsSchedule
            ? this.setState({
                smsImmediate: false
              })
            : this.setState({
                smsImmediate: true
              })
        })
        break
      case 'eventselect':
        this.setState({ commsevent: e.target.value }, () => {
          this.props.getEvent({ name: this.state.commsevent })
          this.props.checkEvent(
            {
              text: this.state.commsevent
            },
            () => {
              if (this.props.commsReducer.checkEventsStatus === 'success') {
                if (this.props.commsReducer.checkEventsData.comms.length > 0) {
                  let existEvent = this.props.commsReducer.checkEventsData.comms.find(function(element) {
                    return element.status === 'active'
                  })

                  if (existEvent) {
                    this.setState({
                      showModal: true,
                      modalText: `There is already an active communication for this event. Name: ${
                        existEvent.description
                      },
                      event: ${existEvent.event}, id: ${existEvent.id}`
                    })
                  }
                }
              }
            }
          )
        })
        break
      case 'smscontent':
        this.setState({ smscontent: e.target.value })
        break
      case 'emailcontent':
        this.setState({ emailcontent: e.target.value })
        break
      case 'emailsubject':
        this.setState({ emailsubject: e.target.value })
        break
      case 'deeplink':
        this.setState({ deeplink: e.target.value })
        break
      case 'content':
        this.setState({ content: e.target.value })
        break
      case 'multimediaurl':
        this.setState({ pushUrl: e.target.value })
        break
      case 'inappurl':
        this.setState({ inAppUrl: e.target.value })
        break
      case 'inappcontent':
        this.setState({ inappcontent: e.target.value })
        break
      case 'inappheader':
        this.setState({ inappheader: e.target.value })
        break
      case 'inappdaystolive':
        this.setState({ inappdaystolive: e.target.value })
        break
      case 'inappdeeplink':
        this.setState({ inappdeeplink: e.target.value })
        break
      case 'emailurl':
        this.setState({ emailUrl: e.target.value })
        break
      case 'userfile':
        this.uploadMultiMedia('manual', e.target.files[0])
        break
      case 'fileforEmail':
        this.uploadMultiMedia('email', e.target.files[0])
        break
      case 'fileforInApp':
        this.uploadMultiMedia('inapp', e.target.files[0])
        break
      case 'fileforPush':
        this.uploadMultiMedia('push', e.target.files[0])
        break
      case 'header':
        this.setState({ header: e.target.value })
        break
      case 'daystolive':
        this.setState({ daystolive: e.target.value })
        break
      case 'commname':
        this.setState({ commName: e.target.value })
        break
      case 'referencename':
        this.setState({ refName: e.target.value })
        break
      case 'referenceid':
        this.setState({ refId: e.target.value })
        break
      default:
        break
    }
  }

  handleOptionChange = event => {
    this.setState({
      selectedOption: event.target.value
    })
  }

  handleEmailOptionChange = event => {
    this.setState({
      emailOption: event.target.value,
      emailUrl: '',
      emailcontent: ''
    })
  }

  handleStatusChange = event => {
    this.setState({
      selectedStatus: event.target.value
    })
  }

  componentDidMount() {
    this.props.eventList({}, () => {})
  }

  showFile = type => {
    if (type === 'email') this.emailFileRef.current.click()
    else if (type === 'inapp') this.inAppFileRef.current.click()
    else if (type === 'push') this.pushFileRef.current.click()
    else if (type === 'manual') this.manualFileRef.current.click()
  }

  smsDateChange = date => {
    this.setState({
      smsStartDate: date
    })
  }

  smsTimeChange = date => {
    this.setState({
      smsStartTime: date
    })
  }

  emailDateChange = date => {
    this.setState({
      emailStartDate: date
    })
  }

  emailTimeChange = date => {
    this.setState({
      emailStartTime: date
    })
  }

  pushDateChange = date => {
    this.setState({
      pushStartDate: date
    })
  }

  pushTimeChange = date => {
    this.setState({
      pushStartTime: date
    })
  }

  renderEventData() {
    if (this.props.commsReducer.eventListStatus === 'success') {
      return this.props.commsReducer.eventListData.event.map((event, pos) => {
        return (
          <option key={pos} value={event}>
            {event}
          </option>
        )
      })
    }
  }

  unixTime(date, time) {
    const totalTime = `${date} ${time}`
    return moment(totalTime, 'MM.DD.YYYY HH:mm')
      .utc()
      .valueOf()
      .toString()
  }

  uploadMultiMedia = (type, file) => {
    aws.config.update({
      accessKeyId: 'AKIAICTUEHDGBAQZNNRA',
      secretAccessKey: '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'
    })

    const s3bucket = new aws.S3({ params: { Bucket: 'fpstaging' } })
    const params = {
      Key: uuidv1(),
      ContentType: file.type,
      Body: file
    }
    s3bucket.upload(params, (err, data) => {
      if (data && !err) {
        if (type === 'email') this.setState({ emailUrl: data.Location })
        else if (type === 'push') this.setState({ pushUrl: data.Location })
        else if (type === 'inapp') this.setState({ inAppUrl: data.Location })
        else if (type === 'manual') this.setState({ s3url: data.key })
      }
    })
  }

  editComm = () => {
    if (this.state.commName === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter Communication name'
      })
    } else if (this.state.refName === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter reference name'
      })
    } else if (this.state.refId === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter reference id'
      })
    } else if (this.state.selectedOption === 'manual' && this.state.s3url === '') {
      this.setState({
        showModal: true,
        modalText: 'Uplaod file for manual communication'
      })
    } else if (!this.state.typePush && !this.state.typeInApp && !this.state.typeSms && !this.state.typeEmail) {
      this.setState({
        showModal: true,
        modalText: 'Select at least one Channel'
      })
    } else if (this.state.typeSms && this.state.smscontent === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter sms text'
      })
    } else if (this.state.typePush && this.state.header === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter push header'
      })
    } else if (this.state.typePush && this.state.content === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter push content'
      })
    } else if (this.state.typePush && this.state.daystolive === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter days for push notification to valid'
      })
    } else if (this.state.typeInApp && this.state.inappheader === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter InApp header'
      })
    } else if (this.state.typeInApp && this.state.inappcontent === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter InApp content'
      })
    } else if (this.state.typeInApp && this.state.inappdaystolive === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter days for InApp notification to valid'
      })
    } else if (this.state.typeEmail && this.state.emailsubject === '') {
      this.setState({
        showModal: true,
        modalText: 'Enter email subject'
      })
    } else {
      let status
      if (this.state.selectedStatus === 'active') status = 'Y'
      else if (this.state.selectedStatus === 'inactive') status = 'N'
      else status = undefined
      const pushData = {
        header: this.state.header,
        multimediaurl: this.state.multimediaurl,
        content: this.state.content,
        deeplink: JSON.parse(this.state.deeplink),
        daystolive: parseInt(this.state.daystolive, 10)
      }
      const inAppData = {
        header: this.state.inappheader,
        multimediaurl: this.state.inAppUrl,
        content: this.state.inappcontent,
        deeplink: JSON.parse(this.state.inappdeeplink),
        daystolive: parseInt(this.state.inappdaystolive, 10)
      }
      const emailData = {
        content: this.state.emailOption === 'content' ? this.state.emailcontent : undefined,
        url: this.state.emailOption === 'url' ? this.state.emailUrl : undefined,
        subject: this.state.emailsubject
      }
      const data = {
        isactive: status,
        event: this.state.selectedOption === 'event' ? this.state.commsevent : '',
        description: this.state.commName,
        smscontent: this.state.typeSms ? this.state.smscontent : undefined,
        inappcontent: this.state.typeInApp ? inAppData : undefined,
        emailercontent: this.state.typeEmail ? emailData : undefined,
        pushcontent: this.state.typePush ? pushData : undefined,
        smsstarttime:
          this.state.smsSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.smsStartDate.format('MM.DD.YYYY'), this.state.smsStartTime.format('HH:mm'))
            : undefined,
        emailstarttime:
          this.state.emailSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.emailStartDate.format('MM.DD.YYYY'), this.state.emailStartTime.format('HH:mm'))
            : undefined,
        pushstarttime:
          this.state.pushSchedule && this.state.selectedOption === 'manual'
            ? this.unixTime(this.state.pushStartDate.format('MM.DD.YYYY'), this.state.pushStartTime.format('HH:mm'))
            : undefined,
        referenceid: this.state.refId,
        referencename: this.state.refName,
        s3url: this.state.selectedOption === 'event' ? '' : this.state.s3url
      }
      this.props.updateComm({ id: this.props.comm.id, data: data }, () => {
        if (this.props.commsReducer.updateCommStatus === 'success') {
          this.props.commsList({ count: 10 })
        } else if (this.props.commsReducer.updateCommStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.commsReducer.updateCommMessage
          })
        }
      })
    }
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="medium" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">EDIT</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <Row>
              <Col sm={8}>
                <ControlLabel>Status</ControlLabel>
                <Col xs={4}>
                  <FormGroup>
                    <Radio
                      name="statusradioGroup"
                      inline
                      value="active"
                      checked={this.state.selectedStatus === 'active'}
                      onChange={this.handleStatusChange}
                    >
                      Active
                    </Radio>
                  </FormGroup>
                </Col>
                <Col xs={4}>
                  <FormGroup>
                    <Radio
                      name="statusradioGroup"
                      inline
                      value="inactive"
                      checked={this.state.selectedStatus === 'inactive'}
                      onChange={this.handleStatusChange}
                    >
                      InActive
                    </Radio>
                  </FormGroup>
                </Col>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Communication ID</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="commid"
                    type="text"
                    value={this.state.commId}
                    onChange={this.onChange}
                    disabled={true}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Communication Name</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="commname"
                    type="text"
                    value={this.state.commName}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Reference ID</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="referenceid"
                    type="text"
                    value={this.state.refId}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Reference Name</ControlLabel>
                  <FormControl
                    className="form-control"
                    name="referencename"
                    type="text"
                    value={this.state.refName}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xs={12}>
                <label className="form-group">Communication Trigger</label>
                <FormGroup>
                  <Row>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="event"
                        checked={this.state.selectedOption === 'event'}
                        onChange={this.handleOptionChange}
                      >
                        Event
                      </Radio>
                    </Col>
                    <Col xs={2}>
                      <Radio
                        name="radioGroup"
                        inline
                        value="manual"
                        checked={this.state.selectedOption === 'manual'}
                        onChange={this.handleOptionChange}
                      >
                        Manual
                      </Radio>
                    </Col>
                    <Col xs={5}>
                      <input
                        type="text"
                        name="s3url"
                        className="form-control"
                        value={this.state.s3url}
                        disabled={this.state.selectedOption === 'event'}
                        onChange={this.onChange}
                      />
                    </Col>
                    <Col xs={3}>
                      <input
                        type="file"
                        name="userfile"
                        ref={this.manualFileRef}
                        key={this.state.manualFilekey || ''}
                        accept=".xlsx, .xls, .csv"
                        style={{ display: 'none' }}
                        onChange={this.onChange}
                      />
                      <Button
                        bsStyle="primary"
                        onClick={this.showFile.bind(this, 'manual')}
                        disabled={this.state.selectedOption === 'event'}
                      >
                        <Glyphicon glyph="glyphicon glyphicon-open" />
                        Upload
                      </Button>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>Select Event</ControlLabel>
                  <select
                    className="form-control"
                    name="eventselect"
                    onChange={this.onChange}
                    value={this.state.commsevent}
                  >
                    {this.renderEventData()}
                  </select>
                </FormGroup>
              </Col>
              <Col xs={6}>
                <h3 className="personalize_var" onClick={this.showVariable}>
                  Personalization Variable
                  <Glyphicon glyph="glyphicon glyphicon-info-sign" />
                </h3>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <label>Select Channel</label>
                <FormGroup>
                  <Row>
                    <Col xs={2}>
                      <Checkbox
                        className="mt28"
                        name="pushcheck"
                        inline
                        checked={this.state.typePush}
                        onChange={this.onChange}
                      >
                        Push
                      </Checkbox>
                    </Col>
                    <Col xs={2}>
                      <Checkbox
                        className="mt28"
                        name="inappcheck"
                        inline
                        checked={this.state.typeInApp}
                        onChange={this.onChange}
                      >
                        In App
                      </Checkbox>
                    </Col>
                    <Col xs={2}>
                      <Checkbox
                        className="mt28"
                        name="smscheck"
                        inline
                        checked={this.state.typeSms}
                        onChange={this.onChange}
                      >
                        SMS
                      </Checkbox>
                    </Col>
                    <Col xs={2}>
                      <Checkbox
                        className="mt28"
                        name="emailcheck"
                        inline
                        checked={this.state.typeEmail}
                        onChange={this.onChange}
                      >
                        Email
                      </Checkbox>
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Row>
            <hr />
            <Row>
              <Col xs={6}>
                <FormGroup>
                  <ControlLabel>SMS</ControlLabel>
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup className="preview">
                  <ControlLabel onClick={this.openPreview.bind(this, 'sms')}>
                    <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                    PREVIEW
                  </ControlLabel>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Text</ControlLabel>
                  <FormControl
                    name="smscontent"
                    type="text"
                    disabled={!this.state.typeSms}
                    value={this.state.smscontent}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Sender ID</ControlLabel>
                  <FormControl disabled={!this.state.typeSms} />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Push</ControlLabel>
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup className="preview">
                  <ControlLabel onClick={this.openPreview.bind(this, 'push')}>
                    <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                    PREVIEW
                  </ControlLabel>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Header</ControlLabel>
                  <FormControl
                    name="header"
                    type="text"
                    disabled={!this.state.typePush}
                    value={this.state.header}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={9}>
                <FormGroup>
                  <ControlLabel>Multimedia URL</ControlLabel>
                  <FormControl
                    name="multimediaurl"
                    type="text"
                    disabled={!this.state.typePush}
                    value={this.state.pushUrl}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={3}>
                <FormGroup>
                  <input
                    type="file"
                    accept="image/*"
                    name="fileforPush"
                    key={this.state.pushFileKey || ''}
                    ref={this.pushFileRef}
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button
                    className="mt26"
                    bsStyle="primary"
                    onClick={this.showFile.bind(this, 'push')}
                    disabled={!this.state.typePush}
                  >
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Content</ControlLabel>
                  <FormControl
                    name="content"
                    type="text"
                    disabled={!this.state.typePush}
                    value={this.state.content}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Deeplink Screen</ControlLabel>
                  <select
                    className="form-control"
                    disabled={!this.state.typePush}
                    name="deeplink"
                    value={this.state.deeplink}
                    onChange={this.onChange}
                  >
                    <option value='{"screen_id":"1"}'>Dashboard</option>
                    <option value='{"screen_id":"2"}'>Instant OTP</option>
                    <option value='{"screen_id":"3","index_id":"1"}'>Load Money BB</option>
                    <option value='{"screen_id":"3","index_id":"2"}'>Load Money CW</option>
                    <option value='{"screen_id":"3","index_id":"3"}'>Load Money BF</option>
                    <option value='{"screen_id":"3","index_id":"4"}'>Load Money ED</option>
                    <option value='{"screen_id":"3","index_id":"5"}'>Load Money OW</option>
                    <option value='{"screen_id":"4"}'>Link Payback</option>
                    <option value='{"screen_id":"5"}'>Link BBPC</option>
                    <option value='{"screen_id":"6"}'>Price Match</option>
                    <option value='{"screen_id":"7","index_id":"1"}'>My Wallet</option>
                    <option value='{"screen_id":"7","index_id":"2"}'>My Wallet BB</option>
                    <option value='{"screen_id":"7","index_id":"3"}'>My Wallet CW</option>
                    <option value='{"screen_id":"7","index_id":"4"}'>My Wallet BF</option>
                    <option value='{"screen_id":"7","index_id":"5"}'>My Wallet ED</option>
                  </select>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Notification Valid For (No. of Days)</ControlLabel>
                  <FormControl
                    name="daystolive"
                    type="number"
                    min="1"
                    disabled={!this.state.typePush}
                    value={this.state.daystolive}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>In App</ControlLabel>
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup className="preview">
                  <ControlLabel onClick={this.openInAppPreview}>
                    <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                    PREVIEW
                  </ControlLabel>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Header</ControlLabel>
                  <FormControl
                    name="inappheader"
                    type="text"
                    disabled={!this.state.typeInApp}
                    value={this.state.inappheader}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={9}>
                <FormGroup>
                  <ControlLabel>Multimedia URL</ControlLabel>
                  <FormControl
                    name="inappurl"
                    type="text"
                    disabled={!this.state.typeInApp}
                    value={this.state.inAppUrl}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={3}>
                <FormGroup>
                  <input
                    type="file"
                    accept="image/*"
                    name="fileforInApp"
                    key={this.state.inAppFileKey || ''}
                    ref={this.inAppFileRef}
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button
                    bsStyle="primary"
                    className="mt26"
                    onClick={this.showFile.bind(this, 'inapp')}
                    disabled={!this.state.typeInApp}
                  >
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Content</ControlLabel>
                  <FormControl
                    name="inappcontent"
                    type="text"
                    disabled={!this.state.typeInApp}
                    value={this.state.inappcontent}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <ControlLabel>Deeplink Screen</ControlLabel>
                  <select
                    className="form-control"
                    disabled={!this.state.typeInApp}
                    name="inappdeeplink"
                    value={this.state.inappdeeplink}
                    onChange={this.onChange}
                  >
                    <option value='{"screen_id":"1"}'>Dashboard</option>
                    <option value='{"screen_id":"2"}'>Instant OTP</option>
                    <option value='{"screen_id":"3","index_id":"1"}'>Load Money BB</option>
                    <option value='{"screen_id":"3","index_id":"2"}'>Load Money CW</option>
                    <option value='{"screen_id":"3","index_id":"3"}'>Load Money BF</option>
                    <option value='{"screen_id":"3","index_id":"4"}'>Load Money ED</option>
                    <option value='{"screen_id":"3","index_id":"5"}'>Load Money OW</option>
                    <option value='{"screen_id":"4"}'>Link Payback</option>
                    <option value='{"screen_id":"5"}'>Link BBPC</option>
                    <option value='{"screen_id":"6"}'>Price Match</option>
                    <option value='{"screen_id":"7","index_id":"1"}'>My Wallet</option>
                    <option value='{"screen_id":"7","index_id":"2"}'>My Wallet BB</option>
                    <option value='{"screen_id":"7","index_id":"3"}'>My Wallet CW</option>
                    <option value='{"screen_id":"7","index_id":"4"}'>My Wallet BF</option>
                    <option value='{"screen_id":"7","index_id":"5"}'>My Wallet ED</option>
                  </select>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Notification Valid For (No. of Days)</ControlLabel>
                  <FormControl
                    name="inappdaystolive"
                    type="number"
                    min="1"
                    disabled={!this.state.typeInApp}
                    value={this.state.inappdaystolive}
                    onChange={this.onChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Email</ControlLabel>
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup className="preview">
                  <ControlLabel onClick={this.emailPreview}>
                    <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                    PREVIEW
                  </ControlLabel>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>Subject</ControlLabel>
                  <FormControl
                    name="emailsubject"
                    type="text"
                    value={this.state.emailsubject}
                    onChange={this.onChange}
                    disabled={!this.state.typeEmail}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xs={2}>
                <FormGroup>
                  <Radio
                    name="radioGroupemail"
                    inline
                    value="url"
                    checked={this.state.emailOption === 'url'}
                    onChange={this.handleEmailOptionChange}
                    disabled={!this.state.typeEmail}
                  >
                    URL
                  </Radio>
                </FormGroup>
              </Col>
              <Col xs={2}>
                <FormGroup>
                  <Radio
                    name="radioGroupemail"
                    inline
                    value="content"
                    checked={this.state.emailOption === 'content'}
                    onChange={this.handleEmailOptionChange}
                    disabled={!this.state.typeEmail}
                  >
                    Content
                  </Radio>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>HTML URL</ControlLabel>
                  <FormControl
                    name="emailurl"
                    type="text"
                    value={this.state.emailUrl}
                    onChange={this.onChange}
                    disabled={!this.state.typeEmail || this.state.emailOption !== 'url'}
                  />
                </FormGroup>
              </Col>
              <Col sm={3}>
                <FormGroup>
                  <input
                    type="file"
                    accept=".html"
                    name="fileforEmail"
                    key={this.state.emailFileKey || ''}
                    ref={this.emailFileRef}
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                  />
                  <Button
                    bsStyle="primary"
                    className="mt26"
                    onClick={this.showFile.bind(this, 'email')}
                    disabled={!this.state.typeEmail || this.state.emailOption !== 'url'}
                  >
                    <Glyphicon glyph="glyphicon glyphicon-open" />
                    Upload
                  </Button>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <ControlLabel>HTML Content</ControlLabel>
                  <textarea
                    name="emailcontent"
                    rows="10"
                    cols="40"
                    value={this.state.emailcontent}
                    onChange={this.onChange}
                    disabled={!this.state.typeEmail || this.state.emailOption !== 'content'}
                  />
                </FormGroup>
              </Col>
            </Row>
            {this.state.selectedOption === 'manual' ? (
              <React.Fragment>
                <hr />
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>SMS</ControlLabel>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs={2}>
                    <Checkbox
                      className="mt28"
                      name="smsimmediate"
                      inline
                      checked={this.state.smsImmediate}
                      onChange={this.onChange}
                    >
                      Immediate
                    </Checkbox>
                  </Col>
                  <Col xs={2}>
                    <Checkbox
                      className="mt28"
                      name="smsschedule"
                      inline
                      checked={this.state.smsSchedule}
                      onChange={this.onChange}
                    >
                      Schedule
                    </Checkbox>
                  </Col>
                  <Col xs={4}>
                    <FormGroup>
                      <ControlLabel>Start Date</ControlLabel>
                      <DatePicker
                        className="form-control"
                        dateFormat="DD/MM/YYYY"
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                        selected={this.state.smsStartDate}
                        onChange={this.smsDateChange}
                        disabled={!this.state.smsSchedule}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={4}>
                    <FormGroup>
                      <ControlLabel>Start Time</ControlLabel>
                      <DatePicker
                        className="form-control"
                        selected={this.state.smsStartTime}
                        onChange={this.smsTimeChange}
                        showTimeSelect
                        showTimeSelectOnly
                        timeIntervals={15}
                        dateFormat="LT"
                        timeCaption="Time"
                        disabled={!this.state.smsSchedule}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Push</ControlLabel>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs={2}>
                    <Checkbox
                      className="mt28"
                      name="pushimmediate"
                      inline
                      checked={this.state.pushImmediate}
                      onChange={this.onChange}
                    >
                      Immediate
                    </Checkbox>
                  </Col>
                  <Col xs={2}>
                    <Checkbox
                      className="mt28"
                      name="pushschedule"
                      inline
                      checked={this.state.pushSchedule}
                      onChange={this.onChange}
                    >
                      Schedule
                    </Checkbox>
                  </Col>
                  <Col xs={4}>
                    <FormGroup>
                      <ControlLabel>Start Date</ControlLabel>
                      <DatePicker
                        className="form-control"
                        dateFormat="DD/MM/YYYY"
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                        selected={this.state.pushStartDate}
                        onChange={this.pushDateChange}
                        disabled={!this.state.pushSchedule}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={4}>
                    <FormGroup>
                      <ControlLabel>Start Time</ControlLabel>
                      <DatePicker
                        className="form-control"
                        selected={this.state.pushStartTime}
                        onChange={this.pushTimeChange}
                        showTimeSelect
                        showTimeSelectOnly
                        timeIntervals={15}
                        dateFormat="LT"
                        timeCaption="Time"
                        disabled={!this.state.pushSchedule}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <ControlLabel>Email</ControlLabel>
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col xs={2} className="form-group">
                    <Checkbox
                      className="mt28"
                      name="emailimmediate"
                      inline
                      checked={this.state.emailImmediate}
                      onChange={this.onChange}
                    >
                      Immediate
                    </Checkbox>
                  </Col>
                  <Col xs={2} className="form-group">
                    <Checkbox
                      className="mt28"
                      name="emailschedule"
                      inline
                      checked={this.state.emailSchedule}
                      onChange={this.onChange}
                    >
                      Schedule
                    </Checkbox>
                  </Col>
                  <Col xs={4} className="form-group">
                    <FormGroup>
                      <ControlLabel>Start Date</ControlLabel>
                      <DatePicker
                        className="form-control"
                        dateFormat="DD/MM/YYYY"
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                        selected={this.state.emailStartDate}
                        onChange={this.emailDateChange}
                        disabled={!this.state.emailSchedule}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={4} className="form-group">
                    <FormGroup>
                      <ControlLabel>Start Time</ControlLabel>
                      <DatePicker
                        className="form-control"
                        selected={this.state.emailStartTime}
                        onChange={this.emailTimeChange}
                        showTimeSelect
                        showTimeSelectOnly
                        timeIntervals={15}
                        dateFormat="LT"
                        timeCaption="Time"
                        disabled={!this.state.emailSchedule}
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </React.Fragment>
            ) : null}
          </form>
          <PushPreview show={this.state.showPreview} onHide={this.showPreviewClose} data={this.state.previewData} />
          <InAppPreview
            show={this.state.showInAppPreview}
            onHide={this.inAppPreviewClose}
            data={this.state.previewData}
          />
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
          <Modal show={this.state.showVariableModal} onHide={this.variableModalClose}>
            <Modal.Header closeButton />
            <Modal.Body>
              {this.props.commsReducer.eventData && this.props.commsReducer.eventData.event.length > 0 ? (
                <div>
                  {this.props.commsReducer.eventData.event.map((ev, index) => {
                    return <h4 key={index}>{`$${ev}`}</h4>
                  })}
                </div>
              ) : (
                <p>No variable found</p>
              )}
            </Modal.Body>
          </Modal>
        </Modal.Body>
        <Modal.Footer>
          <Row className="form-group">
            <Col xs={4} xsOffset={2}>
              <Button bsStyle="secondary" onClick={onHide}>
                CANCEL
              </Button>
            </Col>
            <Col xs={4}>
              <Button bsStyle="primary" onClick={this.editComm}>
                UPDATE
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

function mapStateToProps({ commsReducer }) {
  return { commsReducer }
}

export default connect(
  mapStateToProps,
  actions
)(EditComm)
