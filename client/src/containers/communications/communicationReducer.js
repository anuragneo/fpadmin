import {
  COMMS_LIST,
  GET_EVENTS,
  CREATE_COMM,
  TOGGLE_COMM,
  GET_REPORT,
  REMOVE_COMM,
  UPDATE_COMM,
  GET_EVENT,
  GET_REFERENCE_NAMES,
  GET_COMM_NAMES,
  CHECK_EVENTS
} from './actions/types'

export default function(state = {}, action) {
  switch (action.type) {
    case COMMS_LIST:
      return {
        ...state,
        commsListStatus: action.payload.status,
        commsListData: action.payload.data,
        commsListMessage: action.payload.message
      }

    case CHECK_EVENTS:
      return {
        ...state,
        checkEventsStatus: action.payload.status,
        checkEventsData: action.payload.data,
        checkEventsMessage: action.payload.message
      }

    case GET_EVENTS:
      return {
        ...state,
        eventListStatus: action.payload.status,
        eventListData: action.payload.data,
        eventListMessage: action.payload.message
      }

    case GET_REFERENCE_NAMES:
      return {
        ...state,
        referenceListStatus: action.payload.status,
        referenceListData: action.payload.data,
        referenceListMessage: action.payload.message
      }

    case GET_COMM_NAMES:
      return {
        ...state,
        commNamesListStatus: action.payload.status,
        commNamesListData: action.payload.data,
        commNamesListMessage: action.payload.message
      }

    case GET_EVENT:
      return {
        ...state,
        eventStatus: action.payload.status,
        eventData: action.payload.data,
        eventMessage: action.payload.message,
        updateCommStatus: ''
      }

    case CREATE_COMM:
      return {
        ...state,
        newCommStatus: action.payload.status,
        newCommData: action.payload.data,
        newCommMessage: action.payload.message
      }
    case TOGGLE_COMM:
      return {
        ...state,
        toggleCommStatus: action.payload.status,
        toggleCommData: action.payload.data,
        toggleCommMessage: action.payload.message
      }

    case REMOVE_COMM:
      return {
        ...state,
        removeCommStatus: action.payload.status,
        removeCommData: action.payload.data,
        removeCommMessage: action.payload.message
      }

    case UPDATE_COMM:
      return {
        ...state,
        updateCommStatus: action.payload.status,
        updateCommData: action.payload.data,
        updateCommMessage: action.payload.message
      }

    case GET_REPORT:
      return {
        ...state,
        reportStatus: action.payload.status,
        reportData: action.payload.data,
        reportMessage: action.payload.message
      }

    default:
      return state
  }
}
