import axios from 'axios'
import * as types from './types'

const url = process.env.PUBLIC_URL

export const commsList = req => async dispatch => {
  const res = await axios.post(`${url}/api/comms/list`, req)
  dispatch({ type: types.COMMS_LIST, payload: res.data })
}

export const checkEvent = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/search`, req)
  dispatch({ type: types.CHECK_EVENTS, payload: res.data })
  callback()
}

export const eventList = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/getevents`, req)
  dispatch({ type: types.GET_EVENTS, payload: res.data })
  callback()
}

export const getEvent = req => async dispatch => {
  const res = await axios.post(`${url}/api/comms/getevent`, req)
  dispatch({ type: types.GET_EVENT, payload: res.data })
}

export const createComm = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/create`, req)
  dispatch({ type: types.CREATE_COMM, payload: res.data })
  callback()
}

export const toggleComm = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/toggle`, req)
  dispatch({ type: types.TOGGLE_COMM, payload: res.data })
  callback()
}

export const updateComm = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/update`, req)
  dispatch({ type: types.UPDATE_COMM, payload: res.data })
  callback()
}

export const removeComm = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/toggle`, req)
  dispatch({ type: types.REMOVE_COMM, payload: res.data })
  callback()
}

export const referenceList = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/getreferencenames`, req)
  dispatch({ type: types.GET_REFERENCE_NAMES, payload: res.data })
  callback()
}

export const commNamesList = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/comms/getcommnames`, req)
  dispatch({ type: types.GET_COMM_NAMES, payload: res.data })
  callback()
}

export const getReport = req => async dispatch => {
  const res = await axios.post(`${url}/api/comms/report`, req)
  dispatch({ type: types.GET_REPORT, payload: res.data })
}
