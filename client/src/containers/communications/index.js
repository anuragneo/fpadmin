import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import Sidebar from '../../components/sidebar'
import CommunicationsSearch from './Search'
import NewCommunication from './NewCommunication'
import Report from './Report'

export default class Communications extends Component {

  render() {
    if (this.props.history.location.pathname === '/' || this.props.history.location.pathname === '/communications')
      this.props.history.push('/communications/search');
    return (
      <div>
        <Sidebar type="communications" />
        <Route path="/communications/search" component={CommunicationsSearch} />
        <Route path="/communications/add" component={NewCommunication} />
        <Route path="/communications/reports" component={Report} />
      </div>
    )
  }
}
