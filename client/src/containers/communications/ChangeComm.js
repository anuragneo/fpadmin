import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Modal, Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap'

class ChangeComm extends Component {
  toggleComm = () => {
    this.props.toggleComm(
      {
        id: this.props.comm.id
      },
      () => {
        if (this.props.commsReducer.toggleCommStatus === 'success') {
          this.props.commsList({ count: 10, pagenumber: this.props.page })
        }
      }
    )
  }

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">Remove</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Row>
            <Col xs={12}>
              <FormGroup>
                <ControlLabel>
                  Do you want to{' '}
                  {this.props.comm.status === 'active' || this.props.comm.status === 'scheduled'
                    ? `inactivate`
                    : `activate`}{' '}
                  the communication
                </ControlLabel>
              </FormGroup>
            </Col>
          </Row>
        </Modal.Body>

        <Modal.Footer>
          <Row>
            <Col xs={6}>
              <Button bsClass="btn btn-secondary" onClick={onHide}>
                CANCEL
              </Button>
            </Col>
            <Col xs={6}>
              <Button bsStyle="primary" onClick={this.toggleComm}>
                SUBMIT
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

function mapStateToProps({ commsReducer }) {
  return { commsReducer }
}

export default connect(
  mapStateToProps,
  actions
)(ChangeComm)
