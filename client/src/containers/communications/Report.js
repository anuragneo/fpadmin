import React, { Component } from 'react'
import { connect } from 'react-redux'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import * as actions from './actions'
import { Col, PageHeader, Row, FormGroup, FormControl, ControlLabel, Table, Grid, Button, Modal } from 'react-bootstrap'

class Report extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startDate: moment(),
      endDate: moment(),
      event: '',
      communication: '',
      referencesystem: '',
      showModal: false,
      modalText: ''
    }
  }

  componentDidMount() {
    this.props.eventList({}, () => {})

    this.props.referenceList({ id: 1 }, () => {})

    this.props.commNamesList({ id: 1 }, () => {})
  }

  handleClose = () => this.setState({ showModal: false })

  startDateChange = date => {
    this.setState({
      startDate: date
    })
  }

  endDateChange = date => {
    this.setState({
      endDate: date
    })
  }

  onChange = e => {
    switch (e.target.name) {
      case 'event':
        this.setState({ event: e.target.value })
        break
      case 'communication':
        this.setState({ communication: e.target.value })
        break
      case 'referencesystem':
        this.setState({ referencesystem: e.target.value })
        break
      default:
        break
    }
  }

  getReport = e => {
    e.preventDefault()
    if (this.state.event === '' && this.state.referencesystem === '' && this.state.communication === '') {
      this.setState({
        showModal: true,
        modalText: 'Select one event or reference system or communication name'
      })
    } else {
      this.props.getReport({
        event: this.state.event === '' ? undefined : this.state.event,
        name: this.state.communication === '' ? undefined : this.state.communication,
        referencename: this.state.referencesystem === '' ? undefined : this.state.referencesystem,
        starttime: this.state.startDate,
        endtime: this.state.endDate
      })
    }
  }

  renderPushData() {
    if (this.props.commsReducer.reportStatus === 'success') {
      return (
        <tr>
          <td>{this.props.commsReducer.reportData.pushlogs.attempted}</td>
          <td>{this.props.commsReducer.reportData.pushlogs.successes}</td>
          <td>{this.props.commsReducer.reportData.pushlogs.failures}</td>
          <td>{this.props.commsReducer.reportData.pushlogs.clicks}</td>
        </tr>
      )
    }
  }

  renderSmsData() {
    if (this.props.commsReducer.reportStatus === 'success') {
      return (
        <tr>
          <td>{this.props.commsReducer.reportData.smslogs.attempted}</td>
          <td>{this.props.commsReducer.reportData.smslogs.successes}</td>
          <td>{this.props.commsReducer.reportData.smslogs.failures}</td>
        </tr>
      )
    }
  }

  renderEventData() {
    if (this.props.commsReducer.eventListStatus === 'success') {
      return this.props.commsReducer.eventListData.event.map((event, pos) => {
        return (
          <option key={pos} value={event}>
            {event}
          </option>
        )
      })
    }
  }

  renderReferenceData() {
    if (this.props.commsReducer.referenceListStatus === 'success') {
      return this.props.commsReducer.referenceListData.referencenames.map((ref, pos) => {
        return (
          <option key={pos} value={ref}>
            {ref}
          </option>
        )
      })
    }
  }

  renderCommNamesData() {
    if (this.props.commsReducer.commNamesListStatus === 'success') {
      return this.props.commsReducer.commNamesListData.commnames.map((comm, pos) => {
        return (
          <option key={pos} value={comm}>
            {comm}
          </option>
        )
      })
    }
  }

  render() {
    return (
      <Col xs={10} className="scroll">
        <PageHeader>REPORTS</PageHeader>
        <Grid fluid>
          <Row>
            <Col xs={10} xsOffset={1}>
              <form className="form-group">
                <Row className="form-group">
                  <Col sm={3}>
                    <FormGroup>
                      <ControlLabel>From</ControlLabel>
                      <DatePicker
                        className="form-control"
                        dateFormat="DD/MM/YYYY"
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                        selected={this.state.startDate}
                        onChange={this.startDateChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={3}>
                    <FormGroup>
                      <ControlLabel>To</ControlLabel>
                      <DatePicker
                        className="form-control"
                        dateFormat="DD/MM/YYYY"
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                        selected={this.state.endDate}
                        onChange={this.endDateChange}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col xs={12} className="form-group">
                    <h4 className="acct_manage_hist_label">Please select either of the one option</h4>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col sm={4}>
                    <FormGroup>
                      <ControlLabel>Select Event</ControlLabel>
                      <select
                        className="form-control form-group"
                        name="event"
                        value={this.state.event}
                        onChange={this.onChange}
                      >
                        <option value="">Select</option>
                        {this.renderEventData()}
                      </select>
                    </FormGroup>
                  </Col>
                  <Col sm={4}>
                    <FormGroup>
                      <ControlLabel>Select Communication Name</ControlLabel>
                      <select
                        className="form-control"
                        name="communication"
                        value={this.state.communication}
                        onChange={this.onChange}
                      >
                        <option value="">Select</option>
                        {this.renderCommNamesData()}
                      </select>
                    </FormGroup>
                  </Col>
                  <Col sm={4}>
                    <FormGroup>
                      <ControlLabel>Select Reference System</ControlLabel>
                      <select
                        className="form-control"
                        name="referencesystem"
                        value={this.state.referencesystem}
                        onChange={this.onChange}
                      >
                        <option value="">Select</option>
                        {this.renderReferenceData()}
                      </select>
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col sm={4} smOffset={4} className="form-group">
                    <Button bsStyle="primary" type="submit" onClick={this.getReport} className="form-group">
                      SEARCH
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <Col sm={4} smOffset={4}>
                    <a
                      className="text-center w100"
                      href={
                        this.props.commsReducer.reportStatus === 'success'
                          ? this.props.commsReducer.reportData.s3url
                          : 'javascript:void(0)'
                      }
                      download
                    >
                      Download Failure Report
                    </a>
                  </Col>
                </Row>
              </form>
              <Row className="mt28">
                <Col sm={6}>
                  <Table responsive striped bordered condensed hover>
                    <thead>
                      <tr>
                        <th colSpan="4">Priority 1:Push</th>
                      </tr>
                    </thead>
                    <thead>
                      <tr>
                        <th>No.of Customers Attempted</th>
                        <th>Success</th>
                        <th>Failure</th>
                        <th>No. of Customers Clicked on Push</th>
                      </tr>
                    </thead>
                    <tbody>{this.renderPushData()}</tbody>
                  </Table>
                </Col>
                <Col sm={6}>
                  <Table responsive striped bordered condensed hover>
                    <thead>
                      <tr>
                        <th colSpan="4"> Priority 2:SMS</th>
                      </tr>
                    </thead>
                    <thead>
                      <tr>
                        <th>No.of Customers Attempted</th>
                        <th>Success</th>
                        <th>Failure</th>
                      </tr>
                    </thead>
                    <tbody>{this.renderSmsData()}</tbody>
                  </Table>
                </Col>
              </Row>
            </Col>
          </Row>
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
        </Grid>
      </Col>
    )
  }
}

function mapStateToProps({ commsReducer }) {
  return { commsReducer }
}

export default connect(
  mapStateToProps,
  actions
)(Report)
