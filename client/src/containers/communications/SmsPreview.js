import React, { Component } from 'react'
import { Modal } from 'react-bootstrap'

class SmsPreview extends Component {

  render() {
    const { show, onHide } = this.props
    return (
      <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>

        </Modal.Header>

        <Modal.Body>

        </Modal.Body>

        <Modal.Footer>

        </Modal.Footer>
      </Modal>
    )
  }
}

export default SmsPreview