import React, { Component } from 'react'
import { connect } from 'react-redux'
import { PanelGroup, Col } from 'react-bootstrap'
import SearchBar from './SearchBar'
import Profile from './Profile'
import Wallet from './Wallet'
import Transactions from './Transactions'
import Loyalty from './Loyalty'
import PriceMatch from './PriceMatch'
import Feedbacks from './Feedbacks'
import Communications from './Communications'
import PaymentPriority from './PaymentPriority'

class CustomerCare extends Component {
  constructor(props) {
    super(props)
    this.handleSelect = this.handleSelect.bind(this)

    this.state = {
      activeKey: ''
    }
  }

  handleSelect(activeKey) {
    this.setState({ activeKey })
  }

  componentDidUpdate(prevProps) {
    if (this.props.customarecare.profileData !== prevProps.customarecare.profileData) {
      if (this.props.customarecare.profileStatus) {
        this.handleSelect('1')
      } else {
        this.handleSelect('')
      }
    }
  }

  render() {
    return (
      <Col xs={12} className="scroll">
        <SearchBar />

        <PanelGroup accordion id="accordion-example" activeKey={this.state.activeKey} onSelect={this.handleSelect}>
          <Profile />

          <Wallet />

          <Transactions />

          <Loyalty />

          <PriceMatch />

          <Feedbacks />

          <Communications />

          <PaymentPriority />
        </PanelGroup>
      </Col>
    )
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(mapStateToProps)(CustomerCare)
