import React, { Component } from 'react'
import { Panel, Glyphicon } from 'react-bootstrap'
import PriceMatchData from './PriceMatchData'

class PriceMatch extends Component {
  render() {
    return (
      <Panel eventKey="5">
        <Panel.Heading>
          <Panel.Title toggle>
            Price Match
            <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
            <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body collapsible>
          <PriceMatchData />
        </Panel.Body>
      </Panel>
    )
  }
}

export default PriceMatch
