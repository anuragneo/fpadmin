import React, { Component } from 'react'
import { Panel, Glyphicon } from 'react-bootstrap'
import LoyaltyData from './LoyaltyData'

class Loyalty extends Component {
  render() {
    return (
      <Panel eventKey="4">
        <Panel.Heading>
          <Panel.Title toggle>
            Loyalty Balances
            <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
            <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body collapsible>
          <LoyaltyData />
        </Panel.Body>
      </Panel>
    )
  }
}

export default Loyalty
