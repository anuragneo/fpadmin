import React, { Component } from 'react'
import { Row, Col, FormGroup, ControlLabel, FormControl, Button, Glyphicon, Table } from 'react-bootstrap'
import { connect } from 'react-redux'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import * as actions from './actions'

class PriceMatchData extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startDate: moment(),
      endDate: moment()
    }
  }

  startDateChange = date => {
    this.setState({
      startDate: date
    })
  }

  endDateChange = date => {
    this.setState({
      endDate: date
    })
  }

  renderTableData() {
    if (this.props.customarecare.pmData && this.props.customarecare.pmData.length > 0) {
      return this.props.customarecare.pmData.map(pm => {
        return (
          <tr key={pm.billnumber}>
            <td>{pm.billnumber}</td>
            <td>{pm.till}</td>
            <td>{pm.billdate}</td>
            <td>{pm.state}</td>
            <td>{pm.storecode}</td>
            <td>{pm.transactiondate}</td>
            <td>{pm.active === 'Y' ? 'Active' : 'Deactive'}</td>
            <td>{pm.refundAmount}</td>
            <td>
              <Glyphicon glyph="glyphicon glyphicon-eye-open" />
            </td>
          </tr>
        )
      })
    } else {
      return (
        <tr>
          <td align="center" colSpan="8">
            No data Found
          </td>
        </tr>
      )
    }
  }

  render() {
    return (
      <div>
        {this.props.customarecare.pmStatus === 'success' ? (
          <div>
            <Row>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">FROM</ControlLabel>
                  <DatePicker
                    className="form-control"
                    dateFormat="DD/MM/YYYY"
                    selected={this.state.startDate}
                    onChange={this.startDateChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">TO</ControlLabel>
                  <DatePicker
                    className="form-control"
                    dateFormat="DD/MM/YYYY"
                    selected={this.state.endDate}
                    onChange={this.endDateChange}
                  />
                </FormGroup>
              </Col>

              <Col sm={2} mdOffset={4}>
                <ControlLabel className="greylalbel invisible">Test</ControlLabel>
                <Button bsStyle="primary">
                  <Glyphicon glyph="glyphicon glyphicon-search" /> Search
                </Button>
              </Col>
              <Col sm={2}>
                <ControlLabel className="greylalbel invisible">Test</ControlLabel>
                <Button bsStyle="secondary">
                  <Glyphicon glyph="glyphicon glyphicon-repeat" /> Reset
                </Button>
              </Col>
            </Row>

            <Table responsive striped bordered condensed hover>
              <thead>
                <tr>
                  <th>Bill No.</th>
                  <th>Till No.</th>
                  <th>Date</th>
                  <th>City</th>
                  <th>Store Name</th>
                  <th>Price Match Submission Date</th>
                  <th>Status</th>
                  <th>Amount Refunded</th>
                  <th>View Detail</th>
                </tr>
              </thead>
              <tbody>{this.renderTableData()}</tbody>
            </Table>
          </div>
        ) : null}
      </div>
    )
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(
  mapStateToProps,
  actions
)(PriceMatchData)
