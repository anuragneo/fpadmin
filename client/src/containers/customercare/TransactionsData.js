import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, FormGroup, ControlLabel, FormControl, Table, Checkbox, Button, Glyphicon } from 'react-bootstrap'

class TransactionsData extends Component {
  renderCashbackTable() {
    // return this.props.customarecare.transactionsData.cashbacks.map(cashback => {
    //   return (
    //     <tr key={cashback.name}>
    //       <td>{cashback.name}</td>
    //       <td>{cashback.amount}</td>
    //       <td>{cashback.exp}</td>
    //     </tr>
    //   )
    // })
  }

  renderTransactionsTable() {
    if (this.props.customarecare.transactionsData && this.props.customarecare.transactionsData.length > 0) {
      return this.props.customarecare.transactionsData.map(tran => {
        return (
          <tr key={tran.txnNumber}>
            <td>{tran.txndate}</td>
            <td>{tran.mobile}</td>
            <td>{tran.source}</td>
            <td>{tran.txnid}</td>
            <td>{tran.pgid}</td>
            <td>{tran.city}</td>
            <td>{tran.storecode}</td>
            <td>{tran.storename}</td>
            <td>{tran.tillnumber}</td>
            <td>{tran.receiptNo}</td>
            <td>{tran.txntype}</td>
            <td>{tran.subtype}</td>
            <td>{tran.campaignname}</td>
            <td>{tran.state}</td>
            <td>{tran.walletname}</td>
            <td>{tran.amount}</td>
            <td>{tran.balance}</td>
          </tr>
        )
      })
    } else {
      return (
        <tr>
          <td align="center" colSpan="17">
            No data Found
          </td>
        </tr>
      )
    }
  }

  render() {
    return (
      <div>
        {this.props.customarecare.transactionsStatus === 'success' ? (
          <div>
            <Row>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">TRANSACTION NO.</ControlLabel>
                  <Checkbox>
                    <FormControl type="text" />
                  </Checkbox>
                </FormGroup>
              </Col>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">WALLET TYPE</ControlLabel>
                  <FormControl componentClass="select" placeholder="select">
                    <option value="select">select</option>
                    <option value="other">...</option>
                  </FormControl>
                </FormGroup>
              </Col>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">FROM</ControlLabel>
                  <FormControl componentClass="select" placeholder="select">
                    <option value="select">select</option>
                    <option value="other">...</option>
                  </FormControl>
                </FormGroup>
              </Col>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">TO</ControlLabel>
                  <FormControl componentClass="select" placeholder="select">
                    <option value="select">select</option>
                    <option value="other">...</option>
                  </FormControl>
                </FormGroup>
              </Col>

              <Col xs={2}>
                <ControlLabel className="greylalbel invisible">Test</ControlLabel>
                <Button bsStyle="primary">
                  <Glyphicon glyph="glyphicon glyphicon-search" /> Search
                </Button>
              </Col>
              <Col xs={2}>
                <ControlLabel className="greylalbel invisible">Test</ControlLabel>
                <Button bsStyle="secondary">
                  <Glyphicon glyph="glyphicon glyphicon-repeat" /> Reset
                </Button>
              </Col>
            </Row>
            <FormGroup>
              <Table responsive striped bordered condensed hover>
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Mobile No.</th>
                    <th>Source</th>
                    <th>Txn Number</th>
                    <th>PGID</th>
                    <th>City</th>
                    <th>Store Code</th>
                    <th>Store Name</th>
                    <th>Till No.</th>
                    <th>Receipt Number</th>
                    <th>Txn Type</th>
                    <th>Txn Sub Type</th>
                    <th>Campaigns Name</th>
                    <th>Status</th>
                    <th>Wallet</th>
                    <th>Amount</th>
                    <th>Balance</th>
                  </tr>
                </thead>
                <tbody>{this.renderTransactionsTable()}</tbody>
              </Table>
            </FormGroup>
            <Table responsive striped bordered condensed hover>
              <thead>
                <tr>
                  <th>Offer Name</th>
                  <th>Cashback Amount</th>
                  <th>Expiry Date</th>
                </tr>
              </thead>

              <tbody>{this.renderCashbackTable()}</tbody>
            </Table>
          </div>
        ) : null}
      </div>
    )
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(mapStateToProps)(TransactionsData)
