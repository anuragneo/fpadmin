import React, { Component } from 'react'
import { Table } from 'react-bootstrap'
import { connect } from 'react-redux'

class ProfileData extends Component {
  formatAddress() {
    let profileData = this.props.customarecare.profileData
    let addressarr = []

    if (profileData.address1 && profileData.address1 !== '') addressarr.push(profileData.address1)
    if (profileData.address2 && profileData.address2 !== '') addressarr.push(profileData.address2)
    if (profileData.address3 && profileData.address3 !== '') addressarr.push(profileData.address3)
    if (profileData.address4 && profileData.address4 !== '') addressarr.push(profileData.address4)

    let address = addressarr.join(', ')
    return address
  }

  render() {
    return (
      <div>
        {this.props.customarecare.profileStatus === 'success' ? (
          <Table responsive striped bordered condensed hover>
            <thead>
              <tr>
                <th>Mobile Number</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email ID</th>
                <th>Date of Birth</th>
                <th>Address Information</th>
                <th>Pincode</th>
                <th>Membership ID</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{this.props.customarecare.profileData.mobile}</td>
                <td>{this.props.customarecare.profileData.firstname}</td>
                <td>{this.props.customarecare.profileData.lastname}</td>
                <td>{this.props.customarecare.profileData.email}</td>
                <td>{this.props.customarecare.profileData.dob}</td>
                <td>{this.formatAddress()}</td>
                <td>{this.props.customarecare.profileData.pincode}</td>
                <td>{this.props.customarecare.profileData.customerid}</td>
                <td>{this.props.customarecare.profileData.status}</td>
              </tr>
            </tbody>
          </Table>
        ) : (
          <div>
            <p>
              {this.props.customarecare.profileData
                ? this.props.customarecare.profileData.message
                : 'No customer found.'}
            </p>
          </div>
        )}
      </div>
    )
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(mapStateToProps)(ProfileData)
