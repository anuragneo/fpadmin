import React, { Component } from 'react'
import { Panel, Glyphicon } from 'react-bootstrap'
import FeedbacksData from './FeedbacksData'

class Feedbacks extends Component {
  render() {
    return (
      <Panel eventKey="6">
        <Panel.Heading>
          <Panel.Title toggle>
            Feedbacks
            <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
            <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body collapsible>
          <FeedbacksData />
        </Panel.Body>
      </Panel>
    )
  }
}

export default Feedbacks
