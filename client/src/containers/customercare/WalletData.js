import React, { Component } from 'react'
import { Form, FormGroup, Col, Row, ControlLabel, FormControl } from 'react-bootstrap'
import { connect } from 'react-redux'
import * as _ from 'lodash'

class WalletData extends Component {
  render() {
    if (this.props.customarecare.prepaidStatus === 'success') {
      let customerData = this.props.customarecare
      let prepaidData = customerData.prepaidData
      let monthlylimit = customerData.monthlylimit
      let bbwallet = _.filter(prepaidData, { name: 'Big Bazaar Wallet' })[0]
      let bfwallet = _.filter(prepaidData, { name: 'Brand Factory Wallet' })[0]
      let cwallet = _.filter(prepaidData, { name: 'Central Wallet' })[0]
      let edwallet = _.filter(prepaidData, { name: 'Easy Day Wallet' })[0]
      let payback = customerData.paybackStatus === 'success' ? customerData.paybackData : null
      let bbpc = customerData.bbpcStatus === 'success' ? customerData.bbpcData : null
      return (
        <div>
          <Form horizontal className="bor-bro-bot form-group">
            <FormGroup controlId="formHorizontalEmail">
              <Col componentClass={ControlLabel} sm={3} smOffset={6} className="blacklabel1">
                Monthly Wallet Limit
              </Col>
              <Col sm={3}>
                <FormControl className="redinput" value={monthlylimit} disabled />
              </Col>
            </FormGroup>
          </Form>

          <h4 className="acct_manage_hist_label form-group">Big Bazaar Wallet</h4>

          <Row>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Top Up</ControlLabel>
                <FormControl type="text" value={bbwallet.topup} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>

            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Cashback</ControlLabel>
                <FormControl type="text" value={bbwallet.cashback} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>

            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Expirable Cashback</ControlLabel>
                <FormControl type="text" value={bbwallet.expirable} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>

            <Col sm={3}>
              <FormGroup className="greylalbel">
                <ControlLabel>TOTAL</ControlLabel>
                <FormControl type="text" className="redinput" value={bbwallet.balance} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
          </Row>

          <h4 className="acct_manage_hist_label form-group">Brand Factory Wallet</h4>

          <Row>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Top Up</ControlLabel>
                <FormControl type="text" value={bfwallet.topup} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>

            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Cashback</ControlLabel>
                <FormControl type="text" value={bfwallet.cashback} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Expirable Cashback</ControlLabel>
                <FormControl type="text" value={bfwallet.expirable} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">TOTAL</ControlLabel>
                <FormControl type="text" className="redinput" value={bfwallet.balance} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
          </Row>

          <h4 className="acct_manage_hist_label form-group">Central Wallet</h4>

          <Row>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Top Up</ControlLabel>
                <FormControl type="text" value={cwallet.topup} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Cashback</ControlLabel>
                <FormControl type="text" value={cwallet.cashback} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Expirable Cashback</ControlLabel>
                <FormControl type="text" value={cwallet.expirable} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">TOTAL</ControlLabel>
                <FormControl type="text" className="redinput" value={cwallet.balance} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
          </Row>

          <h4 className="acct_manage_hist_label form-group">EasyDay Wallet</h4>

          <Row>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Top Up</ControlLabel>
                <FormControl type="text" value={edwallet.topup} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>

            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Cashback</ControlLabel>
                <FormControl type="text" value={edwallet.cashback} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>

            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">Expirable Cashback</ControlLabel>
                <FormControl type="text" value={edwallet.expirable} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>

            <Col sm={3}>
              <FormGroup>
                <ControlLabel className="greylalbel">TOTAL</ControlLabel>
                <FormControl type="text" className="redinput" value={edwallet.balance} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel>Payback</ControlLabel>
                <FormControl
                  type="text"
                  value={customerData.paybackStatus === 'success' ? payback.totalpoints : ''}
                  disabled
                />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
            <Col sm={3}>
              <FormGroup>
                <ControlLabel>BBPC</ControlLabel>
                <FormControl type="text" value={customerData.bbpcStatus === 'success' ? bbpc.balance : ''} disabled />
                <FormControl.Feedback />
              </FormGroup>
            </Col>
          </Row>
        </div>
      )
    } else {
      return (
        <div>
          <p>
            {' '}
            {this.props.customarecare.prepaidData
              ? this.props.customarecare.prepaidData.message
              : 'Failed to fetch customer details'}{' '}
          </p>
        </div>
      )
    }
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(mapStateToProps)(WalletData)
