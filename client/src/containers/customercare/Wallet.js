import React, { Component } from 'react'
import { Panel, Glyphicon } from 'react-bootstrap'
import WalletData from './WalletData'

class Wallet extends Component {
  render() {
    return (
      <Panel eventKey="2">
        <Panel.Heading>
          <Panel.Title toggle>
            Wallets & Balances
            <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
            <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body collapsible>
          <WalletData />
        </Panel.Body>
      </Panel>
    )
  }
}

export default Wallet
