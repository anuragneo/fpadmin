import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, FormGroup, ControlLabel, FormControl, Table, Checkbox, Button, Glyphicon } from 'react-bootstrap'
class PaymentPriorityData extends Component {
  render() {
    return (
      <div>
        <Table responsive striped bordered condensed hover>
          <thead>
            <tr>
              <th>Sub Wallet</th>
              <th>Current Priority</th>
              <th>Last Priority</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td />
              <td />
              <td />
            </tr>
          </tbody>
        </Table>
      </div>
    )
  }
}

export default PaymentPriorityData
