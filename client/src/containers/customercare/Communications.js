import React, { Component } from 'react'
import { Panel, Glyphicon } from 'react-bootstrap'
import CommunicationsData from './CommunicationsData'

class Communications extends Component {
  render() {
    return (
      <Panel eventKey="7">
        <Panel.Heading>
          <Panel.Title toggle>
            Promotional Communications
            <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
            <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body collapsible>
          <CommunicationsData />
        </Panel.Body>
      </Panel>
    )
  }
}

export default Communications
