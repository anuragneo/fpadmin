import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import * as actions from './actions'
import { Row, Col, Button, Glyphicon, FormControl, FormGroup } from 'react-bootstrap'

class SearchBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mobile: ''
    }
  }

  onChange = event => {
    this.setState({ mobile: event.target.value })
  }

  submitData = event => {
    event.preventDefault()
    if (this.state.mobile.length === 10) {
      this.props.getProfile(
        {
          mobile: this.state.mobile
        },
        () => {
          if (this.props.customarecare.profileStatus === 'success') {
            this.props.getPrepaidBalance({
              managerid: 1,
              mobile: this.state.mobile
            })
            this.props.getPaybackBalance({
              fpaccountid: this.props.customarecare.profileData.fpaccountid
            })
            this.props.getBbpcBalance({
              mobile: this.state.mobile,
              fpaccountid: this.props.customarecare.profileData.fpaccountid
            })
            this.props.getTransactions({
              mobile: this.props.customarecare.profileData.mobile
            })
            this.props.getCommunications({
              id: this.props.customarecare.profileData.id
            })
            this.props.getFeedbacks({
              mobile: this.props.customarecare.profileData.mobile,
              startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
              enddate: moment(this.state.endDate).format('DD/MM/YYYY')
            })
            this.props.getLoyalty({
              id: this.props.customarecare.profileData.id
            })
            this.props.getPricematch({
              mobile: this.props.customarecare.profileData.mobile,
              startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
              enddate: moment(this.state.endDate).format('DD/MM/YYYY')
            })
          }
        }
      )
    } else {
    }
  }

  reset = () => {
    this.props.reset()
    this.setState({
      mobile: ''
    })
  }

  render() {
    return (
      <form className="bor-bro-bot searchbar" onSubmit={this.submitData}>
        <Row>
          <Col xs={8}>
            <FormGroup>
              <FormControl
                type="text"
                placeholder="Enter mobile number"
                value={this.state.mobile}
                onChange={this.onChange}
                pattern="\d*"
                maxLength="10"
                required
                title="Please enter a valid mobile number."
              />
            </FormGroup>
          </Col>
          <Col xs={2}>
            <Button type="submit" bsStyle="primary">
              <Glyphicon glyph="glyphicon glyphicon-search" /> Search
            </Button>
          </Col>
          <Col xs={2}>
            <Button type="button" bsStyle="secondary" onClick={this.reset}>
              <Glyphicon glyph="glyphicon glyphicon-repeat" /> Reset
            </Button>
          </Col>
        </Row>
      </form>
    )
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(
  mapStateToProps,
  actions
)(SearchBar)
