import React, { Component } from 'react'
import { Table, FormGroup } from 'react-bootstrap'
import { connect } from 'react-redux'

class LoyaltyData extends Component {
  renderLoyaltyData() {
    return this.props.customarecare.loyaltyData.loyalty.map(loyalty => {
      return (
        <tr key={loyalty.name}>
          <td>{loyalty.name}</td>
          <td>{loyalty.cardno}</td>
        </tr>
      )
    })
  }

  renderTransactionsData() {
    return this.props.customarecare.loyaltyData.transactions.map(tran => {
      return (
        <tr key={tran.id}>
          <td>{tran.id}</td>
          <td>{tran.store}</td>
          <td>{tran.date}</td>
          <td>{tran.type}</td>
          <td>{tran.subtype}</td>
          <td>{tran.amount}</td>
          <td>{tran.status === 'Y' ? 'Active' : 'Deactive'}</td>
        </tr>
      )
    })
  }

  render() {
    let loyaltyData = []
    let ccare = this.props.customarecare

    if (ccare.paybackStatus === 'success')
      loyaltyData.push({ name: 'Payback', cardno: ccare.paybackData.cardno ? ccare.paybackData.cardno : '' })

    if (ccare.bbpcStatus === 'success')
      loyaltyData.push({ name: 'BBPC', cardno: ccare.bbpcData.cardno ? ccare.bbpcData.cardno : '' })

    if (this.props.customarecare.loyaltyData && this.props.customarecare.loyaltyData.transactions.length > 0) {
      return (
        <div>
          <FormGroup>
            <Table responsive striped bordered condensed hover>
              <thead>
                <tr>
                  <th>Loyalt Program Name</th>
                  <th>Card Number</th>
                </tr>
              </thead>
              <tbody>{this.renderLoyaltyData(loyaltyData)}</tbody>
            </Table>
          </FormGroup>
          <h4 className="acct_manage_hist_label form-group">BBPC Transactions Details</h4>

          <Table responsive striped bordered condensed hover>
            <thead>
              <tr>
                <th>Transactions ID</th>
                <th>Date</th>
                <th>Store</th>
                <th>Type</th>
                <th>Sub Type</th>
                <th>Amount</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>{this.renderTransactionsData()}</tbody>
          </Table>
        </div>
      )
    } else {
      return (
        <div>
          <p> No loyalty program found for this customer </p>
        </div>
      )
    }
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(mapStateToProps)(LoyaltyData)
