import React, { Component } from 'react'
import { Panel, Glyphicon } from 'react-bootstrap'
import TransactionsData from './TransactionsData'

class Transactions extends Component {
  render() {
    return (
      <Panel eventKey="3">
        <Panel.Heading>
          <Panel.Title toggle>
            Wallet Transactions & Cashbacks
            <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
            <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body collapsible>
          <TransactionsData />
        </Panel.Body>
      </Panel>
    )
  }
}

export default Transactions
