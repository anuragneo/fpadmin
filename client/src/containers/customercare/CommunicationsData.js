import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, FormGroup, FormControl, ControlLabel, Button, Table, Glyphicon } from 'react-bootstrap'

class CommunicationsData extends Component {
  renderTableData() {
    return this.props.customarecare.communicationsData.map(com => {
      return (
        <tr key={com.id}>
          <td>{com.time}</td>
          <td>{com.channel}</td>
          <td>{com.message}</td>
          <td>{com.campaign}</td>
          <td>{com.campaignName}</td>
          <td>{com.active === 'Y' ? 'Active' : 'Deactive'}</td>
        </tr>
      )
    })
  }

  render() {
    return (
      <div>
        {this.props.customarecare.communicationsStatus === 'success' ? (
          <div>
            <Row>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">FROM</ControlLabel>
                  <FormControl componentClass="select" placeholder="select">
                    <option value="select">select</option>
                    <option value="other">...</option>
                  </FormControl>
                </FormGroup>
              </Col>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">TO</ControlLabel>
                  <FormControl componentClass="select" placeholder="select">
                    <option value="select">select</option>
                    <option value="other">...</option>
                  </FormControl>
                </FormGroup>
              </Col>

              <Col sm={2} mdOffset={4}>
                <ControlLabel className="greylalbel invisible">Test</ControlLabel>
                <Button bsStyle="primary">
                  <Glyphicon glyph="glyphicon glyphicon-search" /> Search
                </Button>
              </Col>
              <Col sm={2}>
                <ControlLabel className="greylalbel invisible">Test</ControlLabel>
                <Button bsStyle="secondary">
                  <Glyphicon glyph="glyphicon glyphicon-repeat" /> Reset
                </Button>
              </Col>
            </Row>

            <Table responsive striped bordered condensed hover>
              <thead>
                <tr>
                  <th>Timestamp</th>
                  <th>Channel</th>
                  <th>Message</th>
                  <th>Campaign ID</th>
                  <th>Campaign Name</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>{this.renderTableData()}</tbody>
            </Table>
          </div>
        ) : null}
      </div>
    )
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(mapStateToProps)(CommunicationsData)
