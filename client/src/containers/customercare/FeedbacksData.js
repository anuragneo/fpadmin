import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, ControlLabel, FormGroup, Button, Glyphicon, Table } from 'react-bootstrap'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import * as actions from './actions'

class FeedbacksData extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startDate: moment(),
      endDate: moment()
    }
  }

  startDateChange = date => {
    this.setState({
      startDate: date
    })
  }

  endDateChange = date => {
    this.setState({
      endDate: date
    })
  }

  search = () => {
    this.props.getFeedbacks({
      mobile: this.props.customarecare.profileData.mobile,
      startdate: this.state.startDate,
      enddate: this.state.endDate
    })
  }

  renderTableData() {
    if (this.props.customarecare.feedbackData && this.props.customarecare.feedbackData.length > 0) {
      return this.props.customarecare.feedbackData.map(feedback => {
        return (
          <tr key={feedback.txnid}>
            <td>{feedback.date}</td>
            <td>{feedback.txnid}</td>
            <td>{feedback.feedback}</td>
          </tr>
        )
      })
    } else {
      return (
        <tr>
          <td align="center" colSpan="3">
            No data Found
          </td>
        </tr>
      )
    }
  }

  render() {
    return (
      <div>
        {this.props.customarecare.feedbackStatus === 'success' ? (
          <div>
            <Row>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">FROM</ControlLabel>
                  <DatePicker
                    className="form-control"
                    dateFormat="DD/MM/YYYY"
                    selected={this.state.startDate}
                    onChange={this.startDateChange}
                  />
                </FormGroup>
              </Col>
              <Col sm={2}>
                <FormGroup>
                  <ControlLabel className="blacklabel2">TO</ControlLabel>
                  <DatePicker
                    className="form-control"
                    dateFormat="DD/MM/YYYY"
                    selected={this.state.endDate}
                    onChange={this.endDateChange}
                  />
                </FormGroup>
              </Col>

              <Col sm={2} mdOffset={4}>
                <ControlLabel className="greylalbel invisible">Test</ControlLabel>
                <Button bsStyle="primary" onClick={this.search}>
                  <Glyphicon glyph="glyphicon glyphicon-search" /> Search
                </Button>
              </Col>
              <Col sm={2}>
                <ControlLabel className="greylalbel invisible">Test</ControlLabel>
                <Button bsStyle="secondary">
                  <Glyphicon glyph="glyphicon glyphicon-repeat" /> Reset
                </Button>
              </Col>
            </Row>

            <Table responsive striped bordered condensed hover>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Txn Number</th>
                  <th>Feedback</th>
                </tr>
              </thead>
              <tbody>{this.renderTableData()}</tbody>
            </Table>
          </div>
        ) : null}
      </div>
    )
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(
  mapStateToProps,
  actions
)(FeedbacksData)
