import React, { Component } from 'react'
import { Glyphicon, Panel } from 'react-bootstrap'
import PaymenrPriorityData from './PaymentPriorityData'

class PaymentPriority extends Component {
  render() {
    return (
      <Panel eventKey="8">
        <Panel.Heading>
          <Panel.Title toggle>
            Payment Priority Details
            <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
            <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
          </Panel.Title>
        </Panel.Heading>

        <Panel.Body collapsible>
          <PaymenrPriorityData />
        </Panel.Body>
      </Panel>
    )
  }
}

export default PaymentPriority
