import axios from 'axios'
import * as types from './types'

const url = process.env.PUBLIC_URL

export function reset() {
  return {
    type: types.RESET
  }
}

export const getProfile = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/getprofile`, req)
  dispatch({ type: types.GET_PROFILE, payload: res.data })
  callback()
}

export const getPrepaidBalance = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/getprepaidbalance`, req)
  dispatch({ type: types.GET_PREPAID_BALANCE, payload: res.data })
}

export const getPaybackBalance = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/getpaybackbalance`, req)
  dispatch({ type: types.GET_PAYBACK_BALANCE, payload: res.data })
}

export const getBbpcBalance = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/getbbpcbalance`, req)
  dispatch({ type: types.GET_BBPC_BALANCE, payload: res.data })
}

export const getTransactions = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/transactions`, req)
  dispatch({ type: types.GET_TRANSACTIONS, payload: res.data })
}

export const getCommunications = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/communications`, req)
  dispatch({ type: types.GET_COMMUNICATIONS, payload: res.data })
}

export const getFeedbacks = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/feedbacks`, req)
  dispatch({ type: types.GET_FEEDBACK, payload: res.data })
}

export const getLoyalty = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/loyalty`, req)
  dispatch({ type: types.GET_LOYALTY, payload: res.data })
}

export const getPricematch = req => async dispatch => {
  const res = await axios.post(`${url}/api/customarecare/pricematch`, req)
  dispatch({ type: types.GET_PRICE_MATCH, payload: res.data })
}
