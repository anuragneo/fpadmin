import {
  GET_PROFILE,
  GET_PREPAID_BALANCE,
  GET_PAYBACK_BALANCE,
  GET_BBPC_BALANCE,
  GET_TRANSACTIONS,
  GET_COMMUNICATIONS,
  GET_FEEDBACK,
  GET_LOYALTY,
  GET_PRICE_MATCH,
  RESET
} from './actions/types'

export default function (state = {}, action) {
  switch (action.type) {
    case GET_PROFILE:
      return {
        ...state,
        profileStatus: action.payload.status,
        profileData: action.payload.data
      }
    case GET_PREPAID_BALANCE:
      return {
        ...state,
        prepaidStatus: action.payload.status,
        prepaidData: action.payload.data.prepaids,
        monthlylimit: action.payload.data.limit
      }
    case GET_PAYBACK_BALANCE:
      return {
        ...state,
        paybackStatus: action.payload.status,
        paybackData: action.payload.data
      }
    case GET_BBPC_BALANCE:
      return {
        ...state,
        bbpcStatus: action.payload.status,
        bbpcData: action.payload.data
      }
    case GET_TRANSACTIONS:
      return {
        ...state,
        transactionsStatus: action.payload.status,
        transactionsData: action.payload.data
      }
    case GET_COMMUNICATIONS:
      return {
        ...state,
        communicationsStatus: action.payload.status,
        communicationsData: action.payload.data
      }
    case GET_FEEDBACK:
      return {
        ...state,
        feedbackStatus: action.payload.status,
        feedbackData: action.payload.data
      }
    case GET_LOYALTY:
      return {
        ...state,
        loyaltyStatus: action.payload.status,
        loyaltyData: action.payload.data
      }
    case GET_PRICE_MATCH:
      return {
        ...state,
        pmStatus: action.payload.status,
        pmData: action.payload.data
      }
    case RESET:
      return {}
    default:
      return state
  }
}
