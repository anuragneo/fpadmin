import React, { Component } from 'react'
import { Glyphicon, Panel } from 'react-bootstrap'
import ProfileData from './ProfileData'

class Profile extends Component {
  render() {
    return (
      <Panel eventKey="1">
        <Panel.Heading>
          <Panel.Title toggle>
            Profile Details
            <Glyphicon glyph="glyphicon glyphicon-triangle-right" />
            <Glyphicon glyph="glyphicon glyphicon-triangle-bottom" />
          </Panel.Title>
        </Panel.Heading>

        <Panel.Body collapsible>
          <ProfileData />
        </Panel.Body>
      </Panel>
    )
  }
}

export default Profile
