import React, { Component } from 'react'
import { CanvasJS, CanvasJSChart } from './canvasjs.react'

class LineChart extends Component {
  render() {
    const options = {
      animationEnabled: true,
      theme: 'light2', // "light1", "dark1", "dark2"
      data: [
        {
          type: 'line',
          dataPoints: this.props.data
        }
      ]
    }
    return (
      <div>
        <CanvasJSChart options={options} />
      </div>
    )
  }
}
export default LineChart
