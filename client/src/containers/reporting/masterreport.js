import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import {
  Col,
  Grid,
  Row,
  ControlLabel,
  ButtonGroup,
  DropdownButton,
  MenuItem,
  Button,
  FormGroup,
  Modal
} from 'react-bootstrap'

class MasterReport extends Component {
  constructor(props) {
    super(props)
    this.customerReportFileRef = React.createRef()
    this.bbpcReportFileRef = React.createRef()
    this.bbpcTransactionFileRef = React.createRef()
    this.paybackReportFileRef = React.createRef()
    this.paybackTransactionFileRef = React.createRef()
    this.state = {
      showModal: false,
      modalText: '',
      selectDate: moment(),
      customerReportURL: 'javascript:void(0)',
      bbpcReportURL: 'javascript:void(0)',
      bbpcTransactionReportURL: 'javascript:void(0)',
      paybackReportURL: 'javascript:void(0)',
      paybackTransactionReportURL: 'javascript:void(0)'
    }
  }

  selectDateChange = date => {
    this.setState({
      selectDate: date
    })
  }

  handleClose = () => this.setState({ showModal: false })

  downloadMasterReport = () => {
    this.props.customerReport(
      {
        startdate: moment(this.state.selectDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.selectDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.customerReportStatus === 'success') {
          this.setState(
            {
              customerReportURL: this.props.report.customerReportData.url
            },
            () => {
              this.customerReportFileRef.current.click()
            }
          )
        } else if (this.props.report.customerReportStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.report.customerReportMessage
          })
        }
      }
    )
  }

  downloadbbpcReport = () => {
    this.props.bbpcReport(
      {
        startdate: moment(this.state.selectDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.selectDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.bbpcReportStatus === 'success') {
          this.setState(
            {
              bbpcReportURL: this.props.report.bbpcReportData.url
            },
            () => {
              this.bbpcReportFileRef.current.click()
            }
          )
        } else if (this.props.report.bbpcReportStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.report.bbpcReportMessage
          })
        }
      }
    )
  }

  downloadbbpcTransactionReport = () => {
    this.props.bbpcTransactionReport(
      {
        startdate: moment(this.state.selectDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.selectDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.bbpcTransactionReportStatus === 'success') {
          this.setState(
            {
              bbpcTransactionReportURL: this.props.report.bbpcTransactionReportData.url
            },
            () => {
              this.bbpcTransactionFileRef.current.click()
            }
          )
        } else if (this.props.report.bbpcTransactionReportStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.report.bbpcTransactionReportMessage
          })
        }
      }
    )
  }

  downloadPaybackReport = () => {
    this.props.paybackReport(
      {
        startdate: moment(this.state.selectDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.selectDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.paybackReportStatus === 'success') {
          this.setState(
            {
              paybackReportURL: this.props.report.paybackReportData.url
            },
            () => {
              this.paybackReportFileRef.current.click()
            }
          )
        } else if (this.props.report.paybackReportStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.report.paybackReportMessage
          })
        }
      }
    )
  }

  downloadPaybackTransactionReport = () => {
    this.props.paybackTransactionReport(
      {
        startdate: moment(this.state.selectDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.selectDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.paybackTransactionReportStatus === 'success') {
          this.setState(
            {
              paybackTransactionReportURL: this.props.report.paybackTransactionReportData.url
            },
            () => {
              this.paybackTransactionFileRef.current.click()
            }
          )
        } else if (this.props.report.paybackTransactionReportStatus === 'failed') {
          this.setState({
            showModal: true,
            modalText: this.props.report.paybackTransactionReportMessage
          })
        }
      }
    )
  }

  render() {
    return (
      <Col xs={10} className="scroll">
        <Grid fluid>
          <Row className="form-group">
            <Col sm={4}>
              <FormGroup>
                <DatePicker
                  className="form-control"
                  dateFormat="DD/MM/YYYY"
                  peekNextMonth
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                  selected={this.state.selectDate}
                  onChange={this.selectDateChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <hr />
          <Row className="form-group">
            <Col sm={6}>
              <ControlLabel>Customer Master Report</ControlLabel>
            </Col>
            <Col sm={6} right className="right reporting_groupbtn">
              <ButtonGroup>
                <DropdownButton title="Select Format" id="bg-nested-dropdown" style={{ display: 'none' }}>
                  <MenuItem eventKey="1">.tiff</MenuItem>
                  <MenuItem eventKey="2">.xls</MenuItem>
                </DropdownButton>
                <Button onClick={this.downloadMasterReport}>Export</Button>
              </ButtonGroup>
              <br />

              <a
                style={{ display: 'none' }}
                ref={this.customerReportFileRef}
                href={this.state.customerReportURL}
                download
              >
                Download Report
              </a>
            </Col>
          </Row>

          <hr />
          <Row className="form-group">
            <Col sm={6}>
              <ControlLabel>BBPC Linking Report</ControlLabel>
            </Col>
            <Col sm={6} right className="right reporting_groupbtn">
              <ButtonGroup>
                <DropdownButton title="Dropdown" id="bg-nested-dropdown" style={{ display: 'none' }}>
                  <MenuItem eventKey="1">.tiff</MenuItem>
                  <MenuItem eventKey="2">.xls</MenuItem>
                </DropdownButton>
                <Button onClick={this.downloadbbpcReport}>Export</Button>
              </ButtonGroup>
              <a style={{ display: 'none' }} ref={this.bbpcReportFileRef} href={this.state.bbpcReportURL} download>
                Download Report
              </a>
            </Col>
          </Row>
          <hr />
          <Row className="form-group">
            <Col sm={6}>
              <ControlLabel>BBPC Transaction Report</ControlLabel>
            </Col>
            <Col sm={6} right className="right reporting_groupbtn ">
              <ButtonGroup>
                <DropdownButton title="Dropdown" id="bg-nested-dropdown" style={{ display: 'none' }}>
                  <MenuItem eventKey="1">.tiff</MenuItem>
                  <MenuItem eventKey="2">.xls</MenuItem>
                </DropdownButton>
                <Button onClick={this.downloadbbpcTransactionReport}>Export</Button>
              </ButtonGroup>
              <a
                style={{ display: 'none' }}
                ref={this.bbpcTransactionFileRef}
                href={this.state.bbpcTransactionReportURL}
                download
              >
                Download Report
              </a>
            </Col>
          </Row>
          <hr />
          <Row className="form-group">
            <Col sm={6}>
              <ControlLabel>Payback Linking Report</ControlLabel>
            </Col>
            <Col sm={6} right className="right reporting_groupbtn">
              <ButtonGroup>
                <DropdownButton title="Select Format" id="bg-nested-dropdown" style={{ display: 'none' }}>
                  <MenuItem eventKey="1">.tiff</MenuItem>
                  <MenuItem eventKey="2">.xls</MenuItem>
                </DropdownButton>
                <Button onClick={this.downloadPaybackReport}>Export</Button>
              </ButtonGroup>
              <a
                style={{ display: 'none' }}
                ref={this.paybackReportFileRef}
                href={this.state.paybackReportURL}
                download
              >
                Download Report
              </a>
            </Col>
          </Row>
          <hr />
          <Row className="form-group">
            <Col sm={6}>
              <ControlLabel>Payback Transaction Report</ControlLabel>
            </Col>
            <Col sm={6} right className="right reporting_groupbtn">
              <ButtonGroup>
                <DropdownButton title="Select Format" id="bg-nested-dropdown" style={{ display: 'none' }}>
                  <MenuItem eventKey="1">.tiff</MenuItem>
                  <MenuItem eventKey="2">.xls</MenuItem>
                </DropdownButton>
                <Button onClick={this.downloadPaybackTransactionReport}>Export</Button>
              </ButtonGroup>
              <a
                style={{ display: 'none' }}
                ref={this.paybackTransactionFileRef}
                href={this.state.paybackTransactionReportURL}
                download
              >
                Download Report
              </a>
            </Col>
          </Row>
          <hr />
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
        </Grid>
      </Col>
    )
  }
}

function mapStateToProps({ report }) {
  return { report }
}

export default connect(
  mapStateToProps,
  actions
)(MasterReport)
