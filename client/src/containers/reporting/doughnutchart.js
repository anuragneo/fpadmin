import React, { Component } from 'react'
import { CanvasJS, CanvasJSChart } from './canvasjs.react'

class Doughnutchart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      graphData: []
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      const total = nextProps.data.reduce((a, b) => +a + +b.y, 0)
      this.setState({
        graphData: nextProps.data.map(element => {
          return {
            name: element.name,
            y: Number((element.y * 100) / total).toFixed(2)
          }
        })
      })
    }
  }

  render() {
    const options = {
      animationEnabled: true,

      data: [
        {
          type: 'doughnut',
          showInLegend: true,
          indexLabel: '{name}: {y}',
          yValueFormatString: "#,###'%'",
          dataPoints: this.state.graphData
        }
      ]
    }
    return (
      <div>
        <CanvasJSChart options={options} />
      </div>
    )
  }
}
export default Doughnutchart
