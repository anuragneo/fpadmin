import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import Sidebar from '../../components/sidebar'
import Customers from './customers'
import Payback from './payback'
import BBPC from './bbpc'
import BigBazaar from './bigbazaar'
import Central from './central'
import EasyDay from './easyday'
import BrandFactory from './brandfactory'
import MasterReport from './masterreport'

export default class Reporting extends Component {
  render() {
    if (this.props.history.location.pathname === '/' || this.props.history.location.pathname === '/reporting')
      this.props.history.push('/reporting/customers')
    return (
      <div>
        <Sidebar type="reporting" />
        <Route path="/reporting/customers" component={Customers} />
        <Route path="/reporting/payback" component={Payback} />
        <Route path="/reporting/bbpc" component={BBPC} />
        <Route path="/reporting/bigbazaar" component={BigBazaar} />
        <Route path="/reporting/central" component={Central} />
        <Route path="/reporting/easyday" component={EasyDay} />
        <Route path="/reporting/brandfactory" component={BrandFactory} />
        <Route path="/reporting/masterreport" component={MasterReport} />
      </div>
    )
  }
}
