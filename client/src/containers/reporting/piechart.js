import React, { Component } from 'react'
import { CanvasJS, CanvasJSChart } from './canvasjs.react'

class Piechart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      graphData: []
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      const total = nextProps.data.reduce((a, b) => +a + +b.y, 0)
      this.setState({
        graphData: nextProps.data.map(element => {
          return {
            label: element.label,
            y: Number((element.y * 100) / total).toFixed(2)
          }
        })
      })
    }
  }

  render() {
    const options = {
      animationEnabled: true,

      data: [
        {
          type: 'pie',
          startAngle: 75,
          toolTipContent: '<b>{label}</b>: {y}%',
          showInLegend: 'true',
          legendText: '{label}',
          indexLabelFontSize: 16,
          indexLabel: '{label} - {y}%',
          dataPoints: this.state.graphData
        }
      ]
    }
    return (
      <div>
        <CanvasJSChart options={options} />
      </div>
    )
  }
}
export default Piechart
