import React, { Component } from 'react'
import {
  Col,
  PageHeader,
  Row,
  FormGroup,
  FormControl,
  ControlLabel,
  Table,
  Grid,
  Button,
  Modal,
  Well,
  ButtonGroup,
  Glyphicon,
  DropdownButton,
  MenuItem
} from 'react-bootstrap'

export default class temp extends Component {
  render() {
    return (
      <Col xs={10} className="mt30 scroll">
        <Grid fluid>
          <Row>
            <Col xs={10} xsOffset={1} id="customer">
              <Row>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>From</ControlLabel>
                  </FormGroup>
                </Col>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>To</ControlLabel>
                  </FormGroup>
                </Col>
              </Row>
              <Row id="tabs-with-dropdown-pane-fifth">
                <Col xs={4}>
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title">Total No. of Users</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">BANNER</div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4}>
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title">Total No. of Active Users</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">BANNER</div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4}>
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title">Total New Registered Users</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">BANNER</div>
                    </div>
                  </Well>
                </Col>
              </Row>
              <FormGroup>
                <ControlLabel className="form-group">Customer Data</ControlLabel>
              </FormGroup>
              <ControlLabel>User Base</ControlLabel>
              <hr />
              <Row>
                <Col sm={6}>
                  <ControlLabel>Active vs Inactive</ControlLabel>
                  <hr />
                </Col>
                <Col sm={6}>
                  <ControlLabel>User Registation Through Various Channels</ControlLabel>
                  <hr />
                </Col>
              </Row>
              <Row>
                <Col sm={6}>
                  <ControlLabel>Customer Master</ControlLabel>
                </Col>
                <Col sm={6} right className="right">
                  <ButtonGroup>
                    <Button>Export</Button>
                    <DropdownButton title="Dropdown" id="bg-nested-dropdown">
                      <MenuItem eventKey="1">.tiff</MenuItem>
                      <MenuItem eventKey="2">.xls</MenuItem>
                    </DropdownButton>
                  </ButtonGroup>
                </Col>
              </Row>

              <Table responsive className="cspd panel">
                <thead>
                  <tr>
                    <th>Mobile Number</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>DOB</th>
                    <th>Age</th>
                    <th>Email</th>
                    <th>Postal Code</th>
                    <th>City Name</th>
                    <th>State Name</th>
                    <th>Date of Registration</th>
                    <th>Device Platform</th>
                    <th>Time of Registration</th>
                    <th>Device IMEI Platform</th>
                    <th>OS Version</th>
                    <th>Wallet Status</th>
                  </tr>
                </thead>
              </Table>

              <Row>
                <Col sm={6}>
                  <ControlLabel>App Data</ControlLabel>
                </Col>
                <Col sm={6} right className="right">
                  <ButtonGroup>
                    <Button>Export</Button>
                    <DropdownButton title="Dropdown" id="bg-nested-dropdown">
                      <MenuItem eventKey="1">.tiff</MenuItem>
                      <MenuItem eventKey="2">.xls</MenuItem>
                    </DropdownButton>
                  </ButtonGroup>
                </Col>
              </Row>

              <Table responsive className="cspd panel">
                <thead>
                  <tr>
                    <th>Mobile Number</th>
                    <th>Login Date</th>
                    <th>Device ID</th>
                    <th>Last Login Date</th>
                    <th>GPS Location</th>
                  </tr>
                </thead>
              </Table>

              <Row>
                <Col sm={6}>
                  <ControlLabel>Customer Information Update</ControlLabel>
                </Col>
                <Col sm={6} right className="right">
                  <ButtonGroup>
                    <Button>Export</Button>
                    <DropdownButton title="Dropdown" id="bg-nested-dropdown">
                      <MenuItem eventKey="1">.tiff</MenuItem>
                      <MenuItem eventKey="2">.xls</MenuItem>
                    </DropdownButton>
                  </ButtonGroup>
                </Col>
              </Row>

              <Table responsive className="cspd panel">
                <thead>
                  <tr>
                    <th>Mobile Number</th>
                    <th>Date & Time of Update</th>
                    <th>Field Updated</th>
                    <th>Old Value</th>
                    <th>New Value</th>
                  </tr>
                </thead>
              </Table>
            </Col>
            <Col xs={10} xsOffset={1} id="payback" hidden>
              <Row>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>From</ControlLabel>
                  </FormGroup>
                </Col>
                <Col sm={6}>
                  <FormGroup>
                    <ControlLabel>To</ControlLabel>
                  </FormGroup>
                </Col>
              </Row>
              <Row id="tabs-with-dropdown-pane-fifth">
                <Col xs={4}>
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title">Total Payback Active User</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">BANNER</div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4}>
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title">Total Newly Payback Registered User</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">BANNER</div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4}>
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title">Payback Delinking User</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">BANNER</div>
                    </div>
                  </Well>
                </Col>
              </Row>
              <FormGroup>
                <ControlLabel className="form-group">Payback Analysis</ControlLabel>
              </FormGroup>
              <ControlLabel>Payback User Base</ControlLabel>
              <hr />
              <Row>
                <Col sm={6}>
                  <ControlLabel>Linking Report</ControlLabel>
                  <hr />
                </Col>
                <Col sm={6}>
                  <ControlLabel>User Registration Through Various Channels</ControlLabel>
                  <hr />
                </Col>
              </Row>
              <Row>
                <Col sm={6}>
                  <ControlLabel>Linking Report</ControlLabel>
                </Col>
                <Col sm={6} right className="right">
                  <ButtonGroup>
                    <Button>Export</Button>
                    <DropdownButton title="Dropdown" id="bg-nested-dropdown">
                      <MenuItem eventKey="1">.tiff</MenuItem>
                      <MenuItem eventKey="2">.xls</MenuItem>
                    </DropdownButton>
                  </ButtonGroup>
                </Col>
              </Row>

              <Table responsive className="cspd panel">
                <thead>
                  <tr>
                    <th>Payback Membership ID</th>
                    <th>Future Pay Account ID</th>
                    <th>Mobile Number</th>
                    <th>Card Type</th>
                    <th>Card Number</th>
                    <th>Customer Name</th>
                    <th>Customer Mobile Number</th>
                  </tr>
                </thead>
              </Table>

              <Row>
                <Col sm={6}>
                  <ControlLabel>Transaction Report</ControlLabel>
                </Col>
                <Col sm={6} right className="right">
                  <ButtonGroup>
                    <Button>Export</Button>
                    <DropdownButton title="Dropdown" id="bg-nested-dropdown">
                      <MenuItem eventKey="1">.tiff</MenuItem>
                      <MenuItem eventKey="2">.xls</MenuItem>
                    </DropdownButton>
                  </ButtonGroup>
                </Col>
              </Row>

              <Table responsive className="cspd panel">
                <thead>
                  <tr>
                    <th>Card Type</th>
                    <th>ID</th>
                    <th>Mobile Number</th>
                    <th>Wallet ID</th>
                    <th>Wallet Owner</th>
                    <th>Store Code</th>
                    <th>Store Name</th>
                    <th>FG Txn Number</th>
                    <th>Oxigen Txn Number</th>
                    <th>Transaction Type</th>
                    <th>Transaction Sub Type</th>
                    <th>Trnasaction Source</th>
                    <th>Amount</th>
                    <th>Transaction Date</th>
                    <th>Transaction status</th>
                    <th>Cash Memo No.</th>
                    <th>Terminal ID</th>
                    <th>Store Type</th>
                    <th>Balance</th>
                    <th>Linked Card</th>
                    <th>Time Stamp Number</th>
                    <th>Pay UID</th>
                  </tr>
                </thead>
              </Table>
            </Col>
          </Row>
        </Grid>
      </Col>
    )
  }
}
