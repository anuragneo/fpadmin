import axios from 'axios'
import * as types from './types'

const url = process.env.PUBLIC_URL

export const totalUser = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/totaluser`, req)
  dispatch({ type: types.TOTAL_USER, payload: res.data })
  callback()
}

export const activeUser = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/activeuser`, req)
  dispatch({ type: types.ACTIVE_USER, payload: res.data })
  callback()
}

export const newUser = req => async dispatch => {
  const res = await axios.post(`${url}/api/report/newregistered`, req)
  dispatch({ type: types.NEW_REGISTERED, payload: res.data })
}

export const registerThroughSource = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/registrationthroughsource`, req)
  dispatch({ type: types.REGISTERED_THROUGH_SOURCE, payload: res.data })
  callback()
}

export const groupCustomer = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/customerbydate`, req)
  dispatch({ type: types.GROUP_CUSTOMER, payload: res.data })
  callback()
}

export const customerReport = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/customerreport`, req)
  dispatch({ type: types.CUSTOMER_REPORT, payload: res.data })
  callback()
}

export const totalActiveUserWallet = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/totalactiveuserwallet`, req)
  dispatch({ type: types.TOTAL_ACTIVE_USER_WALLET, payload: res.data })
  callback()
}

export const totalSpent = req => async dispatch => {
  const res = await axios.post(`${url}/api/report/totalspent`, req)
  dispatch({ type: types.TOTAL_SPENT, payload: res.data })
}

export const totalTopup = req => async dispatch => {
  const res = await axios.post(`${url}/api/report/totaltopup`, req)
  dispatch({ type: types.TOTAL_TOPUP, payload: res.data })
}

export const totalCashback = req => async dispatch => {
  const res = await axios.post(`${url}/api/report/totalcashback`, req)
  dispatch({ type: types.TOTAL_CASHBACK, payload: res.data })
}

export const debitVolume = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/debitvolume`, req)
  dispatch({ type: types.DEBIT_VOLUME, payload: res.data })
  callback()
}

export const countgroupDebit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/countgroupdebit`, req)
  dispatch({ type: types.COUNT_GROUP_DEBIT, payload: res.data })
  callback()
}

export const creditVolume = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/creditvolume`, req)
  dispatch({ type: types.CREDIT_VOLUME, payload: res.data })
  callback()
}

export const countgroupCredit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/countgroupcredit`, req)
  dispatch({ type: types.COUNT_GROUP_CREDIT, payload: res.data })
  callback()
}

export const cashbackAnalysis = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/cashbackanalysis`, req)
  dispatch({ type: types.CASHBACK_ANALYSIS, payload: res.data })
  callback()
}

export const cashbackExperiable = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/cashbackexperiable`, req)
  dispatch({ type: types.CASHBACK_EXPRIABLE, payload: res.data })
  callback()
}

export const groupByCampaign = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/groupbycampaignname`, req)
  dispatch({ type: types.GROUP_BY_CAMPAIGN_NAME, payload: res.data })
  callback()
}

export const newBbpcRegistered = req => async dispatch => {
  const res = await axios.post(`${url}/api/report/newbbpcpaybackregistered`, req)
  dispatch({ type: types.NEW_BBPC_REGISTERED, payload: res.data })
}

export const newPaybackRegistered = req => async dispatch => {
  const res = await axios.post(`${url}/api/report/newbbpcpaybackregistered`, req)
  dispatch({ type: types.NEW_PAYBACK_REGISTERED, payload: res.data })
}

export const bbpcDelinking = req => async dispatch => {
  const res = await axios.post(`${url}/api/report/totaldelinkingbbpcpayback`, req)
  dispatch({ type: types.TOTAL_BBPC_DELINKING, payload: res.data })
}

export const paybackDelinking = req => async dispatch => {
  const res = await axios.post(`${url}/api/report/totaldelinkingbbpcpayback`, req)
  dispatch({ type: types.TOTAL_PAYBACK_DELINKING, payload: res.data })
}

export const bbpcLinked = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/totallinkedbbpcpayback`, req)
  dispatch({ type: types.TOTAL_BBPC_LINKED, payload: res.data })
  callback()
}

export const paybackLinked = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/totallinkedbbpcpayback`, req)
  dispatch({ type: types.TOTAL_PAYBACK_LINKED, payload: res.data })
  callback()
}

export const bbpcDelinked = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/totaldeinkedbbpcpayback`, req)
  dispatch({ type: types.TOTAL_BBPC_DELINKED, payload: res.data })
  callback()
}

export const paybackDelinked = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/totaldeinkedbbpcpayback`, req)
  dispatch({ type: types.TOTAL_PAYBACK_DELINKED, payload: res.data })
  callback()
}

export const bbpcReport = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/bbpcreport`, req)
  dispatch({ type: types.BBPC_REPORT, payload: res.data })
  callback()
}

export const paybackReport = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/paybackreport`, req)
  dispatch({ type: types.PAYBACK_REPORT, payload: res.data })
  callback()
}

export const bbpcTransactionReport = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/bbpctransactionreport`, req)
  dispatch({ type: types.BBPC_TRANSACTION_REPORT, payload: res.data })
  callback()
}

export const paybackTransactionReport = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/paybacktransactionreport`, req)
  dispatch({ type: types.PAYBACK_TRANSACTION_REEPORT, payload: res.data })
  callback()
}

export const successRatioDebit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/successratiodebit`, req)
  dispatch({ type: types.SUCCESS_RATIO_DEBIT, payload: res.data })
  callback()
}

export const successRatioCredit = (req, callback) => async dispatch => {
  const res = await axios.post(`${url}/api/report/successratiocredit`, req)
  dispatch({ type: types.SUCCESS_RATIO_CREDIT, payload: res.data })
  callback()
}
