import {
  TOTAL_USER,
  ACTIVE_USER,
  NEW_REGISTERED,
  REGISTERED_THROUGH_SOURCE,
  GROUP_CUSTOMER,
  CUSTOMER_REPORT,
  TOTAL_SPENT,
  TOTAL_TOPUP,
  TOTAL_CASHBACK,
  DEBIT_VOLUME,
  COUNT_GROUP_DEBIT,
  CREDIT_VOLUME,
  COUNT_GROUP_CREDIT,
  CASHBACK_ANALYSIS,
  CASHBACK_EXPRIABLE,
  GROUP_BY_CAMPAIGN_NAME,
  PAYBACK_REPORT,
  BBPC_REPORT,
  TOTAL_ACTIVE_USER_WALLET,
  TOTAL_PAYBACK_DELINKED,
  TOTAL_BBPC_DELINKED,
  TOTAL_PAYBACK_LINKED,
  TOTAL_BBPC_LINKED,
  TOTAL_BBPC_DELINKING,
  TOTAL_PAYBACK_DELINKING,
  NEW_BBPC_REGISTERED,
  NEW_PAYBACK_REGISTERED,
  BBPC_TRANSACTION_REPORT,
  PAYBACK_TRANSACTION_REEPORT,
  SUCCESS_RATIO_DEBIT,
  SUCCESS_RATIO_CREDIT
} from './actions/types'

export default function(state = {}, action) {
  switch (action.type) {
    case TOTAL_USER:
      return {
        ...state,
        totalUserStatus: action.payload.status,
        totalUserData: action.payload.data,
        totalUserMessage: action.payload.message
      }
    case ACTIVE_USER:
      return {
        ...state,
        activeUserStatus: action.payload.status,
        activeUserData: action.payload.data,
        activeUserMessage: action.payload.message
      }
    case NEW_REGISTERED:
      return {
        ...state,
        newRegisteredStatus: action.payload.status,
        newRegisteredData: action.payload.data,
        newRegisteredMessage: action.payload.message
      }
    case REGISTERED_THROUGH_SOURCE:
      return {
        ...state,
        rtsStatus: action.payload.status,
        rtsData: action.payload.data,
        rtsMessage: action.payload.message
      }
    case GROUP_CUSTOMER:
      return {
        ...state,
        groupCustomerStatus: action.payload.status,
        groupCustomerData: action.payload.data,
        groupCustomerMessage: action.payload.message
      }
    case CUSTOMER_REPORT:
      return {
        ...state,
        customerReportStatus: action.payload.status,
        customerReportData: action.payload.data,
        customerReportMessage: action.payload.message
      }
    case TOTAL_ACTIVE_USER_WALLET:
      return {
        ...state,
        totalActiveUserWalletStatus: action.payload.status,
        totalActiveUserWalletData: action.payload.data,
        totalActiveUserWalletMessage: action.payload.message
      }
    case TOTAL_SPENT:
      return {
        ...state,
        totalSpentStatus: action.payload.status,
        totalSpentData: action.payload.data,
        totalSpentMessage: action.payload.message
      }
    case TOTAL_TOPUP:
      return {
        ...state,
        totalTopupStatus: action.payload.status,
        totalTopupData: action.payload.data,
        totalTopupMessage: action.payload.message
      }
    case TOTAL_CASHBACK:
      return {
        ...state,
        totalCashbackStatus: action.payload.status,
        totalCashbackData: action.payload.data,
        totalCashbackMessage: action.payload.message
      }
    case DEBIT_VOLUME:
      return {
        ...state,
        debitVolumeStatus: action.payload.status,
        debitVolumeData: action.payload.data,
        debitVolumeMessage: action.payload.message
      }
    case COUNT_GROUP_DEBIT:
      return {
        ...state,
        countGroupDebitStatus: action.payload.status,
        countGroupDebitData: action.payload.data,
        countGroupDebitMessage: action.payload.message
      }
    case CREDIT_VOLUME:
      return {
        ...state,
        creditVolumeStatus: action.payload.status,
        creditVolumeData: action.payload.data,
        creditVolumeMessage: action.payload.message
      }
    case COUNT_GROUP_CREDIT:
      return {
        ...state,
        countGroupCreditStatus: action.payload.status,
        countGroupCreditData: action.payload.data,
        countGroupCreditMessage: action.payload.message
      }
    case CASHBACK_ANALYSIS:
      return {
        ...state,
        cashbackAnalysisStatus: action.payload.status,
        cashbackAnalysisData: action.payload.data,
        cashbackAnalysisMessage: action.payload.message
      }
    case CASHBACK_EXPRIABLE:
      return {
        ...state,
        cashbackExperiableStatus: action.payload.status,
        cashbackExperiableData: action.payload.data,
        cashbackExperiableMessage: action.payload.message
      }
    case GROUP_BY_CAMPAIGN_NAME:
      return {
        ...state,
        groupByCampaignStatus: action.payload.status,
        groupByCampaignData: action.payload.data,
        groupByCampaignMessage: action.payload.message
      }
    case NEW_BBPC_REGISTERED:
      return {
        ...state,
        bbpcRegisteredStatus: action.payload.status,
        bbpcregisteredData: action.payload.data,
        bbpcRegisteredMessage: action.payload.message
      }
    case NEW_PAYBACK_REGISTERED:
      return {
        ...state,
        paybackRegisteredStatus: action.payload.status,
        paybackRegisteredData: action.payload.data,
        paybackRegisteredMessage: action.payload.message
      }
    case TOTAL_BBPC_DELINKING:
      return {
        ...state,
        bbpcDelinkingStatus: action.payload.status,
        bbpcDelinkingData: action.payload.data,
        bbpcDelinkingMessage: action.payload.message
      }
    case TOTAL_PAYBACK_DELINKING:
      return {
        ...state,
        paybackDelinkingStatus: action.payload.status,
        paybackDelinkingData: action.payload.data,
        paybackDelinkingMessage: action.payload.message
      }
    case TOTAL_BBPC_LINKED:
      return {
        ...state,
        bbpcLinkedStatus: action.payload.status,
        bbpcLinkedData: action.payload.data,
        bbpcLinkedMessage: action.payload.message
      }
    case TOTAL_PAYBACK_LINKED:
      return {
        ...state,
        paybackLinkedStatus: action.payload.status,
        paybackLinkedData: action.payload.data,
        paybackLinkedMessage: action.payload.message
      }
    case TOTAL_BBPC_DELINKED:
      return {
        ...state,
        bbpcDelinkedStatus: action.payload.status,
        bbpcDelinkedData: action.payload.data,
        bbpcDelinkedMessage: action.payload.message
      }
    case TOTAL_PAYBACK_DELINKED:
      return {
        ...state,
        paybackDelinkedStatus: action.payload.status,
        paybackDelinkedData: action.payload.data,
        paybackDelinkedMessage: action.payload.message
      }
    case BBPC_REPORT:
      return {
        ...state,
        bbpcReportStatus: action.payload.status,
        bbpcReportData: action.payload.data,
        bbpcReportMessage: action.payload.message
      }
    case PAYBACK_REPORT:
      return {
        ...state,
        paybackReportStatus: action.payload.status,
        paybackReportData: action.payload.data,
        paybackReportMessage: action.payload.message
      }
    case BBPC_TRANSACTION_REPORT:
      return {
        ...state,
        bbpcTransactionReportStatus: action.payload.status,
        bbpcTransactionReportData: action.payload.data,
        bbpcTransactionReportMessage: action.payload.message
      }
    case PAYBACK_TRANSACTION_REEPORT:
      return {
        ...state,
        paybackTransactionReportStatus: action.payload.status,
        paybackTransactionReportData: action.payload.data,
        paybackTransactionReportMessage: action.payload.message
      }
    case SUCCESS_RATIO_DEBIT:
      return {
        ...state,
        successRatioDebitStatus: action.payload.status,
        successRatioDebitData: action.payload.data,
        successRatioDebitMessage: action.payload.message
      }
    case SUCCESS_RATIO_CREDIT:
      return {
        ...state,
        successRatioCreditStatus: action.payload.status,
        successRatioCreditData: action.payload.data,
        successRatioCreditMessage: action.payload.message
      }

    default:
      return state
  }
}
