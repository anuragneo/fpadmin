import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Row, FormGroup, ControlLabel, Grid, Well } from 'react-bootstrap'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import * as actions from './actions'
import LineChart from './linechart'
import Piechart from './piechart'
import Doughnutchart from './doughnutchart'

class Payback extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lineChartData: [],
      doughnutChartDataLinked: '',
      doughnutChartDataDeLinked: '',
      startDate: moment(),
      endDate: moment()
    }
  }

  startDateChange = date => {
    this.setState(
      {
        startDate: date
      },
      () => {
        this.props.totalActiveUserWallet(
          {
            walletid: '97',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {}
        )
        this.props.newPaybackRegistered({
          type: 'payback',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.paybackDelinking({
          type: 'payback',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.groupCustomer(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.groupCustomerStatus === 'success' && this.props.report.groupCustomerData) {
              let graphData = []
              this.props.report.groupCustomerData.forEach(element => {
                graphData.push({ y: element.customer, x: moment(element.RegistrationDate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ lineChartData: graphData })
            }
          }
        )
        this.props.paybackLinked(
          {
            type: 'payback',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.paybackLinkedStatus === 'success' && this.props.report.paybackLinkedData) {
              this.setState({ doughnutChartDataLinked: this.props.report.paybackLinkedData[0].linked_count })
            }
          }
        )
        this.props.paybackDelinked(
          {
            type: 'payback',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.paybackDelinkedStatus === 'success' && this.props.report.paybackDelinkedData) {
              this.setState({ doughnutChartDataDeLinked: this.props.report.paybackDelinkedData[0].delinked_count })
            }
          }
        )
      }
    )
  }

  endDateChange = date => {
    this.setState(
      {
        endDate: date
      },
      () => {
        this.props.totalActiveUserWallet(
          {
            walletid: '97',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {}
        )
        this.props.newPaybackRegistered({
          type: 'payback',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.paybackDelinking({
          type: 'payback',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.groupCustomer(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.groupCustomerStatus === 'success' && this.props.report.groupCustomerData) {
              let graphData = []
              this.props.report.groupCustomerData.forEach(element => {
                graphData.push({ y: element.customer, x: moment(element.RegistrationDate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ lineChartData: graphData })
            }
          }
        )
        this.props.paybackLinked(
          {
            type: 'payback',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.paybackLinkedStatus === 'success' && this.props.report.paybackLinkedData) {
              this.setState({ doughnutChartDataLinked: this.props.report.paybackLinkedData[0].linked_count })
            }
          }
        )
        this.props.paybackDelinked(
          {
            type: 'payback',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.paybackDelinkedStatus === 'success' && this.props.report.paybackDelinkedData) {
              this.setState({ doughnutChartDataDeLinked: this.props.report.paybackDelinkedData[0].delinked_count })
            }
          }
        )
      }
    )
  }

  componentDidMount() {
    this.props.totalActiveUserWallet(
      {
        walletid: '97',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {}
    )
    this.props.newPaybackRegistered({
      type: 'payback',
      startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
      enddate: moment(this.state.endDate).format('DD/MM/YYYY')
    })
    this.props.paybackDelinking({
      type: 'payback',
      startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
      enddate: moment(this.state.endDate).format('DD/MM/YYYY')
    })
    this.props.groupCustomer(
      {
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.groupCustomerStatus === 'success' && this.props.report.groupCustomerData) {
          let graphData = []
          this.props.report.groupCustomerData.forEach(element => {
            graphData.push({ y: element.customer, x: moment(element.RegistrationDate, 'DD-MM-YYYY').toDate() })
          })
          this.setState({ lineChartData: graphData })
        }
      }
    )
    this.props.paybackLinked(
      {
        type: 'payback',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.paybackLinkedStatus === 'success' && this.props.report.paybackLinkedData) {
          this.setState({ doughnutChartDataLinked: this.props.report.paybackLinkedData[0].linked_count })
        }
      }
    )
    this.props.paybackDelinked(
      {
        type: 'payback',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.paybackDelinkedStatus === 'success' && this.props.report.paybackDelinkedData) {
          this.setState({ doughnutChartDataDeLinked: this.props.report.paybackDelinkedData[0].delinked_count })
        }
      }
    )
  }

  render() {
    return (
      <Col xs={10} className="scroll">
        <Grid fluid>
          <Row>
            <Col xs={12} id="payback">
              <Row className="form-group">
                <Col sm={4}>
                  <FormGroup>
                    <ControlLabel>From</ControlLabel>
                    <DatePicker
                      className="form-control"
                      dateFormat="DD/MM/YYYY"
                      minDate={new Date(new Date().setDate(new Date().getDate() - 90))}
                      selected={this.state.startDate}
                      onChange={this.startDateChange}
                    />
                  </FormGroup>
                </Col>
                <Col sm={4}>
                  <FormGroup>
                    <ControlLabel>To</ControlLabel>
                    <DatePicker
                      className="form-control"
                      dateFormat="DD/MM/YYYY"
                      selected={this.state.endDate}
                      onChange={this.endDateChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row id="tabs-with-dropdown-pane-fifth" className="reporting form-group">
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total payback Active User</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.totalActiveUserWalletStatus === 'success'
                          ? this.props.report.totalActiveUserWalletData[0].totalactive
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total Newly payback Registered User</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.paybackRegisteredStatus === 'success'
                          ? this.props.report.paybackRegisteredData[0].registration_count
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Payback Delinking User</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.paybackDelinkingStatus === 'success'
                          ? this.props.report.paybackDelinkingData[0].registration_count
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
              </Row>
              <FormGroup>
                <h4 className="acct_manage_hist_label">Payback Analysis</h4>
              </FormGroup>
              <ControlLabel>Payback User Base</ControlLabel>
              <hr />
              <Row className="form-group">
                <Col xs={12} className="form-group">
                  <LineChart data={this.state.lineChartData} />
                </Col>
              </Row>
              <Row className="form-group">
                <Col sm={6} className="form-group">
                  <ControlLabel className="form-group">Linking vs Delinking</ControlLabel>
                  <hr />
                  <Doughnutchart
                    data={[
                      { name: 'Linked', y: this.state.doughnutChartDataLinked },
                      { name: 'Delinked', y: this.state.doughnutChartDataDeLinked }
                    ]}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </Col>
    )
  }
}

function mapStateToProps({ report }) {
  return { report }
}

export default connect(
  mapStateToProps,
  actions
)(Payback)
