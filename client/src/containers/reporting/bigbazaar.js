import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Row, FormGroup, ControlLabel, Grid, Well, Table, Image } from 'react-bootstrap'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import * as actions from './actions'
import LineChart from './linechart'
import Piechart from './piechart'
import Doughnutchart from './doughnutchart'
import BarChart from './barchart'
import html2canvas from 'html2canvas'
import pdficon from '../../images/pdficon.png'
import * as jsPDF from 'jspdf'
import * as _ from 'underscore'

class BigBazaar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pieChartData: [],
      doughnutChartDataActive: '',
      doughnutChartDataTotal: '',
      debitVolumeLineChart: [],
      debitBarChart: [],
      debitTranscationChart: [],
      creditVolumeLineChart: [],
      creditBarChart: [],
      creditTranscationChart: [],
      cashbackAnalysisChart: [],
      cashbackExperiableChart: [],
      cashbackCampaignChart: [],
      startDate: moment(),
      endDate: moment()
    }
  }

  startDateChange = date => {
    this.setState(
      {
        startDate: date
      },
      () => {
        this.props.totalUser({ enddate: this.state.endDate }, () => {
          if (this.props.report.totalUserStatus === 'success') {
            this.setState({ doughnutChartDataTotal: this.props.report.totalUserData[0].totalcount })
          }
        })
        this.props.totalActiveUserWallet(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.totalActiveUserWalletStatus === 'success')
              this.setState({ doughnutChartDataActive: this.props.report.totalActiveUserWalletData[0].totalactive })
          }
        )
        this.props.newUser({
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.totalCashback({
          walletid: '01',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.totalSpent({
          walletid: '01',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.totalTopup({
          walletid: '01',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.registerThroughSource(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.rtsStatus === 'success' && this.props.report.rtsData) {
              let graphData = []
              this.props.report.rtsData.forEach(element => {
                graphData.push({ y: element.customercount, label: element.regsource })
              })
              this.setState({ pieChartData: graphData })
            }
          }
        )
        this.props.debitVolume(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.debitVolumeStatus === 'success' && this.props.report.debitVolumeData) {
              let graphData = []
              this.props.report.debitVolumeData.forEach(element => {
                graphData.push({ y: element.spentvolume, x: moment(element.spentdate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ debitVolumeLineChart: graphData })
            }
          }
        )
        this.props.countgroupDebit(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.countGroupDebitStatus === 'success' && this.props.report.countGroupDebitData) {
              let graphData = []
              this.props.report.countGroupDebitData.forEach(element => {
                graphData.push({ y: element.spentvolume, label: element.channeltype })
              })
              this.setState({ debitTranscationChart: graphData })
            }
          }
        )
        this.props.creditVolume(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.creditVolumeStatus === 'success' && this.props.report.creditVolumeData) {
              let graphData = []
              this.props.report.creditVolumeData.forEach(element => {
                graphData.push({ y: element.topupvolume, x: moment(element.topupdate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ creditVolumeLineChart: graphData })
            }
          }
        )
        this.props.countgroupCredit(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.countGroupCreditStatus === 'success' && this.props.report.countGroupCreditData) {
              let graphData = []
              this.props.report.countGroupCreditData.forEach(element => {
                graphData.push({ y: element.topupvolume, label: element.channeltype })
              })
              this.setState({ creditTranscationChart: graphData })
            }
          }
        )
        this.props.cashbackAnalysis(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.cashbackAnalysisStatus === 'success' && this.props.report.cashbackAnalysisData) {
              let graphData = []
              this.props.report.cashbackAnalysisData.forEach(element => {
                graphData.push({ y: element.cbvolume, x: moment(element.cbdate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ cashbackAnalysisChart: graphData })
            }
          }
        )
        this.props.cashbackExperiable(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.cashbackExperiableStatus === 'success' && this.props.report.cashbackExperiableData) {
              let graphData = []
              graphData.push({
                name: 'Experiable',
                y: this.props.report.cashbackExperiableData[0].expirable
              })
              graphData.push({ name: 'NonExperiable', y: this.props.report.cashbackExperiableData[0].nonexpirable })
              this.setState({ cashbackExperiableChart: graphData })
            }
          }
        )
        this.props.groupByCampaign(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.groupByCampaignStatus === 'success' && this.props.report.groupByCampaignData) {
              let graphData = []
              this.props.report.groupByCampaignData.forEach(element => {
                graphData.push({ y: element.cbvolume, label: element.cbname })
              })
              this.setState({ cashbackCampaignChart: graphData })
            }
          }
        )
        this.props.successRatioDebit(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.successRatioDebitStatus === 'success' && this.props.report.successRatioDebitData) {
              const channeltype = _.uniq(_.pluck(this.props.report.successRatioDebitData, 'channeltype'))

              const successRatio = channeltype.map(element => {
                return {
                  label: element,
                  y: this.props.report.successRatioDebitData.reduce(function(sum, record) {
                    if (record.state === 'completed' && record.channeltype === element) return sum + record.txncount
                    else return sum
                  }, 0)
                }
              })

              const failedRatio = channeltype.map(element => {
                return {
                  label: element,
                  y: this.props.report.successRatioDebitData.reduce(function(sum, record) {
                    if (record.state === 'failed' && record.channeltype === element) return sum + record.txncount
                    else return sum
                  }, 0)
                }
              })

              this.setState({ debitBarChart: { success: successRatio, failed: failedRatio } })
            }
          }
        )
        this.props.successRatioCredit(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.successRatioCreditStatus === 'success' && this.props.report.successRatioCreditData) {
              const channeltype = _.uniq(_.pluck(this.props.report.successRatioCreditData, 'channeltype'))

              const successRatio = channeltype.map(element => {
                return {
                  label: element,
                  y: this.props.report.successRatioCreditData.reduce(function(sum, record) {
                    if (record.state === 'completed' && record.channeltype === element) return sum + record.txncount
                    else return sum
                  }, 0)
                }
              })

              const failedRatio = channeltype.map(element => {
                return {
                  label: element,
                  y: this.props.report.successRatioCreditData.reduce(function(sum, record) {
                    if (record.state === 'failed' && record.channeltype === element) return sum + record.txncount
                    else return sum
                  }, 0)
                }
              })

              this.setState({ creditBarChart: { success: successRatio, failed: failedRatio } })
            }
          }
        )
      }
    )
  }

  endDateChange = date => {
    this.setState(
      {
        endDate: date
      },
      () => {
        this.props.totalUser({ enddate: this.state.endDate }, () => {
          if (this.props.report.totalUserStatus === 'success') {
            this.setState({ doughnutChartDataTotal: this.props.report.totalUserData[0].totalcount })
          }
        })
        this.props.totalActiveUserWallet(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.totalActiveUserWalletStatus === 'success')
              this.setState({ doughnutChartDataActive: this.props.report.totalActiveUserWalletData[0].totalactive })
          }
        )
        this.props.newUser({
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.totalCashback({
          walletid: '01',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.totalSpent({
          walletid: '01',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.totalTopup({
          walletid: '01',
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.registerThroughSource(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.rtsStatus === 'success' && this.props.report.rtsData) {
              let graphData = []
              this.props.report.rtsData.forEach(element => {
                graphData.push({ y: element.customercount, label: element.regsource })
              })
              this.setState({ pieChartData: graphData })
            }
          }
        )
        this.props.debitVolume(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.debitVolumeStatus === 'success' && this.props.report.debitVolumeData) {
              let graphData = []
              this.props.report.debitVolumeData.forEach(element => {
                graphData.push({ y: element.spentvolume, x: moment(element.spentdate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ debitVolumeLineChart: graphData })
            }
          }
        )
        this.props.countgroupDebit(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.countGroupDebitStatus === 'success' && this.props.report.countGroupDebitData) {
              let graphData = []
              this.props.report.countGroupDebitData.forEach(element => {
                graphData.push({ y: element.spentvolume, label: element.channeltype })
              })
              this.setState({ debitTranscationChart: graphData })
            }
          }
        )
        this.props.creditVolume(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.creditVolumeStatus === 'success' && this.props.report.creditVolumeData) {
              let graphData = []
              this.props.report.creditVolumeData.forEach(element => {
                graphData.push({ y: element.topupvolume, x: moment(element.topupdate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ creditVolumeLineChart: graphData })
            }
          }
        )
        this.props.countgroupCredit(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.countGroupCreditStatus === 'success' && this.props.report.countGroupCreditData) {
              let graphData = []
              this.props.report.countGroupCreditData.forEach(element => {
                graphData.push({ y: element.topupvolume, label: element.channeltype })
              })
              this.setState({ creditTranscationChart: graphData })
            }
          }
        )
        this.props.cashbackAnalysis(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.cashbackAnalysisStatus === 'success' && this.props.report.cashbackAnalysisData) {
              let graphData = []
              this.props.report.cashbackAnalysisData.forEach(element => {
                graphData.push({ y: element.cbvolume, x: moment(element.cbdate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ cashbackAnalysisChart: graphData })
            }
          }
        )
        this.props.cashbackExperiable(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.cashbackExperiableStatus === 'success' && this.props.report.cashbackExperiableData) {
              let graphData = []
              graphData.push({
                name: 'Experiable',
                y: this.props.report.cashbackExperiableData[0].experiable
              })
              graphData.push({ name: 'NonExperiable', y: this.props.report.cashbackExperiableData[0].nonexperiable })
              this.setState({ cashbackExperiableChart: graphData })
            }
          }
        )
        this.props.groupByCampaign(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.groupByCampaignStatus === 'success' && this.props.report.groupByCampaignData) {
              let graphData = []
              graphData.push({
                name: 'Experiable',
                y: this.props.report.cashbackExperiableData[0].expirable
              })
              graphData.push({ name: 'NonExperiable', y: this.props.report.cashbackExperiableData[0].nonexpirable })
              this.setState({ cashbackExperiableChart: graphData })
            }
          }
        )
        this.props.successRatioDebit(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.successRatioDebitStatus === 'success' && this.props.report.successRatioDebitData) {
              const channeltype = _.uniq(_.pluck(this.props.report.successRatioDebitData, 'channeltype'))

              const successRatio = channeltype.map(element => {
                return {
                  label: element,
                  y: this.props.report.successRatioDebitData.reduce(function(sum, record) {
                    if (record.state === 'completed' && record.channeltype === element) return sum + record.txncount
                    else return sum
                  }, 0)
                }
              })

              const failedRatio = channeltype.map(element => {
                return {
                  label: element,
                  y: this.props.report.successRatioDebitData.reduce(function(sum, record) {
                    if (record.state === 'failed' && record.channeltype === element) return sum + record.txncount
                    else return sum
                  }, 0)
                }
              })

              this.setState({ debitBarChart: { success: successRatio, failed: failedRatio } })
            }
          }
        )
        this.props.successRatioCredit(
          {
            walletid: '01',
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.successRatioCreditStatus === 'success' && this.props.report.successRatioCreditData) {
              const channeltype = _.uniq(_.pluck(this.props.report.successRatioCreditData, 'channeltype'))

              const successRatio = channeltype.map(element => {
                return {
                  label: element,
                  y: this.props.report.successRatioCreditData.reduce(function(sum, record) {
                    if (record.state === 'completed' && record.channeltype === element) return sum + record.txncount
                    else return sum
                  }, 0)
                }
              })

              const failedRatio = channeltype.map(element => {
                return {
                  label: element,
                  y: this.props.report.successRatioCreditData.reduce(function(sum, record) {
                    if (record.state === 'failed' && record.channeltype === element) return sum + record.txncount
                    else return sum
                  }, 0)
                }
              })

              this.setState({ creditBarChart: { success: successRatio, failed: failedRatio } })
            }
          }
        )
      }
    )
  }

  componentDidMount() {
    this.props.totalUser({ enddate: this.state.endDate }, () => {
      if (this.props.report.totalUserStatus === 'success') {
        this.setState({ doughnutChartDataTotal: this.props.report.totalUserData[0].totalcount })
      }
    })
    this.props.totalActiveUserWallet(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.totalActiveUserWalletStatus === 'success')
          this.setState({ doughnutChartDataActive: this.props.report.totalActiveUserWalletData[0].totalactive })
      }
    )
    this.props.newUser({
      startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
      enddate: moment(this.state.endDate).format('DD/MM/YYYY')
    })
    this.props.totalCashback({
      walletid: '01',
      startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
      enddate: moment(this.state.endDate).format('DD/MM/YYYY')
    })
    this.props.totalSpent({
      walletid: '01',
      startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
      enddate: moment(this.state.endDate).format('DD/MM/YYYY')
    })
    this.props.totalTopup({
      walletid: '01',
      startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
      enddate: moment(this.state.endDate).format('DD/MM/YYYY')
    })
    this.props.registerThroughSource(
      {
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.rtsStatus === 'success' && this.props.report.rtsData) {
          let graphData = []
          this.props.report.rtsData.forEach(element => {
            graphData.push({ y: element.customercount, label: element.regsource })
          })
          this.setState({ pieChartData: graphData })
        }
      }
    )
    this.props.debitVolume(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.debitVolumeStatus === 'success' && this.props.report.debitVolumeData) {
          let graphData = []
          this.props.report.debitVolumeData.forEach(element => {
            graphData.push({ y: element.spentvolume, x: moment(element.spentdate, 'DD-MM-YYYY').toDate() })
          })
          this.setState({ debitVolumeLineChart: graphData })
        }
      }
    )
    this.props.countgroupDebit(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.countGroupDebitStatus === 'success' && this.props.report.countGroupDebitData) {
          let graphData = []
          this.props.report.countGroupDebitData.forEach(element => {
            graphData.push({ y: element.spentvolume, label: element.channeltype })
          })
          this.setState({ debitTranscationChart: graphData })
        }
      }
    )
    this.props.creditVolume(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.creditVolumeStatus === 'success' && this.props.report.creditVolumeData) {
          let graphData = []
          this.props.report.creditVolumeData.forEach(element => {
            graphData.push({ y: element.topupvolume, x: moment(element.topupdate, 'DD-MM-YYYY').toDate() })
          })
          this.setState({ creditVolumeLineChart: graphData })
        }
      }
    )
    this.props.countgroupCredit(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.countGroupCreditStatus === 'success' && this.props.report.countGroupCreditData) {
          let graphData = []
          this.props.report.countGroupCreditData.forEach(element => {
            graphData.push({ y: element.topupvolume, label: element.channeltype })
          })
          this.setState({ creditTranscationChart: graphData })
        }
      }
    )
    this.props.cashbackAnalysis(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.cashbackAnalysisStatus === 'success' && this.props.report.cashbackAnalysisData) {
          let graphData = []
          this.props.report.cashbackAnalysisData.forEach(element => {
            graphData.push({ y: element.cbvolume, x: moment(element.cbdate, 'DD-MM-YYYY').toDate() })
          })
          this.setState({ cashbackAnalysisChart: graphData })
        }
      }
    )
    this.props.cashbackExperiable(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.cashbackExperiableStatus === 'success' && this.props.report.cashbackExperiableData) {
          let graphData = []
          graphData.push({
            name: 'Experiable',
            y: this.props.report.cashbackExperiableData[0].expirable
          })
          graphData.push({ name: 'NonExperiable', y: this.props.report.cashbackExperiableData[0].nonexpirable })
          this.setState({ cashbackExperiableChart: graphData })
        }
      }
    )
    this.props.groupByCampaign(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.groupByCampaignStatus === 'success' && this.props.report.groupByCampaignData) {
          let graphData = []
          this.props.report.groupByCampaignData.forEach(element => {
            graphData.push({ y: element.cbvolume, label: element.cbname })
          })
          this.setState({ cashbackCampaignChart: graphData })
        }
      }
    )
    this.props.successRatioDebit(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.successRatioDebitStatus === 'success' && this.props.report.successRatioDebitData) {
          const channeltype = _.uniq(_.pluck(this.props.report.successRatioDebitData, 'channeltype'))

          const successRatio = channeltype.map(element => {
            return {
              label: element,
              y: this.props.report.successRatioDebitData.reduce(function(sum, record) {
                if (record.state === 'completed' && record.channeltype === element) return sum + record.txncount
                else return sum
              }, 0)
            }
          })

          const failedRatio = channeltype.map(element => {
            return {
              label: element,
              y: this.props.report.successRatioDebitData.reduce(function(sum, record) {
                if (record.state === 'failed' && record.channeltype === element) return sum + record.txncount
                else return sum
              }, 0)
            }
          })

          this.setState({ debitBarChart: { success: successRatio, failed: failedRatio } })
        }
      }
    )
    this.props.successRatioCredit(
      {
        walletid: '01',
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.successRatioCreditStatus === 'success' && this.props.report.successRatioCreditData) {
          const channeltype = _.uniq(_.pluck(this.props.report.successRatioCreditData, 'channeltype'))

          const successRatio = channeltype.map(element => {
            return {
              label: element,
              y: this.props.report.successRatioCreditData.reduce(function(sum, record) {
                if (record.state === 'completed' && record.channeltype === element) return sum + record.txncount
                else return sum
              }, 0)
            }
          })

          const failedRatio = channeltype.map(element => {
            return {
              label: element,
              y: this.props.report.successRatioCreditData.reduce(function(sum, record) {
                if (record.state === 'failed' && record.channeltype === element) return sum + record.txncount
                else return sum
              }, 0)
            }
          })

          this.setState({ creditBarChart: { success: successRatio, failed: failedRatio } })
        }
      }
    )
  }

  pdfDownload = () => {
    const input = document.getElementById('bigbazarReport')
    html2canvas(input).then(canvas => {
      const imgData = canvas.toDataURL('image/png')
      const pdf = new jsPDF({
        format: 'b0'
      })
      pdf.addImage(imgData, 'PNG', 0, 0)
      pdf.save('bigbazaar.pdf')
    })
  }

  render() {
    return (
      <Col xs={10} className="scroll">
        <Grid fluid>
          <Row id="bigbazarReport">
            <Col xs={12} id="customer">
              <Row className="form-group">
                <Col sm={4}>
                  <FormGroup>
                    <ControlLabel>From</ControlLabel>
                    <DatePicker
                      className="form-control"
                      dateFormat="DD/MM/YYYY"
                      minDate={new Date(new Date().setDate(new Date().getDate() - 90))}
                      selected={this.state.startDate}
                      onChange={this.startDateChange}
                    />
                  </FormGroup>
                </Col>
                <Col sm={4}>
                  <FormGroup>
                    <ControlLabel>To</ControlLabel>
                    <DatePicker
                      className="form-control"
                      dateFormat="DD/MM/YYYY"
                      selected={this.state.endDate}
                      onChange={this.endDateChange}
                    />
                  </FormGroup>
                </Col>
                <Col sm={4} className="right">
                  <button onClick={this.pdfDownload} className="downloadxl_btn">
                    <Image src={pdficon} />
                  </button>
                </Col>
              </Row>

              <Row id="tabs-with-dropdown-pane-fifth" className="reporting form-group">
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total Spent</div>
                      </Col>
                    </Row>
                    <div>
                      {this.props.report.totalSpentStatus === 'success' ? (
                        <Table striped bordered condensed hover>
                          <thead>
                            <tr>
                              <th>Count</th>
                              <th>Volume</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td> {this.props.report.totalSpentData[0].spentcount}</td>
                              <td>{this.props.report.totalSpentData[0].spentvolume}</td>
                            </tr>
                          </tbody>
                        </Table>
                      ) : null}
                    </div>
                  </Well>
                </Col>
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total Top Up</div>
                      </Col>
                    </Row>
                    <div>
                      {this.props.report.totalTopupStatus === 'success' ? (
                        <Table striped bordered condensed hover>
                          <thead>
                            <tr>
                              <th>Count</th>
                              <th>Volume</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>{this.props.report.totalTopupData[0].topupcount}</td>
                              <td> {this.props.report.totalTopupData[0].topupvolume}</td>
                            </tr>
                          </tbody>
                        </Table>
                      ) : null}
                    </div>
                  </Well>
                </Col>
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total Cashback</div>
                      </Col>
                    </Row>
                    <div>
                      {this.props.report.totalCashbackStatus === 'success' ? (
                        <Table striped bordered condensed hover>
                          <thead>
                            <tr>
                              <th>Count</th>
                              <th>Volume</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>{this.props.report.totalCashbackData[0].cbcount}</td>
                              <td>{this.props.report.totalCashbackData[0].cbvolume}</td>
                            </tr>
                          </tbody>
                        </Table>
                      ) : null}
                    </div>
                  </Well>
                </Col>
              </Row>

              <Row id="tabs-with-dropdown-pane-fifth" className="reporting form-group">
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total User</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.totalUserStatus === 'success'
                          ? this.props.report.totalUserData[0].totalcount
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total Active User</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.totalActiveUserWalletStatus === 'success'
                          ? this.props.report.totalActiveUserWalletData[0].totalactive
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total New Registered</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.newRegisteredStatus === 'success'
                          ? this.props.report.newRegisteredData[0].customercount
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
              </Row>

              <Row className="form-group">
                <Col sm={6} className="form-group">
                  <ControlLabel>Active vs Inactive</ControlLabel>
                  <hr />
                  <Doughnutchart
                    data={[
                      { name: 'Active', y: this.state.doughnutChartDataActive },
                      { name: 'In Active', y: this.state.doughnutChartDataTotal - this.state.doughnutChartDataActive }
                    ]}
                  />
                </Col>
                <Col sm={6} className="form-group">
                  <ControlLabel>User Registation Through Various Channels</ControlLabel>
                  <hr />
                  <Piechart data={this.state.pieChartData} />
                </Col>
              </Row>
              <FormGroup>
                <h4 className="acct_manage_hist_label">Spent Analysis</h4>
              </FormGroup>
              <ControlLabel className="form-group">Debit Volume Over Time</ControlLabel>
              <hr />
              <Row className="form-group">
                <Col className="form-group" xs={12}>
                  <LineChart data={this.state.debitVolumeLineChart} />
                </Col>
              </Row>
              <Row className="form-group">
                <Col sm={6} className="form-group">
                  <ControlLabel>Chanel Wise Success ratio</ControlLabel>
                  <hr />
                  <BarChart data={this.state.debitBarChart} />
                </Col>
                <Col sm={6} className="form-group">
                  <ControlLabel>Debit Transactions Through Various Sources</ControlLabel>
                  <hr />
                  <Piechart data={this.state.debitTranscationChart} />
                </Col>
              </Row>
              <FormGroup>
                <h4 className="acct_manage_hist_label">Credit Analysis</h4>
              </FormGroup>
              <ControlLabel className="form-group">Credit Volume Over Time</ControlLabel>
              <hr />
              <Row className="form-group">
                <Col xs={12} className="form-group">
                  <LineChart data={this.state.creditVolumeLineChart} />
                </Col>
              </Row>

              <Row className="form-group">
                <Col sm={6} className="form-group">
                  <ControlLabel>Chanel Wise Success ratio</ControlLabel>
                  <hr />
                  <BarChart data={this.state.creditBarChart} />
                </Col>
                <Col sm={6} className="form-group">
                  <ControlLabel>Credit Transactions Through Various Sources</ControlLabel>
                  <hr />
                  <Piechart data={this.state.creditTranscationChart} />
                </Col>
              </Row>
              <FormGroup>
                <h4 className="acct_manage_hist_label">Cashback Analysis</h4>
              </FormGroup>
              <ControlLabel>Cashback Volume Flow</ControlLabel>
              <hr />
              <Row className="form-group">
                <Col xs={12} className="form-group">
                  <LineChart data={this.state.cashbackAnalysisChart} />
                </Col>
              </Row>
              <Row className="form-group">
                <Col sm={6} className="form-group">
                  <ControlLabel>Experiable vs Non Experiable</ControlLabel>
                  <hr />
                  <Doughnutchart data={this.state.cashbackExperiableChart} />
                </Col>
                <Col sm={6} className="form-group">
                  <ControlLabel>Cashback Through Various Campaign Name</ControlLabel>
                  <hr />
                  <Piechart data={this.state.cashbackCampaignChart} />
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </Col>
    )
  }
}

function mapStateToProps({ report }) {
  return { report }
}

export default connect(
  mapStateToProps,
  actions
)(BigBazaar)
