import React, { Component } from 'react'
import { CanvasJS, CanvasJSChart } from './canvasjs.react'

class BarChart extends Component {
  constructor() {
    super()
  }

  render() {
    const options = {
      animationEnabled: true,
      toolTip: {
        shared: true,
        reversed: true
      },
      data: [
        {
          type: 'stackedColumn100',
          name: 'Success',
          showInLegend: true,
          yValueFormatString: '#,###',
          //dataPoints: [{ label: 'EDO', y: 40 }, { label: 'BBO', y: 30 }, { label: 'BFO', y: 65 }]
          dataPoints: this.props.data.success
        },
        {
          type: 'stackedColumn100',
          name: 'Failed',
          showInLegend: true,
          yValueFormatString: '#,###',
          //dataPoints: [{ label: 'EDO', y: 60 }, { label: 'BBO', y: 70 }, { label: 'BFO', y: 35 }]
          dataPoints: this.props.data.failed
        }
      ]
    }
    return (
      <div>
        <CanvasJSChart options={options} onRef={ref => (this.chart = ref)} />
      </div>
    )
  }
}
export default BarChart
