import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Row, FormGroup, ControlLabel, Grid, Well } from 'react-bootstrap'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import * as actions from './actions'
import LineChart from './linechart'
import Piechart from './piechart'
import Doughnutchart from './doughnutchart'

class Customers extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lineChartData: [],
      pieChartData: [],
      doughnutChartDataActive: '',
      doughnutChartDataTotal: '',
      startDate: moment(),
      endDate: moment()
    }
  }

  handleClose = () => this.setState({ showModal: false })

  startDateChange = date => {
    this.setState(
      {
        startDate: date
      },
      () => {
        this.props.totalUser({ enddate: this.state.endDate }, () => {
          if (this.props.report.totalUserStatus === 'success') {
            this.setState({ doughnutChartDataTotal: this.props.report.totalUserData[0].totalcount })
          }
        })
        this.props.activeUser(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.activeUserStatus === 'success') {
              this.setState({ doughnutChartDataActive: this.props.report.activeUserData[0].totalactive })
            }
          }
        )
        this.props.newUser({
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.groupCustomer(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.groupCustomerStatus === 'success' && this.props.report.groupCustomerData) {
              let graphData = []
              this.props.report.groupCustomerData.forEach(element => {
                graphData.push({ y: element.customer, x: moment(element.RegistrationDate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ lineChartData: graphData })
            }
          }
        )
        this.props.registerThroughSource(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.rtsStatus === 'success' && this.props.report.rtsData) {
              let graphData = []
              this.props.report.rtsData.forEach(element => {
                graphData.push({ y: element.customercount, label: element.regsource })
              })
              this.setState({ pieChartData: graphData })
            }
          }
        )
      }
    )
  }

  endDateChange = date => {
    this.setState(
      {
        endDate: date
      },
      () => {
        this.props.totalUser({ enddate: this.state.endDate }, () => {
          if (this.props.report.totalUserStatus === 'success') {
            this.setState({ doughnutChartDataTotal: this.props.report.totalUserData[0].totalcount })
          }
        })
        this.props.activeUser(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.activeUserStatus === 'success') {
              this.setState({ doughnutChartDataActive: this.props.report.activeUserData[0].totalactive })
            }
          }
        )
        this.props.newUser({
          startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
          enddate: moment(this.state.endDate).format('DD/MM/YYYY')
        })
        this.props.groupCustomer(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.groupCustomerStatus === 'success') {
              let graphData = []
              this.props.report.groupCustomerData.forEach(element => {
                graphData.push({ y: element.customer, x: moment(element.RegistrationDate, 'DD-MM-YYYY').toDate() })
              })
              this.setState({ lineChartData: graphData })
            }
          }
        )
        this.props.registerThroughSource(
          {
            startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
            enddate: moment(this.state.endDate).format('DD/MM/YYYY')
          },
          () => {
            if (this.props.report.rtsStatus === 'success' && this.props.report.rtsData) {
              let graphData = []
              this.props.report.rtsData.forEach(element => {
                graphData.push({ y: element.customercount, label: element.regsource })
              })
              this.setState({ pieChartData: graphData })
            }
          }
        )
      }
    )
  }

  componentDidMount() {
    this.props.totalUser({ enddate: this.state.endDate }, () => {
      if (this.props.report.totalUserStatus === 'success') {
        this.setState({ doughnutChartDataTotal: this.props.report.totalUserData[0].totalcount })
      }
    })
    this.props.activeUser(
      {
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.activeUserStatus === 'success') {
          this.setState({ doughnutChartDataActive: this.props.report.activeUserData[0].totalactive })
        }
      }
    )
    this.props.newUser({
      startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
      enddate: moment(this.state.endDate).format('DD/MM/YYYY')
    })
    this.props.groupCustomer(
      {
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.groupCustomerStatus === 'success' && this.props.report.groupCustomerData) {
          let graphData = []
          this.props.report.groupCustomerData.forEach(element => {
            graphData.push({ y: element.customer, x: moment(element.RegistrationDate, 'DD-MM-YYYY').toDate() })
          })
          this.setState({ lineChartData: graphData })
        }
      }
    )
    this.props.registerThroughSource(
      {
        startdate: moment(this.state.startDate).format('DD/MM/YYYY'),
        enddate: moment(this.state.endDate).format('DD/MM/YYYY')
      },
      () => {
        if (this.props.report.rtsStatus === 'success' && this.props.report.rtsData) {
          let graphData = []
          this.props.report.rtsData.forEach(element => {
            graphData.push({ y: element.customercount, label: element.regsource })
          })
          this.setState({ pieChartData: graphData })
        }
      }
    )
  }

  render() {
    return (
      <Col xs={10} className="scroll">
        <Grid fluid>
          <Row>
            <Col xs={12} id="customer">
              <Row className="form-group">
                <Col sm={4}>
                  <FormGroup>
                    <ControlLabel>From</ControlLabel>
                    <DatePicker
                      className="form-control"
                      dateFormat="DD/MM/YYYY"
                      minDate={new Date(new Date().setDate(new Date().getDate() - 90))}
                      selected={this.state.startDate}
                      onChange={this.startDateChange}
                    />
                  </FormGroup>
                </Col>
                <Col sm={4}>
                  <FormGroup>
                    <ControlLabel>To</ControlLabel>
                    <DatePicker
                      className="form-control"
                      dateFormat="DD/MM/YYYY"
                      selected={this.state.endDate}
                      onChange={this.endDateChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row id="tabs-with-dropdown-pane-fifth" className="reporting form-group">
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total No. of Users</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.totalUserStatus === 'success'
                          ? this.props.report.totalUserData[0].totalcount
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total No. of Active Users</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.activeUserStatus === 'success'
                          ? this.props.report.activeUserData[0].totalactive
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
                <Col xs={4} className="form-group">
                  <Well>
                    <Row className="bluebg">
                      <Col xs={12}>
                        <div className="well-title text-center">Total New Registered Users</div>
                      </Col>
                    </Row>
                    <div className="img_text">
                      <div className="empty_img_txt">
                        {this.props.report.newRegisteredStatus === 'success'
                          ? this.props.report.newRegisteredData[0].customercount
                          : null}
                      </div>
                    </div>
                  </Well>
                </Col>
              </Row>

              <FormGroup>
                <h4 className="acct_manage_hist_label">Customer Data</h4>
              </FormGroup>
              <ControlLabel>User Base</ControlLabel>
              <hr className="form-group" />
              <Row className="form-group">
                <Col xs={12} className="form-group">
                  <LineChart data={this.state.lineChartData} />
                </Col>
              </Row>
              <Row className="form-group">
                <Col sm={6} className="form-group">
                  <ControlLabel>Active vs Inactive</ControlLabel>
                  <hr />
                  <Doughnutchart
                    data={[
                      { name: 'Active', y: this.state.doughnutChartDataActive },
                      { name: 'In Active', y: this.state.doughnutChartDataTotal - this.state.doughnutChartDataActive }
                    ]}
                  />
                </Col>
                <Col sm={6} className="form-group">
                  <ControlLabel>User Registation Through Various Channels</ControlLabel>
                  <hr />
                  <Piechart data={this.state.pieChartData} />
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </Col>
    )
  }
}

function mapStateToProps({ report }) {
  return { report }
}

export default connect(
  mapStateToProps,
  actions
)(Customers)
