import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import {
  Grid,
  Row,
  Col,
  FormControl,
  FormGroup,
  ButtonGroup,
  Button,
  Glyphicon,
  Table,
  Modal,
  InputGroup
} from 'react-bootstrap'
import Cookies from 'universal-cookie'
import BulkApprove from './BulkApprove'
import BulkReject from './BulkReject'

class BulkCredit extends Component {
  constructor(props, context) {
    super(props, context)

    const cookies = new Cookies()
    const admin = cookies.get('admin')
    const { id, accountaccess } = admin
    let viewer, maker, checker
    if (`${accountaccess}`.length === 3) {
      viewer = accountaccess.charAt(0)
      maker = accountaccess.charAt(1)
      checker = accountaccess.charAt(2)
    }

    this.state = {
      file: null,
      data: {
        managerid: id
      },
      showModal: false,
      modalText: '',
      showApprovalModel: false,
      approvalData: null,
      showRejectModel: false,
      rejectData: null,
      id,
      viewer,
      maker,
      checker,
      acceptedFiles: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
    }
    this.handleUploadImage = this.handleUploadImage.bind(this)
    this.onchange = this.onchange.bind(this)
  }

  showApproveModal(data) {
    const { id } = this.state
    this.props.generateBulkCreditOTP(
      {
        managerid: id,
        platform: 'web',
        otptype: 20
      },
      () => {
        const { generateBulkOTPStatus, generateBulkOTPData, generateBulkOTPMessage } = this.props.accountManagement
        if (generateBulkOTPStatus === 'success') {
          data.authtoken = generateBulkOTPData.authtoken
          data.platform = 'web'
          data.otptype = 20
          this.setState({
            showApprovalModel: true,
            approvalData: data
          })
        } else {
          this.setState({
            showModal: true,
            modalText: generateBulkOTPMessage
          })
        }
      }
    )
  }

  showRejectModal(data) {
    this.setState({
      showRejectModel: true,
      rejectData: data
    })
  }

  componentDidMount() {
    const { id, checker, maker } = this.state
    if (checker === '1') {
      this.props.getBulkAdminOps({
        managerid: id
      })
    }
    if (maker === '1') {
      this.props.getBulkAdminOpsMaker({
        managerid: id
      })
    }
  }

  handleClose = () => this.setState({ showModal: false })
  closeApproveModal = () => this.setState({ showApprovalModel: false })
  closeRejectModal = () => this.setState({ showRejectModel: false })
  onchange({ target }) {
    const { files } = target
    const { acceptedFiles } = this.state
    const acceptTypes = acceptedFiles.split(',')
    if (files && files.length > 0) {
      const file = files[0]
      if (acceptTypes.indexOf(file.type) === -1) {
        target.value = null
        return this.setState({ showModal: true, modalText: `Only excel files are supported.` })
      }
      this.setState({ file })
    }
  }

  handleUploadImage(ev) {
    ev.preventDefault()
    const { acceptedFiles, file, data } = this.state
    const acceptTypes = acceptedFiles.split(',')
    if (!file) {
      return this.setState({ showModal: true, modalText: `Please select a valid excel file` })
    }
    if (file) {
      if (acceptTypes.indexOf(file.type) === -1) {
        return this.setState({ showModal: true, modalText: `Only excel files are supported.` })
      }
    }
    const formData = new FormData()
    formData.append('file', file)
    formData.append('data', JSON.stringify(data))
    this.props.doBulkCredit(formData, () => {
      const { bulkCreditStatus, bulkCreditMessage } = this.props.accountManagement
      const { id, maker } = this.state
      if (bulkCreditStatus === 'success' && maker === '1') {
        this.props.getBulkAdminOpsMaker({
          managerid: id
        })
      }
      this.setState({ showModal: true, modalText: bulkCreditMessage })
    })
  }

  renderBulkAdminOps() {
    const { bulkAdminOpsStatus, bulkAdminOpsData } = this.props.accountManagement
    if (bulkAdminOpsStatus === 'success') {
      if (bulkAdminOpsData && bulkAdminOpsData.length) {
        return bulkAdminOpsData.map(data => {
          const { id, name, originalname, filename, recordscount, date, ip, status } = data
          return (
            <tr key={id}>
              <td>{id}</td>
              <td>
                <a href={`https://s3-ap-southeast-1.amazonaws.com/livquik.dev/${filename}`}>
                  {' '}
                  {originalname || filename}
                </a>
              </td>
              <td>{recordscount}</td>
              <td>{new Intl.DateTimeFormat('en-IN').format(new Date(date))}</td>
              <td>{name}</td>
              <td>{ip}</td>
              <td>{status}</td>
              <td>
                {status === 'pending' ? (
                  <ButtonGroup>
                    <Button
                      bsStyle="primary"
                      className="btn-primary-small block_unbloc_top_btn"
                      onClick={this.showApproveModal.bind(this, data)}
                    >
                      Approve
                    </Button>
                    <Button
                      bsStyle="secondary"
                      className="btn-primary-small block_unbloc_top_btn"
                      onClick={this.showRejectModal.bind(this, data)}
                    >
                      Reject
                    </Button>
                  </ButtonGroup>
                ) : (
                  ''
                )}
              </td>
            </tr>
          )
        })
      } else
        return (
          <tr>
            <td align="center" colSpan="10">
              No Record Found
            </td>
          </tr>
        )
    } else {
      return (
        <tr>
          <td align="center" colSpan="10">
            No Record Found
          </td>
        </tr>
      )
    }
  }

  renderBulkAdminOpsMaker() {
    const { bulkAdminOpsMakerStatus, bulkAdminOpsMakerData } = this.props.accountManagement
    if (bulkAdminOpsMakerStatus === 'success') {
      if (bulkAdminOpsMakerData && bulkAdminOpsMakerData.length) {
        return bulkAdminOpsMakerData.map(data => {
          const { id, name, originalname, filename, recordscount, date, ip, status, comments } = data
          return (
            <tr key={id}>
              <td>{id}</td>
              <td>
                <a href={`https://s3-ap-southeast-1.amazonaws.com/livquik.dev/${filename}`}>
                  {' '}
                  {originalname || filename}
                </a>
              </td>
              <td>{recordscount}</td>
              <td>{new Intl.DateTimeFormat('en-IN').format(new Date(date))}</td>
              <td>{name}</td>
              <td>{ip}</td>
              <td>{comments}</td>
              <td>{status}</td>
            </tr>
          )
        })
      } else
        return (
          <tr>
            <td align="center" colSpan="10">
              No Record Found
            </td>
          </tr>
        )
    } else {
      return (
        <tr>
          <td align="center" colSpan="10">
            No Record Found
          </td>
        </tr>
      )
    }
  }

  render() {
    return (
      <Grid fluid className="scroll">
        <Row>
          <Col xs={12}>
            {this.state.maker === '1' ? (
              <form className="searchbar" onSubmit={this.handleUploadImage}>
                <Row className="form-group">
                  <Col xs={6}>
                    <FormGroup>
                      <InputGroup>
                        <FormControl type="file" onChange={this.onchange} accept={this.state.acceptedFiles} />
                        <InputGroup.Button>
                          <Button bsStyle="primary" type="submit" className="form-group">
                            <Glyphicon glyph="glyphicon glyphicon-open" />
                            Upload
                          </Button>
                        </InputGroup.Button>
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>
              </form>
            ) : (
              ''
            )}
            <FormGroup>
              <Table responsive striped bordered condensed hover>
                <thead>
                  <tr>
                    <th>Transactions ID</th>
                    <th>File Name</th>
                    <th>No. of Records</th>
                    <th>Date</th>
                    <th>User</th>
                    <th>IP</th>
                    {this.state.checker === '1' ? '' : <th>Comments</th>}

                    <th>Status</th>
                    {this.state.checker === '1' ? <th>Action</th> : ''}
                  </tr>
                </thead>
                <tbody>{this.state.checker === '1' ? this.renderBulkAdminOps() : ''}</tbody>
                <tbody>{this.state.maker === '1' ? this.renderBulkAdminOpsMaker() : ''}</tbody>
              </Table>
            </FormGroup>
          </Col>
        </Row>
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <BulkApprove
          show={this.state.showApprovalModel}
          onHide={this.closeApproveModal}
          data={this.state.approvalData}
        />
        <BulkReject show={this.state.showRejectModel} onHide={this.closeRejectModal} data={this.state.rejectData} />
      </Grid>
    )
  }
}

function mapStateToProps({ accountManagement }) {
  return { accountManagement }
}
export default connect(
  mapStateToProps,
  actions
)(BulkCredit)
