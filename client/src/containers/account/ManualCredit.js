import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Grid, Row, Col, FormControl, FormGroup, Button, Glyphicon, Table, Modal, ButtonGroup } from 'react-bootstrap'
import Cookies from 'universal-cookie'
import TopUp from './TopUp'
import TopUpApprove from './TopUpApprove'
import TopUpReject from './TopUpReject'

class ManualCredit extends Component {
  constructor(props, context) {
    super(props, context)
    const cookies = new Cookies()
    const admin = cookies.get('admin')
    const { id, accountaccess } = admin
    let viewer, maker, checker
    if (`${accountaccess}`.length === 3) {
      viewer = accountaccess.charAt(0)
      maker = accountaccess.charAt(1)
      checker = accountaccess.charAt(2)
    }

    this.state = {
      topupModal: false,
      showModal: false,
      showApproveModal: false,
      approvalData: null,
      showRejectModal: false,
      rejectData: null,
      modalText: '',
      searchtext: '',
      topupCustomer: null,
      id,
      viewer,
      maker,
      checker
    }
    this.handleClose = this.handleClose.bind(this)
    this.topupClose = this.topupClose.bind(this)
    this.onchange = this.onchange.bind(this)
  }

  componentDidMount() {
    const { id, checker, maker } = this.state
    if (checker === '1') {
      this.props.getAdminOps({
        managerid: id
      })
    }
    if (maker === '1') {
      this.props.getAdminOpsMaker({
        managerid: id
      })
    }
  }

  showApproveModal(data) {
    const { id } = this.state
    this.props.generateManualCreditOTP(
      {
        managerid: id,
        platform: 'web',
        otptype: 21
      },
      () => {
        const {
          generateManualOTPStatus,
          generateManualOTPData,
          generateManualOTPMessage
        } = this.props.accountManagement
        if (generateManualOTPStatus === 'success') {
          data.authtoken = generateManualOTPData.authtoken
          data.platform = 'web'
          data.otptype = 21
          this.setState({
            showApproveModal: true,
            approvalData: data
          })
        } else {
          this.setState({
            showModal: true,
            modalText: generateManualOTPMessage
          })
        }
      }
    )
  }

  showRejectModal(data) {
    this.setState({
      showRejectModal: true,
      rejectData: data
    })
  }

  showTopUpModal(customer) {
    this.setState({
      topupModal: true,
      topupCustomer: customer
    })
  }

  handleClose = () => this.setState({ showModal: false })
  topupClose = () => this.setState({ topupModal: false })
  approveClose = () => this.setState({ showApproveModal: false })
  rejectClose = () => this.setState({ showRejectModal: false })

  onchange = ({ target }) => {
    const { value } = target
    if (isNaN(value)) {
      return
    }
    this.setState({ searchtext: value })
  }

  searchuser = event => {
    event.preventDefault()
    if (this.state.searchtext.length !== 10)
      this.setState({
        showModal: true,
        modalText: `Enter Valid Mobile`
      })
    else
      this.props.getCreditCustomer({
        mobile: this.state.searchtext
      })
  }

  renderCustomerData() {
    const { maker } = this.state
    if (maker !== '1') {
      return (
        <tr>
          <td align="center" colSpan="6">
            Checker table to be Implemented
          </td>
        </tr>
      )
    }
    const { creditCustomerStatus, creditCustomerData } = this.props.accountManagement
    if (!creditCustomerStatus) {
      return (
        <tr>
          <td align="center" colSpan="6">
            Please enter a mobile number to search
          </td>
        </tr>
      )
    }
    if (creditCustomerStatus === 'success') {
      if (creditCustomerData) {
        const { id, mobile, firstname, lastname } = creditCustomerData
        return (
          <tr key={id}>
            <td>{mobile}</td>
            <td>{firstname}</td>
            <td>{lastname}</td>
            <td>
              <Button
                bsStyle="primary"
                className="btn-primary-small block_unbloc_top_btn"
                onClick={this.showTopUpModal.bind(this, creditCustomerData)}
              >
                Top up
              </Button>
            </td>
          </tr>
        )
      } else
        return (
          <tr>
            <td align="center" colSpan="6">
              No Customer Found
            </td>
          </tr>
        )
    } else {
      return (
        <tr>
          <td align="center" colSpan="6">
            No Customer Found
          </td>
        </tr>
      )
    }
  }

  renderCheckerCustomerData() {
    const { adminOpsStatus, adminOpsData } = this.props.accountManagement
    if (adminOpsStatus === 'success') {
      if (adminOpsData) {
        return adminOpsData.map(data => {
          const { id, name, amount, walletid, date, status } = data
          return (
            <tr key={id}>
              <td>{id}</td>
              <td>{name}</td>
              <td>{amount}</td>
              <td>{walletid}</td>
              <td>{new Intl.DateTimeFormat('en-IN').format(new Date(date))}</td>
              <td>{status}</td>
              <td>
                <ButtonGroup>
                  <Button
                    bsStyle="primary"
                    className="btn-primary-small block_unbloc_top_btn"
                    onClick={this.showApproveModal.bind(this, data)}
                  >
                    Approve
                  </Button>
                  <Button
                    bsStyle="secondary"
                    className="btn-primary-small block_unbloc_top_btn"
                    onClick={this.showRejectModal.bind(this, data)}
                  >
                    Reject
                  </Button>
                </ButtonGroup>
              </td>
            </tr>
          )
        })
      } else
        return (
          <tr>
            <td align="center" colSpan="6">
              No Record Found
            </td>
          </tr>
        )
    } else {
      return (
        <tr>
          <td align="center" colSpan="6">
            No Record Found
          </td>
        </tr>
      )
    }
  }

  renderMakerHistoryData() {
    const { adminOpsMakerStatus, adminOpsMakerData } = this.props.accountManagement
    if (adminOpsMakerStatus === 'success') {
      if (adminOpsMakerData) {
        return adminOpsMakerData.map(data => {
          const { id, name, amount, walletid, date, comments, status } = data
          return (
            <tr key={id}>
              <td>{id}</td>
              <td>{name}</td>
              <td>{amount}</td>
              <td>{walletid}</td>
              <td>{new Intl.DateTimeFormat('en-IN').format(new Date(date))}</td>
              <td>{comments}</td>
              <td>{status}</td>
            </tr>
          )
        })
      } else
        return (
          <tr>
            <td align="center" colSpan="6">
              No Record Found
            </td>
          </tr>
        )
    } else {
      return (
        <tr>
          <td align="center" colSpan="6">
            No Record Found
          </td>
        </tr>
      )
    }
  }

  render() {
    return (
      <Grid fluid className="scroll">
        <Row>
          <Col xs={12}>
            <form className="searchbar" onSubmit={this.searchuser}>
              <Row className="form-group">
                <Col xs={9}>
                  <FormGroup>
                    <FormControl
                      type="text"
                      placeholder="Enter Mobile Number"
                      name="search"
                      maxLength="10"
                      onChange={this.onchange}
                      value={this.state.searchtext}
                    />
                  </FormGroup>
                </Col>
                <Col xs={3}>
                  <Button type="submit" bsStyle="primary">
                    <Glyphicon glyph="glyphicon glyphicon-search mr5" />
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
            {this.state.maker === '1' ? (
              <FormGroup>
                <Table responsive striped bordered condensed hover>
                  <thead>
                    <tr>
                      <th>Mobile Number</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderCustomerData()}</tbody>
                </Table>
              </FormGroup>
            ) : (
              ''
            )}
            {this.state.checker === '1' ? (
              <Table responsive striped bordered condensed hover>
                <thead>
                  <tr>
                    <th>Transaction ID</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Wallet ID</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>{this.renderCheckerCustomerData()}</tbody>
              </Table>
            ) : (
              ''
            )}
          </Col>
          <Modal show={this.state.showModal} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title />
            </Modal.Header>
            <Modal.Body>
              <p>{this.state.modalText}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
          <TopUp show={this.state.topupModal} onHide={this.topupClose} customer={this.state.topupCustomer} />
          <TopUpApprove show={this.state.showApproveModal} onHide={this.approveClose} data={this.state.approvalData} />
          <TopUpReject show={this.state.showRejectModal} onHide={this.rejectClose} data={this.state.rejectData} />
        </Row>
        {this.state.maker === '1' ? (
          <Row className="form-group">
            <Col xs="12">
              <h2 className="acct_manage_hist_label">History</h2>
            </Col>
          </Row>
        ) : (
          ''
        )}
        {this.state.maker === '1' ? (
          <Row>
            <Col xs={12}>
              <FormGroup>
                <Table responsive striped bordered condensed hover>
                  <thead>
                    <tr>
                      <th>Transaction ID</th>
                      <th>Name</th>
                      <th>Amount</th>
                      <th>Wallet ID</th>
                      <th>Date</th>
                      <th>Comments</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderMakerHistoryData()}</tbody>
                </Table>
              </FormGroup>
            </Col>
          </Row>
        ) : (
          ''
        )}
      </Grid>
    )
  }
}

function mapStateToProps({ accountManagement }) {
  return { accountManagement }
}
export default connect(
  mapStateToProps,
  actions
)(ManualCredit)
