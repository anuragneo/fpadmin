import React, { Component } from 'react'
import { Col, Tabs, Tab } from 'react-bootstrap'
import ManualDebit from './ManualDebit'
import ManualReversal from './ManualReversal'

class Debit extends Component {
  render() {
    return (
      <div>
        <Col xs="10 pad-l-r-0 ">
          <Tabs defaultActiveKey={1} id="manual_debit_reversal">
            <Tab eventKey={1} title="Manual Debit">
              <ManualDebit />
            </Tab>
            <Tab eventKey={2} title="Manual Reversal">
              <ManualReversal />
            </Tab>
          </Tabs>
        </Col>
      </div>
    )
  }
}

export default Debit
