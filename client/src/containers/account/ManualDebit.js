import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import {
  Grid,
  Row,
  Col,
  FormControl,
  FormGroup,
  Button,
  Glyphicon,
  Table,
  ControlLabel,
  Radio,
  Modal
} from 'react-bootstrap'
import Cookies from 'universal-cookie'
import ManualDebitModal from './ManualDebitModal'

class ManualDebit extends Component {
  constructor(props, context) {
    super(props, context)
    const cookies = new Cookies()
    const admin = cookies.get('admin')
    const { id, accountaccess } = admin
    let viewer, maker, checker
    if (`${accountaccess}`.length === 3) {
      viewer = accountaccess.charAt(0)
      maker = accountaccess.charAt(1)
      checker = accountaccess.charAt(2)
    }

    this.state = {
      showModal: false,
      showTxnModal: false,
      modalText: '',
      id,
      viewer,
      maker,
      checker,
      disableSearchByMobile: false,
      disableSearchByPaymentGateway: false,
      mobile: '',
      walletid: '',
      pgid: ''
    }

    this.updateInput = this.updateInput.bind(this)
    this.showTxnModal = this.showTxnModal.bind(this)
    // this.search = this.search.bind(this)
  }

  handleClose = () => this.setState({ showModal: false })
  closeTxnModal = () => this.setState({ showTxnModal: false })
  componentDidMount() {
    this.props.getAdminWallet()
  }

  showTxnModal() {
    this.setState({
      showTxnModal: true
    })
  }

  updateInput({ target }) {
    const { name, value } = target
    switch (name) {
      case 'searchby':
        if (value === 'mobile') {
          this.setState({ disableSearchByPaymentGateway: true })
        } else {
          this.setState({ disableSearchByPaymentGateway: false })
        }
        if (value === 'paymentgateway') {
          this.setState({ walletid: '' })
          this.setState({ disableSearchByMobile: true })
        } else {
          this.setState({ disableSearchByMobile: false })
        }
        this.setState({ searchby: value })
        break
      case 'mobile':
        if (isNaN(value)) {
          return
        }
        this.setState({ mobile: value })
        break
      case 'walletid':
        this.setState({ walletid: value })
        break
      case 'pgid':
        if (isNaN(value)) {
          return
        }
        this.setState({ pgid: value })
        break
      default:
        break
    }
  }

  renderwalletlist() {
    const { walletStatus, walletData } = this.props.admin
    if (walletStatus === 'success' && walletData) {
      return (
        <optgroup>
          <option key="" value="">
            Select
          </option>
          {walletData.details.map((v, k) => (
            <option key={v.walletid} value={v.walletid}>
              {v.name}
            </option>
          ))}
        </optgroup>
      )
    }
  }

  renderSearchData() {
    const data = [
      {
        mobile: '9878786767',
        id: 12
      },
      {
        mobile: '9864786567',
        id: 13
      },
      {
        mobile: '9864678767',
        id: 14
      },
      {
        mobile: '9864678767',
        id: 15
      },
      {
        mobile: '9864678767',
        id: 16
      },
      {
        mobile: '7867564567',
        id: 17
      }
    ]
    return data.map(data => {
      const { mobile, id } = data
      return (
        <tr>
          <td>{mobile}</td>
          <td>{id}</td>
          <td>
            <Button bsStyle="primary" className="btn-primary-small" onClick={this.showTxnModal}>
              Select
            </Button>
          </td>
        </tr>
      )
    })
  }

  render() {
    return (
      <Grid fluid className="scroll">
        <Row>
          <Col xs={12}>
            <Row className="form-group">
              <Col xs={4}>
                <ControlLabel>Search By</ControlLabel>
              </Col>
              <FormGroup>
                <Col xs={4}>
                  <Radio name="searchby" value="mobile" onChange={this.updateInput}>
                    Mobile Number
                  </Radio>
                </Col>
                <Col xs={4}>
                  <Radio name="searchby" value="paymentgateway" onChange={this.updateInput}>
                    Payment Gateway ID
                  </Radio>
                </Col>
              </FormGroup>
            </Row>
            <Row className="form-group">
              <Col sm={4}>
                <FormGroup>
                  <ControlLabel>Enter Mobile Number</ControlLabel>
                  <FormControl
                    type="tel"
                    name="mobile"
                    maxLength="10"
                    value={this.state.mobile}
                    disabled={this.state.disableSearchByMobile}
                    onChange={this.updateInput}
                  />
                </FormGroup>
              </Col>
              <Col sm={4}>
                <FormGroup>
                  <ControlLabel>Select Wallet</ControlLabel>
                  <FormControl
                    name="walletid"
                    onChange={this.updateInput}
                    componentClass="select"
                    placeholder="select"
                    disabled={this.state.disableSearchByMobile}
                    value={this.state.walletid}
                  >
                    {this.renderwalletlist()}
                  </FormControl>
                </FormGroup>
              </Col>
              <Col sm={4}>
                <FormGroup>
                  <ControlLabel>Enter PG ID</ControlLabel>
                  <FormControl
                    type="tel"
                    name="pgid"
                    value={this.state.pgid}
                    disabled={this.state.disableSearchByPaymentGateway}
                    onChange={this.updateInput}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row className="form-group">
              <Col xs={4} xsOffset={4} className="form-group">
                <Button bsStyle="primary" onClick={this.search} className="form-group">
                  <Glyphicon glyph="glyphicon glyphicon-search mr5" />
                  Search
                </Button>
              </Col>
            </Row>
            <FormGroup>
              <Table responsive striped bordered condensed hover>
                <thead>
                  <tr>
                    <th>Mobile Number</th>
                    <th>Transaction ID</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>{this.renderSearchData()}</tbody>
              </Table>
            </FormGroup>
          </Col>
        </Row>
        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
        <ManualDebitModal show={this.state.showTxnModal} onHide={this.closeTxnModal} />
      </Grid>
    )
  }
}

function mapStateToProps({ accountManagement, admin }) {
  return { accountManagement, admin }
}
export default connect(
  mapStateToProps,
  actions
)(ManualDebit)
