import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Row, Col, Tabs, Tab } from 'react-bootstrap'
import ManualCredit from './ManualCredit'
import BulkCredit from './BulkCredit'

class Credit extends Component {
  render() {
    return (
        <Col xs="10 pad-l-r-0 ">
        <div>
          <Tabs defaultActiveKey={1} id="manual_bulk_credit">
            <Tab eventKey={1} title="Manual Credit">
              <ManualCredit />
            </Tab>
            <Tab eventKey={2} title="Bulk Credit">
              <BulkCredit />
            </Tab>
          </Tabs>
        </div>
      </Col>
    )
  }
}

function mapStateToProps({ accountManagement }) {
  return { accountManagement }
}
export default connect(
  mapStateToProps,
  actions
)(Credit)
