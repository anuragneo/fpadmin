import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import {
  Grid,
  Row,
  Col,
  FormControl,
  FormGroup,
  Button,
  ButtonGroup,
  Glyphicon,
  Table,
  PageHeader,
  Modal
} from 'react-bootstrap'
import BlockCustomer from './BlockCustomer'

class BlockUnblockAccount extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      showModal: false,
      modalText: '',
      blockCustomerModal: false,
      blockCustomer: {},
      searchtext: ''
    }
  }

  showBlockModal(customer) {
    this.setState({
      blockCustomerModal: true,
      blockCustomer: customer
    })
  }

  blockCustomer(customer) {
    this.props.blockCustomer(
      {
        mobile: customer.mobile
      },
      () => {
        if (this.props.accountManagement.blockCustomerStatus === 'success') {
          this.setState({
            showModal: true,
            modalText: `${this.props.accountManagement.blockCustomerData.msg}`
          })
          this.props.getBlockUnblockCustomer({
            mobile: customer.mobile
          })
        } else {
          this.setState({
            showModal: true,
            modalText: `${this.props.accountManagement.blockCustomerMessage}`
          })
        }
      }
    )
  }

  unblockCustomer(customer) {
    this.props.unblockCustomer(
      {
        mobile: customer.mobile
      },
      () => {
        if (this.props.accountManagement.unblockCustomerStatus === 'success') {
          this.setState({
            showModal: true,
            modalText: `${this.props.accountManagement.unblockCustomerData.msg}`
          })
          this.props.getBlockUnblockCustomer({
            mobile: customer.mobile
          })
        } else {
          this.setState({
            showModal: true,
            modalText: `${this.props.accountManagement.unblockCustomerMessage}`
          })
        }
      }
    )
  }

  handleClose = () => this.setState({ showModal: false })
  blockCustomerClose = () => this.setState({ blockCustomerModal: false })

  onchange = ({ target }) => {
    const { value } = target
    if (isNaN(value)) {
      return
    }
    this.setState({ searchtext: value })
  }

  searchuser = event => {
    event.preventDefault()
    if (this.state.searchtext.length !== 10)
      this.setState({
        showModal: true,
        modalText: `Enter Valid Mobile`
      })
    else
      this.props.getBlockUnblockCustomer({
        mobile: this.state.searchtext
      })
  }

  renderCustomerData() {
    if (this.props.accountManagement.blockUnblockCustomerStatus === 'success') {
      if (this.props.accountManagement.blockUnblockCustomerData) {
        const customer = { ...this.props.accountManagement.blockUnblockCustomerData }
        const { id, mobile, firstname, lastname, status } = customer
        return (
          <tr key={id}>
            <td>{mobile}</td>
            <td>{firstname}</td>
            <td>{lastname}</td>
            <td>{status === 'active' ? 'Active' : 'Inactive'}</td>
            <td>
              {status === 'active' ? (
                <Button
                  bsStyle="primary"
                  className="btn-primary-small block_unbloc_top_btn"
                  onClick={this.blockCustomer.bind(this, customer)}
                >
                  BLOCK
                </Button>
              ) : (
                <Button
                  type="button"
                  bsStyle="secondary"
                  className="btn-primary-small unblock_acct_manage"
                  onClick={this.unblockCustomer.bind(this, customer)}
                >
                  UNBLOCK
                </Button>
              )}
            </td>
          </tr>
        )
      } else
        return (
          <tr>
            <td align="center" colSpan="6">
              No Customer Found
            </td>
          </tr>
        )
    } else {
      return (
        <tr>
          <td align="center" colSpan="6">
            No Customer Found
          </td>
        </tr>
      )
    }
  }

  render() {
    return (
      <div>
        <Col xs="10" className="block_unblock_acct scroll">
          <PageHeader>BLOCK/UNBLOCK ACCOUNT</PageHeader>
          <Grid fluid>
            <Row>
              <Col xs={12}>
                <form className="searchbar" onSubmit={this.searchuser}>
                  <Row className="form-group">
                    <Col xs={9}>
                      <FormGroup>
                        <FormControl
                          type="text"
                          placeholder="Enter Mobile Number"
                          maxLength="10"
                          onChange={this.onchange}
                          value={this.state.searchtext}
                        />
                      </FormGroup>
                    </Col>
                    <Col xs={3}>
                      <Button type="submit" bsStyle="primary">
                        <Glyphicon glyph="glyphicon glyphicon-search mr5" />
                        Search
                      </Button>
                    </Col>
                  </Row>
                </form>
                <FormGroup>
                  <Table responsive striped bordered condensed hover>
                    <thead>
                      <tr>
                        <th>Mobile Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>{this.renderCustomerData()}</tbody>
                  </Table>
                </FormGroup>
              </Col>
              <Modal show={this.state.showModal} onHide={this.handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title />
                </Modal.Header>
                <Modal.Body>
                  <p>{this.state.modalText}</p>
                </Modal.Body>
                <Modal.Footer>
                  <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
                    Cancel
                  </Button>
                </Modal.Footer>
              </Modal>
            </Row>
          </Grid>
        </Col>
        <BlockCustomer
          show={this.state.blockCustomerModal}
          onHide={this.blockCustomerClose}
          customer={this.state.blockCustomer}
        />
      </div>
    )
  }
}

function mapStateToProps({ accountManagement }) {
  return { accountManagement }
}
export default connect(
  mapStateToProps,
  actions
)(BlockUnblockAccount)
