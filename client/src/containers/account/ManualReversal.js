import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Grid, Row, Col, FormControl, FormGroup, Button, Glyphicon, Table } from 'react-bootstrap'
import Cookies from 'universal-cookie'

class ManualReversal extends Component {
  constructor(props, context) {
    super(props, context)
    const cookies = new Cookies()
    const admin = cookies.get('admin')
    const { id, accountaccess } = admin
    let viewer, maker, checker
    if (`${accountaccess}`.length === 3) {
      viewer = accountaccess.charAt(0)
      maker = accountaccess.charAt(1)
      checker = accountaccess.charAt(2)
    }

    this.state = {
      topupModal: false,
      showModal: false,
      showApproveModal: false,
      approvalData: null,
      showRejectModal: false,
      rejectData: null,
      modalText: '',
      searchtext: '',
      topupCustomer: null,
      id,
      viewer,
      maker,
      checker
    }
  }

  render() {
    return (
      <Grid fluid className="scroll">
        <Row>
          <Col xs={12}>
            <form className="searchbar">
              <Row className="form-group">
                <Col xs={9}>
                  <FormGroup>
                    <FormControl type="text" placeholder="Enter Transaction ID" />
                  </FormGroup>
                </Col>
                <Col xs={3}>
                  <Button bsStyle="primary">
                    <Glyphicon glyph="glyphicon glyphicon-search mr5" />
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
            <FormGroup>
              <Table responsive striped bordered condensed hover>
                <thead>
                  <tr>
                    <th>Transaction ID</th>
                    <th>Mobile Number</th>
                    <th>Debit Amount</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>111223344</td>
                    <td>9821123456</td>
                    <td>100</td>
                    <td>
                      <Button bsStyle="primary" className="btn-primary-small">
                        Reverse
                      </Button>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </FormGroup>
          </Col>
        </Row>
      </Grid>
    )
  }
}

function mapStateToProps({ accountManagement }) {
  return { accountManagement }
}
export default connect(
  mapStateToProps,
  actions
)(ManualReversal)
