import * as types from './actions/types'

export default function (state = {}, action) {
  switch (action.type) {
    case types.GET_BLOCK_UNBLOCK_CUSTOMER:
      return {
        ...state,
        blockUnblockCustomerStatus: action.payload.status,
        blockUnblockCustomerData: action.payload.data,
        blockUnblockCustomerMessage: action.payload.message
      }
    case types.CUSTOMER_TOPUP:
      return {
        ...state,
        customerTopUpStatus: action.payload.status,
        customerTopUpData: action.payload.data,
        customerTopUpMessage: action.payload.message
      }
    case types.GET_CREDIT_CUSTOMER:
      return {
        ...state,
        creditCustomerStatus: action.payload.status,
        creditCustomerData: action.payload.data,
        creditCustomerMessage: action.payload.message
      }
    case types.CUSTOMER_BLOCK:
      return {
        ...state,
        blockCustomerStatus: action.payload.status,
        blockCustomerData: action.payload.data,
        blockCustomerMessage: action.payload.message
      }
    case types.CUSTOMER_UNBLOCK:
      return {
        ...state,
        unblockCustomerStatus: action.payload.status,
        unblockCustomerData: action.payload.data,
        unblockCustomerMessage: action.payload.message
      }
    case types.BULK_CREDIT:
      return {
        ...state,
        bulkCreditStatus: action.payload.status,
        bulkCreditData: action.payload.data,
        bulkCreditMessage: action.payload.message
      }
    case types.GET_ADMIN_OPS:
      return {
        ...state,
        adminOpsStatus: action.payload.status,
        adminOpsData: action.payload.data,
        adminOpsMessage: action.payload.message
      }
    case types.GET_ADMIN_OPS_MAKER:
      return {
        ...state,
        adminOpsMakerStatus: action.payload.status,
        adminOpsMakerData: action.payload.data,
        adminOpsMakerMessage: action.payload.message
      }
    case types.GET_BULK_ADMIN_OPS:
      return {
        ...state,
        bulkAdminOpsStatus: action.payload.status,
        bulkAdminOpsData: action.payload.data,
        bulkAdminOpsMessage: action.payload.message
      }
    case types.GET_BULK_ADMIN_OPS_MAKER:
      return {
        ...state,
        bulkAdminOpsMakerStatus: action.payload.status,
        bulkAdminOpsMakerData: action.payload.data,
        bulkAdminOpsMakerMessage: action.payload.message
      }
    case types.GENERATE_BULK_CREDIT_OTP:
      return {
        ...state,
        generateBulkOTPStatus: action.payload.status,
        generateBulkOTPData: action.payload.data,
        generateBulkOTPMessage: action.payload.message
      }
    case types.GENERATE_MANUAL_CREDIT_OTP:
      return {
        ...state,
        generateManualOTPStatus: action.payload.status,
        generateManualOTPData: action.payload.data,
        generateManualOTPMessage: action.payload.message
      }
    case types.APPROVE_BULK_CREDIT:
      return {
        ...state,
        approveBulkCreditStatus: action.payload.status,
        approveBulkCreditData: action.payload.data,
        approveBulkCreditMessage: action.payload.message
      }
    case types.REJECT_BULK_CREDIT:
      return {
        ...state,
        rejectBulkCreditStatus: action.payload.status,
        rejectBulkCreditData: action.payload.data,
        rejectBulkCreditMessage: action.payload.message
      }
    case types.APPROVE_MANUAL_CREDIT:
      return {
        ...state,
        approveManualCreditStatus: action.payload.status,
        approveManualCreditData: action.payload.data,
        approveManualCreditMessage: action.payload.message
      }
    case types.REJECT_MANUAL_CREDIT:
      return {
        ...state,
        rejectManualCreditStatus: action.payload.status,
        rejectManualCreditData: action.payload.data,
        rejectManualCreditMessage: action.payload.message
      }
    default:
      return state
  }
}
