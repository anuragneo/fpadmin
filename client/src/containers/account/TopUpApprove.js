import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Row, Col, Button, FormControl, FormGroup, Modal, ControlLabel } from 'react-bootstrap'
import Cookies from 'universal-cookie'

class TopUpApprove extends Component {
  constructor(props) {
    super(props)
    const cookies = new Cookies()
    const admin = cookies.get('admin')
    const { id, accountaccess } = admin
    let viewer, maker, checker
    if (`${accountaccess}`.length === 3) {
      viewer = accountaccess.charAt(0)
      maker = accountaccess.charAt(1)
      checker = accountaccess.charAt(2)
    }
    this.state = {
      managerid: id,
      viewer,
      maker,
      checker,
      otp: '',
      comments: '',
      showModal: false,
      modalText: ''
    }
    this.updateInput = this.updateInput.bind(this)
  }

  handleClose = () => this.setState({ showModal: false })
  updateInput = ({ target }) => {
    const { name, value } = target
    switch (name) {
      case 'otp':
        if (isNaN(value)) {
          break
        }
        this.setState({ otp: value })
        break
      case 'comments':
        this.setState({ comments: value })
        break
      default:
        break
    }
  }

  approveBulkUpload = () => {
    const { otp, comments, managerid } = this.state
    const { id, authtoken, platform, otptype } = this.props.data
    if (!otp || `${otp}`.length !== 6) {
      return this.setState({
        showModal: true,
        modalText: 'Please enter a 6 digit valid otp'
      })
    }
    if (!comments) {
      return this.setState({
        showModal: true,
        modalText: 'Please add a comment'
      })
    }
    this.props.approveManualCredit(
      {
        managerid,
        id,
        platform,
        otptype,
        authtoken,
        otp,
        comments
      },
      () => {
        const {
          approveManualCreditStatus,
          approveManualCreditData,
          approveManualCreditMessage
        } = this.props.accountManagement
        if (approveManualCreditStatus === 'success') {
          this.props.onHide()
          this.props.getAdminOps({
            managerid
          })
          this.setState({
            showModal: true,
            modalText: approveManualCreditData.message,
            otp: '',
            comments: ''
          })
        } else {
          this.setState({
            showModal: true,
            modalText: approveManualCreditMessage
          })
        }
      }
    )
  }

  render() {
    const { show, onHide } = this.props
    return (
      <div>
        <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-lg">
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg">Approve</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Row>
              <Col xs={12}>
                <FormGroup>
                  <ControlLabel>Enter OTP</ControlLabel>
                  <FormControl name="otp" value={this.state.otp} maxLength="6" onChange={this.updateInput} />
                </FormGroup>
              </Col>
              <Col xs={12}>
                <FormGroup>
                  <ControlLabel>Comments</ControlLabel>
                  <FormControl name="comments" value={this.state.comments} onChange={this.updateInput} />
                </FormGroup>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer>
            <Row>
              <Col xs={6}>
                <Button bsStyle="secondary" onClick={onHide}>
                  Cancel
                </Button>
              </Col>
              <Col xs={6}>
                <Button bsStyle="primary" onClick={this.approveBulkUpload}>
                  Done
                </Button>
              </Col>
            </Row>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps({ accountManagement, admin }) {
  return { accountManagement, admin }
}

export default connect(
  mapStateToProps,
  actions
)(TopUpApprove)
