import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import Sidebar from '../../components/sidebar'
import BlockUnblockAccount from './BlockUnblockAccount'
import Credit from './Credit'
import Debit from './Debit'

export default class Account extends Component {

  render() {
    if (this.props.history.location.pathname === '/' || this.props.history.location.pathname === '/account')
      this.props.history.push('/account/block');
    return (
      <div>
        <Sidebar type="account" />
        <Route path="/account/block" component={BlockUnblockAccount} />
        <Route path="/account/credit" component={Credit} />
        <Route path="/account/debit" component={Debit} />
      </div>
    )
  }
}
