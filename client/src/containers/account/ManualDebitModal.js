import React, { Component } from 'react'
import { Row, Col, Button, FormControl, FormGroup, Modal, ControlLabel } from 'react-bootstrap'

class ManualDebitModal extends Component {
  render() {
    const { show, onHide } = this.props
    return (
      <div>
        <Modal show={show} onHide={onHide} bsSize="medium" aria-labelledby="contained-modal-title-lg">
          <Modal.Body>
            <Row>
              <Col xs={3} className="mt12">
                <FormGroup>
                  <ControlLabel>Transaction Id</ControlLabel>
                </FormGroup>
              </Col>
              <Col xs={9}>
                <FormGroup>
                  <FormControl type="text" name="id" value="11223344" disabled="disabled" />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={3} className="mt12">
                <FormGroup>
                  <ControlLabel>Current Balance</ControlLabel>
                </FormGroup>
              </Col>
              <Col xs={9}>
                <FormGroup>
                  <FormControl type="text" name="currentbalance" value="1000" disabled="disabled" />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={3} className="mt12">
                <FormGroup>
                  <ControlLabel>Top Up Balance</ControlLabel>
                </FormGroup>
              </Col>
              <Col xs={9}>
                <FormGroup>
                  <FormControl type="text" name="topupbalance" value="1000" disabled="disabled" />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={3} className="mt12">
                <FormGroup>
                  <ControlLabel>Debit Amount</ControlLabel>
                </FormGroup>
              </Col>
              <Col xs={9}>
                <FormGroup>
                  <FormControl type="text" name="debitamount" value="1000" />
                </FormGroup>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer>
            <Row>
              <Col xs={6}>
                <Button bsStyle="secondary" onClick={onHide}>
                  Cancel
                </Button>
              </Col>
              <Col xs={6}>
                <Button bsStyle="primary" onClick={onHide}>
                  Submit
                </Button>
              </Col>
            </Row>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

export default ManualDebitModal
