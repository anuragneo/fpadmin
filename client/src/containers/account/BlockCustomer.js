import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Modal, Row, Col, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'

class BlockCustomer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      blockCustomerModal: false,
      comments: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      comments: ''
    })
  }

  onchange = event => {
    this.setState({
      comments: event.target.value
    })
  }

  blockcustomer = () => {
    this.props.blockCustomer(
      {
        mobile: this.props.customer.mobile
      },
      () => {
        if (this.props.accountManagement.blockCustomerStatus === 'success') {
          this.props.onHide()
        } else {
          this.props.getCustomer({})
        }
      }
    )
  }

  render() {
    const { show, onHide } = this.props
    return (
      <div>
        <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-lg">
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg">Remove</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Row>
              <Col xs={12}>
                <FormGroup>
                  <ControlLabel>Comments</ControlLabel>
                  <FormControl name="comments" value={this.state.comments} onChange={this.onchange} />
                </FormGroup>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer>
            <Row>
              <Col xs={6}>
                <Button bsStyle="secondary" onClick={onHide}>
                  CANCEL
                </Button>
              </Col>
              <Col xs={6}>
                <Button bsStyle="primary" onClick={this.blockcustomer}>
                  SUBMIT
                </Button>
              </Col>
            </Row>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps({ accountManagement }) {
  return { accountManagement }
}

export default connect(
  mapStateToProps,
  actions
)(BlockCustomer)
