import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { Row, Col, Button, FormControl, FormGroup, Modal, ControlLabel } from 'react-bootstrap'
import Cookies from 'universal-cookie'

class TopUp extends Component {
  constructor(props) {
    super(props)
    const cookies = new Cookies()
    const admin = cookies.get('admin')
    const { id } = admin
    this.state = {
      amount: '',
      walletid: '',
      showModal: false,
      modalText: '',
      id
    }
    this.updateInput = this.updateInput.bind(this)
  }

  componentDidMount() {
    this.props.getAdminWallet()
  }

  handleClose = () => this.setState({ showModal: false })
  updateInput = ({ target }) => {
    const { name, value, max } = target
    switch (name) {
      case 'amount':
        if (isNaN(value) || parseFloat(value) > parseFloat(max)) {
          break
        }
        this.setState({ amount: value })
        break
      case 'walletid':
        this.setState({ walletid: value })
        break
      default:
        break
    }
  }

  doTopUp = () => {
    const { id, amount, walletid } = this.state
    const { mobile } = this.props.customer
    if (!amount) {
      return this.setState({
        showModal: true,
        modalText: 'Please enter the amount'
      })
    }
    if (!walletid) {
      return this.setState({
        showModal: true,
        modalText: 'Please select a wallet'
      })
    }
    this.props.doCustomerTopUp(
      {
        mobile: mobile,
        amount: amount,
        walletid: walletid,
        managerid: id
      },
      () => {
        const { customerTopUpStatus, customerTopUpMessage } = this.props.accountManagement
        if (customerTopUpStatus === 'success') {
          this.props.onHide()
          this.setState({ amount: '' })
          this.setState({ walletid: '' })
          this.props.getAdminOpsMaker({
            managerid: id
          })
          this.setState({
            showModal: true,
            modalText: 'Topup request submitted successfully.'
          })
        } else {
          this.setState({
            showModal: true,
            modalText: customerTopUpMessage
          })
        }
      }
    )
  }

  renderwalletlist() {
    const { walletStatus, walletData } = this.props.admin
    if (walletStatus === 'success' && walletData) {
      return (
        <optgroup>
          <option key="" value="">
            Select
          </option>
          {walletData.details.map((v, k) => (
            <option key={v.walletid} value={v.walletid}>
              {v.name}
            </option>
          ))}
        </optgroup>
      )
    }
  }

  render() {
    const { show, onHide } = this.props
    return (
      <div>
        <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-lg">
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg">Top Up</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Row>
              <Col xs={12}>
                <FormGroup>
                  <ControlLabel>Amount</ControlLabel>
                  <FormControl name="amount" value={this.state.amount} max="50000" onChange={this.updateInput} />
                </FormGroup>
              </Col>
              <Col xs={12}>
                <FormGroup>
                  <ControlLabel>Wallet</ControlLabel>
                  <FormControl
                    name="walletid"
                    onChange={this.updateInput}
                    componentClass="select"
                    placeholder="select"
                    value={this.state.walletid}
                  >
                    {this.renderwalletlist()}
                  </FormControl>
                </FormGroup>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer>
            <Row>
              <Col xs={6}>
                <Button bsStyle="secondary" onClick={onHide}>
                  Cancel
                </Button>
              </Col>
              <Col xs={6}>
                <Button bsStyle="primary" onClick={this.doTopUp}>
                  Done
                </Button>
              </Col>
            </Row>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps({ accountManagement, admin }) {
  return { accountManagement, admin }
}

export default connect(
  mapStateToProps,
  actions
)(TopUp)
