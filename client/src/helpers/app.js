import Cookies from 'universal-cookie'
import { capitalize } from './utils'

export function getAdmin() {
  const cookies = new Cookies()
  return cookies.get('admin')
}

export function isLoggedIn() {
  const cookies = new Cookies()
  return cookies.get('admin') ? true : false
}

export function getAdminName() {
  const cookies = new Cookies()
  const admin = cookies.get('admin')
  return admin ? capitalize(admin.firstname) : ''
}

export function getNavItems() {
  let items = []
  let admin = getAdmin()

  if (admin.adminaccess.indexOf(1) > -1) items.push({ name: 'Administration', route: '/admin' })
  if (admin.accountaccess.indexOf(1) > -1) items.push({ name: 'Account Management', route: '/account' })
  if (admin.customercare.indexOf(1) > -1) items.push({ name: 'Customer Care', route: '/customercare' })
  if (admin.offersaccess.indexOf(1) > -1) items.push({ name: 'Campaigns & Offers', route: '/campaigns' })
  if (admin.contentaccess.indexOf(1) > -1) items.push({ name: 'Content Management', route: '/content' })
  if (admin.commsaccess.indexOf(1) > -1) items.push({ name: 'Communications', route: '/communications' })
  if (admin.reporting.indexOf(1) > -1) items.push({ name: 'Reporting', route: '/reporting' })

  return items
}

export function getSidebarData(type) {
  let admin = getAdmin()
  let commAccess = admin.commsaccess.split('')
  switch (type) {
    case 'admin':
      return [
        { name: 'User Management', route: '/admin/user' },
        { name: 'Channel Type', route: '/admin/channel' },
        { name: 'Wallet Configuration', route: '/admin/wallet' }
      ]
    case 'account':
      return [
        { name: 'Block/Unblock Account', route: '/account/block' },
        { name: 'Credit', route: '/account/credit' },
        { name: 'Debit', route: '/account/debit' }
      ]

    case 'communications':
      if (commAccess[1] === '0' && commAccess[2] === '0')
        return [
          { name: 'Search', route: '/communications/search' },
          { name: 'Reports', route: '/communications/reports' }
        ]
      return [
        { name: 'Search', route: '/communications/search' },
        { name: 'Add', route: '/communications/add' },
        { name: 'Reports', route: '/communications/reports' }
      ]

    case 'content':
      return [
        { name: 'Banner', route: '/content/banner' },
        { name: 'Start Screen', route: '/content/start' },
        { name: 'Static Content', route: '/content/static' }
      ]

    case 'reporting':
      return [
        { name: 'Customers', route: '/reporting/customers' },
        { name: 'Payback', route: '/reporting/payback' },
        { name: 'BBPC', route: '/reporting/bbpc' },
        { name: 'Big Bazaar', route: '/reporting/bigbazaar' },
        { name: 'Central', route: '/reporting/central' },
        { name: 'Easy Day', route: '/reporting/easyday' },
        { name: 'Brand Factory', route: '/reporting/brandfactory' },
        { name: 'Master Report', route: '/reporting/masterreport' }
      ]
    default:
      return []
  }
}
