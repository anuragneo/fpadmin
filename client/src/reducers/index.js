import { combineReducers } from 'redux'
import { reducer as reduxForm } from 'redux-form'
import customarecareReducer from '../containers/customercare/customarecareReducer'
import adminReducer from '../containers/admin/adminReducer'
import accountManagementReducer from '../containers/account/accountManagementReducer'
import commsReducer from '../containers/communications/communicationReducer'
import contentReducer from '../containers/content/contentReducer'
import reportReducer from '../containers/reporting/reportReducer'

export default combineReducers({
  form: reduxForm,
  customarecare: customarecareReducer,
  admin: adminReducer,
  accountManagement: accountManagementReducer,
  commsReducer: commsReducer,
  contentReducer: contentReducer,
  report: reportReducer
})
