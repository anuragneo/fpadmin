import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import {
  Navbar,
  Nav,
  NavItem,
  Image,
  Glyphicon,
  Row,
  Col,
  Tab,
  Grid,
  PanelGroup,
  FormGroup,
  FormControl,
  Button,
  Table,
  ButtonGroup,
  DropdownButton,
  MenuItem,
  PageHeader,
  ControlLabel,
  Checkbox
} from 'react-bootstrap'
import Profile from './Profile'
import SearchBar from './SearchBar'
import Wallet from './Wallet'
import Transactions from './Transactions'
import Loyalty from './Loyalty'
import Communications from './Communications'
import Feedbacks from './Feedbacks'
import PriceMatch from './PriceMatch'
import Administration from './Administration'
import AccountManagement from './AccountManagement'
import Cookies from 'universal-cookie'
// import Cookies from 'js-cookie'

class CustomareCare extends Component {
  constructor(props) {
    super(props)
    this.handleSelect = this.handleSelect.bind(this)

    this.state = {
      activeKey: ''
    }
  }

  handleSelect(activeKey) {
    this.setState({ activeKey })
  }

  componentDidUpdate(prevProps) {
    if (this.props.customarecare !== prevProps.customarecare) {
      if (this.props.customarecare.profileStatus) {
        this.handleSelect('1')
      } else {
        this.handleSelect('')
      }
    }
  }

  async handleLogout(e) {
    e.preventDefault()
    const res = await axios.post(`/api/logout`, {})
    if (res.data.status === 'success') {
      window.location.href = "http://localhost:4000/"
    }
  }

  render() {
    const cookies = new Cookies()
    const admin = cookies.get('admin')
    console.log(cookies.get('admin'))
    return (
      <div>
        <Navbar fluid>
          <Navbar.Header>
            <Image src="./fp_white_logo.svg" />
            <Navbar.Toggle />
          </Navbar.Header>

          <Navbar.Text pullRight>
            <span> {`Welcome ${admin ? admin.firstname : ''}`} </span>
            <a onClick={e => this.handleLogout(e)} className="logout">
              <Glyphicon glyph="glyphicon glyphicon-off" />
            </a>
          </Navbar.Text>
        </Navbar>

        <Tab.Container id="tabs-with-dropdown" defaultActiveKey="first">
          <Grid fluid>
            <Row className="clearfix">
              <Col sm={12} className="pad-l-r-0">
                <Nav bsStyle="tabs">
                  {admin.adminaccess.indexOf(1) > -1 && <NavItem eventKey="first">Administration</NavItem>}

                  {admin.accountaccess.indexOf(1) > -1 && (
                    <NavItem eventKey="second">
                      Account Management
                      <div className="bbot" />
                    </NavItem>
                  )}

                  {admin.customercare.indexOf(1) > -1 && (
                    <NavItem eventKey="third">
                      Customer Care
                      <div className="bbot" />
                    </NavItem>
                  )}

                  {admin.offersaccess.indexOf(1) > -1 && (
                    <NavItem eventKey="fourth">
                      Campaigns & Offers
                      <div className="bbot" />
                    </NavItem>
                  )}

                  {admin.contentaccess.indexOf(1) > -1 && (
                    <NavItem eventKey="fifth">
                      Content Management
                      <div className="bbot" />
                    </NavItem>
                  )}

                  {admin.commsaccess.indexOf(1) > -1 && (
                    <NavItem eventKey="sixth">
                      Communications
                      <div className="bbot" />
                    </NavItem>
                  )}

                  {admin.reporting.indexOf(1) > -1 && (
                    <NavItem eventKey="sixth">
                      Reporting
                      <div className="bbot" />
                    </NavItem>
                  )}
                </Nav>
              </Col>
              <Col sm={12} className="pad-l-r-0">
                <Tab.Content animation>
                  <Tab.Pane eventKey="second">
                    <AccountManagement />
                    <Grid fluid>
                      <Row />
                    </Grid>
                  </Tab.Pane>
                  <Tab.Pane eventKey="third">
                    <Grid>
                      <Row>
                        <Col xs={12}>
                          <SearchBar />

                          <PanelGroup
                            accordion
                            id="accordion-example"
                            activeKey={this.state.activeKey}
                            onSelect={this.handleSelect}
                          >
                            <Profile />

                            <Wallet />

                            <Transactions />

                            <Loyalty />

                            <PriceMatch />

                            <Feedbacks />

                            <Communications />
                          </PanelGroup>
                        </Col>
                      </Row>
                    </Grid>
                  </Tab.Pane>
                  <Tab.Pane eventKey="first">
                    <Administration />
                  </Tab.Pane>
                  <Tab.Pane eventKey="sixth">
                    <Grid fluid>
                      <Row>
                        <Col xs="2" className="admin-left-drawer">
                          <ul>
                            <li className="active">
                              <a>
                                Uset Management <Glyphicon glyph="glyphicon glyphicon-play" />
                              </a>
                            </li>
                            <li>
                              <a>
                                Channel Type <Glyphicon glyph="glyphicon glyphicon-play" />
                              </a>
                            </li>
                            <li>
                              <a>
                                Wallet Configura <Glyphicon glyph="glyphicon glyphicon-play" />
                              </a>
                            </li>
                          </ul>
                        </Col>
                        <Col xs="10" hidden>
                          <PageHeader>SEARCH</PageHeader>
                          <Grid fluid>
                            <Row>
                              <Col xs={12}>
                                <form className="searchbar">
                                  <Row>
                                    <Col xs={9}>
                                      <FormGroup>
                                        <FormControl type="text" placeholder="Enter Mobile Number" />
                                      </FormGroup>
                                    </Col>
                                    <Col xs={3}>
                                      <Button bsStyle="primary">
                                        <Glyphicon glyph="glyphicon glyphicon-search" />
                                        Search
                                      </Button>
                                    </Col>
                                  </Row>
                                </form>
                                <Table responsive className="cspd panel">
                                  <thead>
                                    <tr>
                                      <th>
                                        Communica-
                                        <br />
                                        tions ID
                                      </th>
                                      <th>
                                        Communica-
                                        <br />
                                        tions Name
                                      </th>
                                      <th>Event/Manual Trigger</th>
                                      <th>Status</th>
                                      <th>Schedule</th>
                                      <th>Created At</th>
                                      <th>Updated At</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>1123456</td>
                                      <td>ABC</td>
                                      <td>ABC</td>
                                      <td>
                                        <Button bsStyle="primary" className="btn-primary-small">
                                          Top Up
                                        </Button>
                                        <Button
                                          type="button"
                                          bsStyle="secondary"
                                          className="btn-primary-small"
                                          onClick={this.reset}
                                        >
                                          <Glyphicon glyph="glyphicon glyphicon-repeat" /> Reset
                                        </Button>
                                      </td>
                                      <td>
                                        <DropdownButton>
                                          <MenuItem eventKey="1">Action</MenuItem>
                                          <MenuItem eventKey="2">Another action</MenuItem>
                                          <MenuItem eventKey="3" active>
                                            Active Item
                                          </MenuItem>
                                          <MenuItem divider />
                                          <MenuItem eventKey="4">Separated link</MenuItem>
                                        </DropdownButton>
                                      </td>
                                      <td>2/8/2018 12:00</td>
                                      <td>2/8/2018 12:00</td>
                                      <td>
                                        <ButtonGroup>
                                          <Button className="edit_btn">
                                            <Glyphicon glyph=" glyphicon glyphicon-edit" /> Edit
                                          </Button>
                                          <Button className="remove_btn">
                                            <Glyphicon glyph=" glyphicon glyphicon-trash" /> Remove
                                          </Button>
                                        </ButtonGroup>
                                      </td>
                                    </tr>
                                  </tbody>
                                </Table>
                              </Col>
                            </Row>
                          </Grid>
                        </Col>
                        <Col xs="10" hidden>
                          <PageHeader>ADD</PageHeader>
                          <Grid fluid>
                            <Row>
                              <Col xs={10} xsOffset={1}>
                                <form>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Communication ID</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Communication Name</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Reference ID</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Reference Name</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>

                                  <Row>
                                    <Col xs={12}>
                                      <label className="form-group">Communication Trigger</label>
                                      <FormGroup>
                                        <Row>
                                          <Col xs={2}>
                                            <Checkbox inline>Event</Checkbox>
                                          </Col>
                                          <Col xs={2}>
                                            <Checkbox inline>Manual</Checkbox>
                                          </Col>
                                          <Col xs={6}>
                                            <FormControl />
                                          </Col>
                                          <Col xs={2}>
                                            <Button bsStyle="primary">
                                              <Glyphicon glyph="glyphicon glyphicon-open" />
                                              Upload
                                            </Button>
                                          </Col>
                                        </Row>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col xs={6}>
                                      <FormGroup>
                                        <ControlLabel>Select Event</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                    <Col xs={6}>
                                      <h3 className="personalize_var">
                                        Personalization Variable
                                        <Glyphicon glyph="glyphicon glyphicon-info-sign" />
                                      </h3>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col xs={12}>
                                      <label>Select Channel</label>
                                      <FormGroup>
                                        <Row>
                                          <Col xs={2}>
                                            <Checkbox inline>Push</Checkbox>
                                          </Col>
                                          <Col xs={2}>
                                            <Checkbox inline>In App</Checkbox>
                                          </Col>
                                          <Col xs={2}>
                                            <Checkbox inline>SMS</Checkbox>
                                          </Col>
                                          <Col xs={2}>
                                            <Checkbox inline>Email</Checkbox>
                                          </Col>
                                        </Row>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <hr />
                                  <Row>
                                    <Col xs={6}>
                                      <FormGroup>
                                        <ControlLabel>SMS</ControlLabel>
                                      </FormGroup>
                                    </Col>
                                    <Col xs={6}>
                                      <FormGroup className="preview">
                                        <ControlLabel>
                                          <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                                          PREVIEW
                                        </ControlLabel>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={12}>
                                      <FormGroup>
                                        <ControlLabel>Text</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Sender ID</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Push</ControlLabel>
                                      </FormGroup>
                                    </Col>
                                    <Col xs={6}>
                                      <FormGroup className="preview">
                                        <ControlLabel>
                                          <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                                          PREVIEW
                                        </ControlLabel>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={12}>
                                      <FormGroup>
                                        <ControlLabel>Header</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={12}>
                                      <FormGroup>
                                        <ControlLabel>Multimedia URL</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={12}>
                                      <FormGroup>
                                        <ControlLabel>Contet</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={12}>
                                      <FormGroup>
                                        <ControlLabel>Deeplink Screen</ControlLabel>
                                        <DropdownButton>
                                          <MenuItem eventKey="1">Action</MenuItem>
                                          <MenuItem eventKey="2">Another action</MenuItem>
                                          <MenuItem eventKey="3" active>
                                            Active Item
                                          </MenuItem>
                                          <MenuItem divider />
                                          <MenuItem eventKey="4">Separated link</MenuItem>
                                        </DropdownButton>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Notification Valid For (No. of Days)</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Email</ControlLabel>
                                      </FormGroup>
                                    </Col>
                                    <Col xs={6}>
                                      <FormGroup className="preview">
                                        <ControlLabel>
                                          <Glyphicon glyph="glyphicon glyphicon-eye-open" />
                                          PREVIEW
                                        </ControlLabel>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>HTML URL/Content</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <hr />
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>SMS</ControlLabel>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col xs={2}>
                                      <Checkbox className="mt28" inline>
                                        Immediate
                                      </Checkbox>
                                    </Col>
                                    <Col xs={2}>
                                      <Checkbox className="mt28" inline>
                                        Schedule
                                      </Checkbox>
                                    </Col>
                                    <Col xs={4}>
                                      <FormGroup>
                                        <ControlLabel>Start Date</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                    <Col xs={4}>
                                      <FormGroup>
                                        <ControlLabel>Start Time</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Push</ControlLabel>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col xs={2}>
                                      <Checkbox className="mt28" inline>
                                        Immediate
                                      </Checkbox>
                                    </Col>
                                    <Col xs={2}>
                                      <Checkbox className="mt28" inline>
                                        Schedule
                                      </Checkbox>
                                    </Col>
                                    <Col xs={4}>
                                      <FormGroup>
                                        <ControlLabel>Start Date</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                    <Col xs={4}>
                                      <FormGroup>
                                        <ControlLabel>Start Time</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Email</ControlLabel>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row className="form-group">
                                    <Col xs={2} className="form-group">
                                      <Checkbox className="mt28" inline>
                                        Immediate
                                      </Checkbox>
                                    </Col>
                                    <Col xs={2} className="form-group">
                                      <Checkbox className="mt28" inline>
                                        Schedule
                                      </Checkbox>
                                    </Col>
                                    <Col xs={4} className="form-group">
                                      <FormGroup>
                                        <ControlLabel>Start Date</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                    <Col xs={4} className="form-group">
                                      <FormGroup>
                                        <ControlLabel>Start Time</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={4}>
                                      <Button type="button" bsStyle="secondary" onClick={this.reset}>
                                        <Glyphicon glyph="glyphicon glyphicon-repeat" /> Reset
                                      </Button>
                                    </Col>
                                    <Col sm={4}>
                                      <Button bsStyle="primary">SAVE</Button>
                                    </Col>
                                    <Col sm={4}>
                                      <Button bsStyle="primary">SUBMIT</Button>
                                    </Col>
                                  </Row>
                                </form>
                              </Col>
                            </Row>
                          </Grid>
                        </Col>
                        <Col xs="10">
                          <PageHeader>REPORTS</PageHeader>
                          <Grid fluid>
                            <Row>
                              <Col xs={10} xsOffset={1}>
                                <form className="form-group">
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Select Event</ControlLabel>
                                        <FormControl className="form-group" />
                                        <ControlLabel className="text-center w100">OR</ControlLabel>
                                      </FormGroup>
                                    </Col>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>SelectCommunication Name</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>Select Reference System</ControlLabel>
                                        <FormControl />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>From</ControlLabel>
                                        <DropdownButton>
                                          <MenuItem eventKey="1">Action</MenuItem>
                                          <MenuItem eventKey="2">Another action</MenuItem>
                                          <MenuItem eventKey="3" active>
                                            Active Item
                                          </MenuItem>
                                          <MenuItem divider />
                                          <MenuItem eventKey="4">Separated link</MenuItem>
                                        </DropdownButton>
                                      </FormGroup>
                                    </Col>
                                    <Col sm={6}>
                                      <FormGroup>
                                        <ControlLabel>To</ControlLabel>
                                        <DropdownButton>
                                          <MenuItem eventKey="1">Action</MenuItem>
                                          <MenuItem eventKey="2">Another action</MenuItem>
                                          <MenuItem eventKey="3" active>
                                            Active Item
                                          </MenuItem>
                                          <MenuItem divider />
                                          <MenuItem eventKey="4">Separated link</MenuItem>
                                        </DropdownButton>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row className="form-group">
                                    <Col sm={6} smOffset={3} className="form-group">
                                      <Button bsStyle="primary" className="form-group">
                                        SAVE
                                      </Button>
                                    </Col>
                                  </Row>
                                </form>
                                <Row className="mt28">
                                  <Col sm={6}>
                                    <Table responsive className="cspd panel">
                                      <thead>
                                        <tr>
                                          <th colSpan="4">Priority 1:Push</th>
                                        </tr>
                                      </thead>
                                      <thead>
                                        <tr>
                                          <th>No.of Customers Attempted</th>
                                          <th>Success</th>
                                          <th>Failure</th>
                                          <th>No. of Customers Clicked on Push</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>1100</td>
                                          <td>8500</td>
                                          <td>1500</td>
                                          <td>1500</td>
                                        </tr>
                                      </tbody>
                                    </Table>
                                  </Col>
                                  <Col sm={6}>
                                    <Table responsive className="cspd panel">
                                      <thead>
                                        <tr>
                                          <th colSpan="4"> Priority 2:SMS</th>
                                        </tr>
                                      </thead>
                                      <thead>
                                        <tr>
                                          <th>No.of Customers Attempted</th>
                                          <th>Success</th>
                                          <th>Failure</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>1100</td>
                                          <td>8500</td>
                                          <td>1500</td>
                                        </tr>
                                      </tbody>
                                    </Table>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </Grid>
                        </Col>
                      </Row>
                    </Grid>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Grid>
        </Tab.Container>
      </div>
    )
  }
}

function mapStateToProps({ customarecare }) {
  return { customarecare }
}

export default connect(mapStateToProps)(CustomareCare)
