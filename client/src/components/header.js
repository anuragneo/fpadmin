import React, { Component } from 'react'
import axios from 'axios'
import { getAdminName } from '../helpers/app'
import { Navbar, Image, Glyphicon } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from '../images/fp_white_logo.svg' 

class Header extends Component {
  constructor() {
    super()
    this.logoutURL = process.env.REACT_APP_LOGOUT_URL
  }

  handleLogout(e) {
    e.preventDefault()
    axios.get(`/api/logout`)
  }

  render() {
    return (
      <Navbar fluid>
        <Navbar.Header>
          <Link to='/'>
            <Image src={logo} />
          </Link>
          <Navbar.Toggle />
        </Navbar.Header>

        <Navbar.Text pullRight>
          <span> Welcome {getAdminName()} </span>
          <a href={this.logoutURL} className="logout">
            <Glyphicon glyph="glyphicon glyphicon-off" />
          </a>
        </Navbar.Text>
      </Navbar>
    )
  }
}

export default Header
