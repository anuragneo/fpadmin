import React, { Component } from 'react'
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'
import * as actions from '../actions'
import BlockUnblockAccount from './BlockUnblockAccount'
import Credit from './Credit'
import Debit from './Debit'

class Administration extends Component {
  constructor(props) {
    super(props)
    this.state = {
      blockUnblock: true,
      credit: false,
      debit: false,
      activeLink: 'blockUnblock'
    }
  }

  blockUnblockClick = () => {
    this.setState({
      blockUnblock: true,
      credit: false,
      debit: false,
      activeLink: 'blockUnblock'
    })
  }

  creditClick = () => {
    this.setState({
      blockUnblock: false,
      credit: true,
      debit: false,
      activeLink: 'credit'
    })
  }

  debitClick = () => {
    this.setState({
      blockUnblock: false,
      credit: false,
      debit: true,
      activeLink: 'debit'
    })
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Col xs={2} className="admin-left-drawer">
            <ul>
              <li className={this.state.activeLink === 'blockUnblock' ? 'active' : ''} onClick={this.blockUnblockClick}>
                <a className="small-menu">
                  Block/Unblock Account <Glyphicon glyph="glyphicon glyphicon-play" />
                </a>
              </li>
              <li className={this.state.activeLink === 'credit' ? 'active' : ''} onClick={this.creditClick}>
                <a>
                  Credit <Glyphicon glyph="glyphicon glyphicon-play" />
                </a>
              </li>
              <li className={this.state.activeLink === 'debit' ? 'active' : ''} onClick={this.debitClick}>
                <a>
                  Debit
                  <Glyphicon glyph="glyphicon glyphicon-play" />
                </a>
              </li>
            </ul>
          </Col>
          {this.state.blockUnblock ? <BlockUnblockAccount /> : null}
          {this.state.credit ? <Credit /> : null}
          {this.state.debit ? <Debit /> : null}
        </Row>
      </Grid>
    )
  }
}

export default connect(
  null,
  actions
)(Administration)
