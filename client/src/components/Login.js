import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions'
import Cookies from 'universal-cookie'
import { Redirect } from 'react-router-dom'
import { Row, Modal, FormControl, FormGroup, ControlLabel, Col, Button, PageHeader } from 'react-bootstrap'

class Login extends Component {
  constructor() {
    super()
    this.state = {
      loginId: '',
      password: '',
      showModal: false,
      modalText: '',
      isLoggedIn: false
    }
  }

  onchange = evt => {
    switch (evt.target.name) {
      case 'loginid':
        this.setState({
          loginId: evt.target.value
        })
        break
      case 'password':
        this.setState({
          password: evt.target.value
        })
        break
      default:
        break
    }
  }

  handleClose = () => this.setState({ showModal: false })

  onSubmit = event => {
    event.preventDefault()
    if (this.state.loginId === '' || this.state.loginId.length < 2)
      this.setState({
        showModal: true,
        modalText: 'Enter valid Login Id'
      })
    else if (this.setState.password === '' || this.state.password.length < 8)
      this.setState({
        showModal: true,
        modalText: 'Enter valid 8 character Password'
      })
    else {
      this.props.login(
        {
          loginid: this.state.loginId,
          password: this.state.password
        },
        () => {
          if (this.props.admin.loginStatus === 'success') {
            const cookies = new Cookies()
            this.setState({
              isLoggedIn: true,
              loginData: this.props.admin.loginData
            })
            cookies.set('loggedIn', true, {
              path: '/',
              expires: new Date(Date.now() + 604800000)
            })
            cookies.set('admin', this.props.admin.loginData, {
              path: '/',
              expires: new Date(Date.now() + 604800000)
            })
            window.location = '/'
          } else if (this.props.admin.loginStatus === 'failed') {
            this.setState({
              showModal: true,
              modalText: this.props.admin.loginMessage
            })
          }
        }
      )
    }
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    if (this.state.isLoggedIn === true) {
      return <Redirect to={from} />
    }
    return (
      <div className="login-page">
        <form onSubmit={this.onSubmit}>
          <Row>
            <Col xs={4} xsOffset={4}>
              <PageHeader className="mt-100">Login</PageHeader>
              <FormGroup>
                <ControlLabel>Login ID</ControlLabel>
                <FormControl name="loginid" value={this.state.loginId} onChange={this.onchange} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Password</ControlLabel>
                <FormControl name="password" type="password" value={this.state.password} onChange={this.onchange} />
              </FormGroup>
              <Row>
                <Col xs={4} xsOffset={4}>
                  <Button bsStyle="primary" type="submit">
                    LOGIN
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </form>

        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title />
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modalText}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button className="modal_btn" bsSize="xsmall" onClick={this.handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps({ admin }) {
  return { admin }
}

export default connect(
  mapStateToProps,
  actions
)(Login)
