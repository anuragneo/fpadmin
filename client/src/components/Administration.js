import React, { Component } from 'react'
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'
import * as actions from '../actions'
import UserManagement from './UserManagement'
import ChannelType from './ChannelType'
import WalletConfiguration from './WalletConfiguration'

class Administration extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: true,
      channel: false,
      wallet: false,
      activeLink: 'user'
    }
  }

  componentDidMount() {
    this.props.getAdminUsers()
    this.props.getAdminChannel()
    this.props.getAdminWallet()
  }

  userClick = () => {
    this.setState({
      user: true,
      channel: false,
      wallet: false,
      activeLink: 'user'
    })
  }

  channelClick = () => {
    this.setState({
      user: false,
      channel: true,
      wallet: false,
      activeLink: 'channel'
    })
  }

  walletClick = () => {
    this.setState({
      user: false,
      channel: false,
      wallet: true,
      activeLink: 'wallet'
    })
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Col xs="2" className="admin-left-drawer">
            <ul>
              <li className={this.state.activeLink === 'user' ? 'active' : ''} onClick={this.userClick}>
                <a>
                  User Management <Glyphicon glyph="glyphicon glyphicon-play" />
                </a>
              </li>
              <li className={this.state.activeLink === 'channel' ? 'active' : ''} onClick={this.channelClick}>
                <a>
                  Channel Type <Glyphicon glyph="glyphicon glyphicon-play" />
                </a>
              </li>
              <li className={this.state.activeLink === 'wallet' ? 'active' : ''} onClick={this.walletClick}>
                <a>
                  Wallet Configuration
                  <Glyphicon glyph="glyphicon glyphicon-play" />
                </a>
              </li>
            </ul>
          </Col>
          {this.state.user ? <UserManagement /> : null}
          {this.state.channel ? <ChannelType /> : null}
          {this.state.wallet ? <WalletConfiguration /> : null}
        </Row>
      </Grid>
    )
  }
}

export default connect(
  null,
  actions
)(Administration)
