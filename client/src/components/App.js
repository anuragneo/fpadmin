import React, { Component } from 'react'
import { HashRouter as Router, Route } from 'react-router-dom'
import { Grid, Row } from 'react-bootstrap'
import { getNavItems } from '../helpers/app'
import Header from './header'
import Tabs from './tabs'
import Admin from '../containers/admin/index'
import Account from '../containers/account/index'
import Campaigns from '../containers/campaigns/index'
import Communications from '../containers/communications/index'
import Content from '../containers/content/index'
import Customercare from '../containers/customercare/index'
import Reporting from '../containers/reporting/index'

class App extends Component {
  renderRootRoute() {
    let item = getNavItems()[0]
    if (item.route === '/admin') return <Route exact path="/" component={Admin} />
    else if (item.route === '/account') return <Route exact path="/" component={Account} />
    else if (item.route === '/campaigns') return <Route exact path="/" component={Campaigns} />
    else if (item.route === '/communications') return <Route exact path="/" component={Communications} />
    else if (item.route === '/content') return <Route exact path="/" component={Content} />
    else if (item.route === '/customercare') return <Route exact path="/" component={Customercare} />
    else if (item.route === '/reporting') return <Route exact path="/" component={Reporting} />
    else return <Route exact path="/" component={() => <h1> Not found. </h1>} />
  }

  render() {
    return (
      <Router>
        <div className="app">
          <Header />
          <Tabs />
          {/* <div class="loading">Loading&#8230;</div> */}
          <Grid fluid className="fixed-container">
            <Row>
              {this.renderRootRoute()}
              <Route path="/admin" component={Admin} />
              <Route path="/account" component={Account} />
              <Route path="/campaigns" component={Campaigns} />
              <Route path="/communications" component={Communications} />
              <Route path="/reporting" component={Reporting} />
              <Route path="/content" component={Content} />
              <Route path="/customercare" component={Customercare} />
            </Row>
          </Grid>
        </div>
      </Router>
    )
  }
}

export default App
