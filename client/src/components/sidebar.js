import React, { Component } from 'react'
import { Col, Glyphicon } from 'react-bootstrap'
import { getSidebarData } from '../helpers/app'
import { NavLink } from 'react-router-dom'

export default class Sidebar extends Component {
  constructor(props) {
    super(props)
    this.state = { activeKey: 1 }
  }

  renderList = () => {
    let items = getSidebarData(this.props.type)

    return items.map((item, index) => {
      return (
        <li key={index}>
          <NavLink to={item.route} activeClassName='active'>
            {item.name}
            <Glyphicon glyph="glyphicon glyphicon-play" />
          </NavLink>
        </li>
      )
    })
  }

  render() {
    return (
      <Col xs={2} className="admin-left-drawer">
        <ul>
          {this.renderList()}
        </ul>
      </Col>
    )
  }
}
