import React, { Component } from 'react'
import { Nav, NavItem } from 'react-bootstrap';
import { getNavItems } from '../helpers/app'
import { LinkContainer } from 'react-router-bootstrap';

export default class Tabs extends Component {
  constructor(props) {
    super(props)
    this.state = { activeKey: '' }
  }

  handleSelect = (key) => {
    this.setState({ activeKey: key })
  }

  renderNavItems = () => {
    let items = getNavItems()
    return items.map((item, index) => {
      return (
        <LinkContainer to={item.route} eventKey={index + 1} key={index}>
          <NavItem > {item.name}</NavItem>
        </LinkContainer>
      )
    })
  }

  render() {
    return (
      <Nav bsStyle="pills" activeKey={this.state.activeKey} onSelect={this.handleSelect}>
        {this.renderNavItems()}
      </Nav>
    )
  }
}
