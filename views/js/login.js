(function() {
  document.getElementById('loginButton').onclick = function(e) {
    e.preventDefault()
    if (document.getElementById('loginid').value.length < 2) {
      document.getElementById('loginidalert').style.display = 'block'
      document.getElementById('passwordalert').style.display = 'none'
    } else if (document.getElementById('inputPassword').value.length < 8) {
      document.getElementById('loginidalert').style.display = 'none'
      document.getElementById('passwordalert').style.display = 'block'
    } else {
      document.getElementById('loginidalert').style.display = 'none'
      document.getElementById('passwordalert').style.display = 'none'
      fetch(window.location.href, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          loginid: document.getElementById('loginid').value,
          password: document.getElementById('inputPassword').value
        })
      })
        .then(function(response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status)
            return
          }

          // Examine the text in the response
          response.json().then(function(data) {
            if (data != null && data.status === 'success') {
              document.cookie = `admin = ${JSON.stringify(data.data)}`
              window.location.href = window.location.href.replace('login', '')
            } else if (data != null && data.status === 'failed') {
              document.getElementById('serveralert').style.display = 'block'
              document.getElementById('serveralert').innerText = data.message
            } else {
              alert('somthing went wrong')
            }
          })
        })
        .catch(function(err) {
          console.log('Fetch Error :-S', err)
        })
    }
  }
})()
