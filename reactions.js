/**
 * Channels the servie subscribes/reacts to
 */
module.exports = {
  'random.event': 'samplereaction',
  'customer.otp.registered': 'customerotpregistered'
}
