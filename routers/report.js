const express = require('express')
const router = express.Router()
const comms = require('../sys/comms')
const log = require('../sys/log')
const helper = require('../helpers/report')

router.post('/totaluser', (req, res) => {
  helper.gettotaluser(req.body, response => {
    res.send(response)
  })
})

router.post('/activeuser', (req, res) => {
  helper.getactiveinactiveuser(req.body, response => {
    res.send(response)
  })
})

router.post('/newregistered', (req, res) => {
  helper.totalnewregistered(req.body, response => {
    res.send(response)
  })
})

router.post('/registrationthroughsource', (req, res) => {
  helper.registrationthroughsource(req.body, response => {
    res.send(response)
  })
})

router.post('/customerbydate', (req, res) => {
  helper.getcustomerbydaterange(req.body, response => {
    res.send(response)
  })
})

router.post('/customerreport', (req, res) => {
  helper.getcustomerreport(req.body, response => {
    res.send(response)
  })
})

router.post('/totalactiveuserwallet', (req, res) => {
  helper.totalactiveuserwallet(req.body, response => {
    res.send(response)
  })
})

router.post('/totalspent', (req, res) => {
  helper.totalspent(req.body, response => {
    res.send(response)
  })
})

router.post('/totaltopup', (req, res) => {
  helper.totaltopup(req.body, response => {
    res.send(response)
  })
})

router.post('/totalcashback', (req, res) => {
  helper.totalcashback(req.body, response => {
    res.send(response)
  })
})

router.post('/debitvolume', (req, res) => {
  helper.debitvolume(req.body, response => {
    res.send(response)
  })
})

router.post('/countgroupdebit', (req, res) => {
  helper.countgroupdebit(req.body, response => {
    res.send(response)
  })
})

router.post('/creditvolume', (req, res) => {
  helper.creditvolume(req.body, response => {
    res.send(response)
  })
})

router.post('/countgroupcredit', (req, res) => {
  helper.countgroupcredit(req.body, response => {
    res.send(response)
  })
})

router.post('/cashbackanalysis', (req, res) => {
  helper.cashbackanalysis(req.body, response => {
    res.send(response)
  })
})

router.post('/cashbackexperiable', (req, res) => {
  helper.cashbackexperiable(req.body, response => {
    res.send(response)
  })
})

router.post('/groupbycampaignname', (req, res) => {
  helper.groupbycampaignname(req.body, response => {
    res.send(response)
  })
})

router.post('/newbbpcpaybackregistered', (req, res) => {
  helper.newbbpcpaybackregistered(req.body, response => {
    res.send(response)
  })
})

router.post('/totaldelinkingbbpcpayback', (req, res) => {
  helper.totaldelinkingbbpcpayback(req.body, response => {
    res.send(response)
  })
})

router.post('/totallinkedbbpcpayback', (req, res) => {
  helper.totallinkedbbpcpayback(req.body, response => {
    res.send(response)
  })
})

router.post('/totaldeinkedbbpcpayback', (req, res) => {
  helper.totaldeinkedbbpcpayback(req.body, response => {
    res.send(response)
  })
})

router.post('/bbpcreport', (req, res) => {
  helper.bbpcreport(req.body, response => {
    res.send(response)
  })
})

router.post('/paybackreport', (req, res) => {
  helper.paybackreport(req.body, response => {
    res.send(response)
  })
})

router.post('/paybacktransactionreport', (req, res) => {
  helper.paybacktransactionreport(req.body, response => {
    res.send(response)
  })
})

router.post('/bbpctransactionreport', (req, res) => {
  helper.bbpctransactionreport(req.body, response => {
    res.send(response)
  })
})

router.post('/successratiodebit', (req, res) => {
  helper.successratiodebit(req.body, response => {
    res.send(response)
  })
})

router.post('/successratiocredit', (req, res) => {
  helper.successratiocredit(req.body, response => {
    res.send(response)
  })
})

module.exports = router
