const express = require('express')
const router = express.Router()
const comms = require('../sys/comms')
const grpc = require('../sys/grpc')

router.post('/list', (req, res) => {
  comms.request('comms/list', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/getevents', (req, res) => {
  comms.request('comms/getevents', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/search', (req, res) => {
  comms.request('comms/search', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/create', (req, res) => {
  comms.request('comms/create', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/update', (req, res) => {
  comms.request('comms/update', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/toggle', (req, res) => {
  comms.request('comms/toggleactivation', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/getevent', (req, res) => {
  comms.request('comms/getevent', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/getactiveevents', (req, res) => {
  comms.request('comms/getactiveevents', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/getreferencenames', (req, res) => {
  comms.request('comms/getreferencenames', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/getcommnames', (req, res) => {
  comms.request('comms/getcommnames', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

router.post('/report', (req, res) => {
  comms.request('comms/searchcommlogs', req.body, grpc.getMetaData(), (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})



module.exports = router