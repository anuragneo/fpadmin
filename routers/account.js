const express = require('express')
const router = express.Router()
const comms = require('../sys/comms')
const log = require('../sys/log')
const helper = require('../helpers/account')
const multer = require('multer')
const uploads = multer({ dest: '/tmp/bulkupload/' })
const grpc = require('../sys/grpc')

router.post('/bulkupload', uploads.single('file'), (req, res) => {
  const file = req.file
  console.log("REQ IP: ", req.ip)
  console.log("cf-connecting-ip", req.headers['cf-connecting-ip'])
  console.log("x-forwarded-for", req.headers['x-forwarded-for'])
  console.log("connection.remoteAddress", req.connection.remoteAddress)
  console.log("Headers:", req.headers)
  const ip = req.ip || req.headers['x-forwarded-for'] || req.connection.remoteAddress
  const incoming = decodeURIComponent(req.body.data)

  helper.bulkupload(incoming, file, ip, response => {
    res.send(response)
  })
})

router.post('/getbulkadminops', (req, res) => {
  helper.getbulkops(req.body, response => {
    res.send(response)
  })
})

router.post('/getbulkadminopsformaker', (req, res) => {
  helper.getbulkopsformaker(req.body, response => {
    res.send(response)
  })
})

router.post('/approvebulkadminops', (req, res) => {
  helper.approvebulkops(req.body, response => {
    res.send(response)
  })
})

router.post('/rejectbulkadminops', (req, res) => {
  helper.rejectbulkops(req.body, response => {
    res.send(response)
  })
})

router.post('/generateotp', (req, res) => {
  helper.generateotp(req.body, response => {
    res.send(response)
  })
})

router.post('/getcustomer', (req, res) => {
  comms.request('customers/getbymobile', req.body, grpc.getMetaData(), (err, response) => {
    if (err) {
      res.send({ status: 'failed', message: err.details })
    } else {
      res.send({ status: 'success', data: response.customer })
    }
  })
})

router.post('/blockcustomer', (req, res) => {
  comms.request('customers/block', req.body, grpc.getMetaData(), (err, response) => {
    if (err) {
      res.send({ status: 'failed', message: err.details })
    } else {
      res.send({ status: 'success', data: response })
    }
  })
})

router.post('/activatecustomer', (req, res) => {
  comms.request('customers/activate', req.body, grpc.getMetaData(), (err, response) => {
    if (err) {
      res.send({ status: 'failed', message: err.details })
    } else {
      res.send({ status: 'success', data: response })
    }
  })
})

router.post('/topup', (req, res) => {
  helper.topup(req.body, response => {
    res.send(response)
  })
})

router.post('/rejecttopup', (req, res) => {
  helper.rejecttopup(req.body, response => {
    res.send(response)
  })
})

router.post('/approvetopup', (req, res) => {
  helper.approvetopup(req.body, response => {
    res.send(response)
  })
})

router.post('/getadminops', (req, res) => {
  helper.getadminops(req.body, response => {
    res.send(response)
  })
})

router.post('/getadminopsformaker', (req, res) => {
  helper.getadminopsformaker(req.body, response => {
    res.send(response)
  })
})

module.exports = router
