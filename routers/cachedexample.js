const express = require('express')
const cachedexamplerouter = express.Router()
let counters = require('../sys/counters')

// CHECK CACHE MIDDLEWARE
cachedexamplerouter.use((req, res, next) => {
  counters.incrementroutecount(req.originalUrl)
  routecacher.checkcache('${req.body.id}:${req.body.token}', req.originalUrl, cachedresponse => {
    if (cachedresponse) {
      res.send(cachedresponse)
      counters.decrementroutecount(req.originalUrl)
    } else next()
  })
})

// REAL VERSION
cachedexamplerouter.get('/how', (req, res, next) => {
  let realdata = 'This is real data'
  res.locals.data = realdata
  res.send(realdata)
  next()
})

cachedexamplerouter.get('/how2', (req, res, next) => {
  let realdata = 'This is real data 2'
  res.locals.data = realdata
  res.send(realdata)
  next()
})

cachedexamplerouter.post('/how2', (req, res, next) => {
  let realdata = req.body
  res.locals.data = realdata
  res.send(realdata)
  next()
})

// UPDATE CACHE MIDDLEWARE
cachedexamplerouter.use((req, res, next) => {
  counters.decrementroutecount(req.originalUrl)
  routecacher.update('${req.body.id}:${req.body.token}', req.originalUrl, res.locals.data, () => next())
})

module.exports = cachedexamplerouter
