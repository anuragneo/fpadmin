const express = require('express')
const diagnosticrouter = express.Router()
let counters = require('../sys/counters')

diagnosticrouter.get('/alive', (req, res) => {
  res.send('Alive')
})

diagnosticrouter.get('/counters', (req, res) => {
  if (process.env.ENABLE_COUNTERS) res.send(JSON.stringify(counters.getall()))
  else res.send('Counters not enabled')
})

module.exports = diagnosticrouter
