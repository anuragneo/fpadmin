const express = require('express')
const administrationRoutes = express.Router()
const UserHelper = require('../helpers/users')
const ChannelHelper = require('../helpers/channel')
const comms = require('../sys/comms')

administrationRoutes.post('/wallet', (req, res) => {
  comms.request('prepaid/getwalletlist', req.body, { getMap: function () { } }, (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

administrationRoutes.post('/wallet/create', (req, res) => {
  comms.request('prepaid/create', req.body, { getMap: function () { } }, (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

administrationRoutes.post('/wallet/update', (req, res) => {
  comms.request('prepaid/update', req.body, { getMap: function () { } }, (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

administrationRoutes.post('/wallet/search', (req, res) => {
  comms.request('prepaid/search', req.body, { getMap: function () { } }, (err, resp) => {
    if (err) res.send({ status: 'failed', message: err.details })
    else res.send({ status: 'success', data: resp })
  })
})

administrationRoutes.post('/users', (req, res) => {
  UserHelper.get(resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/users/create', (req, res) => {
  UserHelper.create(req.body, resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/users/update', (req, res) => {
  UserHelper.update(req.body, resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/users/delete', (req, res) => {
  UserHelper.delete(req.body, resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/users/search', (req, res) => {
  UserHelper.search(req.body, resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/channel/create', (req, res) => {
  ChannelHelper.create(req.body, resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/channel', (req, res) => {
  ChannelHelper.get(resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/channel/update', (req, res) => {
  ChannelHelper.update(req.body, resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/channel/delete', (req, res) => {
  ChannelHelper.delete(req.body, resp => {
    res.send(resp)
  })
})

administrationRoutes.post('/channel/search', (req, res) => {
  ChannelHelper.search(req.body, resp => {
    res.send(resp)
  })
})

module.exports = administrationRoutes
