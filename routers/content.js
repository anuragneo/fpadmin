const express = require('express')
const router = express.Router()
const comms = require('../sys/comms')
const log = require('../sys/log')
const helper = require('../helpers/content')
const multer = require('multer')
const uploads = multer({ dest: '/tmp/bulkupload/' })
const grpc = require('../sys/grpc')

const fields = [
  { name: 'file', maxCount: 1 },
]

router.post('/createbanner', uploads.fields(fields), (req, res) => {
  const files = req.files
  const incoming = decodeURIComponent(req.body.data)

  helper.createbanner(incoming, files, response => {
    res.send(response)
  })
})

router.post('/getbanners', (req, res) => {
  helper.getbanners(req.body, response => {
    res.send(response)
  })
})

router.post('/updatebanner', uploads.fields(fields), (req, res) => {
  const files = req.files
  incoming = decodeURIComponent(req.body.data)

  helper.updatebanner(incoming, files, response => {
    res.send(response)
  })
})

router.post('/deletebanner', (req, res) => {
  helper.deletebanner(req.body, response => {
    res.send(response)
  })
})

router.post('/updatebannerposition', (req, res) => {
  helper.updatebannerposition(req.body, response => {
    res.send(response)
  })
})

router.post('/searchbanners', (req, res) => {
  helper.getbanners(req.body, response => {
    res.send(response)
  })
})

router.post('/getintroscreens', (req, res) => {
  helper.getintroscreens(req.body, response => {
    res.send(response)
  })
})

router.post('/createintroscreen', uploads.single('file'), (req, res) => {
  const file = req.file
  const _incoming = decodeURIComponent(req.body.data)

  helper.createintroscreens(_incoming, file, response => {
    res.send(response)
  })
})

router.post('/updateintroscreen', uploads.single('file'), (req, res) => {
  const file = req.file
  incoming = decodeURIComponent(req.body.data)

  helper.updateintroscreen(incoming, file, response => {
    res.send(response)
  })
})

router.post('/deleteintroscreen', (req, res) => {
  helper.deleteintroscreen(req.body, response => {
    res.send(response)
  })
})

router.post('/invalidatecache', (req, res) => {
  helper.invalidatecache(req.body, response => {
    res.send(response)
  })
})

module.exports = router
