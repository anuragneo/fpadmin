const express = require('express')
const router = express.Router()
const comms = require('../sys/comms')
const log = require('../sys/log')
const grpc = require('../sys/grpc')
const helper = require('../helpers/customer')

router.post('/getprepaidbalance', (req, res) => {
  helper.getprepaidbalance(req.body, response => {
    res.send(response)
  })
})

router.post('/getpaybackbalance', (req, res) => {
  helper.getpaybackbalance(req.body, response => {
    res.send(response)
  })
})

router.post('/getbbpcbalance', (req, res) => {
  helper.getbbpcbalance(req.body, response => {
    res.send(response)
  })
})

router.post('/getbbpctransaction', (req, res) => {
  console.log(req.body)
  helper.getbbpctransactions(req.body, response => {
    res.send(response)
  })
})

router.post('/getprofile', (req, res) => {
  comms.request('customers/get', req.body, grpc.getMetaData(), (err, response) => {
    if (err) {
      res.send({ status: 'failed', data: { message: err.details } })
    } else {
      res.send({ status: 'success', data: response.customer })
    }
  })
})

router.post('/transactions', (req, res) => {
  comms.request('prepaid/gethistoryforadmin', req.body, grpc.getMetaData(), (err, response) => {
    console.log(response)
    if (err) {
      res.send({ status: 'failed', data: { message: err.details } })
    } else {
      res.send({ status: 'success', data: response.history })
    }
  })
})

router.post('/loyalty', (req, res) => {
  res.send({
    status: 'success',
    data: {
      loyalty: [
        {
          name: 'BBPC',
          cardno: '1234567890'
        },
        {
          name: 'Payback',
          cardno: '1234567890'
        }
      ],
      transactions: [
        {
          id: '123',
          store: 'R-city',
          date: new Date(),
          type: 'credit',
          subtype: 'cashback',
          amount: '100',
          status: 'Y'
        },
        {
          id: '124',
          store: 'R-city',
          date: new Date(),
          type: 'credit',
          subtype: 'cashback',
          amount: '100',
          status: 'Y'
        },
        {
          id: '125',
          store: 'R-city',
          date: new Date(),
          type: 'credit',
          subtype: 'cashback',
          amount: '100',
          status: 'Y'
        }
      ]
    }
  })
})

router.post('/pricematch', (req, res) => {
  comms.request('pricematch/history', req.body, grpc.getMetaData(), (err, response) => {
    if (err) {
      res.send({ status: 'failed', data: { message: err.details } })
    } else {
      res.send({ status: 'success', data: response.customer })
    }
  })
})

router.post('/feedbacks', (req, res) => {
  comms.request('customers/getfeedbackhistory', req.body, grpc.getMetaData(), (err, response) => {
    if (err) {
      res.send({ status: 'failed', data: { message: err.details } })
    } else {
      res.send({ status: 'success', data: response.customer })
    }
  })
})

router.post('/communications', (req, res) => {
  res.send({
    status: 'success',
    data: [
      {
        id: 1,
        time: new Date(),
        channel: 'sms',
        message: 'Hi there!',
        campaign: '12345',
        campaignName: 'ABC',
        active: 'Y'
      }
    ]
  })
})

module.exports = router
