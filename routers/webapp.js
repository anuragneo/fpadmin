const express = require('express')
const router = express.Router()
const comms = require('../sys/comms')
const path = require('path')
const grpc = require('../sys/grpc')
const LoginHelper = require('../helpers/login')

router.get('/', (req, res) => {
  if (process.env.NODE_ENV === 'development') {
    req.session && req.session.isloggedin ? res.redirect('http://localhost:3000/') : res.redirect('/login')
  } else {
    req.session && req.session.isloggedin
      ? res.sendFile(path.resolve(__dirname, '../client/build/index.html'))
      : res.redirect(`${req.headers['x-envoy-original-path']}login`)
  }
})

router.get('/login', (req, res) => {
  if (req.session) {
    req.session.destroy()
  }
  res.sendFile(path.resolve(__dirname, '../views/login.html'))
})

router.get('/logout', (req, res) => {
  if (req.session) {
    req.session.destroy()
  }
  if (process.env.NODE_ENV === 'development') res.redirect('http://localhost:4000/')
  else res.redirect(req.headers['x-envoy-original-path'].replace('logout', ''))
})

router.post('/login', (req, res) => {
  LoginHelper.login(req.body, (err, admin) => {
    if (err) res.send({ status: 'failed', message: err })
    else {
      req.session.isloggedin = true
      req.session.admin = admin
      res.send({ status: 'success', data: admin })
    }
  })
})

router.get('/verify/:hash', (req, res) => {
  let params = req.params

  comms.request('customers/verifyemail', req.params, grpc.getMetaData(), (err, response) => {
    if (err) {
      res.send({ status: 'failed', message: err.details })
    } else {
      res.send({ status: 'success', data: response })
    }
  })
})

module.exports = router
