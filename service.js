/**
 * Service entry point
 */
'use strict'
const express = require('express')
const http = require('http')
const pjson = require('./package.json')
const session = require('express-session')
const cors = require('cors')
const app = express()
const bodyParser = require('body-parser')
const gracefulexit = require('./sys/gracefulexit')
require('colors')

let RedisStore = require('connect-redis')(session)

// Load env from file for dev. Set NODE_ENV in your bashrc or zshrc.
if (process.env.NODE_ENV === 'development') {
  require('env2')('./devenv.json')
}

let { sysnats, rxnats, grpcnorthsouthserver, grpceastwestserver, grpcclient } = require('./sys/servicewrapper')()

app.use(bodyParser.json())
app.use(bodyParser.raw())
app.use(cors())
app.enable('trust proxy')

app.use(
  session({
    secret: 'asdodweodnqjndoskxmlaskxmklsmx',
    store: new RedisStore({ client: require('./connectors/sysredis').getClient() }),
    resave: true,
    saveUninitialized: true
  })
)

const authenticate = (req, res, next) => {
  if (req.session.isloggedin) {
    next()
  } else {
    res.send({ status: 'failed', message: `Authentication failed. Please Log In` })
  }
}

const webapprouter = require('./routers/webapp')
const diagnosticsrouter = require('./routers/servicediagnostics')
const customarecarerouter = require('./routers/customarecare')
const adminrouter = require('./routers/administration')
const accountrouter = require('./routers/account')
const contentrouter = require('./routers/content')
const communicationrouter = require('./routers/communication')
const reportrouter = require('./routers/report')

app.use('/', webapprouter)
app.use('/diagnostics/', diagnosticsrouter)
app.use('/api/customarecare', customarecarerouter)
app.use('/api/admin', authenticate, adminrouter)
app.use('/api/content', authenticate, contentrouter)
app.use('/api/account', authenticate, accountrouter)
app.use('/api/comms', authenticate, communicationrouter)
app.use('/api/report', reportrouter)

// Serve static file
if (process.env.NODE_ENV !== 'development') app.use(express.static(__dirname + '/client/build'))

//serve login view
app.use(express.static(__dirname + '/views'))

const port = process.env.SERVICE_PORT || 3000
const server = http.createServer(app)
server.listen(port)
gracefulexit(sysnats, rxnats, server, grpcnorthsouthserver, grpceastwestserver, grpcclient)
