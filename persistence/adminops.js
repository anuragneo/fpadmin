const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Admin {
  createbulkadminops(incoming, callback) {
    let query = `
      INSERT INTO bulkadminops (filename, originalname, recordscount, makerid, checkerid, ip, comments, status)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?) 
    `
    let params = [
      incoming.filename,
      incoming.originalname,
      incoming.recordscount,
      incoming.makerid,
      incoming.checkerid,
      incoming.ip,
      incoming.comments,
      incoming.status
    ]

    mysql.query(query, params, (err, res) => {
      callback(err)
    })
  }

  createadminops(incoming, callback) {
    let query = `
      INSERT INTO adminops (amount, walletid, makerid, checkerid, comments, status)
      VALUES (?, ?, ?, ?, ?, ?) 
    `
    let params = [
      incoming.amount,
      incoming.walletid,
      incoming.makerid,
      incoming.checkerid,
      incoming.comments,
      incoming.status
    ]

    mysql.query(query, params, (err, res) => {
      callback(err)
    })
  }

  updateadminops(id, checkerid, status, comments, callback) {
    let query = `
      UPDATE adminops SET checkerid = ?, status = ?, comments = ?, updated_at = NOW()
      WHERE id = ?
    `
    let params = [checkerid, status, comments, id]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  getbulkops(incoming, callback) {
    let query = `
      SELECT b.id, b.filename, b.originalname, b.recordscount, b.created_at as date, b.ip, b.comments, b.status, m.firstname as name
      FROM bulkadminops b  
      LEFT JOIN managers m ON b.makerid = m.id
      WHERE b.status = 'pending'
    `
    let params = [incoming.managerid]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  getbulkopsformaker(incoming, callback) {
    let query = `
      SELECT b.id, b.filename, b.originalname, b.recordscount, b.created_at as date, b.ip, b.comments, b.status, m.firstname as name
      FROM bulkadminops b  
      LEFT JOIN managers m ON b.makerid = m.id
      WHERE b.makerid = ?
    `
    let params = [incoming.managerid]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  getadminops(incoming, callback) {
    let query = `
      SELECT a.id, a.amount, a.walletid, a.created_at as date, a.comments, a.status, m.firstname as name
      FROM adminops a  
      LEFT JOIN managers m ON a.makerid = m.id
      WHERE a.status = 'pending'
    `
    let params = [incoming.managerid]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  getadminopsformaker(incoming, callback) {
    let query = `
      SELECT a.id, a.amount, a.walletid, a.created_at as date, a.comments, a.status, m.firstname as name
      FROM adminops a  
      LEFT JOIN managers m ON a.makerid = m.id
      WHERE a.makerid = ?
    `
    let params = [incoming.managerid]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  getbulkadminopsbyid(id, callback) {
    let query = `
      SELECT b.id, b.filename, b.recordscount, b.created_at as date, b.ip, b.comments, b.status, m.firstname as name
      FROM bulkadminops b  
      LEFT JOIN managers m ON b.makerid = m.id
      WHERE b.id = ?
    `
    let params = [id]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  getadminopsbyid(id, callback) {
    let query = `
      SELECT * from adminops WHERE id = ?
    `
    let params = [id]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  updatebulkadminops(id, checkerid, status, comments, callback) {
    let query = `
      UPDATE bulkadminops SET checkerid = ?, status = ?, comments = ?, updated_at = NOW()
      WHERE id = ?
    `
    let params = [checkerid, status, comments, id]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }
}

module.exports = new Admin()
