const moment = require('moment')
const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Report {
  gettotaluser(incomingenddate, callback) {
    let enddate = moment(incomingenddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS totalcount FROM customers WHERE isactive = 'Y' AND DATE(created_at) <= '${enddate}';`
    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  getactiveinactiveuser(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(DISTINCT(A.mobile)) AS totalactive
    FROM (SELECT DISTINCT(mobile) FROM payments UNION SELECT DISTINCT(mobile) FROM recharges) A JOIN customers c
    ON A.mobile = c.mobile
    WHERE DATE(c.created_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  totalnewregistered(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS customercount 
    FROM customers WHERE isactive = 'Y' AND DATE(created_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  registrationthroughsource(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT f.regsource, COUNT(1) AS customercount 
    FROM customers c JOIN fpaccounts f ON f.id = c.fpaccountid 
    WHERE c.isactive = 'Y' 
    AND DATE(c.created_at) BETWEEN '${startdate}' AND '${enddate}' GROUP BY f.regsource;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  getcustomerbydaterange(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS customer,DATE_FORMAT(created_at, "%d-%c-%Y") AS RegistrationDate,
    DATE_FORMAT(created_at, '%Y/%m/%d') AS sortdate from customers
    WHERE DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY sortdate,RegistrationDate order by sortdate;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  getcustomerreport(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT mobile, firstname, lastname, email, deviceid, osversion, status, dob,
    FLOOR(DATEDIFF(CURRENT_DATE, STR_TO_DATE(dob, '%d/%m/%Y'))/365.25) AS age,
    DATE_FORMAT(created_at, '%d/%m/%Y') AS txndate,
    DATE_FORMAT(created_at, '%h.%m') AS txntime,
    pincode, '' AS city, '' AS state
    FROM customers
    WHERE DATE(created_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  totalspent(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS spentcount, SUM(amount) AS spentvolume
    FROM payments
    WHERE state IN ('completed', 'debited') AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  totaltopup(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS topupcount, SUM(amount) AS topupvolume
    FROM recharges
    WHERE state = 'completed' AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  totalcashback(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS cbcount, SUM(amount) AS cbvolume
    FROM recharges
    WHERE campaignname IS NOT NULL AND
    state = 'completed' AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  debitvolume(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT DATE_FORMAT(created_at, '%d/%m/%Y') AS spentdate, DATE_FORMAT(created_at, '%Y/%m/%d') AS sortdate, 
    SUM(amount) AS spentvolume
    FROM payments
    WHERE state = 'completed' AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY sortdate,spentdate order by sortdate;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  countgroupdebit(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT channeltype, COUNT(1) AS spentcount, SUM(amount) AS spentvolume
    FROM payments
    WHERE state = 'completed' AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY channeltype;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  creditvolume(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT DATE_FORMAT(created_at, '%d/%m/%Y') AS topupdate,  DATE_FORMAT(created_at, '%Y/%m/%d') AS sortdate,
    SUM(amount) AS topupvolume
    FROM recharges
    WHERE state = 'completed' AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY sortdate,topupdate order by sortdate;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  countgroupcredit(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT channeltype, COUNT(1) AS topupcount, SUM(amount) AS topupvolume
    FROM recharges
    WHERE state = 'completed' AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY channeltype;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  cashbackanalysis(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT DATE_FORMAT(created_at, '%d/%m/%Y') AS cbdate, DATE_FORMAT(created_at, '%Y/%m/%d') AS sortdate,
    SUM(amount) AS cbvolume
    FROM recharges
    WHERE state = 'completed' AND
    campaignname IS NOT NULL AND
    campaignname != '' AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY sortdate,cbdate order by sortdate;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  cashbackexperiable(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT a.expirable, b.nonexpirable
    FROM 
    (SELECT COALESCE(SUM(amount),0) AS expirable FROM recharges WHERE campaignname IS NOT NULL AND 
    expirydate IS NOT NULL AND walletid = '${
      incoming.walletid
    }' AND DATE(created_at) BETWEEN '${startdate}' AND '${enddate}') a,
    (SELECT COALESCE(SUM(amount), 0) AS nonexpirable FROM recharges WHERE campaignname IS NOT NULL AND 
    expirydate IS NULL AND walletid = '${
      incoming.walletid
    }' AND DATE(created_at) BETWEEN '${startdate}' AND '${enddate}') b;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  groupbycampaignname(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT campaignname AS cbname, SUM(amount) AS cbvolume
    FROM recharges
    WHERE state = 'completed' AND
    campaignname IS NOT NULL AND
    campaignname != '' AND
    walletid = '${incoming.walletid}' AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY cbname;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  totalactiveuserwallet(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(DISTINCT(A.mobile)) AS totalactive FROM 
    (SELECT DISTINCT(mobile) FROM payments WHERE walletid = '${incoming.walletid}'
    UNION
    SELECT DISTINCT(mobile) FROM recharges WHERE walletid = '${incoming.walletid}') A JOIN customers c
    ON A.mobile = c.mobile
    WHERE DATE(c.created_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  newbbpcpaybackregistered(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS registration_count
    FROM ${incoming.type}
    WHERE islinked = 'Y' AND DATE(created_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  totaldelinkingbbpcpayback(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS registration_count
    FROM ${incoming.type}
    WHERE islinked = 'N' AND DATE(updated_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  totallinkedbbpcpayback(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS linked_count 
    FROM ${incoming.type} WHERE islinked = 'Y' AND DATE(updated_at) < '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  totaldeinkedbbpcpayback(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS delinked_count 
    FROM ${incoming.type} WHERE islinked = 'N' AND DATE(updated_at) < '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  bbpcreport(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT membershipid, c.fpaccountid, c.mobile, 'BBPC' AS card_type, cardnumber, 
    CONCAT(c.firstname, ' ', c.lastname) AS customer_name
    FROM bbpc b JOIN customers c ON c.fpaccountid = b.fpaccountid
    WHERE islinked = 'Y' AND DATE(b.created_at) < '${enddate}';
    `

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  paybackreport(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT c.fpaccountid, c.mobile, 'Payback' AS card_type, cardno AS cardnumber, 
    CONCAT(c.firstname, ' ', c.lastname) AS customer_name
    FROM payback p JOIN customers c ON c.fpaccountid = p.fpaccountid
    WHERE islinked = 'Y' AND DATE(p.created_at) < '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  bbpctransactionreport(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT mobile,walletid, 'BBPC' AS wallet_owner, storecode, txnid, txnrefno, 
    IF (state = 'refunded', 'refund', 'debit') AS txntype, subtype, source, amount, DATE_FORMAT(updated_at, '%d/%m/%Y') 
    AS txndate, DATE_FORMAT(updated_at, '%k.%i') AS txntime, state AS txnstatus,  billnumber AS cashmemono, 
    tillnumber AS terminalid, balance, timestampno FROM payments where walletid = '96'
    AND DATE(updated_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  paybacktransactionreport(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT mobile,walletid, 'Payback' AS wallet_owner, storecode, txnid, txnrefno, 
    IF (state = 'refunded', 'refund', 'debit') AS txntype, subtype, source, amount, DATE_FORMAT(updated_at, '%d/%m/%Y') 
    AS txndate, DATE_FORMAT(updated_at, '%k.%i') AS txntime, state AS txnstatus,  billnumber AS cashmemono, 
    tillnumber AS terminalid, balance, timestampno FROM payments where walletid = '97'
    AND DATE(updated_at) BETWEEN '${startdate}' AND '${enddate}';`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  successratiodebit(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS txncount, state, channeltype
    FROM payments
    WHERE walletid = '${incoming.walletid}' AND
    channeltype IS NOT NULL AND channeltype != '' AND
    state IN ('completed', 'failed') AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY channeltype, state;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  successratiocredit(incoming, callback) {
    let startdate = moment(incoming.startdate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let enddate = moment(incoming.enddate, `DD/MM/YYYY`, true).format(`YYYY-MM-DD`)
    let query = `SELECT COUNT(1) AS txncount, state, channeltype
    FROM recharges
    WHERE walletid = '${incoming.walletid}' AND
    channeltype IS NOT NULL AND channeltype != '' AND
    state IN ('completed', 'failed') AND
    DATE(created_at) BETWEEN '${startdate}' AND '${enddate}'
    GROUP BY channeltype, state;`

    mysql.query(query, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }
}

module.exports = new Report()
