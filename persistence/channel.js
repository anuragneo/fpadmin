const moment = require('moment')
const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Channel {
  create(data, callback) {
    let createquery = `INSERT INTO channels(name, source, description, businessformat) VALUES (?, ?, ?, ?)`,
      params = [data.name, data.source, data.description, data.businessformat]
    mysql.query(createquery, params, (err, res) => {
      if (err) log(`Error while creating customer entry for mobile ${data.mobile}`, true)
      else data.id = res.insertId
      callback(err)
    })
  }

  getbyname(name, callback) {
    let query = 'SELECT * from channels WHERE name = ?'
    let params = [name]
    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  get(callback) {
    let getquery = `SELECT id, name, source, description, businessformat,
    created_at, updated_at
    FROM channels 
    WHERE  isactive = 'Y' AND deleted = 'N'`
    mysql.query(getquery, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  search(data, callback) {
    let getquery = `SELECT id, name, source, description, businessformat,
    created_at, updated_at
    FROM channels 
    WHERE  name like '%${data.name}%' AND isactive = 'Y' AND deleted = 'N'`
    mysql.query(getquery, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  delete(data, callback) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss'),
      deletequery = `UPDATE channels SET deleted = 'Y', isactive = 'N', comments = '${data.comments}', updated_at = '${now}' 
      WHERE id = ? AND isactive = 'Y' AND deleted = 'N'`
    mysql.query(deletequery, [data.id], err => {
      if (err) log(`Deleting channel id ${data.id} failed. Error: ${err}`, true)
      callback(err)
    })
  }

  update(data, callback) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss'),
      updatequery = `UPDATE channels SET name = ?, source = ?, description = ?, 
      businessformat = ?, updated_at = '${now}' 
      WHERE id = ?`,
      params = [data.name, data.source, data.description, data.businessformat, data.id]
    mysql.query(updatequery, params, err => {
      callback(err)
    })
  }
}

module.exports = new Channel()
