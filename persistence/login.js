const async = require('async')
const bcrypt = require('bcrypt')
const moment = require('moment')
const sysredis = require('../connectors/sysredis')
const mysql = require('../connectors/mysql')
const log = require('../sys/log')
const mapname = require('../package.json').name

class Login {
  getbyloginid(loginid, callback) {
    let mobquery = `
        SELECT id, loginid, password, firstname, lastname, email, mobile, adminaccess, 
    accountaccess, customercare, offersaccess, contentaccess, commsaccess, reporting, isactive 
    FROM managers
    WHERE loginid = '${loginid}'
    AND isactive = 'Y' AND deleted = 'N';`
    mysql.query(mobquery, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res[0]
      }
      callback(err, response)
    })
  }

  lockaccount(manager, callback) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss'),
      lockquery = `
    UPDATE managers
    SET status = ?, updated_at = '${now}'
    WHERE id = ?;`
    mysql.query(lockquery, [process.env.ACCOUNT_LOCKED, manager.id], err => {
      if (err) log(`Locking manager id ${manager.id} failed. Error: ${err}`, true)
      else log(`manager ${manager.id} locked due to max invalid login attempts.`)
      callback(err)
    })
  }

  verifylogin(manager, password, callback) {
    let isvalid = false,
      attempts = 0,
      loginsuccess = false,
      that = this

    function getloginattempts(_callback) {
      let redis = sysredis.getReadClient()
      redis.hget(mapname, `${process.env.REDIS_LOGIN_ATTEMPTS}:${manager.id}`, (err, _attempts) => {
        attempts = Number(_attempts || 0) + 1
        _callback(err)
      })
    }

    function verifypassword(_callback) {
      isvalid = bcrypt.compareSync(password, manager.password)
      if (!isvalid && attempts >= process.env.MAX_LOGIN_ATTEMPTS)
        that.lockaccount(manager, err => {
          _callback(err)
        })
      else {
        _callback(null)
      }
    }

    function updatecounter(_callback) {
      let redis = sysredis.getClient()
      if (isvalid) {
        loginsuccess = true
        redis.hdel(mapname, `${process.env.REDIS_LOGIN_ATTEMPTS}:${manager.id}`, err => {
          _callback(err)
        })
      } else {
        redis.hset(mapname, `${process.env.REDIS_LOGIN_ATTEMPTS}:${manager.id}`, `${attempts}`, err => {
          _callback(err)
        })
      }
    }

    async.waterfall([getloginattempts, verifypassword, updatecounter], err => {
      callback(err, loginsuccess, Number(process.env.MAX_LOGIN_ATTEMPTS) - Number(attempts))
    })
  }
}

module.exports = new Login()
