module.exports = {
  getotp(mobile, device, callback) {
    callback(null, { mobile, device, message: 'OTP sent' })
  },
  isRegistered(mobile, callback) {
    const isRegistered = false
    callback(null, mobile, isRegistered)
  }
}
