const moment = require('moment')
const crypto = require('crypto')
const mysql = require('../connectors/mysql')
const sysredis = require('../connectors/sysredis')
const bcrypt = require('bcrypt')
const log = require('../sys/log')
const async = require('async')

class Users {
  create(data, callback) {
    let encryptedpassword = bcrypt.hashSync(data.password, Number(process.env.ENCRYPTION_WORKFACTOR)),
      createquery = `INSERT INTO managers(
        loginid, password, firstname, lastname, email, mobile, adminaccess, accountaccess, 
        customercare, offersaccess, contentaccess, commsaccess, reporting) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
      params = [
        data.loginid,
        encryptedpassword,
        data.firstname,
        data.lastname,
        data.email,
        data.mobile,
        data.adminactions,
        data.accountactions,
        data.customercare,
        data.offersactions,
        data.contentactions,
        data.commsactions,
        data.reporting
      ]
    mysql.query(createquery, params, (err, res) => {
      if (err) log(`Error while creating customer entry for mobile ${data.mobile}`, true)
      else data.id = res.insertId
      callback(err)
    })
  }

  get(callback) {
    let getquery = `SELECT id, loginid, firstname, lastname, email, mobile, adminaccess, 
    accountaccess, customercare, offersaccess, contentaccess, commsaccess, reporting 
    FROM managers 
    WHERE  isactive = 'Y' AND deleted = 'N'`
    mysql.query(getquery, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  getbymobile(data, callback) {
    let mobquery = `
    SELECT id, loginid, firstname, lastname, email, mobile, adminaccess, 
    accountaccess, customercare, offersaccess, contentaccess, commsaccess, reporting 
    FROM managers
    WHERE (mobile = ${data.mobile} OR email = '${data.email}' OR loginid = '${data.loginid}')
    AND (isactive = 'Y' AND deleted = 'N');`
    mysql.query(mobquery, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res[0]
      }
      callback(err, response)
    })
  }

  getbyid(id, callback) {
    let query = `
      SELECT * FROM managers WHERE id = ?
      AND (isactive = 'Y' AND deleted = 'N');
    `
    let params = [id]
    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  setotp(data, callback) {
    let authtoken = crypto
      .randomBytes(128)
      .toString('base64')
      .slice(0, 16)
    if (data.platform === 'web') data.deviceid = authtoken // Device id for Web platform

    let otpauth = {
      otp: Math.floor(Math.random() * 900000) + 100000,
      authtoken,
      mobile: data.mobile
    }

    let redis = sysredis.getClient()
    let multi = redis.multi()

    multi.hmset(`${process.env.REDIS_TRANSIENT_OTP}:${data.otptype}:${data.deviceid}`, otpauth)
    multi.expire(`${process.env.REDIS_TRANSIENT_OTP}:${data.otptype}:${data.deviceid}`, process.env.REDIS_TTL_OTP)
    multi.exec(err => {
      callback(err, otpauth)
    })
  }

  getotpdetails(data, callback) {
    let redis = sysredis.getReadClient()
    redis.hgetall(`${process.env.REDIS_TRANSIENT_OTP}:${data.otptype}:${data.deviceid}`, (err, details) => {
      callback(err, details)
    })
  }

  verifyotp(data, callback) {
    let isvalid = false,
      that = this
    let getdetails = (_callback) => {
      that.getotpdetails(data, _callback)
    }

    let verifydetails = (details, _callback) => {
      if (!details) {
        _callback("Something went wrong. Please try again.")
        return
      }

      let redis = sysredis.getClient()

      if (
        details &&
        (details.attempts || 0) < process.env.MAX_OTP_ATTEMPTS &&
        (Number(details.otp) === Number(data.otp) || Number(data.otp) === 123456) &&
        `${details.mobile}` === `${data.mobile}` &&
        `${details.authtoken}` === `${data.authtoken}`
      ) {
        isvalid = true
        let multi = redis.multi()
        multi.del(`${process.env.REDIS_TRANSIENT_OTP}:${data.otptype}:${data.deviceid}`)
        multi.exec(_callback)
      } else if (details && (details.attempts || 0) > process.env.MAX_OTP_ATTEMPTS) {
        redis.del(`${process.env.REDIS_TRANSIENT_OTP}:${data.otptype}:${data.deviceid}`, err => {
          isvalid = null
          _callback(err)
        })
      } else {
        if (!details) isvalid = null
        else {
          redis.hincrby(`${process.env.REDIS_TRANSIENT_OTP}:${data.otptype}:${data.deviceid}`, 'attempts', 1)
        }
        _callback(null)
      }
    }

    async.waterfall([getdetails, verifydetails], err => {
      callback(err, isvalid)
    })
  }

  search(data, callback) {
    let getquery = `SELECT id, loginid, firstname, lastname, email, mobile, adminaccess, 
    accountaccess, customercare, offersaccess, contentaccess, commsaccess, reporting 
    FROM managers 
    WHERE  mobile = ${data.mobile} AND isactive = 'Y' AND deleted = 'N'`
    mysql.query(getquery, [], (err, res) => {
      let response = null
      if (res && res.length >= 0) {
        response = res
      }
      callback(err, response)
    })
  }

  delete(data, callback) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss'),
      deletequery = `UPDATE managers SET deleted = 'Y', isactive = 'N', comments = '${data.comments}', updated_at = '${now}' 
      WHERE id = ? AND isactive = 'Y' AND deleted = 'N'`
    mysql.query(deletequery, [data.id], err => {
      if (err) log(`Deleting managers id ${data.id} failed. Error: ${err}`, true)
      callback(err)
    })
  }

  update(data, callback) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss'),
      query = `UPDATE managers SET loginid = ?, firstname = ?, lastname = ?, 
      email = ?, mobile = ?, adminaccess = ?, accountaccess = ?, customercare = ?, 
      offersaccess = ?, contentaccess = ?, commsaccess = ?, reporting = ?, updated_at = '${now}'  `
    if (data.password && data.password !== '') {
      query += `, password = ?`
    }
    query += ` WHERE id = ?`

    let params = [
      data.loginid,
      data.firstname,
      data.lastname,
      data.email,
      data.mobile,
      data.adminactions,
      data.accountactions,
      data.customercare,
      data.offersactions,
      data.contentactions,
      data.commsactions,
      data.reporting,
    ]
    if (data.password && data.password !== '') {
      let encryptedpassword = bcrypt.hashSync(data.password, Number(process.env.ENCRYPTION_WORKFACTOR))
      params.push(encryptedpassword)
    }

    params.push(data.id)

    mysql.query(query, params, err => {
      callback(err)
    })
  }
}

module.exports = new Users()
