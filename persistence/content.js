const mysql = require('../connectors/mysql')
const log = require('../sys/log')
const sysredis = require('../connectors/sysredis')
const async = require('async')
const _ = require('lodash')

class Content {
  updateintroscreen(incoming, file, callback) {
    let isactive = incoming.isactive ? incoming.isactive : 'Y'
    let query = `
      UPDATE introscreens set name = ?, seqnumber = ?, sdesc = ?, ldesc = ?, isactive = ?
    `
    if (file && file.originalname) query += ", imagelink = ?"

    query += " WHERE id = ?"

    let params = [incoming.name, incoming.seqnumber, incoming.sdesc, incoming.ldesc, isactive]

    if (file && file.originalname) params.push(file.originalname)

    params.push(incoming.id)

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  deleteintroscreen(id, callback) {
    let query = "UPDATE introscreens SET isactive = 'N', deleted = 'Y' WHERE id = ?"
    mysql.query(query, [id], (err, res) => {
      callback(err, res)
    })
  }

  createintroscreen(incoming, file, callback) {
    let isactive = incoming.isactive ? incoming.isactive : 'Y'
    let query = `
      INSERT INTO introscreens (name, seqnumber, sdesc, ldesc, imagelink, isactive)
      VALUES (?, ?, ?, ?, ?, ?) 
    `
    let params = [
      incoming.name,
      incoming.seqnumber,
      incoming.sdesc,
      incoming.ldesc,
      file.originalname,
      isactive
    ]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  getintroscreens(callback) {
    let query = "SELECT * from introscreens WHERE deleted = 'N'"
    mysql.query(query, [], (err, res) => {
      callback(err, res)
    })
  }


  deletebanner(bannerid, callback) {
    let query = "UPDATE banners SET isactive = 'N', deleted = 'Y' WHERE id = ?"
    mysql.query(query, [bannerid], (err, res) => {
      callback(err, res)
    })
  }

  getbanners(incoming, callback) {
    let query = "SELECT * from banners WHERE deleted = 'N'"
    if (incoming.name) query += `AND name LIKE '%${incoming.name}%'`
    mysql.query(query, [], (err, res) => {
      callback(err, res)
    })
  }

  getbannerbyid(bannerid, callback) {
    let query = "SELECT * from banners WHERE deleted = 'N' AND id = ?"
    mysql.query(query, [bannerid], (err, res) => {
      callback(err, res)
    })
  }

  getintroscreenbyid(introscreenid, callback) {
    let query = "SELECT * from introscreens WHERE deleted = 'N' AND id = ?"
    mysql.query(query, [introscreenid], (err, res) => {
      callback(err, res)
    })
  }

  createbanner(incoming, files, callback) {
    let isactive = incoming.isactive ? incoming.isactive : 'Y'
    let campaignid = incoming.campaignid ? incoming.campaignid : null
    let tags = incoming.tags ? incoming.tags : null
    let homelinkobj = _.find(files, { fieldname: 'homelink' })
    let offerlinkobj = _.find(files, { fieldname: 'offerlink' })
    let htmllinkobj = _.find(files, { fieldname: 'htmllink' })

    let query = `
      INSERT INTO banners (name, seqnumber, formatid, campaignid, tags, homelink, offerlink, htmllink, isactive)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) 
    `
    let params = [
      incoming.name,
      incoming.seqnumber,
      incoming.formatid,
      campaignid,
      tags,
      homelinkobj.filename,
      offerlinkobj.filename,
      htmllinkobj.filename,
      isactive
    ]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  updatebannerposition(banner, callback) {
    let query = "UPDATE banners set seqnumber = ? WHERE id = ?"
    let params = [banner.seqnumber, banner.id]

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

  updatebanner(incoming, files, callback) {
    let isactive = incoming.isactive ? incoming.isactive : 'Y'
    let tags = incoming.tags ? incoming.tags : null
    let homelinkobj = files ? _.find(files, { fieldname: 'homelink' }) : null
    let offerlinkobj = files ? _.find(files, { fieldname: 'offerlink' }) : null
    let htmllinkobj = files ? _.find(files, { fieldname: 'htmllink' }) : null

    let query = `
      UPDATE banners set name = ?, formatid = ?, isactive = ?
    `
    if (incoming.campaignid) query += ", campaignid = ?"
    if (incoming.tags) query += ", tags = ?"
    if (homelinkobj) query += ", homelink = ?"
    if (offerlinkobj) query += ", offerlink = ?"
    if (htmllinkobj) query += ", htmllink = ?"



    query += " WHERE id = ?"

    let params = [incoming.name, incoming.formatid, isactive]

    if (incoming.campaignid) params.push(incoming.campaignid)
    if (incoming.tags) params.push(incoming.tags)
    if (homelinkobj) params.push(homelinkobj.filename)
    if (offerlinkobj) params.push(offerlinkobj.filename)
    if (htmllinkobj) params.push(htmllinkobj.filename)

    params.push(incoming.id)

    mysql.query(query, params, (err, res) => {
      callback(err, res)
    })
  }

}

module.exports = new Content()