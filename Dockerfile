FROM node:10.14.0-alpine
WORKDIR /service
COPY package*.json ./
# build deps needed for node-gyp
RUN apk add --no-cache --virtual .build-deps alpine-sdk python \
    && npm install --production --silent \
    && apk del .build-deps
COPY . .
EXPOSE 3000 3001 3002
USER node
CMD ["node","service.js"]
