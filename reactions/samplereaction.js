//This can also be you require variable
const request = require('request')
const metaservice = require('../persistence/metaservice')
const comms = require('../sys/comms')
const getData = function() {
  request.get('www.get.service?exampleparam=2', (err, response, data) => {
    JSON.stringify({ err, response, data })
  })
}

module.exports = input => {
  request.get('www.get.service?exampleparam=1', (err, response, data) => {
    JSON.stringify({ err, response, data })
  })
  getData()
  request.post('www.ff.aa/getotp', { expected: 'request' }, (err, response, data) => {
    JSON.stringify({ err, response, data })
  })
  request.post('www.ff.aa/register', { expected: 'request' }, (err, response, data) => {
    JSON.stringify({ err, response, data })
  })
  metaservice.getotp(6787456545, 'android', (err, data) => {
    JSON.stringify({ err, data })
  })
  comms.publish('some.event', input)
}
