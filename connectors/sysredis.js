const _redis = require('redis')
const log = require('../sys/log')
const opts = {
    host: process.env.SYSREDIS_HOST,
    port: process.env.SYSREDIS_PORT,
    password: process.env.SYSREDIS_PASSWORD
  },
  readopts = {
    host: process.env.SYSREDIS_READER_HOST,
    port: process.env.SYSREDIS_READER_PORT,
    password: process.env.SYSREDIS_READER_PASSWORD
  }

let redis = null,
  readredis = null
const getClient = function() {
  if (!redis || !redis.connected) {
    log('Renewing sysredis connection')
    redis = _redis.createClient(opts)
  }
  return redis
}

const getReadClient = function() {
  if (!readredis || !readredis.connected) {
    log('Renewing sysredis reader connection')
    readredis = _redis.createClient(readopts)
  }
  return readredis
}

module.exports = { getClient, getReadClient }
