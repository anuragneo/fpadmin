const _redis = require('redis')
const log = require('../sys/log')
const opts = {
    host: process.env.CACHEREDIS_HOST,
    port: process.env.CACHEREDIS_PORT,
    password: process.env.CACHEREDIS_PASSWORD
  },
  readopts = {
    host: process.env.CACHEREDIS_READER_HOST,
    port: process.env.CACHEREDIS_READER_PORT,
    password: process.env.CACHEREDIS_READER_PASSWORD
  }

let redis = null,
  readredis = null

const getClient = function() {
  if (!redis || !redis.connected) {
    log('Renewing cacheredis connection')
    redis = _redis.createClient(opts)
  }
  return redis
}

const getReadClient = function() {
  if (!readredis || !readredis.connected) {
    log('Renewing cacheredis reader connection')
    readredis = _redis.createClient(readopts)
  }
  return readredis
}

module.exports = { getClient, getReadClient }
