const log = require('../sys/log')
const opts = {
  host: process.env.STREAMREDIS_HOST,
  port: process.env.STREAMREDIS_PORT,
  password: process.env.STREAMREDIS_PASSWORD
}
const _redis = require('redis')
let redis = null
const getClient = function() {
  if (!redis || !redis.connected) {
    log('Renewing streamredis connection')
    _redis.add_command('xadd')
    redis = _redis.createClient(opts)
  }
  return redis
}
module.exports = { getClient }
