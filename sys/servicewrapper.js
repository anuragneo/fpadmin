/**
 * Wrapper for nats channels basis channels.js and reactions.js
 */
const async = require('async')
const NATS = require('nats')

// creating array of sysnats servers.
const sysnatsservers = {
  servers: process.env.SYSNATS_HOSTS
    ? process.env.SYSNATS_HOSTS.split(',').map(
        host => `nats://${process.env.SYSNATS_USER}:${process.env.SYSNATS_PASSWORD}@${host}:${process.env.SYSNATS_PORT}`
      )
    : [
        `nats://${process.env.SYSNATS_USER}:${process.env.SYSNATS_PASSWORD}@${process.env.SYSNATS_HOST}:${
          process.env.SYSNATS_PORT
        }`
      ]
}
// creating array of rxnats servers.
const rxnatsservers = {
  servers: process.env.RXNATS_HOSTS
    ? process.env.RXNATS_HOSTS.split(',').map(
        host => `nats://${process.env.RXNATS_USER}:${process.env.RXNATS_PASSWORD}@${host}:${process.env.RXNATS_PORT}`
      )
    : [
        `nats://${process.env.RXNATS_USER}:${process.env.RXNATS_PASSWORD}@${process.env.RXNATS_HOST}:${
          process.env.RXNATS_PORT
        }`
      ]
}
//connecting to sysnats if ENABLE_SYSNATS exists
const sysnats = process.env.ENABLE_SYSNATS
  ? NATS.connect({ ...sysnatsservers, preserveBuffers: true, encoding: 'binary' })
  : null
//connecting to rxnats
const rxnats = NATS.connect({
  ...rxnatsservers,
  preserveBuffers: true,
  encoding: 'binary'
})
const channels = require('../channels')
const reactions = require('../reactions')
const pjson = require('../package.json')
const _ = require('lodash')
const log = require('./log')
const protoroot = require('../protobuf/build')
// const microtime = require('microtime')
let counters = require('./counters')
const grpc = require('./grpc')
const { grpcnorthsouthserver, grpceastwestserver, grpcclient } = grpc

module.exports = function() {
  const synchronousnorthsouthchannels = []
  const synchronouseastwestchannels = []
  const _reactions = []
  const servicename = pjson.name
  let grpcnorthsouthobj = {}
  let grpceastwestobj = {}

  _.each(channels, function(options, functionname) {
    const channel = {
      fullname: `${servicename}/${functionname}`,
      name: `${functionname}`
    }
    if (options) {
      if ('fakekey' in options) {
        channel.fakekey = options.fakekey
        channel.prefixfunction = options.prefixfunction
        channel.invalidationevents = options.invalidationevents
      }
      if (options[process.env.NORTH_SOUTH || 'northsouth']) {
        synchronousnorthsouthchannels.push(channel)
      }
      if (options[process.env.EAST_WEST || 'eastwest']) {
        synchronouseastwestchannels.push(channel)
      }
    } else {
      log(`Please set options for channel ${functionname} in /channels.js file`.red.bold)
    }
  })

  //Updated for using binary protobuf requests and responses
  async.each(synchronousnorthsouthchannels, channel => {
    /*
    sysnats.subscribe(`${channel.fullname}`, { queue: `${channel.fullname}` }, function(request, replyto) {
      if (process.env.ENABLE_COUNTERS) counters.incrementchannelcount(channel.name)
      const protomsg = protoroot[servicename][channel.name]['request'].decode(request)
      const func = require(`../channels/${channel.name}`)
      func(protomsg, protomsg.ctx, function(err, response) {
        if (protomsg.ctx && protomsg.ctx.trail) {
          response.ctx = protomsg.ctx
          response.ctx.trail.push({ servicename: servicename, microtime: microtime.now(), isreq: false })
        } else {
          log(`WARNING : No existing NATS context received for channel ${channel.name}`, true)
        }
        let responseobj = protoroot[servicename][channel.name]['response'].create(response)
        let responsebinary = protoroot[servicename][channel.name]['response'].encode(responseobj).finish()
        sysnats.publish(replyto, responsebinary)
        if (process.env.ENABLE_COUNTERS) counters.decrementchannelcount(channel.name)
      })
    })
    */
    grpcnorthsouthobj[channel.name] = function(call, callback) {
      if (process.env.ENABLE_COUNTERS) counters.incrementchannelcount(channel.name)
      // const metadata = _.pick(call.metadata, ['x-request-id', 'x-b3-traceid', 'x-b3-spanid', 'x-b3-parentspanid', 'x-b3-sampled', 'x-b3-flags', 'x-ot-span-context'])
      let { metadata, request } = call
      if (!metadata || typeof metadata.set !== 'function') {
        metadata = grpc.getMetaData()
      }
      metadata.set(process.env.NORTH_SOUTH || 'northsouth', 'true')
      const func = require(`../channels/${channel.name}`)
      func(request, metadata, function(err, response) {
        callback(err, response)
        const currentcontext = metadata.getMap()
        if (process.env.NODE_ENV === 'staging' && _.has(currentcontext, 'x-breeze-id')) {
          const Recorder = require('./middleware/recorder')
          const recorder = new Recorder()
          recorder.save(
            recorder.capture(channel.fullname, { request, metadata }, err ? { err: err.message } : response)
          )
        }
        if (process.env.ENABLE_COUNTERS) counters.decrementchannelcount(channel.name)
      })
    }
  })
  async.each(synchronouseastwestchannels, channel => {
    grpceastwestobj[channel.name] = function(call, callback) {
      if (process.env.ENABLE_COUNTERS) counters.incrementchannelcount(channel.name)
      // const metadata = _.pick(call.metadata, ['x-request-id', 'x-b3-traceid', 'x-b3-spanid', 'x-b3-parentspanid', 'x-b3-sampled', 'x-b3-flags', 'x-ot-span-context'])
      let { metadata, request } = call
      if (!metadata || typeof metadata.set !== 'function') {
        metadata = grpc.getMetaData()
      }
      metadata.set(process.env.EAST_WEST || 'eastwest', 'true')
      const func = require(`../channels/${channel.name}`)
      func(request, metadata, function(err, response) {
        callback(err, response)
        const currentcontext = metadata.getMap()
        if (process.env.NODE_ENV === 'staging' && _.has(currentcontext, 'x-breeze-id')) {
          const Recorder = require('./middleware/recorder')
          const recorder = new Recorder()
          recorder.save(
            recorder.capture(channel.fullname, { request, metadata }, err ? { err: err.message } : response)
          )
        }
        if (process.env.ENABLE_COUNTERS) counters.decrementchannelcount(channel.name)
      })
    }
  })

  grpc.listennorthsouth(grpcnorthsouthobj)
  grpc.listeneastwest(grpceastwestobj)

  if (process.env.RXNATS_SUBSCRIBE_ALL) {
    rxnats.subscribe(`>`, { queue: `${servicename}` }, function(request, reply, event) {
      if (process.env.ENABLE_COUNTERS) counters.incrementreactioncount(`${servicename}/${event}`)
      const func = require(`../reactions/all`)
      func(event, request, function(err) {
        if (err) {
          log(err, true)
        }
        if (process.env.ENABLE_COUNTERS) counters.decrementreactioncount(`${servicename}/${event}`)
      })
    })
  } else {
    _.each(reactions, function(filename, channelname) {
      const channel = {
        fullname: `${channelname}`,
        filename: `${filename}`
      }
      return _reactions.push(channel)
    })

    async.each(_reactions, channel =>
      rxnats.subscribe(`${channel.fullname}`, { queue: `${pjson.name}` }, function(request) {
        if (process.env.ENABLE_COUNTERS) counters.incrementreactioncount(channel.fullname)
        const protopackage = _.get(protoroot, channel.fullname)
        const protomsg = protopackage['event'].decode(request)
        const func = require(`../reactions/${channel.filename}`)
        func(protomsg, function(err) {
          if (err) {
            log(err, true)
          }
          if (process.env.ENABLE_COUNTERS) counters.decrementreactioncount(channel.fullname)
        })
      })
    )
  }
  log(`Service Up on ${new Date()} with node version ${process.version}`.bold.green)
  return { sysnats, rxnats, grpcnorthsouthserver, grpceastwestserver, grpcclient }
}
