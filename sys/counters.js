let channelcounters = {}
let reactioncounters = {}
let routecounters = {}
let allchannels = 0
let allreactions = 0
let allroutes = 0
let all = 0
module.exports = {
  incrementchannelcount: channelname => {
    channelcounters[channelname]++
    allchannels++
    all++
  },
  decrementchannelcount: channelname => {
    channelcounters[channelname]--
    allchannels--
    all--
  },
  incrementreactioncount: reactionname => {
    reactioncounters[reactionname]++
    allreactions++
    all++
  },
  decrementreactioncount: reactionname => {
    reactioncounters[reactionname]--
    allreactions--
    all--
  },
  incrementroutecount: route => {
    routecounters[route]++
    allroutes++
    all++
  },
  decrementroutecount: route => {
    routecounters[route]--
    allroutes--
    all--
  },
  getall: () => {
    return {
      channelcounters,
      reactioncounters,
      routecounters,
      allchannels,
      allreactions,
      allroutes,
      all
    }
  }
}
