const terminus = require('@godaddy/terminus')
const log = require('./log')
let counters = require('./counters')
let [sysnats, rxnats, grpcnorthsouthserver, grpceastwestserver, grpcclient] = [null, null, null, null, null]
function checkCounters() {
  let _counters = counters.getall()
  log(`Pending requests: ${_counters.all}`)
  if (_counters.all < 1) return true
  else return false
}

function checkgrpccounter(count) {
  if (count > 1) {
    return true
  }
  return false
}

let releaseResources = _cb => {
  require('../connectors/cacheredis')
    .getClient()
    .quit()
  require('../connectors/sysredis')
    .getClient()
    .quit()
  if (process.env.NODE_ENV === 'staging') {
    require('../connectors/streamredis')
      .getClient()
      .quit()
  }
  require('../connectors/mysql').end(err => {
    if (err) {
      log(err, true)
    }
    _cb()
  })
}

function onSignal() {
  return new Promise(resolve => {
    log('Service termination request received. Waiting for pending requests to complete.')
    sysnats && sysnats.unsubscribe()
    rxnats && rxnats.unsubscribe()
    let grpcshutdowncount = 0
    grpceastwestserver.tryShutdown(() => {
      log(`Pending requests completed & gRPC shutdown gracefully.`.green.bold)
      for (let client in grpcclient) {
        if (grpcclient.hasOwnProperty(client)) {
          grpcclient[client].close()
        }
        log(`gRPC clients closed successfully`.green.bold)
      }
      grpcshutdowncount++
    })
    grpcnorthsouthserver.tryShutdown(() => {
      log(`Pending requests completed & gRPC shutdown gracefully.`.green.bold)
      for (let client in grpcclient) {
        if (grpcclient.hasOwnProperty(client)) {
          grpcclient[client].close()
        }
        log(`gRPC clients closed successfully`.green.bold)
      }
      grpcshutdowncount++
    })
    if (!process.env.ENABLE_COUNTERS) {
      const interval = setInterval(() => {
        if (checkgrpccounter(grpcshutdowncount)) {
          clearInterval(interval)
          releaseResources(() => {
            resolve()
          })
        }
      }, 1000)
    }
    if (process.env.ENABLE_COUNTERS) {
      const interval = setInterval(() => {
        if (checkCounters() && checkgrpccounter(grpcshutdowncount)) {
          clearInterval(interval)
          releaseResources(() => {
            resolve()
          })
        }
      }, 1000)
    }
  })
}

function beforeShutdown() {
  return new Promise(resolve => {
    setTimeout(resolve, process.env.KUBERNETES_READINESS_DELAY || 0)
  })
}

function onShutdown() {
  log('Shutting down')
}

function healthCheck() {
  return Promise.resolve('alive')
}

const options = {
  // healtcheck options
  healthChecks: {
    '/healthcheck': healthCheck // a promise returning function indicating service health
  },

  // cleanup options
  timeout: process.env.FORCE_EXIT_TIMEOUT || 10000, // [optional = 1000] number of milliseconds before forcefull exiting
  signals: ['SIGTERM', 'SIGINT'], // [optional = []] array of signals to listen for relative to shutdown
  beforeShutdown, // [optional] called before the HTTP server starts its shutdown
  onSignal, // [optional] cleanup function, returning a promise (used to be onSigterm)
  onShutdown, // [optional] called right before exiting

  // both
  logger: log // [optional] logger function to be called with errors
}

module.exports = (_sysnats, _rxnats, server, _grpcnorthsouthserver, _grpceastwestserver, _grpcclient) => {
  sysnats = _sysnats
  rxnats = _rxnats
  grpcnorthsouthserver = _grpcnorthsouthserver
  grpceastwestserver = _grpceastwestserver
  grpcclient = _grpcclient
  rxnats = _rxnats
  terminus(server, options)
}
