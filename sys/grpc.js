const log = require('./log')
const path = require('path')
const _ = require('lodash')
const protoLoader = require('@grpc/proto-loader')
const grpc = require('grpc')
const pjson = require('../package.json')
const PROTO_PATH = path.resolve(__dirname, '../protobuf/')
const options = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
}
let grpcclient = {}
let packageDefinition
try {
  packageDefinition = protoLoader.loadSync(path.resolve(PROTO_PATH, `${pjson.name}.proto`), options)
} catch (e) {
  packageDefinition = protoLoader.loadSync(path.resolve(PROTO_PATH, 'metaservice.proto'), options)
}
const packageObject = grpc.loadPackageDefinition(packageDefinition)
const grpcnorthsouthserver = new grpc.Server()
const grpceastwestserver = new grpc.Server()
const GRPC_HOST = process.env.GRPC_HOST || '0.0.0.0'
const GRPC_NORTH_SOUTH_PORT = process.env.GRPC_NORTH_SOUTH_PORT || '3001'
const GRPC_EAST_WEST_PORT = process.env.GRPC_EAST_WEST_PORT || '3002'
module.exports = {
  request(channel, message, metadata, callback) {
    const channelnames = channel.split('/')
    if (!grpcclient[channelnames[0]]) {
      log(`Lazy loaded gRPC client ${channelnames[0].bold.yellow} and cached to grpc clients list`)
      const protopath = path.resolve(PROTO_PATH, `${channelnames[0]}.proto`)
      const packageDefinition = protoLoader.loadSync(protopath, options)
      const packageObject = grpc.loadPackageDefinition(packageDefinition)
      if (process.env.NODE_ENV === 'development') {
        grpcclient[channelnames[0]] = new packageObject['services'][channelnames[0]](
          `fpstaging.quikwallet.com:443`,
          grpc.credentials.createSsl()
        )
      } else {
        grpcclient[channelnames[0]] = new packageObject['services'][channelnames[0]](
          `${channelnames[0]}:${GRPC_EAST_WEST_PORT}`,
          grpc.credentials.createInsecure()
        )
      }
    }
    grpcclient[channelnames[0]][channelnames[1]](message, metadata, callback)
  },
  listennorthsouth(grpcObj) {
    const service = packageObject['services']
    grpcnorthsouthserver.addService(
      _.has(service, pjson.name) ? service[pjson.name].service : service.metaservice.service,
      grpcObj
    )
    const grpccreds = grpc.ServerCredentials.createInsecure()
    grpcnorthsouthserver.bind(`${GRPC_HOST}:${GRPC_NORTH_SOUTH_PORT}`, grpccreds)
    grpcnorthsouthserver.start()
    log(`GRPC NORTH SOUTH serving on ${GRPC_HOST}:${GRPC_NORTH_SOUTH_PORT}`.bold.green)
  },
  listeneastwest(grpcObj) {
    const service = packageObject['services']
    grpceastwestserver.addService(
      _.has(service, pjson.name) ? service[pjson.name].service : service.metaservice.service,
      grpcObj
    )
    const grpccreds = grpc.ServerCredentials.createInsecure()
    grpceastwestserver.bind(`${GRPC_HOST}:${GRPC_EAST_WEST_PORT}`, grpccreds)
    grpceastwestserver.start()
    log(`GRPC EAST WEST serving on ${GRPC_HOST}:${GRPC_EAST_WEST_PORT}`.bold.green)
  },
  grpcnorthsouthserver,
  grpceastwestserver,
  grpcclient,
  getMetaData() {
    return new grpc.Metadata()
  }
}
