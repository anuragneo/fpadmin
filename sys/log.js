require('colors')
const pjson = require('../package.json')
module.exports = function(message, critical = false) {
  if (process.env.NODE_ENV === 'unittest') {
    return
  }
  const prefix = critical ? '[ERROR]' : '[INFO]'
  if (critical) {
    // eslint-disable-next-line
    return console.log(`[${pjson.name}@${pjson.version}] ${prefix} ${message}`.red)
  } else {
    // eslint-disable-next-line
    return console.log(`[${pjson.name}@${pjson.version}] ${prefix} ${message}`)
  }
}
