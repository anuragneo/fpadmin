const log = require('../log')
const redisclient = require('../../connectors/streamredis').getClient()

class Recorder {
  capture(channel, call, response) {
    const { metadata, request } = call || {}
    const streamdata = {
      channel,
      request,
      response
    }
    return { streamdata, metadata }
  }

  /**
   * @function save Use this function to save your single capture
   * @param {object} param0 Json object with streamdata to store and metadata
   */
  save({ streamdata, metadata }) {
    const breeze = metadata.getMap() || {}
    const { 'x-breeze-id': breezeid } = breeze

    // if breezeid if found then append to the stream x-breeze-${breezeid}
    try {
      breezeid &&
        redisclient.xadd(`x-breeze-${breezeid}`, '*', JSON.stringify(breeze, /\//), JSON.stringify(streamdata || {}))
    } catch (e) {
      log(e, true)
    }
  }
}

module.exports = Recorder
