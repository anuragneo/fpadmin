const _ = require('lodash')
const nats = require(process.env.NODE_ENV == 'unittest' ? '../test/mocks' : './nats')
const grpc = require(process.env.NODE_ENV == 'unittest' ? '../test/mocks' : './grpc')
module.exports = {
  request(channel, request, context, callback) {
    // gRPC is default. If ENABLE_NATS env is set then nats is enabled for sending request
    if (process.env.ENABLE_NATS) {
      nats.request(channel, request, context, callback)
    } else {
      const currentcontext = context.getMap()
      if (process.env.NODE_ENV === 'staging' && _.has(currentcontext, 'x-breeze-id')) {
        if (!_.has(_.pick(context.getMap(), ['x-breeze-internal']))) {
          context = context.clone()
          context.set('x-breeze-internal', 'true')
        }
      }
      grpc.request(channel, request, context, callback)
    }
  },
  publish(channel, message) {
    nats.publish(channel, message)
  }
}
